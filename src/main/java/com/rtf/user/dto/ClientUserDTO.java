/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.dto;

import java.util.List;

import com.rtf.common.sql.dvo.GridPreferenceDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ClientUserDTO.
 */
public class ClientUserDTO extends RtfSaasBaseDTO {

	/** The userid. */
	private String userid;

	/** The useremail. */
	private String useremail;

	/** The userfname. */
	private String userfname;

	/** The userlname. */
	private String userlname;

	/** The userphone. */
	private String userphone;

	/** The userrole. */
	private String userrole;

	/** The is OTT. */
	private Boolean isOTT = false;

	/** The is SNW. */
	private Boolean isSNW = false;

	/** The is LIVE. */
	private Boolean isLIVE = false;

	/** The is SHOP. */
	private Boolean isSHOP = false;

	/** The is CHALLANGE. */
	private Boolean isCHALLANGE = false;

	/** The is VIDEO. */
	private Boolean isVIDEO = false;

	/** The is HOST. */
	// For Menus
	private Boolean isHOST = false;

	/** The is SETTINGS. */
	private Boolean isSETTINGS = false;

	/** The is REPORTS. */
	private Boolean isREPORTS = false;

	/** The is USERS. */
	private Boolean isUSERS = false;

	/** The is QUICKTASK. */
	private Boolean isQUICKTASK = false;

	/** The is DASHBOARD. */
	private Boolean isDASHBOARD = false;

	/** The prefs. */
	private List<GridPreferenceDVO> prefs;

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 *
	 * @param userid the new userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * Gets the useremail.
	 *
	 * @return the useremail
	 */
	public String getUseremail() {
		return useremail;
	}

	/**
	 * Sets the useremail.
	 *
	 * @param useremail the new useremail
	 */
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	/**
	 * Gets the userfname.
	 *
	 * @return the userfname
	 */
	public String getUserfname() {
		return userfname;
	}

	/**
	 * Sets the userfname.
	 *
	 * @param userfname the new userfname
	 */
	public void setUserfname(String userfname) {
		this.userfname = userfname;
	}

	/**
	 * Gets the userlname.
	 *
	 * @return the userlname
	 */
	public String getUserlname() {
		return userlname;
	}

	/**
	 * Sets the userlname.
	 *
	 * @param userlname the new userlname
	 */
	public void setUserlname(String userlname) {
		this.userlname = userlname;
	}

	/**
	 * Gets the userphone.
	 *
	 * @return the userphone
	 */
	public String getUserphone() {
		return userphone;
	}

	/**
	 * Sets the userphone.
	 *
	 * @param userphone the new userphone
	 */
	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

	/**
	 * Gets the checks if is OTT.
	 *
	 * @return the checks if is OTT
	 */
	public Boolean getIsOTT() {
		return isOTT;
	}

	/**
	 * Sets the checks if is OTT.
	 *
	 * @param isOTT the new checks if is OTT
	 */
	public void setIsOTT(Boolean isOTT) {
		this.isOTT = isOTT;
	}

	/**
	 * Gets the checks if is SNW.
	 *
	 * @return the checks if is SNW
	 */
	public Boolean getIsSNW() {
		return isSNW;
	}

	/**
	 * Sets the checks if is SNW.
	 *
	 * @param isSNW the new checks if is SNW
	 */
	public void setIsSNW(Boolean isSNW) {
		this.isSNW = isSNW;
	}

	/**
	 * Gets the checks if is LIVE.
	 *
	 * @return the checks if is LIVE
	 */
	public Boolean getIsLIVE() {
		return isLIVE;
	}

	/**
	 * Sets the checks if is LIVE.
	 *
	 * @param isLIVE the new checks if is LIVE
	 */
	public void setIsLIVE(Boolean isLIVE) {
		this.isLIVE = isLIVE;
	}

	/**
	 * Gets the checks if is SHOP.
	 *
	 * @return the checks if is SHOP
	 */
	public Boolean getIsSHOP() {
		return isSHOP;
	}

	/**
	 * Sets the checks if is SHOP.
	 *
	 * @param isSHOP the new checks if is SHOP
	 */
	public void setIsSHOP(Boolean isSHOP) {
		this.isSHOP = isSHOP;
	}

	/**
	 * Gets the checks if is CHALLANGE.
	 *
	 * @return the checks if is CHALLANGE
	 */
	public Boolean getIsCHALLANGE() {
		return isCHALLANGE;
	}

	/**
	 * Sets the checks if is CHALLANGE.
	 *
	 * @param isCHALLANGE the new checks if is CHALLANGE
	 */
	public void setIsCHALLANGE(Boolean isCHALLANGE) {
		this.isCHALLANGE = isCHALLANGE;
	}

	/**
	 * Gets the checks if is VIDEO.
	 *
	 * @return the checks if is VIDEO
	 */
	public Boolean getIsVIDEO() {
		return isVIDEO;
	}

	/**
	 * Sets the checks if is VIDEO.
	 *
	 * @param isVIDEO the new checks if is VIDEO
	 */
	public void setIsVIDEO(Boolean isVIDEO) {
		this.isVIDEO = isVIDEO;
	}

	/**
	 * Gets the userrole.
	 *
	 * @return the userrole
	 */
	public String getUserrole() {
		return userrole;
	}

	/**
	 * Sets the userrole.
	 *
	 * @param userrole the new userrole
	 */
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}

	/**
	 * Gets the checks if is HOST.
	 *
	 * @return the checks if is HOST
	 */
	public Boolean getIsHOST() {
		return isHOST;
	}

	/**
	 * Sets the checks if is HOST.
	 *
	 * @param isHOST the new checks if is HOST
	 */
	public void setIsHOST(Boolean isHOST) {
		this.isHOST = isHOST;
	}

	/**
	 * Gets the checks if is SETTINGS.
	 *
	 * @return the checks if is SETTINGS
	 */
	public Boolean getIsSETTINGS() {
		return isSETTINGS;
	}

	/**
	 * Sets the checks if is SETTINGS.
	 *
	 * @param isSETTINGS the new checks if is SETTINGS
	 */
	public void setIsSETTINGS(Boolean isSETTINGS) {
		this.isSETTINGS = isSETTINGS;
	}

	/**
	 * Gets the checks if is REPORTS.
	 *
	 * @return the checks if is REPORTS
	 */
	public Boolean getIsREPORTS() {
		return isREPORTS;
	}

	/**
	 * Sets the checks if is REPORTS.
	 *
	 * @param isREPORTS the new checks if is REPORTS
	 */
	public void setIsREPORTS(Boolean isREPORTS) {
		this.isREPORTS = isREPORTS;
	}

	/**
	 * Gets the checks if is USERS.
	 *
	 * @return the checks if is USERS
	 */
	public Boolean getIsUSERS() {
		return isUSERS;
	}

	/**
	 * Sets the checks if is USERS.
	 *
	 * @param isUSERS the new checks if is USERS
	 */
	public void setIsUSERS(Boolean isUSERS) {
		this.isUSERS = isUSERS;
	}

	/**
	 * Gets the checks if is QUICKTASK.
	 *
	 * @return the checks if is QUICKTASK
	 */
	public Boolean getIsQUICKTASK() {
		return isQUICKTASK;
	}

	/**
	 * Sets the checks if is QUICKTASK.
	 *
	 * @param isQUICKTASK the new checks if is QUICKTASK
	 */
	public void setIsQUICKTASK(Boolean isQUICKTASK) {
		this.isQUICKTASK = isQUICKTASK;
	}

	/**
	 * Gets the checks if is DASHBOARD.
	 *
	 * @return the checks if is DASHBOARD
	 */
	public Boolean getIsDASHBOARD() {
		return isDASHBOARD;
	}

	/**
	 * Sets the checks if is DASHBOARD.
	 *
	 * @param isDASHBOARD the new checks if is DASHBOARD
	 */
	public void setIsDASHBOARD(Boolean isDASHBOARD) {
		this.isDASHBOARD = isDASHBOARD;
	}

	/**
	 * Gets the prefs.
	 *
	 * @return the prefs
	 */
	public List<GridPreferenceDVO> getPrefs() {
		return prefs;
	}

	/**
	 * Sets the prefs.
	 *
	 * @param prefs the new prefs
	 */
	public void setPrefs(List<GridPreferenceDVO> prefs) {
		this.prefs = prefs;
	}

}
