/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.user.dvo.ClientUserDVO;

/**
 * The Class UserDTO.
 */
public class UserDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private ClientUserDVO dvo;

	/** The list. */
	private List<ClientUserDVO> list;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public ClientUserDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ClientUserDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<ClientUserDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<ClientUserDVO> list) {
		this.list = list;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
