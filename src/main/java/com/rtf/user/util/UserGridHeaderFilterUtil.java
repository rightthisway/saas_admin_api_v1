/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.util;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class UserGridHeaderFilterUtil.
 */
public class UserGridHeaderFilterUtil {

	/**
	 * Gets the user filter query.
	 *
	 * @param filter the filter
	 * @return the user filter query
	 */
	public static String getUserFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("userid")) {
							sql.append(" AND c.userid like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("userfname")) {
							sql.append(" AND c.userfname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("userlname")) {
							sql.append(" AND c.userlname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("userphone")) {
							sql.append(" AND c.userphone like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("userrole")) {
							sql.append(" AND c.userrole like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("isactive")) {
							sql.append("AND c.isactive like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("creby")) {
							sql.append(" AND c.creby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("credateStr")) {
							getDatePartQuery(sql, "c.credate", value);
						} else if (name.equalsIgnoreCase("updby")) {
							sql.append(" AND c.updby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("upddateStr")) {
							getDatePartQuery(sql, "c.upddate", value);
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the date part query.
	 *
	 * @param sql        the sql
	 * @param columnName the column name
	 * @param value      the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql, String columnName, String value) {
		try {
			if (DateFormatUtil.extractDateElement(value, "DAY") > 0) {
				sql.append(" AND DATEPART(day, " + columnName + ") = " + DateFormatUtil.extractDateElement(value, "DAY")
						+ " ");
			}
			if (DateFormatUtil.extractDateElement(value, "MONTH") > 0) {
				sql.append(" AND DATEPART(month, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "MONTH") + " ");
			}
			if (DateFormatUtil.extractDateElement(value, "YEAR") > 0) {
				sql.append(" AND DATEPART(year, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "YEAR") + " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validate param values.
	 *
	 * @param name  the name
	 * @param value the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name, String value) {
		if (name == null || name.isEmpty() || name.equalsIgnoreCase("undefined") || value == null || value.isEmpty()
				|| value.equalsIgnoreCase("undefined")) {
			return false;
		}
		return true;
	}
}
