/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.util;

/**
 * The Class UserMsgConstants.
 */
public class UserMsgConstants {

	public static final String EMAIL_VALIDATE_REG_EXP = "^(.+)@(.+)$";
	
	public static String NO_RECORD_FOUND = "No Record Found.";
	
	public static String COMMON_ERR_MSG = "Something went wrong, please try again sometime later";
	
	public static final String VALID_EMAIL_ADDRES_MSG = "Please enter valid email address.";
	
	public static String INVALID_USER_ID = "Invalid User Id";
	
	public static String INVALID_RESET_PWD_TOKEN = "Invalid reset password token.";
	
	public static String USER_NOT_REGISTERED = "Customer is not registered.";
	
	public static String RESET_VALIDATION_IS_SUCCESS = "Reset Validation  is Successfull";
	
	public static String INVALID_USER_NAME = "Invalid User Name.";
	
	public static String INVALID_IP_ADDRESS = "Invalid Ip address.";
	
	public static String USER_ID_NOT_EXIST = "UserID not exists.";
	
	public static String USER_ID_PASSWORD_MISMATCH = "Userid or Password is mismatched.";
	
	public static String SELECT_VALID_USER_NAME = "Select Valid User Name.";
	
	public static String SELECT_VALID_FIRST_NAME = "Select Valid First Name.";
	
	public static String SELECT_VALID_LAST_NAME = "Select Valid Last Name.";
	
	public static String SELECT_VALID_PHONE = "Select Valid Phone No.";
	
	public static String SELECT_VALID_USER_ROLE = "Select Valid User Role";
	
	public static String SELECT_VALID_PASSWORD = "Select Valid Password.";
	
	public static String SELECT_VALID_CONFIRM_PASSWORD = "Select Valid Confirm Password.";
	
	public static String INVALID_ADMIN_USER_ID = "Invalid Admin UserId.";
	
	public static String USER_ID_EXIST_CHOOSE_DIFFERENT_ONE = "UserId Already Exist. Select Different UserId.";
	
	public static String YOUR_LICENSE_LIMITED_TO_USERS = "Your license is limited to 5 users. Please contact product manager for addition of more users.";
	
	public static String USER_CREATED_SUCCESS_MSG = "User Created Successfully";
	
	public static String INVALID_PAGE_NO = "Invalid page no.";
	
	public static String ACCESS_RESTRICTED_FOR_USER = "Access Restricted for this User.";
	
	public static String USER_PROFILE_UPDATE_SUCCESS_MSG = "User Profile Updated Successfully.";
	
	public static String USER_DATA_UPDATE_SUCCESS_MSG = "User Data Updated Successfully.";
	
	public static String USER_LOGOUT_SUCCESS_MSG = "Logged Out Successfully.";
	
	public static String USER_ROLE_ADMIN = "ADMIN";
	/** The active user status. */
	public static String ACTIVE_USER_STATUS = "ACTIVE";

	/** The inactive user status. */
	public static String INACTIVE_USER_STATUS = "INACTIVE";

	/** The min password length. */
	public static Integer MIN_PASSWORD_LENGTH = 4;

	/** The invalid client. */
	public static String INVALID_CLIENT = "Invalid Client Id";

	/** The invalid user. */
	public static String INVALID_USER = "User Id does Not Exist";

	/** The user deleted msg. */
	public static String USER_DELETED_MSG = "User has been Deleted Sucessfully";

	/** The user activated msg. */
	public static String USER_ACTIVATED_MSG = "User has been Activated Sucessfully";

	/** The user deactivated msg. */
	public static String USER_DEACTIVATED_MSG = "User has been De Activated Sucessfully";

	/** The invalid pwd. */
	public static String INVALID_PWD = "Password is not Valid";

	/** The invalid confirm pwd. */
	public static String INVALID_CONFIRM_PWD = "Confirm Password is not Valid";

	/** The pwd confirmpwd no match. */
	public static String PWD_CONFIRMPWD_NO_MATCH = "Password and Confirm Password are not matching.";

	/** The min password length not met. */
	public static String MIN_PASSWORD_LENGTH_NOT_MET = "Password should be minimum 4 characters either a digit, character or alphanumeric.";

	/** The user pwd reset success. */
	public static String USER_PWD_RESET_SUCCESS = "Password is Reset Successfully ";

	/** The user pwd reset instruction. */
	public static String USER_PWD_RESET_INSTRUCTION = "Password Reset Instruction has been e-Mailed ";

	/** The is success. */
	public static String IS_SUCCESS = " is Successfull";
}
