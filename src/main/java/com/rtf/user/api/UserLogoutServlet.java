/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.ClientUserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class UserLogoutServlet.
 */
@WebServlet("/clientuserlogout.json")
public class UserLogoutServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 4117978113888544668L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * update user as logged out
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userIdStr = request.getParameter("userId");
		String lipAddr = request.getParameter("lipa");
		ClientUserDTO respDTO = new ClientUserDTO();
		respDTO.setSts(0);

		try {
			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (lipAddr == null || lipAddr.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_IP_ADDRESS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientUserDVO clientUser = ClientUserDAO.getClientUserWithPasswordByUserId(userIdStr);

			if (clientUser == null) {
				setClientMessage(respDTO, UserMsgConstants.USER_ID_NOT_EXIST, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setClId(clientUser.getClintid());
			respDTO.setUserid(clientUser.getUserid());
			respDTO.setUserrole(clientUser.getUserrole());
			respDTO.setUseremail(clientUser.getUseremail());
			respDTO.setUserfname(clientUser.getUserfname());
			respDTO.setUserlname(clientUser.getUserlname());

			respDTO.setSts(1);
			respDTO.setMsg( UserMsgConstants.USER_LOGOUT_SUCCESS_MSG);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
