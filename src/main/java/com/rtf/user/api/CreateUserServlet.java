/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.SecurityUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.dvo.ClientUserPasswordDVO;
import com.rtf.user.service.AdminUserService;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class CreateUserServlet.
 */
@WebServlet("/createuser.json")
public class CreateUserServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 4534409367524070732L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * register new client user and return user details in response .
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String userIdStr = request.getParameter("userId");
		String firstName = request.getParameter("fname");
		String lName = request.getParameter("lname");
		String phone = request.getParameter("phone");
		String role = request.getParameter("role");
		String password = request.getParameter("pwd");
		String confirmPassword = request.getParameter("cpwd");

		String cau = request.getParameter("cau");
		//String lipAddr = request.getParameter("lipa");
		UserDTO respDTO = new UserDTO();
		respDTO.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_USER_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (firstName == null || firstName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_FIRST_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (lName == null || lName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_LAST_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (phone == null || phone.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_PHONE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (role == null || role.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_USER_ROLE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (password == null || password.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_PASSWORD, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (confirmPassword == null || confirmPassword.isEmpty()) {
				setClientMessage(respDTO,  UserMsgConstants.SELECT_VALID_CONFIRM_PASSWORD, null);
				generateResponse(request, response, respDTO);
				return;
			}
			/*
			 * if (lipAddr == null || lipAddr.isEmpty()) { setClientMessage(respDTO,
			 * UserMsgConstants.INVALID_IP_ADDRESS, null); generateResponse(request,
			 * response, respDTO); return; }
			 */
			if (cau == null || cau.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientUserDVO adminUser = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, cau);
			if (adminUser == null || !adminUser.getUserrole().equals(UserMsgConstants.USER_ROLE_ADMIN)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (!password.equals(confirmPassword)) {
				setClientMessage(respDTO, UserMsgConstants.PWD_CONFIRMPWD_NO_MATCH, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (password.length() < UserMsgConstants.MIN_PASSWORD_LENGTH) {
				setClientMessage(respDTO, UserMsgConstants.MIN_PASSWORD_LENGTH_NOT_MET, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientUserDVO tempUser = ClientUserDAO.getClientUserByUserId(userIdStr);
			if (tempUser != null) {
				setClientMessage(respDTO, UserMsgConstants.USER_ID_EXIST_CHOOSE_DIFFERENT_ONE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientUserDVO user = new ClientUserDVO();
			user.setClintid(clientId);
			user.setUserid(userIdStr);
			user.setUserfname(firstName);
			user.setUserlname(lName);
			user.setUseremail(userIdStr);
			user.setUserphone(phone);
			user.setUserrole(role);
			user.setIsactive(UserMsgConstants.ACTIVE_USER_STATUS);
			user.setCreby(cau);
			user.setCredate(new Date());
			user.setIstestuser(Boolean.FALSE);

			ClientUserPasswordDVO userPassword = new ClientUserPasswordDVO();
			userPassword.setClintid(user.getClintid());
			userPassword.setUserid(user.getUserid());
			userPassword.setUseremail(user.getUseremail());
			userPassword.setCreby(user.getCreby());
			userPassword.setCredate(user.getCredate());
			userPassword.setUserpwd(SecurityUtil.encryptPasswordString(password));

			Integer usersCount = ClientUserDAO.getActiveUsersCountByClientId(clientId);
			if (usersCount >= 5) {
				setClientMessage(respDTO, UserMsgConstants.YOUR_LICENSE_LIMITED_TO_USERS,null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO = AdminUserService.createUser(clientId, respDTO, user, userPassword);
			if (respDTO.getSts() == 0) {
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			respDTO.setMsg(UserMsgConstants.USER_CREATED_SUCCESS_MSG);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
