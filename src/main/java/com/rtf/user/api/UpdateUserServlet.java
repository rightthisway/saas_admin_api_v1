/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class UpdateUserServlet.
 */
@WebServlet("/updateuser.json")
public class UpdateUserServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -2651687759023113678L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Update user details by user id and client id.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String userIdStr = request.getParameter("userId");
		String firstName = request.getParameter("fname");
		String lName = request.getParameter("lname");
		String phone = request.getParameter("phone");
		String role = request.getParameter("role");

		String cau = request.getParameter("cau");
		//String lipAddr = request.getParameter("lipa");
		UserDTO respDTO = new UserDTO();
		respDTO.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_USER_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (firstName == null || firstName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_FIRST_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (lName == null || lName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_LAST_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (phone == null || phone.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_PHONE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (role == null || role.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.SELECT_VALID_USER_ROLE, null);
				generateResponse(request, response, respDTO);
				return;
			}

			/*
			 * if (lipAddr == null || lipAddr.isEmpty()) { setClientMessage(respDTO,
			 * UserMsgConstants.INVALID_IP_ADDRESS, null); generateResponse(request,
			 * response, respDTO); return; }
			 */
			if (cau == null || cau.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientUserDVO adminUser = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, cau);
			if (adminUser == null || !adminUser.getUserrole().equals("ADMIN")) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			ClientUserDVO user = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, userIdStr);
			if (user == null) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			user.setUserfname(firstName);
			user.setUserlname(lName);
			user.setUserphone(phone);
			user.setUserrole(role);
			user.setUpdby(cau);
			user.setUpddate(new Date());

			ClientUserDAO.updateClientUserData(clientId, user);

			respDTO.setSts(1);
			respDTO.setMsg(UserMsgConstants.USER_DATA_UPDATE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
