/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.service.AdminUserService;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class DeActivateUserServlet.
 */
@WebServlet("/deactvuser.json")
public class DeActivateUserServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id 
	 */
	private static final long serialVersionUID = 8340639572477580513L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Deactivate user based on user id and client id
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String userIdStr = request.getParameter("userId");
		String cau = request.getParameter("cau");

		UserDTO dto = new UserDTO();
		dto.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientUserDVO user = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, userIdStr);
			if (user == null || user.getUserid() == null) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientUserDVO dvo = new ClientUserDVO();
			dvo.setClintid(clientId);
			dvo.setUserid(userIdStr);
			dvo.setCreby(cau);
			dto.setDvo(dvo);
			dto = AdminUserService.ativateDeActivateUser(dto, UserMsgConstants.INACTIVE_USER_STATUS);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setMsg(UserMsgConstants.USER_DEACTIVATED_MSG);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, dto);
		}
		return;
	}

}
