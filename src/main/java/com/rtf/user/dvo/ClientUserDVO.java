/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.dvo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ClientUserDVO.
 */
public class ClientUserDVO implements Serializable {

	/** The clintid. */
	private String clintid;

	/** The userid. */
	private String userid;

	/** The useremail. */
	private String useremail;

	/** The userfname. */
	private String userfname;

	/** The userlname. */
	private String userlname;

	/** The userphone. */
	private String userphone;

	/** The userrole. */
	private String userrole;

	/** The isactive. */
	private String isactive;

	/** The istestuser. */
	@JsonIgnore
	private Boolean istestuser;

	/** The creby. */
	private String creby;

	/** The credate. */
	@JsonIgnore
	private Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	@JsonIgnore
	private Date upddate;

	/** The credate str. */
	private String credateStr;

	/** The upddate str. */
	private String upddateStr;

	/** The password. */
	@JsonIgnore
	private String password;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 *
	 * @param userid the new userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * Gets the useremail.
	 *
	 * @return the useremail
	 */
	public String getUseremail() {
		return useremail;
	}

	/**
	 * Sets the useremail.
	 *
	 * @param useremail the new useremail
	 */
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	/**
	 * Gets the userfname.
	 *
	 * @return the userfname
	 */
	public String getUserfname() {
		return userfname;
	}

	/**
	 * Sets the userfname.
	 *
	 * @param userfname the new userfname
	 */
	public void setUserfname(String userfname) {
		this.userfname = userfname;
	}

	/**
	 * Gets the userlname.
	 *
	 * @return the userlname
	 */
	public String getUserlname() {
		return userlname;
	}

	/**
	 * Sets the userlname.
	 *
	 * @param userlname the new userlname
	 */
	public void setUserlname(String userlname) {
		this.userlname = userlname;
	}

	/**
	 * Gets the userphone.
	 *
	 * @return the userphone
	 */
	public String getUserphone() {
		return userphone;
	}

	/**
	 * Sets the userphone.
	 *
	 * @param userphone the new userphone
	 */
	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

	/**
	 * Gets the userrole.
	 *
	 * @return the userrole
	 */
	public String getUserrole() {
		return userrole;
	}

	/**
	 * Sets the userrole.
	 *
	 * @param userrole the new userrole
	 */
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public String getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the istestuser.
	 *
	 * @return the istestuser
	 */
	public Boolean getIstestuser() {
		return istestuser;
	}

	/**
	 * Sets the istestuser.
	 *
	 * @param istestuser the new istestuser
	 */
	public void setIstestuser(Boolean istestuser) {
		this.istestuser = istestuser;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the credate str.
	 *
	 * @return the credate str
	 */
	public String getCredateStr() {
		credateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(credate);
		return credateStr;
	}

	/**
	 * Sets the credate str.
	 *
	 * @param credateStr the new credate str
	 */
	public void setCredateStr(String credateStr) {
		this.credateStr = credateStr;
	}

	/**
	 * Gets the upddate str.
	 *
	 * @return the upddate str
	 */
	public String getUpddateStr() {
		upddateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(upddate);
		return upddateStr;
	}

	/**
	 * Sets the upddate str.
	 *
	 * @param upddateStr the new upddate str
	 */
	public void setUpddateStr(String upddateStr) {
		this.upddateStr = upddateStr;
	}

}
