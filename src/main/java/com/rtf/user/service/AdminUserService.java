/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.service;

import java.util.List;
import java.util.UUID;

import com.rtf.client.dvo.ClientRegistrationDVO;
import com.rtf.client.sql.dao.ClientRegistrationDAO;
import com.rtf.common.util.MailManager;
import com.rtf.common.util.SecurityUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.dvo.ClientUserPasswordDVO;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.sql.dao.ClientUserPasswordDAO;
import com.rtf.user.util.UserGridHeaderFilterUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class AdminUserService.
 */
public class AdminUserService {

	/**
	 * Gets the all users by contest idand filter.
	 *
	 * @param respDTO the resp DTO
	 * @param filter  the filter
	 * @param pgNo    the pg no
	 * @return the all users by contest idand filter
	 * @throws Exception the exception
	 */
	public static UserDTO getAllUsersByContestIdandFilter(UserDTO respDTO, String filter, String pgNo)
			throws Exception {
		List<ClientUserDVO> users = null;
		try {

			String filterQuery = UserGridHeaderFilterUtil.getUserFilterQuery(filter);
			users = ClientUserDAO.getAllUsersByClientIdandFilter(respDTO.getClId(), filterQuery, pgNo,filter);
			Integer count = ClientUserDAO.getAllUsersCountByClientIdandFilter(respDTO.getClId(), filterQuery);

			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setList(users);
			respDTO.setSts(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Gets the all users data to export.
	 *
	 * @param clId   the cl id
	 * @param filter the filter
	 * @return the all users data to export
	 * @throws Exception the exception
	 */
	public static List<ClientUserDVO> getAllUsersDataToExport(String clId, String filter) throws Exception {
		List<ClientUserDVO> users = null;
		try {
			String filterQuery = UserGridHeaderFilterUtil.getUserFilterQuery(filter);
			users = ClientUserDAO.getAllUsersDataToExport(clId, filterQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	/**
	 * Creates the user.
	 *
	 * @param clientId     the client id
	 * @param dto          the dto
	 * @param user         the user
	 * @param userPassword the user password
	 * @return the user DTO
	 * @throws Exception the exception
	 */
	public static UserDTO createUser(String clientId, UserDTO dto, ClientUserDVO user,
			ClientUserPasswordDVO userPassword) throws Exception {

		ClientUserDAO.saveClientUSer(clientId, user);
		ClientUserPasswordDAO.saveClientUSerPassword(clientId, userPassword);

		dto.setSts(1);

		return dto;
	}

	/**
	 * Delete user.
	 *
	 * @param histUser the hist user
	 * @param delUser  the del user
	 * @param dto      the dto
	 * @return the user DTO
	 * @throws Exception the exception
	 */
	public static UserDTO deleteUser(ClientUserDVO histUser, ClientUserDVO delUser, UserDTO dto) throws Exception {

		Boolean isDeleted = ClientUserDAO.deleteUser(delUser.getClintid(), delUser.getUserid());
		if (isDeleted == true) {
			ClientUserPasswordDAO.deleteUserPassword(delUser.getClintid(), delUser.getUserid());

			dto.setSts(1);
		}
		return dto;

	}

	/**
	 * Ativate de activate user.
	 *
	 * @param dto    the dto
	 * @param status the status
	 * @return the user DTO
	 * @throws Exception the exception
	 */
	public static UserDTO ativateDeActivateUser(UserDTO dto, String status) throws Exception {
		ClientUserDVO dvo = dto.getDvo();
		Boolean isUpdtd = ClientUserDAO.activateDeActivateUser(dvo.getClintid(), dvo.getUserid(), status);
		if (isUpdtd == true) {
			dto.setSts(1);
		}
		return dto;

	}

	/**
	 * Admin reset user password.
	 *
	 * @param dto      the dto
	 * @param password the password
	 * @return the user DTO
	 */
	public static UserDTO adminResetUserPassword(UserDTO dto, String password) {
		ClientUserDVO dvo = dto.getDvo();
		ClientUserPasswordDVO userPassword = new ClientUserPasswordDVO();
		userPassword.setClintid(dvo.getClintid());
		userPassword.setUserid(dvo.getUserid());
		userPassword.setUserpwd(password);
		userPassword.setUpdby(dvo.getUpdby());
		userPassword.setUseremail(dvo.getUseremail());
		Boolean isUpdtd = ClientUserPasswordDAO.adminResetUserPassword(userPassword);
		if (isUpdtd == true) {
			/*try {

				String message = "Hello ,  \n " + "" + "Your Password for SaaS Admin Panel Has been Reset.\n " + " \n"
						+ "Your New Password  is [ " + password + " ]  \n " + "\n" + "\n" + "Thank You \n"
						+ "SaaS Admin";
				MailManager.sendMailNow(dvo.getUseremail(), "sales@rewardthefan.com", "Your password for Admin Panel.",
						message);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			dto.setSts(1);
		}
		return dto;
	}

	/**
	 * Admin user forgot password.
	 *
	 * @param dto  the dto
	 * @param clId the cl id
	 * @return the user DTO
	 * @throws Exception the exception
	 */
	public static UserDTO adminUserForgotPassword(UserDTO dto, String clId) throws Exception {
		ClientUserDVO dvo = dto.getDvo();
		String userId = dvo.getUserid();
		ClientRegistrationDVO cldvo = ClientRegistrationDAO.getClientRegistrationByClientId(clId,
				UserMsgConstants.ACTIVE_USER_STATUS);

		if (cldvo == null || GenUtil.isNullOrEmpty(cldvo.getSaasweburl())) {
			return dto;
		}

		String token = UUID.randomUUID().toString();
		String queryString = SecurityUtil.encryptString(clId) + "~~" + SecurityUtil.encryptString(userId) + "~~"
				+ token;
		ClientUserPasswordDVO userPassword = null;
		try {
			userPassword = ClientUserPasswordDAO.getClientUserPasswordByClintIdByUserId(clId, userId);
		} catch (Exception ex) {
			ex.printStackTrace();
			return dto;
		}
		userPassword.setResettoken(token);
		userPassword.setIspwdactivated(UserMsgConstants.INACTIVE_USER_STATUS);
		userPassword.setUpdby(userId);

		Boolean isSaved = false;
		try {
			isSaved = ClientUserPasswordDAO.userUpdatePaswordToken(userPassword);
		} catch (Exception e) {
			e.printStackTrace();
			return dto;
		}

		if (!isSaved) {
			return dto;
		}
		String saasweburl = cldvo.getSaasweburl();
		String text = saasweburl + "#/rst?t=" + queryString;
		String message = "Hello ,  \n " + "" + "Please click on link to reset your password.\n " + " \n"
				+ "Reset Link  is [ " + text + " ]  \n " + "\n" + "\n" + "Thank You \n" + "SaaS Admin";

		try {
			MailManager.sendMailNow(userId, "sales@rewardthefan.com", "Reset password instruction. ", message);
		} catch (Exception e) {
			e.printStackTrace();
			return dto;
		}
		dto.setSts(1);
		return dto;
	}

	/**
	 * Validate email reset tokens.
	 *
	 * @param dto    the dto
	 * @param clId   the cl id
	 * @param custId the cust id
	 * @param uuid   the uuid
	 * @return the user DTO
	 */
	public static UserDTO validateEmailResetTokens(UserDTO dto, String clId, String custId, String uuid) {
		Boolean idUpdtd = ClientUserPasswordDAO.userUpdateEmailRecdResetToken(clId, custId, uuid);
		if (idUpdtd == false) {
			return dto;
		} else {
			dto.setSts(1);
		}
		return dto;

	}

	/**
	 * User reset password.
	 *
	 * @param dto      the dto
	 * @param password the password
	 * @return the user DTO
	 */
	public static UserDTO userResetPassword(UserDTO dto, String password) {
		ClientUserDVO dvo = dto.getDvo();
		ClientUserPasswordDVO userPassword = new ClientUserPasswordDVO();
		userPassword.setClintid(dvo.getClintid());
		userPassword.setUserid(dvo.getUserid());
		userPassword.setUserpwd(password);
		userPassword.setUpdby(dvo.getUpdby());
		userPassword.setUseremail(dvo.getUseremail());
		Boolean isUpdtd = ClientUserPasswordDAO.adminResetUserPassword(userPassword);
		if (isUpdtd == false) {
			return dto;
		} else {
			dto.setSts(1);
		}
		return dto;
	}

}
