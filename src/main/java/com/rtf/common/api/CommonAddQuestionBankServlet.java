/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.CommonQuestionDTO;
import com.rtf.common.service.CommonQuestionBankService;
import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.common.util.MessageConstant;
import com.rtf.ott.util.ContestAnswerTypeEnums;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class CommonAddQuestionBankServlet.
 */
@WebServlet("/commaddquebank.json")
public class CommonAddQuestionBankServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 71134757361403291L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Create new question in common question bank.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String qtx = request.getParameter("qtx");
		String opa = request.getParameter("opa");
		String opb = request.getParameter("opb");
		String opc = request.getParameter("opc");
		String opd = request.getParameter("opd");
		String cans = request.getParameter("cans");
		String anstype = request.getParameter("anstype");
		String ctyp = request.getParameter("ctyp");
		String creBy = request.getParameter("cau"); 
		String qOrientn = request.getParameter("qOrientn");
		String sctyp = request.getParameter("sctyp");
		String mstqbnkid = request.getParameter("mqbId");
		Integer mqbId = null;
		CommonQuestionDTO respDTO = new CommonQuestionDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(mstqbnkid)) {
				setClientMessage(respDTO, MessageConstant.INVALID_QUESTION_BANK, null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				mqbId = Integer.parseInt(mstqbnkid);
			} catch (Exception ex) {
				setClientMessage(respDTO, MessageConstant.INVALID_QUESTION_BANK, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(qtx)) {

				setClientMessage(respDTO, MessageConstant.QUESTION_TEXT_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(opa)) {

				setClientMessage(respDTO, MessageConstant.ANSWER_OPT_A_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(opb)) {
				setClientMessage(respDTO, MessageConstant.ANSWER_OPT_B_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(anstype)) {
				setClientMessage(respDTO, MessageConstant.QUESTION_TYPE_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (ContestAnswerTypeEnums.REGULAR.name().equals(anstype)) {
				if (GenUtil.isNullOrEmpty(cans)) {
					setClientMessage(respDTO, MessageConstant.CORRECT_ANSWER_MENDATORY, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			if (GenUtil.isNullOrEmpty(ctyp)) {
				setClientMessage(respDTO, MessageConstant.CATEGORY_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(qOrientn)) {
				setClientMessage(respDTO, MessageConstant.ORIENTATION_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}

			CommonQuestionBankDVO dvo = new CommonQuestionBankDVO();

			dvo.setAnstype(anstype);
			dvo.setCtyp(ctyp);
			dvo.setSctyp(sctyp);

			dvo.setOpa(opa);
			dvo.setOpb(opb);
			dvo.setOpc(opc);
			dvo.setOpd(opd);
			dvo.setCans(cans);
			dvo.setqOrientn(qOrientn);
			dvo.setQtx(qtx);
			dvo.setIsActive(Boolean.TRUE);
			dvo.setCreBy(creBy);
			dvo.setMstqbankId(mqbId);

			respDTO.setDvo(dvo);
			respDTO = CommonQuestionBankService.createQuestion(clId, respDTO);
			if (respDTO.getSts() == 0) {
				if (respDTO.getMsg() == null || respDTO.getMsg().isEmpty()) {
					setClientMessage(respDTO, null, null);
				}
			}

			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
