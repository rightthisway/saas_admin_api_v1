/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.MasterBankDTO;
import com.rtf.common.service.MasterBankService;
import com.rtf.common.sql.dvo.QuestionBankMasterDVO;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class MasterQbankEditServlet.
 */
@WebServlet("/masterqbankedit.json")
public class MasterQbankEditServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 31413913904269611L;

	/**
	 * Update master question bank.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String mstqsnbnk = request.getParameter("mstqsnbnk");
		String mstqsnbnkdesc = request.getParameter("mstqsnbnkdesc");
		String userName = request.getParameter("cau");
		String qbId = request.getParameter("qbId");
		Integer mqbId = null;
		MasterBankDTO respDTO = new MasterBankDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			
			if(GenUtil.isNullOrEmpty(qbId))
			{
				setClientMessage(respDTO , MessageConstant.INVALID_QUESTION_BANK , null);
				generateResponse(request, response , respDTO );
				return;
			}
			
			try {
				mqbId = Integer.parseInt(qbId);
			}catch(Exception ex) {
				setClientMessage(respDTO , MessageConstant.INVALID_QUESTION_BANK , null);
				generateResponse(request, response , respDTO );
				return;
			}
			
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(mstqsnbnk)) {
				setClientMessage(respDTO, MessageConstant.QUESTION_BANK_NAME_MENDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			QuestionBankMasterDVO dvo = new QuestionBankMasterDVO();
			dvo.setClintid(clId);
			dvo.setMstqsnbnk(mstqsnbnk);
			dvo.setMstqsnbnkdesc(mstqsnbnkdesc);			
			dvo.setCreby(userName);	
			dvo.setMstqsnbnkid(mqbId);
			respDTO.setDvo(dvo);
			respDTO.setSts(0);
			respDTO = MasterBankService.updateMasterBank(dvo,respDTO);
			if (respDTO.getSts() == 0) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (respDTO.getSts() == 2) {
				setClientMessage(respDTO, SAASMessages.QBANK_NAME_EXIST, null);
				respDTO.setSts(0);
				generateResponse(request, response, respDTO);
				return;
			}
			
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
	
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

}
