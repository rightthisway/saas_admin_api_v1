/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.MasterBankDTO;
import com.rtf.common.service.MasterBankService;
import com.rtf.common.sql.dvo.QuestionBankMasterDVO;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class MasterQbankListServlet.
 */
@WebServlet("/masterqbanklist.json")
public class MasterQbankListServlet extends RtfSaasBaseServlet {

	
	private static final long serialVersionUID = -9137128149217092641L;

	/**
	 * Get all master question bank with specified search criteria.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");		
		String userName = request.getParameter("cau");
		String sts = request.getParameter("sts");
		String pgNo = request.getParameter("pgNo");
		String filter = request.getParameter("hfilter");		
		MasterBankDTO respDTO = new MasterBankDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(userName)) {
				setClientMessage(respDTO, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(pgNo)) {
				setClientMessage(respDTO, MessageConstant.INVALID_PAGENO, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(sts)) {
				setClientMessage(respDTO,MessageConstant.INVALID_QUESTION_BANK_STATUS, null);
				generateResponse(request, response, respDTO);
				return;
			}
		
			QuestionBankMasterDVO dvo = new QuestionBankMasterDVO();
			dvo.setClintid(clId);					
			dvo.setCreby(userName);		
			respDTO.setDvo(dvo);
			respDTO.setSts(0);
			
			respDTO = MasterBankService.fetchMasterBankListWithFilter( respDTO,  pgNo, filter, sts);
			if (respDTO.getSts() == 0) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<QuestionBankMasterDVO> list = respDTO.getMqblist();
			if(respDTO.getSts() == 1) {				
				if(list == null || list.size() == 0 )
					respDTO.setMsg(SAASMessages.QBANK_NO_RECORDS);
			}
			
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
	
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

}
