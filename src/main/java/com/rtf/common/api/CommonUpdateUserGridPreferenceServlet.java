/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.GridPreferenceDTO;
import com.rtf.common.service.GridPreferenceService;
import com.rtf.common.sql.dvo.GridPreferenceDVO;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class CommonUpdateUserGridPreferenceServlet.
 */
@WebServlet("/commupdusrgridpref.json")
public class CommonUpdateUserGridPreferenceServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7526345770490921207L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Save/Update Logged in user UI preferences
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String gridName = request.getParameter("gridName");
		String columnString = request.getParameter("columnString");
		String cau = request.getParameter("cau");
		GridPreferenceDTO respDTO = new GridPreferenceDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(gridName)) {
				setClientMessage(respDTO, MessageConstant.INVALID_GRID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(columnString)) {
				setClientMessage(respDTO, MessageConstant.INVALID_GRID_COLUMN, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(cau)) {
				setClientMessage(respDTO,MessageConstant.INVALID_USER, null);
				generateResponse(request, response, respDTO);
				return;
			}

			GridPreferenceDVO preference = GridPreferenceService.getUserGridPreference(clId, cau, gridName);
			if (preference == null) {
				preference = new GridPreferenceDVO();
				preference.setClId(clId);
				preference.setColumnString(columnString);
				preference.setGridName(gridName);
				preference.setUserId(cau);
				GridPreferenceService.savePreference(preference);
				respDTO.setSts(1);
				respDTO.setMsg(MessageConstant.USER_PREFERENCE_SAVED);
				respDTO.setPreference(preference);
				generateResponse(request, response, respDTO);
				return;
			} else {
				preference.setColumnString(columnString);
				GridPreferenceService.updateGridPreference(preference);
				respDTO.setSts(1);
				respDTO.setMsg(MessageConstant.USER_PREFERENCE_UPDATED);
				respDTO.setPreference(preference);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
