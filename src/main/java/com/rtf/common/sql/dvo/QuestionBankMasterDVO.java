/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dvo;

import java.io.Serializable;
import java.util.Date;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class QuestionBankMasterDVO.
 */
public class QuestionBankMasterDVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The master question bank ID. */
	private Integer mstqsnbnkid;

	/** The client id. */
	private String clintid;

	/** The Master question bank. */
	private String mstqsnbnk;

	/** The Master question bank description. */
	private String mstqsnbnkdesc;

	/** The Created by. */
	private String creby;

	/** The Updated by. */
	private String updby;

	/** The Created date. */
	private Date credate;

	/** The Updated date. */
	private Date upddate;

	/** The status isactive. */
	private Boolean isactive;

	/** The created date time string. */
	private String crDateTimeStr;

	/** The updated date time string. */
	private String upDateTimeStr;

	/**
	 * Gets the Master question bank ID.
	 *
	 * @return the mstqsnbnkid
	 */
	public Integer getMstqsnbnkid() {
		return mstqsnbnkid;
	}

	/**
	 * Sets the Master question bank ID.
	 *
	 * @param mstqsnbnkid the new mstqsnbnkid
	 */
	public void setMstqsnbnkid(Integer mstqsnbnkid) {
		this.mstqsnbnkid = mstqsnbnkid;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the client id.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the Master question bank text.
	 *
	 * @return the mstqsnbnk
	 */
	public String getMstqsnbnk() {
		return mstqsnbnk;
	}

	/**
	 * Sets the Master question bank text.
	 *
	 * @param mstqsnbnk the new mstqsnbnk
	 */
	public void setMstqsnbnk(String mstqsnbnk) {
		this.mstqsnbnk = mstqsnbnk;
	}

	/**
	 * Gets the Master question bank description.
	 *
	 * @return the mstqsnbnkdesc
	 */
	public String getMstqsnbnkdesc() {
		return mstqsnbnkdesc;
	}

	/**
	 * Sets the Master question bank description.
	 *
	 * @param mstqsnbnkdesc the new mstqsnbnkdesc
	 */
	public void setMstqsnbnkdesc(String mstqsnbnkdesc) {
		this.mstqsnbnkdesc = mstqsnbnkdesc;
	}

	/**
	 * Gets the Created by.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the Created by.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the Updated by.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the Updated by.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the Created date.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the Created date.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the Updated date.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the Updated date.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Checks if is status isactive.
	 *
	 * @return the boolean
	 */
	public Boolean isIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the created date time string.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(credate);
		return crDateTimeStr;
	}

	/**
	 * Sets the created date time string.
	 *
	 * @param crDateTimeStr the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the updated date time string.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(upddate);
		return upDateTimeStr;
	}

	/**
	 * Sets the updated date time string.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

	/**
	 * Generate string fo all properties
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "QuestionBankMasterDVO [mstqsnbnkid=" + mstqsnbnkid + ", clintid=" + clintid + ", mstqsnbnk=" + mstqsnbnk
				+ ", mstqsnbnkdesc=" + mstqsnbnkdesc + ", creby=" + creby + ", updby=" + updby + ", credate=" + credate
				+ ", upddate=" + upddate + ", isactive=" + isactive + ", crDateTimeStr=" + crDateTimeStr
				+ ", upDateTimeStr=" + upDateTimeStr + "]";
	}

}