/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dvo;

import com.rtf.saas.sql.dvo.SaasBaseDVO;

/**
 * The Class CommonQuestionBankDVO.
 */
public class CommonQuestionBankDVO extends SaasBaseDVO{
	
	

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "CommonQuestionBankDVO [clintid=" + clintid + ", qsnbnkid=" + qsnbnkid + ", opa=" + opa + ", opb=" + opb
				+ ", opc=" + opc + ", opd=" + opd + ", anstype=" + anstype + ", ctyp=" + ctyp + ", cans=" + cans
				+ ", isActive=" + isActive + ", qOrientn=" + qOrientn + ", qtx=" + qtx + ", sctyp=" + sctyp
				+ ", mstqbankId=" + mstqbankId + "]";
	}
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The client id. */
	private String clintid;
	
	/** The question bank  id. */
	private Integer qsnbnkid;
	
	/** The option A. */
	private String opa;
	
	/** The option B. */
	private String opb;
	
	/** The option C. */
	private String opc;
	
	/** The option D. */
	private String opd;
	
	/** The answer type. */
	private String anstype;
	
	/** The category type. */
	private String ctyp;
	
	/** The correct answer. */
	private String cans;
	
	/** The question status. */
	private Boolean isActive;
	
	/** The question orientation*/
	private String qOrientn;
	
	/** The question text. */
	private String qtx;		
	
	/** The question sub category. */
	private String sctyp;
	
	/** The master question bank id. */
	Integer mstqbankId;
	
	/**
	 * Gets the Client Id.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}
	
	/**
	 * Sets the Client Id.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}
	
	/**
	 * Gets the Question bank ID.
	 *
	 * @return the qsnbnkid
	 */
	public Integer getQsnbnkid() {
		return qsnbnkid;
	}
	
	/**
	 * Sets the Question bank ID..
	 *
	 * @param qsnbnkid the new qsnbnkid
	 */
	public void setQsnbnkid(Integer qsnbnkid) {
		this.qsnbnkid = qsnbnkid;
	}
	
	/**
	 * Gets the option A.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}
	
	/**
	 * Sets the option A.
	 *
	 * @param opa the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}
	
	/**
	 * Gets the option B.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}
	
	/**
	 * Sets the option B.
	 *
	 * @param opb the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}
	
	/**
	 * Gets the option C.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}
	
	/**
	 * Sets the option C.
	 *
	 * @param opc the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}
	
	/**
	 * Gets the option D.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}
	
	/**
	 * Sets the option D.
	 *
	 * @param opd the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}
	
	/**
	 * Gets the Answer type.
	 *
	 * @return the anstype
	 */
	public String getAnstype() {
		return anstype;
	}
	
	/**
	 * Sets the Answer type.
	 *
	 * @param anstype the new anstype
	 */
	public void setAnstype(String anstype) {
		this.anstype = anstype;
	}
	
	/**
	 * Gets the Category type.
	 *
	 * @return the ctyp
	 */
	public String getCtyp() {
		return ctyp;
	}
	
	/**
	 * Sets the Category type.
	 *
	 * @param ctyp the new ctyp
	 */
	public void setCtyp(String ctyp) {
		this.ctyp = ctyp;
	}
	
	/**
	 * Gets the Correct answer.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}
	
	/**
	 * Sets the Correct answer.
	 *
	 * @param cans the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}
	
	/**
	 * Gets the checks if question status is active.
	 *
	 * @return the checks if is active
	 */
	public Boolean getIsActive() {
		return isActive;
	}
	
	/**
	 * Sets the question status
	 *
	 * @param isActive the new checks if is active
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * Gets the Question orientation
	 *
	 * @return the q orientn
	 */
	public String getqOrientn() {
		return qOrientn;
	}
	
	/**
	 * Sets the Question orientation
	 *
	 * @param qOrientn the new q orientn
	 */
	public void setqOrientn(String qOrientn) {
		this.qOrientn = qOrientn;
	}
	
	/**
	 * Gets the Question text.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}
	
	/**
	 * Sets the Question text.
	 *
	 * @param qtx the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}
	
	/**
	 * Gets the Subcategory type.
	 *
	 * @return the sctyp
	 */
	public String getSctyp() {
		return sctyp;
	}
	
	/**
	 * Sets the Subcategory type.
	 *
	 * @param sctyp the new sctyp
	 */
	public void setSctyp(String sctyp) {
		this.sctyp = sctyp;
	}
	
	
	/**
	 * Gets the serial version uid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * Gets the Master question bank id.
	 *
	 * @return the mstqbank id
	 */
	public Integer getMstqbankId() {
		return mstqbankId;
	}
	
	/**
	 * Sets the Master question bank id.
	 *
	 * @param mstqbankId the new mstqbank id
	 */
	public void setMstqbankId(Integer mstqbankId) {
		this.mstqbankId = mstqbankId;
	}
	
	

	

}
