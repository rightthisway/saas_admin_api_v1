/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

/**
 * The Class ResourceConstants.
 */
public class ResourceConstants {

	/** The shopproduct image upld folder path key. */
	public static final String SHOPPRODUCT_IMAGE_UPLD_FOLDER_PATH_KEY = "rewardicons.upload.location";

	/** The Constant SHOPPRODUCT_IMAGE_CONTEXT_PATH_KEY. */
	public static final String SHOPPRODUCT_IMAGE_CONTEXT_PATH_KEY = "rewardicons.webapp.context";

	/** The Constant SHOPPRODUCT_IMAGE_IMAGE_SERVER. */
	public static final String SHOPPRODUCT_IMAGE_IMAGE_SERVER = "rewardicons.webapp.serverurl";

}
