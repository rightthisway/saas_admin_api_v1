/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

/**
 * The Class GridHeaderFilterUtil.
 */
public class GridHeaderFilterUtil {

	/**
	 * Generates SQL filter query for Master question bank.
	 *
	 * @param filter the filter
	 * @return the all master qbank filter query
	 * @throws Exception the exception
	 */
	public static String getAllMasterQbankFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}

							if (name.equalsIgnoreCase("mstqsnbnk")) {
								sql.append(" AND mstqsnbnk like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("mstqsnbnkdesc")) {
								sql.append(" AND mstqsnbnkdesc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creby")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("credate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updby")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upddate")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Generates SQL filter query for Date and Time
	 *
	 * @param sql        the sql
	 * @param columnName the column name
	 * @param value      the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql, String columnName, String value) {
		try {
			if (DateFormatUtil.extractDateElement(value, "DAY") > 0) {
				sql.append(" AND DATEPART(day, " + columnName + ") = " + DateFormatUtil.extractDateElement(value, "DAY")
						+ " ");
			}
			if (DateFormatUtil.extractDateElement(value, "MONTH") > 0) {
				sql.append(" AND DATEPART(month, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "MONTH") + " ");
			}
			if (DateFormatUtil.extractDateElement(value, "YEAR") > 0) {
				sql.append(" AND DATEPART(year, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "YEAR") + " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validated given parameter values.
	 *
	 * @param name  the name
	 * @param value the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name, String value) {
		if (name == null || name.isEmpty() || name.equalsIgnoreCase("undefined") || value == null || value.isEmpty()
				|| value.equalsIgnoreCase("undefined")) {
			return false;
		}
		return true;
	}
}
