package com.rtf.common.util;

public class MessageConstant {
	
	
	public static final String INVALID_EMAIL = "Enter Valid Email.";
	public static final String INVALID_CLIENT = "Invalid Client Id";
	public static final String INVALID_USER = "User Id does Not Exist";
	public static final String INVALID_PAGENO= "Invalid Page Number";
	public static final String INVALID_PROD_ID= "Invalid product ID.";
	public static final String INVALID_CLIENT_THEME_DETAILS= "Invalid theme details.";
	
	
	
	
	
	public static final String INVALID_QUESTION_BANK = "Invalid Question Bank ID.";
	public static final String QUESTION_TEXT_MENDATORY = "Question is mandatory";
	public static final String ANSWER_OPT_A_MENDATORY = "Answer Option A is mandatory";
	public static final String ANSWER_OPT_B_MENDATORY = "Answer Option B is mandatory";
	public static final String QUESTION_TYPE_MENDATORY = "Please Specify Question is of: FEEDBACK OR REGUAR type";
	public static final String CORRECT_ANSWER_MENDATORY = "Selecting a Correct Answer  is mandatory";
	public static final String CATEGORY_MENDATORY = "Category is mandatory";
	public static final String ORIENTATION_MENDATORY = "Answer Orientation is mandatory";
	public static final String  INVALID_QUESTION_BANK_STATUS = "Invalid Qbank Fetch status";
	public static final String  QUESTION_BANK_NAME_MENDATORY = "Question Bank Name is mandatory";
	public static final String  QUESTION_BANK_NAME_EXIST = "Question Bank Name exists, Please choose a different name";
	
	
	
	public static final String INVALID_GRID = "Invalid grid name";
	public static final String INVALID_GRID_COLUMN = "Invalid grid column preference string";
	public static final String USER_PREFERENCE_SAVED = "Grid layout preference saved successfully";
	public static final String USER_PREFERENCE_UPDATED = "Grid layout preference updated successfully";
	
	
	public static final String FIRESTORE_CACHE_ERROR = "Error occured while updating firestore cache";
	public static final String APP_CACHE_LOADED = "App Cache Loaded Successfully";
	public static final String APP_CACHE_LOADING_ERROR = "Error occured while Loading Application Values";
	
	
	public static final String INVALID_BASIC_BACKGROUND= "Invalid Basic : background color property";
	public static final String INVALID_BASIC_FONT_FAMILY= "Invalid Basic : font family property";
	public static final String INVALID_BASIC_FONT_NAME= "Invalid Basic : font name color property";
	public static final String INVALID_BASIC_DOT_COLOR= "Invalid Basic : dot color property";
	
	public static final String INVALID_MODAL_BTN_COLOR= "Invalid Modal : Button color property.";
	public static final String INVALID_MODAL_BTN_TEXT_COLOR= "Invalid Modal : Button text color property.";
	
	
	public static final String INVALID_QUESTION_STROK_FINISHING= "Invalid Question : Strok finishing property.";
	public static final String INVALID_QUESTION_STROK_STARTING= "Invalid Question : Strok starrting property.";
	public static final String INVALID_QUESTION_BORDER_COLOR= "Invalid Question : Box border color property.";
	public static final String INVALID_QUESTION_ANSWER_COLOR= "Invalid Question : Answer background color property.";
	public static final String INVALID_QUESTION_RIGHT_ANSWER_COLOR= "Invalid Question : Right answer background color property.";
	public static final String INVALID_QUESTION_WRONG_ANSWER_COLOR= "Invalid Question : Wrong answer background color property.";
	public static final String INVALID_SELECTED_ANSWER_TEXT_COLOR= "Invalid Question : Selected answer text color property.";
	public static final String INVALID_QUESTION_RIGHT_ANSWER_TEXT_COLOR= "Invalid Question : Right answer text color property.";
	public static final String INVALID_QUESTION_WRONG_ANSWER_TEXT_COLOR= "Invalid Question : Wrong answer text color property.";
	
	
	public static final String INVALID_HEADER_CART_COLOR= "Invalid Header : cart color property.";
	public static final String INVALID_HEADER_USER_COLOR= "Invalid Header : user count color property.";
	public static final String INVALID_HEADER_HEIGHT= "Invalid Header : height property.";
	
	
	public static final String INVALID_SUMMARY_TEXT_COLOR= "Invalid Summary : text color property.";
	public static final String INVALID_SUMMARY_CIRCLE_COLOR= "Invalid Summary : circle color property.";
	public static final String INVALID_SUMMARY_BOX= "Invalid Summary : box color property.";
	public static final String INVALID_SUMMARY_TEXT_FONT_SIZE= "Invalid Summary : text font size property.";
	public static final String INVALID_SUMMARY_TEXT_WEIGHT= "Invalid Summary : text weight property.";
	public static final String INVALID_SUMMARY_WINNER_TEXT_SIZE= "Invalid Summary : winner text size property.";
	public static final String INVALID_SUMMARY_LOGO_X= "Invalid Summary : logo X property.";
	public static final String INVALID_SUMMARY_LOGO_Y= "Invalid Summary : logo Y property.";
	public static final String INVALID_SUMMARY_LOGO_SCALE= "Invalid Summary : logo scale property.";
	public static final String INVALID_SUMMARY_LOGO_SIZE= "Invalid Summary : logo size property.";
	public static final String INVALID_SUMMARY_CIRCLE_FILL= "Invalid Summary : circle fill color property.";
	public static final String INVALID_LOTTERY_WINNER_COLOR= "Invalid Lottery : winner color property.";
	public static final String INVALID_LOTTERY_WINNER_FILL= "Invalid Lottery : fill color property.";
	
	
	public static final String INVALID_WINNER_TEXT_COLOR= "Invalid winner : text color property.";
	public static final String INVALID_WINNER_FONT_SIZE= "Invalid winner : font size property.";
	public static final String INVALID_FINALIST_FONT_SIZE= "Invalid summary : font size property.";
	public static final String INVALID_WINNER_FONT_WEIGHT= "Invalid winner : font weight property.";
	public static final String INVALID_WINNER_BOX= "Invalid winner : box color property.";
	public static final String INVALID_WINNER_TEXT= "Invalid winner : text property.";
	public static final String INVALID_WINNER_PRIZE_TEXT= "Invalid winner : prize text property.";
	
	public static final String THEME_IMG_MANDATORY= "Theme images are mandatory.";
	public static final String INVALID_THEME_IMGS= "Theme logo/images not found.";
	public static final String SUMMARY_IMG_MANDATORY= "Summary page image is mandatory";
	public static final String VIDEO_POSTER_IMG_MANDATORY= "Video poster image is mandatory.";
	public static final String LOGO_COLORED_IMG_MANDATORY= "Colored logo image is mandatory.";
	public static final String LOGO_CIRCLE_IMG_MANDATORY= "Circled logo image is mandatory.";
	public static final String LOGO_WHITE_IMG_MANDATORY= "White logo image is mandatory.";
	public static final String DASHBOARD_IMG_MANDATORY= "Dashboard image is mandatory.";
	public static final String NAME_TAG_IMG_MANDATORY= "Name tag image is mandatory.";
	public static final String CART_IMG_MANDATORY= "Cart image is mandatory.";
	public static final String USER_IMG_MANDATORY= "User image is mandatory.";
	public static final String CART_COLORED_IMG_MANDATORY= "Colored cart logo image is mandatory.";
	public static final String USER_COLORED_IMG_MANDATORY= "Colored user logo image is mandatory.";
	public static final String THEME_IMG_UPLOAD_ERROR= "Error occured while uploading theme logo/images.";
	public static final String THEME_DATA_UPDATED= "Theme data updated successfully.";

}
