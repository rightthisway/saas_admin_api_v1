/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;

/**
 * The Class SNWContestQuestionsDTO.
 */
public class SNWContestQuestionsDTO extends RtfSaasBaseDTO {

	/** The cont questions. */
	private SNWContestQuestionsDVO contQuestions;

	/** The list. */
	private List<SNWContestQuestionsDVO> list;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the cont questions.
	 *
	 * @return the cont questions
	 */
	public SNWContestQuestionsDVO getContQuestions() {
		return contQuestions;
	}

	/**
	 * Sets the cont questions.
	 *
	 * @param contQuestions the new cont questions
	 */
	public void setContQuestions(SNWContestQuestionsDVO contQuestions) {
		this.contQuestions = contQuestions;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<SNWContestQuestionsDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<SNWContestQuestionsDVO> list) {
		this.list = list;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
