/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.snw.dvo.ContestRewardDVO;

/**
 * The Class SNWContestRewardDTO.
 */
public class SNWContestRewardDTO extends RtfSaasBaseDTO {

	/** The reward. */
	private ContestRewardDVO reward;

	/** The rewards. */
	private List<ContestRewardDVO> rewards;

	/**
	 * Gets the reward.
	 *
	 * @return the reward
	 */
	public ContestRewardDVO getReward() {
		return reward;
	}

	/**
	 * Sets the reward.
	 *
	 * @param reward the new reward
	 */
	public void setReward(ContestRewardDVO reward) {
		this.reward = reward;
	}

	/**
	 * Gets the rewards.
	 *
	 * @return the rewards
	 */
	public List<ContestRewardDVO> getRewards() {
		return rewards;
	}

	/**
	 * Sets the rewards.
	 *
	 * @param rewards the new rewards
	 */
	public void setRewards(List<ContestRewardDVO> rewards) {
		this.rewards = rewards;
	}

}
