/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.snw.dvo.ContestDVO;

/**
 * The Class SNWContestDTO.
 */
public class SNWContestDTO extends RtfSaasBaseDTO {

	/** The contest mstr DVO. */
	private ContestDVO contestMstrDVO;

	/** The contests. */
	private List<ContestDVO> contests;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the contest mstr DVO.
	 *
	 * @return the contest mstr DVO
	 */
	public ContestDVO getContestMstrDVO() {
		return contestMstrDVO;
	}

	/**
	 * Sets the contest mstr DVO.
	 *
	 * @param contestMstrDVO the new contest mstr DVO
	 */
	public void setContestMstrDVO(ContestDVO contestMstrDVO) {
		this.contestMstrDVO = contestMstrDVO;
	}

	/**
	 * Gets the contests.
	 *
	 * @return the contests
	 */
	public List<ContestDVO> getContests() {
		return contests;
	}

	/**
	 * Sets the contests.
	 *
	 * @param contests the new contests
	 */
	public void setContests(List<ContestDVO> contests) {
		this.contests = contests;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
