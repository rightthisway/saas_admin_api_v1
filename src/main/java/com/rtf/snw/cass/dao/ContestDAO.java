/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.cass.dao;

import java.util.Date;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.ContestDVO;

/**
 * The Class ContestDAO.
 */
public class ContestDAO {

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute("SELECT * from  pa_snwtx_contest_mstr  WHERE conid=? ",
					contestId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(row.getString("conid"));
					contestDVO.setAnsType(row.getString("anstype"));
					contestDVO.setClImgU(row.getString("brndimgurl"));
					contestDVO.setImgU(row.getString("cardimgurl"));
					contestDVO.setSeqNo(row.getInt("cardseqno"));
					contestDVO.setCat(row.getString("cattype"));
					date = row.getTimestamp("conenddate");
					contestDVO.setEnDate(date != null ? date.getTime() : null);
					contestDVO.setName(row.getString("conname"));
					contestDVO.setPlayInterval(row.getInt("playinterval"));
					date = row.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setCrBy(row.getString("creby"));
					date = row.getTimestamp("credate");
					contestDVO.setCrDate(date != null ? date.getTime() : null);
					contestDVO.setIsAct(row.getInt("isactive"));
					contestDVO.setqMode(row.getString("qsnmode"));
					contestDVO.setSubCat(row.getString("subcattype"));
					contestDVO.setUpBy(row.getString("updby"));
					date = row.getTimestamp("upddate");
					contestDVO.setUpDate(date != null ? date.getTime() : null);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return contestDVO;
	}

	/**
	 * Save contest cass.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestCass(ContestDVO co) throws Exception {
		boolean isInserted = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into pa_snwtx_contest_mstr (clintid,conid,anstype,cardseqno,cattype,conenddate,conname,consrtdate,creby,credate")
				.append(",isconactive,qsnmode,subcattype,bgthemcolor,bgthemimgurldes,bgthemimgurlmob,bgthembankid,playimgurl,playinterval) ")
				.append("values (?,?,?,?,?,?,?,?,?,toTimestamp(now()),?,?,?,?,?,?,?,?,?)");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getClId(), co.getCoId(), co.getAnsType(), co.getSeqNo(), co.getCat(),
							new Date(co.getEnDate()), co.getName(), new Date(co.getStDate()), co.getCrBy(),
							co.getIsAct(), co.getqMode(), co.getSubCat(), co.getThmColor(), co.getThmImgDesk(),
							co.getThmImgMob(), co.getThmId(), co.getPlayGameImg(), co.getPlayInterval() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isInserted;
	}

	/**
	 * Update contest cass.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestCass(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_snwtx_contest_mstr set ").append("anstype=?,cardseqno=?,cattype=?,")
				.append("conenddate=?,conname=?,consrtdate=?,").append("updby=?,upddate=toTimestamp(now()),")
				.append("isconactive=?,qsnmode=?,")
				.append("subcattype=?,bgthemcolor=?,bgthemimgurl=?, bgthemimgurlmob=?,bgthembankid=?,playimgurl=?,playinterval=? ")
				.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getAnsType(), co.getSeqNo(), co.getCat(), new Date(co.getEnDate()), co.getName(),
							new Date(co.getStDate()), co.getUpBy(), co.getIsAct(), co.getqMode(), co.getSubCat(),
							co.getThmColor(), co.getThmImgDesk(), co.getThmImgMob(), co.getThmId(), co.getPlayGameImg(),
							co.getPlayInterval(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Delete contest cass.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContestCass(String clientId, String coId) throws Exception {
		boolean isUpdated = false;
		String sql ="delete from  pa_snwtx_contest_mstr  WHERE clintid=? AND conid=?";
		try {
			CassandraConnector.getSession().execute(sql, new Object[] { clientId, coId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

}
