/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.snw.dvo.ContestRewardDVO;

/**
 * The Class ContestRewardDAO.
 */
public class ContestRewardDAO {

	/**
	 * Ge allt contest rewards.
	 *
	 * @param clId the cl id
	 * @param coId the co id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<ContestRewardDVO> geAlltContestRewards(String clId, String coId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestRewardDVO> rewards = new ArrayList<ContestRewardDVO>();
		ContestRewardDVO reward = null;
		StringBuffer sql = new StringBuffer("SELECT * from  pa_snwtx_contest_rewards WHERE clintid=? AND conid=? AND isactive=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			ps.setString(2, coId);
			ps.setBoolean(3, Boolean.TRUE);
			rs = ps.executeQuery();

			while (rs.next()) {
				Date date = null;
				reward = new ContestRewardDVO();
				reward.setClId(rs.getString("clintid"));
				reward.setCoId(rs.getString("conid"));
				reward.setType(rs.getString("rwdtype"));
				reward.setIsAct(rs.getBoolean("isactive"));
				date = rs.getTimestamp("credate");
				reward.setcDate(date != null ? date.getTime() : null);
				reward.setcBy(rs.getString("creby"));
				reward.setRwdVal(rs.getDouble("rwdval"));
				date = rs.getTimestamp("upddate");
				reward.setuDate(date != null ? date.getTime() : null);
				reward.setuBy(rs.getString("updby"));
				rewards.add(reward);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return rewards;
	}

	/**
	 * Update contest rewards.
	 *
	 * @param rwds the rwds
	 * @param coId the co id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestRewards(List<ContestRewardDVO> rwds, String coId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			for (ContestRewardDVO r : rwds) {
				StringBuffer sql = new StringBuffer();
				if (r.getCoId() == null) {
					sql.append("INSERT INTO pa_snwtx_contest_rewards (clintid,conid,rwdtype,isactive,creby,credate,rwdval) values (?,?,?,?,?,getDate(),?)");
					ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
					ps.setString(1, r.getClId());
					ps.setString(2, coId);
					ps.setString(3, r.getType());
					ps.setBoolean(4, Boolean.TRUE);
					ps.setString(5, r.getcBy());
					ps.setDouble(6, r.getRwdVal());
				} else {
					sql.append("update pa_snwtx_contest_rewards set rwdval=?,updby=?,upddate=getDate() WHERE clintid=? and conid=? and  rwdtype=?");
					ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
					ps.setDouble(1, r.getRwdVal());
					ps.setString(2, r.getuBy());
					ps.setString(3, r.getClId());
					ps.setString(4, r.getCoId());
					ps.setString(5, r.getType());
				}				
				int affectedRows = ps.executeUpdate();
				if (affectedRows > 0) {
					isUpdated = true;
				}
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Gets the all contest rewards.
	 *
	 * @param clId the cl id
	 * @param coId the co id
	 * @return the all contest rewards
	 * @throws Exception the exception
	 */
	public static List<ContestRewardDVO> getAllContestRewards(String clId, String coId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestRewardDVO> rewards = new ArrayList<ContestRewardDVO>();
		ContestRewardDVO reward = null;
		StringBuffer sql = new StringBuffer("select rt.clintid as clId,cr.conid as coId,rt.rwdtype as rwdType,cr.rwdval as rwdVal from sd_reward_type rt ");
				sql.append("left join pa_snwtx_contest_rewards cr on (rt.rwdtype=cr.rwdtype and cr.conid=? AND rt.isactive=? and cr.isactive=?) where rt.clintid=? and rt.isactive=? ORDER BY rt.rwdtype");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, coId);
			ps.setBoolean(2, Boolean.TRUE);
			ps.setBoolean(3, Boolean.TRUE);
			ps.setString(4, clId);
			ps.setBoolean(5, Boolean.TRUE);
			rs = ps.executeQuery();

			while (rs.next()) {
				reward = new ContestRewardDVO();
				reward.setClId(rs.getString("clId"));
				reward.setCoId(rs.getString("coId"));
				reward.setType(rs.getString("rwdType"));
				reward.setRwdVal(rs.getDouble("rwdVal"));
				rewards.add(reward);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return rewards;
	}
}
