/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWExportContestToExcelServlet.
 */
@WebServlet("/snwgetconexcel.json")
public class SNWExportContestToExcelServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -5978295396164571433L;

	/**
	 * Generate Http Response for Scratch & win contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment; filename=Contests." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());

	}

	/**
	 * get all contest details based on filter in excel format.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String sts = request.getParameter("sts");
		String filter = request.getParameter("hfilter");
		String userName = request.getParameter("cau");

		int startRowCnt = 0;
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);

		SXSSFWorkbook workbook = new SXSSFWorkbook();
		Sheet ssSheet = workbook.createSheet("Contests");

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (sts == null || sts.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONT_STATUS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Client ID");
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Start Date");
			headerList.add("Contest End Date");
			headerList.add("Contest Category");
			headerList.add("Contest SubCategory");
			headerList.add("Question Mode");
			headerList.add("Nature Of Question");
			headerList.add("Card Display Sequence");
			headerList.add("Play Interval(In Minutes)");
			headerList.add("Status");
			headerList.add("Created By");
			headerList.add("Created Date");
			headerList.add("Updated By");
			headerList.add("Updated Date");

			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			List<ContestDVO> contests = SNWContestService.getAllContestDataToExport(clId, sts, filter);
			if (contests == null || contests.isEmpty()) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}

			int j = 0;
			int rowCount = startRowCnt + 1;
			for (ContestDVO dvo : contests) {
				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dvo.getClId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCoId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getName(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getStDateTimeStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getEnDateTimeStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCat(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getSubCat(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getqMode(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getAnsType(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getSeqNo(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getPlayInterval(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getStatus(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCrBy(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getCrDateTimeStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getUpBy(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getUpDateTimeStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
			}

			generateExcelResponse(response, workbook);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		} finally {
			workbook.close();
		}
		return;
	}
}
