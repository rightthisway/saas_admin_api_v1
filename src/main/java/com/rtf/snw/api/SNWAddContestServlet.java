/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.util.DateFormatUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dto.SNWContestDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWAddContestServlet.
 */
@WebServlet("/snwaddcontest.json")
public class SNWAddContestServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 480585053065440423L;

	/**
	 * Generate Http Response for Scratch & win contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Create Contest with required details.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String name = request.getParameter("name");
		String stDate = request.getParameter("stDate");
		String enDate = request.getParameter("enDate");
		String cat = request.getParameter("cat");
		String subCat = request.getParameter("subCat");
		String qMode = request.getParameter("qMode");
		String qType = request.getParameter("qType");
		String playIntervalStr = request.getParameter("playInterval");
		String dSeq = request.getParameter("dSeq");

		SNWContestDTO respDTO = new SNWContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (subCat == null || subCat.isEmpty()) {
				subCat = null;
			}
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_START_DATE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cat == null || cat.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CATEGORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qMode == null || qMode.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_QUEST_MODE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qType == null || qType.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_NATURE_OF_QUEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (playIntervalStr == null || playIntervalStr.isEmpty()) {
				setClientMessage(respDTO,  SNWMessageConstant.INVALID_PLAY_INTERVAL, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date sDate = null;
			Date eDate = null;
			Integer playInterval = 0;
			Integer seq = 0;

			String stDate1 = stDate + " 00:00:00";
			String enDate1 = enDate + " 23:59:00";

			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			eDate = DateFormatUtil.getDateWithTwentyFourHourFormat(enDate1);

			try {
				playInterval = Integer.parseInt(playIntervalStr);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, SNWMessageConstant.INVALID_PLAY_INTERVAL, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (dSeq != null && !dSeq.isEmpty()) {
				try {
					seq = Integer.parseInt(dSeq);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Date now = new Date();
			ContestDVO contest = new ContestDVO();
			contest.setAnsType(qType);
			contest.setCat(cat);
			contest.setClId(clId);
			contest.setClImgU(null);
			contest.setCoId(clId + now.getTime());
			contest.setCrBy(cau);
			contest.setCrDate(now.getTime());
			contest.setEnDate(eDate.getTime());
			contest.setImgU(null);
			contest.setIsAct(1);
			contest.setName(name);
			contest.setqMode(qMode);
			contest.setPlayInterval(playInterval);
			contest.setSeqNo(seq);
			contest.setStDate(sDate.getTime());
			contest.setSubCat(subCat);
			contest.setThmId(0);
			contest.setCrBy(cau);

			boolean isSaved = SNWContestService.saveContest(contest);
			if (isSaved) {
				respDTO.setSts(1);
				respDTO.setMsg(SNWMessageConstant.CONTEST_CREATED_SUCCESS_MSG);
				respDTO.setContestMstrDVO(contest);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
