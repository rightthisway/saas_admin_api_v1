/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.snw.dto.SNWContestDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWGetAllContestServlet.
 */
@WebServlet("/snwgetallcontests.json")
public class SNWGetAllContestServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -8463541318570128550L;

	/**
	 * Generate Http Response for Scratch & win contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String sts = request.getParameter("sts");
		String filter = request.getParameter("hfilter");
		String pgNo = request.getParameter("pgNo");
		SNWContestDTO respDTO = new SNWContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (sts == null || sts.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONT_STATUS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pgNo == null || pgNo.isEmpty()) {
				pgNo = "1";
			}
			List<ContestDVO> contests = SNWContestService.getAllContestByStatusandFilter(clId, sts, filter, pgNo);
			Integer count = SNWContestService.getAllContestCountByStatusandFilter(clId, sts, filter);
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setContests(contests);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
