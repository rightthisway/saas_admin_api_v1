/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dto.SNWContestDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWEndContestServlet.
 */
@WebServlet("/snwendcontest.json")
public class SNWEndContestServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -8297027480885095088L;

	/**
	 * Generate Http Response for Scratch & win contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cau = request.getParameter("cau");
		SNWContestDTO respDTO = new SNWContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = SNWContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() != 2 || !contest.getIsAct().equals(2)) {
				setClientMessage(respDTO, SNWMessageConstant.CANT_END_NON_ACTIVE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			contest.setUpBy(cau);
			contest.setIsAct(3);
			Boolean isUpdated = SNWContestService.updateContestStatus(contest);
			respDTO.setContestMstrDVO(contest);
			if (isUpdated) {
				respDTO.setSts(1);
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_END_SUCCESS_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(0);
				setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
