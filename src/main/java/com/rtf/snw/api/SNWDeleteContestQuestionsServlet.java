/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dto.SNWContestQuestionsDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;
import com.rtf.snw.service.SNWContestQuestionsService;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWDeleteContestQuestionsServlet.
 */
@WebServlet("/snwDeleteContQuest.json")
public class SNWDeleteContestQuestionsServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -33765613967312935L;

	/**
	 * Generate Http Response for Scratch & win contest question Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Delete contest question by client id and contest question id.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsnIdsStr = request.getParameter("qIds");
		String userName = request.getParameter("cau");

		SNWContestQuestionsDTO respDTO = new SNWContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsnIdsStr == null || qsnIdsStr.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_QUESTION_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			String[] qIdsArr = qsnIdsStr.split(",");
			if (qIdsArr.length == 0) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_QUESTION_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = SNWContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<SNWContestQuestionsDVO> contQuestions = new ArrayList<SNWContestQuestionsDVO>();
			for (String qidStr : qIdsArr) {
				try {
					Integer qId = Integer.parseInt(qidStr);
					SNWContestQuestionsDVO contQuestion = SNWContestQuestionsService
							.getcontestquestionsByQuestionId(clId, contId, qId);
					if (contQuestion == null) {
						setClientMessage(respDTO, SNWMessageConstant.QUESTION_ID_NOT_EXISTS, null);
						generateResponse(request, response, respDTO);
						return;
					}
					contQuestions.add(contQuestion);
				} catch (Exception e) {
					setClientMessage(respDTO, SNWMessageConstant.INVALID_QUESTION_ID, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			List<SNWContestQuestionsDVO> deletedList = new ArrayList<SNWContestQuestionsDVO>();
			for (SNWContestQuestionsDVO contQuestion : contQuestions) {
				Integer updateCnt = SNWContestQuestionsService.deleteContestQuestions(clId, contQuestion);
				if (updateCnt != null && updateCnt > 0) {
					deletedList.add(contQuestion);
				}
			}
			respDTO.setSts(1);
			setClientMessage(respDTO, null,  SNWMessageConstant.CONTEST_QUEST_DEETE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
