/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.service;

import java.util.List;

import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.snw.cass.dao.SNWContestQuestionsCassDAO;
import com.rtf.snw.dto.SNWContestQuestionsDTO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;
import com.rtf.snw.sql.dao.SNWContestQuestionsSQLDAO;
import com.rtf.snw.util.SNWGridHeaderFilterUtil;

/**
 * The Class SNWContestQuestionsService.
 */
public class SNWContestQuestionsService {

	/**
	 * Gets the contestquestions by question id.
	 *
	 * @param clientId   the client id
	 * @param contestId  the contest id
	 * @param questionId the question id
	 * @return the contestquestions by question id
	 * @throws Exception the exception
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByQuestionId(String clientId, String contestId,
			Integer questionId) throws Exception {
		SNWContestQuestionsDVO contQuestion = null;
		try {
			contQuestion = SNWContestQuestionsSQLDAO.getcontestquestionsByQuestionId(clientId, contestId, questionId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the contestquestions bycontest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contestquestions bycontest id
	 * @throws Exception the exception
	 */
	public static List<SNWContestQuestionsDVO> getcontestquestionsBycontestId(String clientId, String contestId)
			throws Exception {
		List<SNWContestQuestionsDVO> list = null;
		try {
			list = SNWContestQuestionsSQLDAO.getcontestquestionsBycontestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the contestquestions by contest idand question seq no.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @param seqno     the seqno
	 * @return the contestquestions by contest idand question seq no
	 * @throws Exception the exception
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByContestIdandQuestionSeqNo(String clientId,
			String contestId, Integer seqno) throws Exception {
		SNWContestQuestionsDVO newseqNocontestQuestion = null;
		try {
			newseqNocontestQuestion = SNWContestQuestionsSQLDAO.getcontestquestionsByContestIdandQuestionSeqNo(clientId,
					contestId, seqno);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return newseqNocontestQuestion;
	}

	/**
	 * Gets the contest questions count.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest questions count
	 * @throws Exception the exception
	 */
	public static Integer getContestQuestionsCount(String clientId, String contestId) throws Exception {
		Integer questionsCount = null;
		try {
			questionsCount = SNWContestQuestionsSQLDAO.getContestQuestionsCount(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return questionsCount;
	}

	/**
	 * Gets the contestquestions by contest idand question bank id.
	 *
	 * @param clientId       the client id
	 * @param contestId      the contest id
	 * @param questionBankId the question bank id
	 * @return the contestquestions by contest idand question bank id
	 * @throws Exception the exception
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByContestIdandQuestionBankId(String clientId,
			String contestId, Integer questionBankId) throws Exception {
		SNWContestQuestionsDVO contQuestion = null;
		try {
			contQuestion = SNWContestQuestionsSQLDAO.getcontestquestionsByContestIdandQuestionBankId(clientId,
					contestId, questionBankId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the all contest questions by contest idand filter.
	 *
	 * @param respDTO the resp DTO
	 * @param filter  the filter
	 * @param pgNo    the pg no
	 * @return the all contest questions by contest idand filter
	 * @throws Exception the exception
	 */
	public static SNWContestQuestionsDTO getAllContestQuestionsByContestIdandFilter(SNWContestQuestionsDTO respDTO,
			String filter, String pgNo) throws Exception {
		List<SNWContestQuestionsDVO> contQuestions = null;
		try {

			String filterQuery = SNWGridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = SNWContestQuestionsSQLDAO.getcontestquestionsBycontestIdAndFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery, pgNo,filter);
			Integer count = SNWContestQuestionsSQLDAO.getAllContestQuestionsCountByContestIdandFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery);

			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setList(contQuestions);
			respDTO.setSts(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Gets the all contest question data to export.
	 *
	 * @param clId   the cl id
	 * @param coId   the co id
	 * @param filter the filter
	 * @return the all contest question data to export
	 * @throws Exception the exception
	 */
	public static List<SNWContestQuestionsDVO> getAllContestQuestionDataToExport(String clId, String coId,
			String filter) throws Exception {
		List<SNWContestQuestionsDVO> contQuestions = null;
		try {
			String filterQuery = SNWGridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = SNWContestQuestionsSQLDAO.getAllContestQuestionDataToExport(clId, coId, filterQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contQuestions;
	}

	/**
	 * Save contest questions.
	 *
	 * @param clientId     the client id
	 * @param contQuestion the cont question
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer saveContestQuestions(String clientId, SNWContestQuestionsDVO contQuestion) throws Exception {
		Integer id = null;
		try {
			id = SNWContestQuestionsSQLDAO.saveContestQuestions(clientId, contQuestion);
			if (id != null) {
				contQuestion.setConqsnid(id);

				SNWContestQuestionsCassDAO.saveContestquestions(contQuestion);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * Update contest questions.
	 *
	 * @param clientId     the client id
	 * @param contQuestion the cont question
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateContestQuestions(String clientId, SNWContestQuestionsDVO contQuestion)
			throws Exception {
		Integer updatecount = null;
		try {
			updatecount = SNWContestQuestionsSQLDAO.updateContestQuestions(clientId, contQuestion);
			if (updatecount != null) {
				SNWContestQuestionsCassDAO.updateContestquestions(contQuestion);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatecount;
	}

	/**
	 * Delete contest questions.
	 *
	 * @param clientId     the client id
	 * @param contQuestion the cont question
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteContestQuestions(String clientId, SNWContestQuestionsDVO contQuestion)
			throws Exception {
		Integer updateCount = null;
		try {
			updateCount = SNWContestQuestionsSQLDAO.deleteContestQuestions(clientId, contQuestion);
			if (updateCount != null && updateCount > 0) {
				SNWContestQuestionsCassDAO.deleteContestquestions(contQuestion);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	/**
	 * Update contest questions seqno.
	 *
	 * @param clientId         the client id
	 * @param contQuestionList the cont question list
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateContestQuestionsSeqno(String clientId, List<SNWContestQuestionsDVO> contQuestionList)
			throws Exception {
		Integer updateCount = null;
		try {
			updateCount = SNWContestQuestionsSQLDAO.updateContestQuestionsSeqno(clientId, contQuestionList);
			if (updateCount != null && updateCount > 0) {
				SNWContestQuestionsCassDAO.updateContestQuestionsSeqNo(contQuestionList);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}
	
	
	
	/**
	 * Fetch SNW question bank listfor contest id.
	 *
	 * @param respDTO
	 *            the resp DTO
	 * @param filter
	 *            the filter
	 * @param catType
	 *            the cat type
	 * @param subCatType
	 *            the sub cat type
	 * @param pgNo
	 *            the pg no
	 * @return the SNW question DTO
	 */
	public static OTTQuestionDTO fetchSNWQuestionBankListforContestId(OTTQuestionDTO respDTO, String filter,
			String catType, String subCatType, String pgNo) {
		List<QuestionBankDVO> qbList = null;
		try {

			Integer count = 0;
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = QuestionBankSQLDAO.getAllSNWQuestionsFromQBankByContestIdWithFilter(respDTO.getClId(),
					respDTO.getCoId(), catType, subCatType, filterQuery, pgNo,filter);
			if (qbList == null || qbList.size() == 0) {
				respDTO.setSts(0);
				respDTO.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = QuestionBankSQLDAO.getAllSNWQuestionsCountFromQBankByContestIdWithFilter(respDTO.getClId(),
						respDTO.getCoId(), catType, subCatType, filterQuery);
			}
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setQbList(qbList);
			respDTO.setSts(1);
		} catch (Exception ex) {
			respDTO.setSts(0);
			ex.printStackTrace();
		}
		return respDTO;
	}

}
