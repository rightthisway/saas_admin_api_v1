package com.rtf.client.utils;

public class MessageConstant {
	
	
	public static final String INVALID_EMAIL = "Enter Valid Email.";
	public static final String INVALID_CLIENT = "Invalid Client Id";
	public static final String INVALID_USER = "User Id does Not Exist";
	
	
	public static final String INVALID_CONTEST = "Invalid contest id";
	public static final String CONTEST_NOT_FOUND = "Contest Details Not Available for Given contest Id";
	
	
	public static final String NO_RECORDS_FOUND = "No Records found.";
	public static final String GENERIC_ERROR_MSG = "Something went wrong";
	
	
	
	public static final String STREAM_STATUS = "Showing Stream Status";
	public static final String INVALID_STREAM = "Invalid stream ID.";
	public static final String STREAM_DOES_NOT_EXIST = "Stream Not Exist.";
	public static final String STREAM_NOT_STARTED = "Stream is not yet started.";
	public static final String STREAM_STARTING = "Starting Stream";
	public static final String STREAM_STOPPING = "Stopping Stream";
	
	

}
