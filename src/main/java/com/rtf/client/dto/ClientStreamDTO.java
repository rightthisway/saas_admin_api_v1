/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dto;

import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.wowza.cloudsdk.client.model.ShmMetrics;

/**
 * The Class ClientStreamDTO.
 */
public class ClientStreamDTO extends RtfSaasBaseDTO {

	/** The ClientStreamDVO dvo. */
	private ClientStreamDVO dvo;

	/** The stats. */
	private ShmMetrics stats;

	/**
	 * Gets the dvo.
	 *
	 * @return ClientStreamDVO the dvo
	 */
	public ClientStreamDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param ClientStreamDVO the dvo
	 */
	public void setDvo(ClientStreamDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the stats.
	 *
	 * @return the stats
	 */
	public ShmMetrics getStats() {
		return stats;
	}

	/**
	 * Sets the stats.
	 *
	 * @param stats the new stats
	 */
	public void setStats(ShmMetrics stats) {
		this.stats = stats;
	}

}
