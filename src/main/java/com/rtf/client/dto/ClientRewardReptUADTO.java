/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dto;

import java.util.List;

import com.rtf.client.dvo.CustomerCartUADVO;

/**
 * The Class ClientRewardReptUADTO.
 */
public class ClientRewardReptUADTO {

	/** The conm. */
	private String conm;

	/** The codte. */
	private String codte;

	/** The crtlst. */
	private List<CustomerCartUADVO> crtlst;

	/** The sts. */
	private Integer sts;

	/** The msg. */
	private String msg;

	/**
	 * Gets the conm.
	 *
	 * @return the conm
	 */
	public String getConm() {
		return conm;
	}

	/**
	 * Sets the conm.
	 *
	 * @param conm the new conm
	 */
	public void setConm(String conm) {
		this.conm = conm;
	}

	/**
	 * Gets the codte.
	 *
	 * @return the codte
	 */
	public String getCodte() {
		return codte;
	}

	/**
	 * Sets the codte.
	 *
	 * @param codte the new codte
	 */
	public void setCodte(String codte) {
		this.codte = codte;
	}

	/**
	 * Gets the crtlst.
	 *
	 * @return the crtlst
	 */
	public List<CustomerCartUADVO> getCrtlst() {
		return crtlst;
	}

	/**
	 * Sets the crtlst.
	 *
	 * @param crtlst the new crtlst
	 */
	public void setCrtlst(List<CustomerCartUADVO> crtlst) {
		this.crtlst = crtlst;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
