package com.rtf.client.dto;

import java.util.List;

import com.rtf.client.dvo.ClientThemeConfigDVO;
import com.rtf.client.dvo.ClientThemeDVO;
import com.rtf.client.dvo.ThemeFont;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ThemeDTO.
 */
public class ThemeDTO extends RtfSaasBaseDTO{

	/** The Client theme details.*/
	private ClientThemeDVO theme;
	
	/** The Client theme parsisted data.*/
	private ClientThemeConfigDVO themeConf;
	
	/** The Font list for theme.*/
	private List<ThemeFont> fonts;
	
	/**
	 * Gets the theme.
	 *
	 * @return the theme.
	 */
	public ClientThemeDVO getTheme() {
		return theme;
	}
	/**
	 * Sets the theme.
	 *
	 * @param  the theme. 
	 */
	public void setTheme(ClientThemeDVO theme) {
		this.theme = theme;
	}
	
	/**
	 * Gets the themeConf.
	 *
	 * @return the themeConf.
	 */
	public ClientThemeConfigDVO getThemeConf() {
		return themeConf;
	}
	/**
	 * Sets the themeConf.
	 *
	 * @param  the themeConf. 
	 */
	public void setThemeConf(ClientThemeConfigDVO themeConf) {
		this.themeConf = themeConf;
	}
	
	/**
	 * Gets the fonts.
	 *
	 * @return the fonts.
	 */
	public List<ThemeFont> getFonts() {
		return fonts;
	}
	/**
	 * Sets the fonts.
	 *
	 * @param  the fonts. 
	 */
	public void setFonts(List<ThemeFont> fonts) {
		this.fonts = fonts;
	}
	
	
	
	
	
	
}
