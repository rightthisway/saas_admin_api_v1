package com.rtf.client.sql.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rtf.client.dvo.ClientThemeConfigDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ClientThemeConfigDAO.
 */
public class ClientThemeConfigDAO {

	
	/**
	 * Get the client theme by client ID and product ID.
	 *
	 *  @param clId the client id
	 * @param prodType  the product Type
	 * @return ClientThemeConfigDVO the ClientThemeConfigDVO
	 * @throws Exception the exception
	 */
	public static ClientThemeConfigDVO getClientThemeConfig(String clientId, String prodType) throws Exception {
		Connection conn = null;		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClientThemeConfigDVO theme = null; 
		String sql = "SELECT * FROM client_trivia_theme_config WHERE clintid=? AND prodcode=?";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql);		
			ps.setString(1, clientId);			
			ps.setString(2, prodType);				
			rs = ps.executeQuery();
			while (rs.next()) {
				theme= new ClientThemeConfigDVO();		
				theme.setClId(rs.getString("clintid"));
				theme.setProdType(rs.getString("prodcode"));
				theme.setThmJson(rs.getString("themjson"));
				theme.setThmUrl(rs.getString("themurl"));
				theme.setUpdBy(rs.getString("updby"));
				theme.setUpdDate(rs.getTimestamp("upddate"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return theme;
	}
	
	
	
	
	/**
	 * Save client theme config. for client.
	 *
	 * @param ClientThemeConfigDVO  the Client Theme Config. DVO
	 * @return Boolean the isSaved
	 * @throws Exception the exception
	 */
	public static Boolean saveClientThemeConfig(ClientThemeConfigDVO theme) throws Exception {
		Connection conn = null;		
		PreparedStatement ps = null;
		Boolean isSaved = false; 
		String sql = "INSERT INTO client_trivia_theme_config(clintid,prodcode,themjson,themurl,updby,upddate) VALUES (?,?,?,?,?,?)";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql);		
			ps.setString(1, theme.getClId());			
			ps.setString(2, theme.getProdType());
			ps.setString(3, theme.getThmJson());
			ps.setString(4, theme.getThmUrl());
			ps.setString(5, theme.getUpdBy());
			ps.setDate(6, new Date(theme.getUpdDate().getTime()));
			int affectedRows = ps.executeUpdate();
			if(affectedRows == 0){
				throw new SQLException("Creating client theme, no rows affected.");
			}
			isSaved = true;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isSaved;
	}
	
	
	
	/**
	 * Update client theme config. for client.
	 *
	 * @param ClientThemeConfigDVO  the Client Theme Config. DVO
	 * @return Boolean the isUpdated
	 * @throws Exception the exception
	 */
	public static Boolean updateClientThemeConfig(ClientThemeConfigDVO theme) throws Exception {
		Connection conn = null;		
		PreparedStatement ps = null;
		Boolean isUpdated = false; 
		String sql = "UPDATE client_trivia_theme_config set themjson=?,themurl=?,updby=?,upddate=? WHERE clintid=? AND prodcode=?";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql);		
			ps.setString(1, theme.getThmJson());
			ps.setString(2, theme.getThmUrl());
			ps.setString(3, theme.getUpdBy());
			ps.setDate(4, new Date(theme.getUpdDate().getTime()));
			ps.setString(5, theme.getClId());			
			ps.setString(6, theme.getProdType());
			int affectedRows = ps.executeUpdate();
			if(affectedRows == 0){
				throw new SQLException("Updating client theme, no rows affected.");
			}
			isUpdated = true;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
}
