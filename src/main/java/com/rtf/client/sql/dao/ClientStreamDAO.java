/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ClientStreamDAO.
 */
public class ClientStreamDAO {

	/**
	 * Get the client stream object by stream id.
	 *
	 * @param clintId  the client id
	 * @param streamId the stream id
	 * @return ClientStreamDVO the client stream by id
	 * @throws Exception the exception
	 */
	public static ClientStreamDVO getClientStreamById(String clintId, String streamId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientStreamDVO dvo = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT clintid, streamengine, streamid,streamname,outputId,trnscdrId,")
		.append("targetid, sourceurl, sourceport,sourcekey,fbRtmpsUrl,fbStrmKey,fbTrgtId,fbOutputTrgtId,fbStrmSts,")
		.append("playbackurl, hostedurl, status, streamstatus,isauth,ytRtmpUrl,ytStrmKey,ytTrgtId,ytOutputTrgtId,ytStrmSts  ")
		.append("from client_stream_details with(nolock) where clintid=? AND StreamType='HLS' ");
		if(streamId!=null && !streamId.isEmpty()){
			sql.append(" and streamid = ? ");
		}

		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clintId);
			if(streamId!=null && !streamId.isEmpty()){
				ps.setString(2, streamId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				dvo = new ClientStreamDVO();
				dvo.setClintid(rs.getString("clintid"));
				dvo.setStreamengine(rs.getString("streamengine"));
				dvo.setStreamid(rs.getString("streamid"));
				dvo.setStreamname(rs.getString("streamname"));
				dvo.setTargetid(rs.getString("targetid"));
				dvo.setSourceurl(rs.getString("sourceurl"));
				dvo.setPort(rs.getString("sourceport"));
				dvo.setSourcekey(rs.getString("sourcekey"));
				dvo.setPlaybackurl(rs.getString("playbackurl"));
				dvo.setStatus(rs.getString("status"));
				dvo.setStreamstatus(rs.getString("streamstatus"));
				dvo.setIsAuthDisabled(rs.getBoolean("isauth"));
				dvo.setTrnscdrId(rs.getString("trnscdrId"));
				dvo.setOutputId(rs.getString("outputId"));
				dvo.setFbOutputTrgtId(rs.getString("fbOutputTrgtId"));
				dvo.setFbRtmpsUrl(rs.getString("fbRtmpsUrl"));
				dvo.setFbStrmKey(rs.getString("fbStrmKey"));
				dvo.setFbStrmSts(rs.getString("fbStrmSts"));
				dvo.setFbTrgtId(rs.getString("fbTrgtId"));
				dvo.setYtOutputTrgtId(rs.getString("ytOutputTrgtId"));
				dvo.setYtRtmpUrl(rs.getString("ytRtmpUrl"));
				dvo.setYtStrmKey(rs.getString("ytStrmKey"));
				dvo.setYtStrmSts(rs.getString("ytStrmSts"));
				dvo.setYtTrgtId(rs.getString("ytTrgtId"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Get the client stream object by stream id and stream engine.
	 *
	 * @param streamId     the stream id
	 * @param streamEngine the stream engine
	 * @return ClientStreamDVO the client stream by stream id and stream engine
	 * @throws Exception the exception
	 */
	public static ClientStreamDVO getClientStreamByStreamIdandStreamEngine(String streamId, String streamEngine)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientStreamDVO dvo = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT clintid, streamengine, streamid,streamname, ")
		.append("targetid, sourceurl, sourceport,sourcekey, ")
		.append("playbackurl, hostedurl, status, streamstatus,isauth ")
		.append("from client_stream_details with(nolock) ")
		.append("where streamid = ? and streamengine=?");

		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, streamId);
			ps.setString(2, streamEngine);
			rs = ps.executeQuery();
			while (rs.next()) {
				dvo = new ClientStreamDVO();
				dvo.setClintid(rs.getString("clintid"));
				dvo.setStreamengine(rs.getString("streamengine"));
				dvo.setStreamid(rs.getString("streamid"));
				dvo.setStreamname(rs.getString("streamname"));
				dvo.setTargetid(rs.getString("targetid"));
				dvo.setSourceurl(rs.getString("sourceurl"));
				dvo.setPort(rs.getString("sourceport"));
				dvo.setSourcekey(rs.getString("sourcekey"));
				dvo.setPlaybackurl(rs.getString("playbackurl"));
				dvo.setStatus(rs.getString("status"));
				dvo.setStreamstatus(rs.getString("streamstatus"));
				dvo.setIsAuthDisabled(rs.getBoolean("isauth"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Get the active client stream object by client id with basic data.
	 *
	 * @param clintId the client id
	 * @return the active client stream by client id with basic data
	 * @throws Exception the exception
	 */
	public static ClientStreamDVO getActiveClientStreamByClientIdWithBasicData(String clintId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientStreamDVO dvo = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT clintid, streamengine, streamid,streamname, ")
		.append("  targetid, sourceurl, sourceport,sourcekey, ")
		.append(" playbackurl, hostedurl, status, streamstatus,isauth ")
		.append(" from client_stream_details with(nolock) ")
		.append(" where status='ACTIVE' and clintid=? ");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clintId);
			rs = ps.executeQuery();
			while (rs.next()) {
				dvo = new ClientStreamDVO();
				dvo.setClintid(rs.getString("clintid"));
				dvo.setStreamengine(rs.getString("streamengine"));
				dvo.setStreamid(rs.getString("streamid"));
				dvo.setStreamname(rs.getString("streamname"));
				dvo.setPlaybackurl(rs.getString("playbackurl"));
				dvo.setStreamstatus(rs.getString("streamstatus"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the active client stream object by client id.
	 *
	 * @param clintId the client id
	 * @return ClientStreamDVO the active client stream by client id
	 * @throws Exception the exception
	 */
	public static ClientStreamDVO getActiveClientStreamByClientId(String clintId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientStreamDVO dvo = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT clintid, streamengine, streamid,streamname, ")
		.append("  targetid, sourceurl, sourceport,sourcekey, ")
		.append(" playbackurl, hostedurl, status, streamstatus,isauth ")
		.append(" from client_stream_details with(nolock) ")
		.append(" where status='ACTIVE' and clintid=? ");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clintId);
			rs = ps.executeQuery();
			while (rs.next()) {
				dvo = new ClientStreamDVO();
				dvo.setClintid(rs.getString("clintid"));
				dvo.setStreamengine(rs.getString("streamengine"));
				dvo.setStreamid(rs.getString("streamid"));
				dvo.setStreamname(rs.getString("streamname"));
				dvo.setTargetid(rs.getString("targetid"));
				dvo.setSourceurl(rs.getString("sourceurl"));
				dvo.setSourcekey(rs.getString("sourcekey"));
				dvo.setPort(rs.getString("sourceport"));
				dvo.setPlaybackurl(rs.getString("playbackurl"));
				dvo.setStatus(rs.getString("status"));
				dvo.setStreamstatus(rs.getString("streamstatus"));
				dvo.setIsAuthDisabled(rs.getBoolean("isauth"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Get the active client stream object by client id and stream engine.
	 *
	 * @param clintId      the client id
	 * @param streamEngine the stream engine
	 * @return ClientStreamDVO the active client stream by client id and stream engine
	 * @throws Exception the exception
	 */
	public static ClientStreamDVO getActiveClientStreamByClientIdAndStreamEngine(String clintId, String streamEngine)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientStreamDVO dvo = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT clintid, streamengine, streamid,streamname, ")
		.append("  targetid, sourceurl, sourceport,sourcekey, ")
		.append(" playbackurl, hostedurl, status, streamstatus,isauth ")
		.append(" from client_stream_details with(nolock) ")
		.append(" where status='ACTIVE' and clintid=? and streamengine=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clintId);
			ps.setString(2, streamEngine);
			rs = ps.executeQuery();
			while (rs.next()) {
				dvo = new ClientStreamDVO();
				dvo.setClintid(rs.getString("clintid"));
				dvo.setStreamengine(rs.getString("streamengine"));
				dvo.setStreamid(rs.getString("streamid"));
				dvo.setStreamname(rs.getString("streamname"));
				dvo.setTargetid(rs.getString("targetid"));
				dvo.setSourceurl(rs.getString("sourceurl"));
				dvo.setSourcekey(rs.getString("sourcekey"));
				dvo.setPort(rs.getString("sourceport"));
				dvo.setPlaybackurl(rs.getString("playbackurl"));
				dvo.setStatus(rs.getString("status"));
				dvo.setStreamstatus(rs.getString("streamstatus"));
				dvo.setIsAuthDisabled(rs.getBoolean("isauth"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Save client stream details.
	 *
	 * @param clientId the client id
	 * @param dvo      the dvo
	 * @return Boolean the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveClientStreamDets(String clientId, ClientStreamDVO dvo) throws Exception {
		Boolean isUpdated = false;
		Connection conn = null;
		PreparedStatement statement = null;
		StringBuffer sql = new StringBuffer();
		sql.append("insert into client_stream_details")
		.append("  (clintid, streamengine, streamid,streamname ,")
		.append("  targetid, sourceurl, sourceport,isauth, usrname, password ,")
		.append("  playbackurl, hostedurl, status, streamstatus,creby, credate) ")
		.append("values(?,?,?,?, ?,?,?, ?,?,?, ?,?,?,?, ?,getDate())");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			statement = conn.prepareStatement(sql.toString());
			statement.setString(1, dvo.getClintid());
			statement.setString(2, dvo.getStreamengine());
			statement.setString(3, dvo.getStreamid());
			statement.setString(4, dvo.getStreamname());
			statement.setString(5, dvo.getTargetid());
			statement.setString(6, dvo.getSourceurl());
			statement.setString(7, dvo.getPort());
			statement.setBoolean(8, dvo.getIsAuthDisabled());
			statement.setString(9, dvo.getUsrname());
			statement.setString(10, dvo.getPassword());
			statement.setString(11, dvo.getPlaybackurl());
			statement.setString(12, dvo.getHostedurl());
			statement.setString(13, dvo.getStatus());
			statement.setString(14, dvo.getStreamstatus());
			statement.setString(15, dvo.getCreBy());
			int affectedRows = statement.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update client stream object status.
	 *
	 * @param cliId    the client id
	 * @param updBy    the upd by
	 * @param streamId the stream id
	 * @param status   the status
	 * @return the boolean
	 */
	public static Boolean updateStreamRunStatus(String cliId, String updBy, String streamId, String status) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String sql;
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			sql = "UPDATE  client_stream_details set streamstatus = ? , upddate = getDate() , updby=?  where clintid=? and streamId=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, status);
			ps.setString(2, updBy);
			ps.setString(3, cliId);
			ps.setString(4, streamId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
	
	/**
	 * Update client stream object facebook stream configurations.
	 *
	 * @param ClientStreamDVO the dvo.
	 * @return the boolean.
	 */
	public static Boolean updateFacebookStreamConfig(ClientStreamDVO dvo) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String sql;
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			sql = "UPDATE  client_stream_details set fbStrmSts=?,fbRtmpsUrl=?,fbStrmKey=?,fbTrgtId=?,fbOutputTrgtId=?, upddate = getDate() , updby=?  where clintid=? and streamId=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getFbStrmSts());
			ps.setString(2, dvo.getFbRtmpsUrl());
			ps.setString(3, dvo.getFbStrmKey());
			ps.setString(4, dvo.getFbTrgtId());
			ps.setString(5, dvo.getFbOutputTrgtId());
			ps.setString(6, dvo.getUpdBy());
			ps.setString(7, dvo.getClintid());
			ps.setString(8, dvo.getStreamid());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
	
	
	/**
	 * Update client stream object youtube stream configurations.
	 *
	 * @param ClientStreamDVO the dvo.
	 * @return the boolean.
	 */
	public static Boolean updateYoutubeStreamConfig(ClientStreamDVO dvo) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String sql;
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			sql = "UPDATE  client_stream_details set ytStrmSts=?,ytRtmpUrl=?,ytStrmKey=?,ytTrgtId=?,ytOutputTrgtId=?, upddate = getDate() , updby=?  where clintid=? and streamId=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getYtStrmSts());
			ps.setString(2, dvo.getYtRtmpUrl());
			ps.setString(3, dvo.getYtStrmKey());
			ps.setString(4, dvo.getYtTrgtId());
			ps.setString(5, dvo.getYtOutputTrgtId());
			ps.setString(6, dvo.getUpdBy());
			ps.setString(7, dvo.getClintid());
			ps.setString(8, dvo.getStreamid());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
	
	
}
