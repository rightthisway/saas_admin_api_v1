package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.StreamConstants;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.util.HTTPUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class DisableStreamCustomTargetServlet.
 */
@WebServlet("/dsblestrmtrgt.json")
public class DisableStreamCustomTargetServlet extends RtfSaasBaseServlet{

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
	
	
	
	/**
	 * Disable Live stream custom target from stream output.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String streamId = request.getParameter("sId");
		String tType = request.getParameter("tType");

		ClientStreamDTO dto = new ClientStreamDTO();
		dto.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (streamId == null || streamId.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (tType == null || tType.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientStreamDVO sdvo = ClientStreamDAO.getClientStreamById(clientId, streamId);
			if (sdvo == null || sdvo.getStreamid() == null) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			String apiUrl = "https://api.cloud.wowza.com/api/v1.6/transcoders/"+sdvo.getTrnscdrId()+"/outputs/"+sdvo.getOutputId()+"/output_stream_targets/";
			System.out.println(apiUrl);
			if(tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)){
				apiUrl = apiUrl + sdvo.getFbOutputTrgtId();
			}else if(tType.equalsIgnoreCase(StreamConstants.YOUTUBE_STREAM)){
				apiUrl = apiUrl + sdvo.getYtOutputTrgtId();
			}
			String data = HTTPUtil.executeWowzaDeleteApi("",apiUrl);
			System.out.println("REMOVE STREAM : "+data);
			if(data != null && !data.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_REMOVING_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			/*Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			JsonObject respObj =  jsonObject.getAsJsonObject("meta");
			if(respObj!=null){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_REMOVING_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}*/
			
			Boolean isUpd = false;
			if(tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)){
				sdvo.setFbStrmSts(StreamConstants.CUSTOM_TARGET_DISABLED);
				sdvo.setFbOutputTrgtId(null);
				sdvo.setUpdBy(cau);
				isUpd = ClientStreamDAO.updateFacebookStreamConfig(sdvo);
			}else if(tType.equalsIgnoreCase(StreamConstants.YOUTUBE_STREAM)){
				sdvo.setYtStrmSts(StreamConstants.CUSTOM_TARGET_DISABLED);
				sdvo.setYtOutputTrgtId(null);
				sdvo.setUpdBy(cau);
				isUpd = ClientStreamDAO.updateYoutubeStreamConfig(sdvo);
			}else{
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			
			if(isUpd){
				dto.setSts(1);
				dto.setDvo(sdvo);
				dto.setMsg(StreamConstants.STREAM_TARGET_DELETED);
				generateResponse(request, response, dto);
				return;
			}

			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
		}catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
		}
	}
	
}
