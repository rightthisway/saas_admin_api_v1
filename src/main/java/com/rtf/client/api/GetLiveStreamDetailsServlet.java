/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState;
import com.rtf.wowza.cloudsdk.client.reference.LiveStreams.FetchALiveStreamState;

/**
 * The Class GetLiveStreamDetailsServlet.
 */
@WebServlet("/livstrmdtls.json")
public class GetLiveStreamDetailsServlet extends RtfSaasBaseServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get live video stream details for specified client.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		
		ClientStreamDTO respDTO = new ClientStreamDTO();
		respDTO.setSts(0);
		String msg = null;	
	
		try {
			if (clId == null || clId.isEmpty()) {
				msg = MessageConstant.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = MessageConstant.INVALID_USER;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			ClientStreamDVO streamObj = ClientStreamDAO.getClientStreamById(clId,null);
			if(streamObj == null) {
				msg = MessageConstant.STREAM_DOES_NOT_EXIST;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
		

			LiveStreamState streamState = FetchALiveStreamState.fetchLiveStreamState(streamObj.getStreamid());		
			if(!streamObj.getStreamstatus().equals(streamState.getState().toString())) {
				ClientStreamDAO.updateStreamRunStatus(clId, "AUTO", streamObj.getStreamid(), streamState.getState().toString());
			}
			streamObj.setStreamstatus(streamState.getState().toString());	
			
			
			respDTO.setSts(1);
			respDTO.setDvo(streamObj);
			respDTO.setClId(clId);			
			generateResponse(request, response, respDTO);
			return;
			
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} 
		return;
	}
}
