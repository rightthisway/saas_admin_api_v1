/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.service;

import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamAuditDVO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamAuditDAO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.StreamConstants;
import com.rtf.wowza.cloudsdk.client.ApiClient;
import com.rtf.wowza.cloudsdk.client.ApiException;
import com.rtf.wowza.cloudsdk.client.Configuration;
import com.rtf.wowza.cloudsdk.client.api.LiveStreamsApi;
import com.rtf.wowza.cloudsdk.client.auth.ApiKeyAuth;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamActionState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState;
import com.rtf.wowza.cloudsdk.client.reference.LiveStreams.FetchALiveStreamState;

/**
 * The Class ClientStreamManagerService.
 */
public class ClientStreamManagerService {

	/**
	 * Stop the live video stream.
	 *
	 * @param clientId the client id
	 * @param dto      the dto
	 * @return ClientStreamDTO the client stream DTO
	 */
	public static ClientStreamDTO stopStream(String clientId, ClientStreamDTO dto) {

		dto.setSts(0);
		ClientStreamDVO dvo = dto.getDvo();
		String processMsg = StreamConstants.STREAM_ACTION_ERROR;
		try {
			processMsg = stopWOWZAStream(dvo.getStreamid());
			ClientStreamDAO.updateStreamRunStatus(clientId, dvo.getCreBy(), dvo.getStreamid(),
					StreamConstants.STREAM_ACTION_STOP_INPROGRESS);
			saveStreamAuditDetails(clientId, dvo.getStreamid(), processMsg, dvo.getCreBy(),
					StreamConstants.STREAM_ACTION_STOP, processMsg);
			dto.setSts(1);
			dvo.setStreamstatus(processMsg);

		} catch (Exception ex) {
			ex.printStackTrace();
			dvo.setStreamstatus(StreamConstants.STREAM_ACTION_ERROR);
		}
		return dto;
	}

	/**
	 * Save the stream actions audit details.
	 *
	 * @param clientId       the client id
	 * @param streamId       the stream id
	 * @param processMsg     the process msg
	 * @param creBy          the cre by
	 * @param action         the action
	 * @param actionResponse the action response
	 */
	private static void saveStreamAuditDetails(String clientId, String streamId, String processMsg, String creBy,
			String action, String actionResponse) {
		try {
			ClientStreamAuditDVO dvo = new ClientStreamAuditDVO();
			dvo.setClintid(clientId);
			dvo.setStreamaction(action);
			dvo.setStreamid(streamId);
			dvo.setCreby(creBy);
			dvo.setActionResponse(actionResponse);
			ClientStreamAuditDAO.saveStreamAuditDets(clientId, dvo);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Stop live video stream on WOWZA server.
	 *
	 * @param streamId the stream id
	 * @return String the string
	 * @throws Exception the exception
	 */
	static String stopWOWZAStream(String streamId) throws Exception {
		String processMsg = StreamConstants.STREAM_ACTION_ERROR;
		ApiClient defaultClient = Configuration.getDefaultApiClient();
		ApiKeyAuth wscaccesskey = (ApiKeyAuth) defaultClient.getAuthentication("wsc-access-key");
		wscaccesskey.setApiKey(defaultClient.wscaccesskey);
		ApiKeyAuth wscapikey = (ApiKeyAuth) defaultClient.getAuthentication("wsc-api-key");
		wscapikey.setApiKey(defaultClient.wscapikey);
		LiveStreamsApi apiInstance = new LiveStreamsApi();

		try {
			LiveStreamActionState result = apiInstance.stopLiveStream(streamId);
			if (result != null) {
				if (StreamConstants.STREAM_ACTION_STOPPING.toLowerCase().equals(result.getState().getValue())) {
					processMsg = StreamConstants.STREAM_ACTION_STOP_INPROGRESS;
				} else if (StreamConstants.STREAM_ACTION_STOPPED.toLowerCase().equals(result.getState().getValue())) {
					processMsg = StreamConstants.STREAM_ACTION_STOPPED;
				} else {
					processMsg = StreamConstants.STREAM_ACTION_ERROR;
				}
			}

		} catch (ApiException e) {
			e.printStackTrace();
			processMsg = StreamConstants.STREAM_ACTION_ERROR;
		}
		return processMsg;

	}

	/**
	 * Start the live video stream.
	 *
	 * @param clientId the client id
	 * @param dto      the dto
	 * @return ClientStreamDTO the client stream DTO
	 */
	public static ClientStreamDTO startStream(String clientId, ClientStreamDTO dto) {
		dto.setSts(0);
		ClientStreamDVO dvo = dto.getDvo();
		String processMsg = StreamConstants.STREAM_ACTION_ERROR;
		try {
			processMsg = startWOWZAStream(dvo.getStreamid());
			ClientStreamDAO.updateStreamRunStatus(clientId, dvo.getCreBy(), dvo.getStreamid(),
					StreamConstants.STREAM_ACTION_START_INPROGRESS);
			saveStreamAuditDetails(clientId, dvo.getStreamid(), processMsg, dvo.getCreBy(),
					StreamConstants.STREAM_ACTION_START, processMsg);
			dto.setSts(1);
			dvo.setStreamstatus(processMsg);
		} catch (Exception ex) {
			ex.printStackTrace();
			dvo.setStreamstatus(StreamConstants.STREAM_ACTION_ERROR);
		}
		return dto;
	}

	/**
	 * Start live video stream on WOWZA server.
	 *
	 * @param streamId the stream id
	 * @return the string
	 * @throws Exception the exception
	 */
	static String startWOWZAStream(String streamId) throws Exception {
		String processMsg = StreamConstants.STREAM_ACTION_ERROR;
		ApiClient defaultClient = Configuration.getDefaultApiClient();
		ApiKeyAuth wscaccesskey = (ApiKeyAuth) defaultClient.getAuthentication("wsc-access-key");
		wscaccesskey.setApiKey(defaultClient.wscaccesskey);
		ApiKeyAuth wscapikey = (ApiKeyAuth) defaultClient.getAuthentication("wsc-api-key");
		wscapikey.setApiKey(defaultClient.wscapikey);
		LiveStreamsApi apiInstance = new LiveStreamsApi();

		try {
			LiveStreamActionState result = apiInstance.startLiveStream(streamId);
			if (result != null) {
				if (StreamConstants.STREAM_ACTION_STARTING.toLowerCase().equals(result.getState().getValue())) {
					processMsg = StreamConstants.STREAM_ACTION_START_INPROGRESS;
				} else if (StreamConstants.STREAM_ACTION_STARTED.toLowerCase().equals(result.getState().getValue())) {
					processMsg = StreamConstants.STREAM_ACTION_STARTED;
				} else {
					processMsg = StreamConstants.STREAM_ACTION_ERROR;
				}
			}
		} catch (ApiException e) {
			e.printStackTrace();
			processMsg = StreamConstants.STREAM_ACTION_ERROR;
		}
		return processMsg;

	}

	/**
	 * Fetch stream status once stream is started.
	 *
	 * @param clientId the client id
	 * @param dto      the dto
	 * @return the client stream DTO
	 * @throws Exception the exception
	 */
	public static ClientStreamDTO fetchStreamStatus(String clientId, ClientStreamDTO dto) throws Exception {
		try {
			dto.setSts(0);
			ClientStreamDVO dvo = dto.getDvo();
			LiveStreamState state = fetchWOWZAStreamStatus(dvo.getStreamid());
			String liveStatusOnWowza = state.getState().getValue();
			if (liveStatusOnWowza != null) {
				ClientStreamDAO.updateStreamRunStatus(clientId, dvo.getCreBy(), dvo.getStreamid(), liveStatusOnWowza);
			} else {
				dto.setSts(0);
				return dto;
			}
			dvo.setStreamstatus(liveStatusOnWowza);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
		}
		return dto;

	}

	/**
	 * Fetch stream status from WOWZA server.
	 *
	 * @param streamId the stream id
	 * @return the live stream state
	 * @throws Exception the exception
	 */
	public static LiveStreamState fetchWOWZAStreamStatus(String streamId) throws Exception {
		LiveStreamState state = FetchALiveStreamState.fetchLiveStreamState(streamId);
		return state;
	}

}
