package com.rtf.client.service;
/**
 * The Class ClientThemeService.
 */

import com.rtf.client.dvo.ClientThemeConfigDVO;
import com.rtf.client.sql.dao.ClientThemeConfigDAO;

public class ClientThemeService {

	/**
	 * Get the client theme by client ID and product ID.
	 *
	 * @param clId the client id
	 * @param prodType  the product Type
	 * @return ClientThemeConfigDVO the ClientThemeConfigDVO
	 */
	public static ClientThemeConfigDVO getClientThemeConfig(String clId,String prodType){
		ClientThemeConfigDVO themeConfig = null;
		try {
			themeConfig = ClientThemeConfigDAO.getClientThemeConfig(clId, prodType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return themeConfig;
	}
	
	
	/**
	 * Save client theme config. for client.
	 *
	 * @param ClientThemeConfigDVO  the Client Theme Config. DVO
	 * @return Boolean the isSaved
	 */
	public static Boolean saveClientThemeConfig(ClientThemeConfigDVO theme){
		Boolean isSaved = null;
		try {
			isSaved = ClientThemeConfigDAO.saveClientThemeConfig(theme);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSaved;
	}
	
	
	/**
	 * Update client theme config. for client.
	 *
	 * @param ClientThemeConfigDVO  the Client Theme Config. DVO
	 * @return Boolean the isUpd
	 */
	public static Boolean updateClientThemeConfig(ClientThemeConfigDVO theme){
		Boolean isUpd = null;
		try {
			isUpd = ClientThemeConfigDAO.updateClientThemeConfig(theme);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpd;
	}
	
	
	
}
