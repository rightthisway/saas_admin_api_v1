/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dvo;

/**
 * The Class ClientRegistrationDVO.
 */
public class ClientRegistrationDVO {

	/** The client id. */
	private String clintid;

	/** The comp name. */
	private String compname;

	/** The email. */
	private String email;

	/**  is Active Status. */
	private String isactive;

	/** The phone num. */
	private String phonenum;

	/** The website url. */
	private String websiteurl;

	/** The saas weburl. */
	private String saasweburl;

	/**
	 * Gets the clientid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clientid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the comp name.
	 *
	 * @return the compname
	 */
	public String getCompname() {
		return compname;
	}

	/**
	 * Sets the comp name.
	 *
	 * @param compname the new compname
	 */
	public void setCompname(String compname) {
		this.compname = compname;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public String getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the phone num.
	 *
	 * @return the phonenum
	 */
	public String getPhonenum() {
		return phonenum;
	}

	/**
	 * Sets the phone num.
	 *
	 * @param phonenum the new phonenum
	 */
	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}

	/**
	 * Gets the website url.
	 *
	 * @return the websiteurl
	 */
	public String getWebsiteurl() {
		return websiteurl;
	}

	/**
	 * Sets the website url.
	 *
	 * @param websiteurl the new websiteurl
	 */
	public void setWebsiteurl(String websiteurl) {
		this.websiteurl = websiteurl;
	}

	/**
	 * Gets the saas web url.
	 *
	 * @return the saasweburl
	 */
	public String getSaasweburl() {
		return saasweburl;
	}

	/**
	 * Sets the saas web url.
	 *
	 * @param saasweburl the new saasweburl
	 */
	public void setSaasweburl(String saasweburl) {
		this.saasweburl = saasweburl;
	}

}
