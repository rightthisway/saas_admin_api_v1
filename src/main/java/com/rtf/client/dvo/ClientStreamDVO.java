/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dvo;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * The Class ClientStreamDVO.
 */
public class ClientStreamDVO {

	/** The client id. */
	private String clintid;

	/** The stream engine. */
	private String streamengine;

	/** The stream id. */
	private String streamid;

	/** The stream name. */
	private String streamname;

	/** The targe tid. */
	@JsonIgnore
	private String targetid;
	
	/** The output ID. */
	@JsonIgnore
	private String outputId;
	
	/** The transcoder ID. */
	@JsonIgnore
	private String trnscdrId;

	/** The source url. */
	@JsonIgnore
	private String sourceurl;

	/** The source key. */
	@JsonIgnore
	private String sourcekey;

	/** The port. */
	@JsonIgnore
	private String port;

	/** The is auth disabled. */
	@JsonIgnore
	private Boolean isAuthDisabled;

	/** The username. */
	@JsonIgnore
	private String usrname;

	/** The password. */
	@JsonIgnore
	private String password;

	/** The playback url. */
	private String playbackurl;

	/** The hosted url. */
	@JsonIgnore
	private String hostedurl;

	/** The status. */
	@JsonIgnore
	private String status;

	/** The stream status. */
	private String streamstatus;

	/** The updated date. */
	@JsonIgnore
	private Date updDt;

	/** The created date. */
	@JsonIgnore
	private Date creDate;

	/** The updated by. */
	@JsonIgnore
	private String updBy;

	/** The created by. */
	@JsonIgnore
	private String creBy;
	
	/** The facebook RTMPS URL. */
	private String fbRtmpsUrl;
	
	/** The facebook stream key. */
	private String fbStrmKey;
	
	/** The facebook stream output target id. */
	private String fbTrgtId;
	
	/** The facebook stream output target id. */
	private String fbOutputTrgtId;
	
	/** The facebook stream status. */
	private String fbStrmSts;
	
	/** The youtube RTMPS URL. */
	private String ytRtmpUrl;

	/** The youtube stream key. */
	private String ytStrmKey;
	
	/** The youtube stream target id. */
	private String ytTrgtId;
	
	/** The youtube stream output target id. */
	private String ytOutputTrgtId;
	
	/** The youtube stream status. */
	private String ytStrmSts;
	
	/** The youtube stream health. */
	private Boolean isYtUp;
	
	/** The facebook stream health. */
	private Boolean isFbUp;
	
	

	/**
	 * Gets the client id.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the client id.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the stream engine.
	 *
	 * @return the streamengine
	 */
	public String getStreamengine() {
		return streamengine;
	}

	/**
	 * Sets the stream engine.
	 *
	 * @param streamengine the new streamengine
	 */
	public void setStreamengine(String streamengine) {
		this.streamengine = streamengine;
	}

	/**
	 * Gets the stream id.
	 *
	 * @return the streamid
	 */
	public String getStreamid() {
		return streamid;
	}

	/**
	 * Sets the stream id.
	 *
	 * @param streamid the new streamid
	 */
	public void setStreamid(String streamid) {
		this.streamid = streamid;
	}

	/**
	 * Gets the stream name.
	 *
	 * @return the streamname
	 */
	public String getStreamname() {
		return streamname;
	}

	/**
	 * Sets the stream name.
	 *
	 * @param streamname the new streamname
	 */
	public void setStreamname(String streamname) {
		this.streamname = streamname;
	}

	/**
	 * Gets the target id.
	 *
	 * @return the targetid
	 */
	public String getTargetid() {
		return targetid;
	}

	/**
	 * Sets the target id.
	 *
	 * @param targetid the new targetid
	 */
	public void setTargetid(String targetid) {
		this.targetid = targetid;
	}

	/**
	 * Gets the source url.
	 *
	 * @return the sourceurl
	 */
	public String getSourceurl() {
		return sourceurl;
	}

	/**
	 * Sets the source url.
	 *
	 * @param sourceurl the new sourceurl
	 */
	public void setSourceurl(String sourceurl) {
		this.sourceurl = sourceurl;
	}

	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the new port
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * Gets the checks if is auth disabled.
	 *
	 * @return the checks if is auth disabled
	 */
	public Boolean getIsAuthDisabled() {
		return isAuthDisabled;
	}

	/**
	 * Sets the checks if is auth disabled.
	 *
	 * @param isAuthDisabled the new checks if is auth disabled
	 */
	public void setIsAuthDisabled(Boolean isAuthDisabled) {
		this.isAuthDisabled = isAuthDisabled;
	}

	/**
	 * Gets the username.
	 *
	 * @return the usrname
	 */
	public String getUsrname() {
		return usrname;
	}

	/**
	 * Sets the username.
	 *
	 * @param usrname the new usrname
	 */
	public void setUsrname(String usrname) {
		this.usrname = usrname;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the playback url.
	 *
	 * @return the playbackurl
	 */
	public String getPlaybackurl() {
		return playbackurl;
	}

	/**
	 * Sets the playback url.
	 *
	 * @param playbackurl the new playbackurl
	 */
	public void setPlaybackurl(String playbackurl) {
		this.playbackurl = playbackurl;
	}

	/**
	 * Gets the hosted url.
	 *
	 * @return the hostedurl
	 */
	public String getHostedurl() {
		return hostedurl;
	}

	/**
	 * Sets the hosted url.
	 *
	 * @param hostedurl the new hostedurl
	 */
	public void setHostedurl(String hostedurl) {
		this.hostedurl = hostedurl;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the stream status.
	 *
	 * @return the streamstatus
	 */
	public String getStreamstatus() {
		return streamstatus;
	}

	/**
	 * Sets the stream status.
	 *
	 * @param streamstatus the new streamstatus
	 */
	public void setStreamstatus(String streamstatus) {
		this.streamstatus = streamstatus;
	}

	/**
	 * Gets the source key.
	 *
	 * @return the sourcekey
	 */
	public String getSourcekey() {
		return sourcekey;
	}

	/**
	 * Sets the source key.
	 *
	 * @param sourcekey the new sourcekey
	 */
	public void setSourcekey(String sourcekey) {
		this.sourcekey = sourcekey;
	}

	/**
	 * Gets the updated dt.
	 *
	 * @return the upd dt
	 */
	public Date getUpdDt() {
		return updDt;
	}

	/**
	 * Sets the updated dt.
	 *
	 * @param updDt the new upd dt
	 */
	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the cre date
	 */
	public Date getCreDate() {
		return creDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param creDate the new cre date
	 */
	public void setCreDate(Date creDate) {
		this.creDate = creDate;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the upd by
	 */
	public String getUpdBy() {
		return updBy;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updBy the new upd by
	 */
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the cre by
	 */
	public String getCreBy() {
		return creBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param creBy the new cre by
	 */
	public void setCreBy(String creBy) {
		this.creBy = creBy;
	}
	
	
	/**
	 * Gets the facebook stream RTMPS URL.
	 *
	 * @return fbRtmpsUrl 
	 */
	public String getFbRtmpsUrl() {
		return fbRtmpsUrl;
	}

	/**
	 * Sets the facebook stream RTMPS URL.
	 *
	 * @param fbRtmpsUrl 
	 */
	public void setFbRtmpsUrl(String fbRtmpsUrl) {
		this.fbRtmpsUrl = fbRtmpsUrl;
	}

	/**
	 * Gets the facebook stream key.
	 *
	 * @return fbStrmKey 
	 */
	public String getFbStrmKey() {
		return fbStrmKey;
	}
	
	/**
	 * Sets the facebook stream key.
	 *
	 * @param fbStrmKey
	 */
	public void setFbStrmKey(String fbStrmKey) {
		this.fbStrmKey = fbStrmKey;
	}

	/**
	 * Gets the facebook stream status.
	 *
	 * @return fbFtrmSts 
	 */
	public String getFbStrmSts() {
		return fbStrmSts;
	}

	/**
	 * Sets the facebook stream status.
	 *
	 * @param  fbFtrmSts
	 */
	public void setFbStrmSts(String fbStrmSts) {
		this.fbStrmSts = fbStrmSts;
	}

	/**
	 * Gets the youtube stream RTMP URL.
	 *
	 * @return ytRtmpsUrl 
	 */
	public String getYtRtmpUrl() {
		return ytRtmpUrl;
	}

	/**
	 * Sets the youtube stream RTMP URL.
	 *
	 * @param  ytRtmpUrl
	 */
	public void setYtRtmpUrl(String ytRtmpsUrl) {
		this.ytRtmpUrl = ytRtmpUrl;
	}

	/**
	 * Gets the youtube stream key.
	 *
	 * @return ytStrmKey
	 */
	public String getYtStrmKey() {
		return ytStrmKey;
	}

	/**
	 * Sets the youtube stream key.
	 *
	 * @param ytStrmKey
	 */
	public void setYtStrmKey(String ytStrmKey) {
		this.ytStrmKey = ytStrmKey;
	}

	/**
	 * Gets the youtube stream status.
	 *
	 * @return the ytStrmSts
	 */
	public String getYtStrmSts() {
		return ytStrmSts;
	}

	/**
	 * Sets the youtube stream status.
	 *
	 * @param ytStrmSts
	 */
	public void setYtStrmSts(String ytStrmSts) {
		this.ytStrmSts = ytStrmSts;
	}
	
	
	/**
	 * Gets the outputId.
	 *
	 * @return the outputId
	 */
	public String getOutputId() {
		return outputId;
	}

	/**
	 * Sets the stream transcoder ID.
	 *
	 * @param trnscdrId
	 */
	public void setOutputId(String outputId) {
		this.outputId = outputId;
	}

	/**
	 * Gets the stream transcoder ID.
	 *
	 * @return the trnscdrId
	 */
	public String getTrnscdrId() {
		return trnscdrId;
	}

	/**
	 * Sets the facebook custom target ID.
	 *
	 * @param fbTrgtId
	 */
	public void setTrnscdrId(String trnscdrId) {
		this.trnscdrId = trnscdrId;
	}

	/**
	 * Gets the facebook custom target ID.
	 *
	 * @return the fbTrgtId
	 */
	public String getFbTrgtId() {
		return fbTrgtId;
	}

	/**
	 * Sets the facebook custom target ID.
	 *
	 * @param ytTrgtId
	 */
	public void setFbTrgtId(String fbTrgtId) {
		this.fbTrgtId = fbTrgtId;
	}

	/**
	 * Gets the youtube custom target ID.
	 *
	 * @return the ytTrgtId
	 */
	public String getYtTrgtId() {
		return ytTrgtId;
	}

	/**
	 * Sets the youtube custom target ID.
	 *
	 * @param ytTrgtId
	 */
	public void setYtTrgtId(String ytTrgtId) {
		this.ytTrgtId = ytTrgtId;
	}

	/**
	 * Gets the facebook custom output target ID.
	 *
	 * @return the fbOutputTrgtId
	 */
	public String getFbOutputTrgtId() {
		return fbOutputTrgtId;
	}

	/**
	 * Sets the facebook custom output target ID.
	 *
	 * @param fbOutputTrgtId
	 */
	public void setFbOutputTrgtId(String fbOutputTrgtId) {
		this.fbOutputTrgtId = fbOutputTrgtId;
	}

	/**
	 * Gets the youtube custom output target ID.
	 *
	 * @return the ytOutputTrgtId
	 */
	public String getYtOutputTrgtId() {
		return ytOutputTrgtId;
	}

	/**
	 * Sets the youtube custom output target ID.
	 *
	 * @param ytOutputTrgtId
	 */
	public void setYtOutputTrgtId(String ytOutputTrgtId) {
		this.ytOutputTrgtId = ytOutputTrgtId;
	}
	
	
	
	/**
	 * Gets the youtube custom output stream is up.
	 *
	 * @return the isYtUp
	 */
	public Boolean getIsYtUp() {
		return isYtUp;
	}
	/**
	 * Sets the youtube custom output stream is up.
	 *
	 * @param isYtUp
	 */
	public void setIsYtUp(Boolean isYtUp) {
		this.isYtUp = isYtUp;
	}
	/**
	 * Gets the facebook custom output stream is up.
	 *
	 * @return the isFbUp
	 */
	public Boolean getIsFbUp() {
		return isFbUp;
	}
	/**
	 * Sets the facebook custom output stream is up.
	 *
	 * @param isFbUp
	 */
	public void setIsFbUp(Boolean isFbUp) {
		this.isFbUp = isFbUp;
	}

	/**
	 * Generate string format of all properties
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ClientStreamDVO [clintid=" + clintid + ", streamengine=" + streamengine + ", streamid=" + streamid
				+ ", streanname=" + streamname + ", targetid=" + targetid + ", sourceurl=" + sourceurl + ", port="
				+ port + ", isAuth=" + isAuthDisabled + ", usrname=" + usrname + ", password=" + password
				+ ", playbackurl=" + playbackurl + ", hostedurl=" + hostedurl + ", status=" + status + ", streamstatus="
				+ streamstatus + "]";
	}

}
