/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dvo;

import com.rtf.saas.sql.dvo.SaasBaseDVO;

/**
 * The Class ClientStreamAuditDVO.
 */
public class ClientStreamAuditDVO extends SaasBaseDVO {

	/** The client id. */
	private String clintid;

	/** The stream id. */
	private String streamid;

	/** The stream action. */
	private String streamaction;

	/** The created by. */
	private String creby;

	/** The created date. */
	private java.util.Date credate;

	/** The action response. */
	private String actionResponse;

	/**
	 * Gets the clientid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clientid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the stream id.
	 *
	 * @return the streamid
	 */
	public String getStreamid() {
		return streamid;
	}

	/**
	 * Sets the stream id.
	 *
	 * @param streamid the new streamid
	 */
	public void setStreamid(String streamid) {
		this.streamid = streamid;
	}

	/**
	 * Gets the stream action.
	 *
	 * @return the streamaction
	 */
	public String getStreamaction() {
		return streamaction;
	}

	/**
	 * Sets the stream action.
	 *
	 * @param streamaction the new streamaction
	 */
	public void setStreamaction(String streamaction) {
		this.streamaction = streamaction;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the created by.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the credate
	 */
	public java.util.Date getCredate() {
		return credate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(java.util.Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the action response.
	 *
	 * @return the action response
	 */
	public String getActionResponse() {
		return actionResponse;
	}

	/**
	 * Sets the action response.
	 *
	 * @param actionResponse the new action response
	 */
	public void setActionResponse(String actionResponse) {
		this.actionResponse = actionResponse;
	}
}
