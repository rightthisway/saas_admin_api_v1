package com.rtf.client.dvo;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

/**
 * The Class ClientThemeDVO.
 */
public class ClientThemeDVO {

	/** The BASIC : Backgroun color. */
	private String bscBgCol;

	/** The BASIC : Font family */
	private String bscFntFmly;

	/** The BASIC : Font name. */
	private String bscFntName;

	/** The BASIC : Dot color. */
	private String bscDotColor;

	/** The Modal : ButtoncColor. */
	private String mdlBtnCol;

	/** The Modal : Button text color. */
	private String mdlBtnTxtCol;

	/** The Question : Strock finish color. */
	private String qsnStrkFnshCol;

	/** The Question : Strock starting color. */
	private String qsnStrkStrtngCol;

	/** The Question : Border color. */
	private String qsnBrdrCol;

	/** The Question : Answer color. */
	private String qsnAnsCol;

	/** The Question : Right answer color. */
	private String qsnRghtAnsCol;

	/** The Question : Wrong answer color. */
	private String qsnWrngAnsCol;

	/** The Question : Selected answer text color. */
	private String qsnSelAnsTxtCol;

	/** The Question : Right answer text color. */
	private String qsnRghtAnsTxtCol;

	/** The Question : Wrong answer text color. */
	private String qsnWrngAnstxtCol;

	/** The Header : Cart color. */
	private String hdrCartCol;

	/** The Header : User count color. */
	private String hdrUsrCntCol;

	/** The Header : Live icon height. */
	private String hdrLivIconHght;

	/** The Summary : Font color. */
	private String smrFntCol;

	/** The Summary : Circle color. */
	private String smrCrclCol;

	/** The Summary : Box color. */
	private String smrBoxCol;

	/** The Summary : Font size. */
	private String smrFntSize;

	/** The Summary : Font wieght. */
	private String smrFntWght;

	/** The Summary : Prize font size. */
	private String smrPrzFntSize;

	/** The Summary : Logo X pos. */
	private String smrLogoX;

	/** The Summary : Logo Y pos. */
	private String smrLogoY;

	/** The Summary : Logo scale. */
	private String smrLogoScl;

	/** The Summary : Logo size. */
	private String smrLogoSize;

	/** The Summary : Circle fill. */
	private String smrCrclFill;

	/** The Lottery : Winner color */
	private String ltryWinCol;

	/** The Lottery : Circle fill. */
	private String ltryCrclFill;

	/** The Winner : Font color. */
	private String winFntCol;

	/** The Summary : Font size. */
	private String fnlstFntSize;

	/** The Winner : Font size. */
	private String winFntSize;

	/** The Winner : Font weight. */
	private String winFntWght;

	/** The Winner : Box color.. */
	private String winBoxCol;

	/** The Winner : Winner color. */
	private String winCol;

	/** The Winner : Prize font color. */
	private String winPrizeFntCol;

	/** The Summary logo image. */
	private String sumLogoImg;

	/** The Video poster image. */
	private String videoPostrImg;

	/** The Logo color image. */
	private String logoColImg;

	/** The Logo circle image. */
	private String logoCrclImg;

	/** The White logo image. */
	private String logoWhtImg;

	/** The Dashboard logo image. */
	private String dashbrdImg;

	/** The Name tag image. */
	private String nameTagImg;

	/** The Cart image. */
	private String cartImg;

	/** The User image. */
	private String userImg;

	/** The Cart color image. */
	private String cartColImg;

	/** The User color image. */
	private String userColImg;

	/**
	 * ClientThemeDVO default constructor.
	 */
	public ClientThemeDVO() {
		this.bscBgCol = "#002244";
		this.bscFntFmly = "https://fonts.googleapis.com/css2?family=Open+Sans:ital@1&display=swap";
		this.bscFntName = "Open Sans";
		this.bscDotColor = "#fff";

		this.mdlBtnCol = "#012e4f";
		this.mdlBtnTxtCol = "#ffffff";

		this.qsnStrkFnshCol = "rgb(255, 250, 230)";
		this.qsnStrkStrtngCol = "rgb(240, 193, 75)";
		this.qsnBrdrCol = "#355677";
		this.qsnAnsCol = "#012e4f";
		this.qsnRghtAnsCol = "#3fa847";
		this.qsnWrngAnsCol = "#012e4f";
		this.qsnRghtAnsTxtCol = "#ffffff";
		this.qsnWrngAnstxtCol = "#002aff";
		this.qsnSelAnsTxtCol = "#ee6d6d";

		this.hdrCartCol = "#ffffff";
		this.hdrUsrCntCol = "#ffffff";
		this.hdrLivIconHght = "40";

		this.smrFntCol = "0x000000";
		this.smrCrclCol = "0xffc658";
		this.smrBoxCol = "0xffffff";
		this.smrFntSize = "20";
		this.smrFntWght = "500";
		this.smrPrzFntSize = "21";
		this.smrLogoX = "107";
		this.smrLogoY = "-80";
		this.smrLogoScl = "0.8,";
		this.smrLogoSize = "50";
		this.smrCrclFill = "0xffffff";
		this.ltryWinCol = "#ffc658";
		this.ltryCrclFill = "#ffffff";

		this.winFntCol = "rgb(0, 0, 0)";
		this.fnlstFntSize = "21";
		this.winFntSize = "21";
		this.winFntWght = "600";
		this.winBoxCol = "#002244";
		this.winCol = "rgb(0, 0, 0)";
		this.winPrizeFntCol = "rgb(240, 193, 75)";

		this.sumLogoImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/logowh.png";
		this.videoPostrImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/videoPoster.jpg";
		this.logoColImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/logowh.png";
		this.logoCrclImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/logowh.png";
		this.logoWhtImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/logowh.png";
		this.dashbrdImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/name_tag.png";
		this.nameTagImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/Summayicon.png";
		this.cartImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/cart.png";
		this.userImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/user.png";
		this.cartColImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/cart_colored.png";
		this.userColImg = "https://rtfmedia.s3.amazonaws.com/saas/DEF/img/user_colored.png";
	}

	/**
	 * ClientThemeDVO parameterized constructor.
	 */
	public ClientThemeDVO(JsonObject obj) {
		JsonObject bscObj =  obj.getAsJsonObject("basic");
		if(bscObj !=null){
			this.bscBgCol = bscObj.get("background").getAsString();
			this.bscFntFmly = bscObj.get("font_family").getAsString();
			this.bscFntName = bscObj.get("font_name").getAsString();
			this.bscDotColor = bscObj.get("dot_color").getAsString();
		}
		
		JsonObject mdlObj =  obj.getAsJsonObject("modal");
		if(mdlObj!=null){
			this.mdlBtnCol = mdlObj.get("buttonColor").getAsString();
			this.mdlBtnTxtCol = mdlObj.get("buttonText").getAsString();
		}
		
		JsonObject qsnObj =  obj.getAsJsonObject("question_box");
		if(qsnObj!=null){
			this.qsnStrkFnshCol = qsnObj.get("stroke_finishing").getAsString();
			this.qsnStrkStrtngCol = qsnObj.get("stroke_starting").getAsString();
			this.qsnBrdrCol = qsnObj.get("que_border_color").getAsString();
			this.qsnAnsCol = qsnObj.get("active_answer_color").getAsString();
			this.qsnRghtAnsCol = qsnObj.get("right_anser_color").getAsString();
			this.qsnWrngAnsCol = qsnObj.get("wrong_answer_color").getAsString();
			this.qsnRghtAnsTxtCol =qsnObj.get("correct_answer_text").getAsString();
			this.qsnWrngAnstxtCol = qsnObj.get("wrong_answer_text").getAsString();
			this.qsnSelAnsTxtCol = qsnObj.get("seleted_option_text").getAsString();

		}
		
		JsonObject hdrObj =  obj.getAsJsonObject("header");
		if(hdrObj!=null){
			this.hdrCartCol = hdrObj.get("cart_user_color").getAsString();
			this.hdrUsrCntCol = hdrObj.get("cart_user").getAsString();
			this.hdrLivIconHght = hdrObj.get("liveIconHeight").getAsString();
		}

		JsonObject smrObj =  obj.getAsJsonObject("summary");
		if(smrObj!=null){
			this.smrFntCol = smrObj.get("summery_test_color").getAsString();
			this.smrCrclCol = smrObj.get("summary_circle_color").getAsString();
			this.smrBoxCol = smrObj.get("summary_box").getAsString();
			this.smrFntSize = smrObj.get("summary_name_Text_fontSize").getAsString();
			this.smrFntWght = smrObj.get("summary_name_Text_Weight").getAsString();
			this.smrPrzFntSize = smrObj.get("summaryWinne_text_FontSize").getAsString();
			this.smrLogoX = smrObj.get("logo_x").getAsString();
			this.smrLogoY = smrObj.get("logo_y").getAsString();
			this.smrLogoScl = smrObj.get("logo_scale").getAsString();
			this.smrLogoSize = smrObj.get("logoiiconsize").getAsString();
			this.smrCrclFill = smrObj.get("summary_circle_fill").getAsString();
			this.ltryWinCol = smrObj.get("lottery_winner_color").getAsString();
			this.ltryCrclFill = smrObj.get("lottery_circle_fill").getAsString();
		}
		
		JsonObject winObj =  obj.getAsJsonObject("grandwinner");
		if(winObj!=null){
			this.winFntCol = winObj.get("grand_test_color").getAsString();
			this.fnlstFntSize = winObj.get("finalist_text_fontsize").getAsString();
			this.winFntSize = winObj.get("grandwinner_font_size").getAsString();
			this.winFntWght = winObj.get("grandwinner_font_weight").getAsString();
			this.winBoxCol = winObj.get("grand_winner_box").getAsString();
			this.winCol = winObj.get("grandPricewinner").getAsString();
			this.winPrizeFntCol = winObj.get("grandPricewinner_text").getAsString();

		}
		
		JsonObject imgObj =  obj.getAsJsonObject("images");
		if(imgObj!=null){
			this.sumLogoImg = imgObj.get("summary_logo").getAsString();
			this.videoPostrImg = imgObj.get("video_poster").getAsString();
			this.logoColImg = imgObj.get("logo_colored").getAsString();
			this.logoCrclImg = imgObj.get("logo_circle").getAsString();
			this.logoWhtImg = imgObj.get("logo_white").getAsString();
			this.dashbrdImg = imgObj.get("dashboardImg").getAsString();
			this.nameTagImg = imgObj.get("name_tag").getAsString();
			this.cartImg = imgObj.get("cart").getAsString();
			this.userImg = imgObj.get("user").getAsString();
			this.cartColImg = imgObj.get("cart_colored").getAsString();
			this.userColImg = imgObj.get("user_colored").getAsString();
		}
		
	}

	/**
	 * Gets the bscBgCol.
	 *
	 * @return the bscBgCol.
	 */
	public String getBscBgCol() {
		return bscBgCol;
	}

	/**
	 * Sets the bscBgCol.
	 *
	 * @param the
	 *            bscBgCol.
	 */
	public void setBscBgCol(String bscBgCol) {
		this.bscBgCol = bscBgCol;
	}

	/**
	 * Gets the bscFntFmly.
	 *
	 * @return the bscFntFmly.
	 */
	public String getBscFntFmly() {
		return bscFntFmly;
	}

	/**
	 * Sets the bscFntFmly.
	 *
	 * @param the
	 *            bscFntFmly.
	 */
	public void setBscFntFmly(String bscFntFmly) {
		this.bscFntFmly = bscFntFmly;
	}

	/**
	 * Gets the bscFntName.
	 *
	 * @return the bscFntName.
	 */
	public String getBscFntName() {
		return bscFntName;
	}

	/**
	 * Sets the bscFntName.
	 *
	 * @param the
	 *            bscFntName.
	 */
	public void setBscFntName(String bscFntName) {
		this.bscFntName = bscFntName;
	}

	/**
	 * Gets the bscDotColor.
	 *
	 * @return the bscDotColor.
	 */
	public String getBscDotColor() {
		return bscDotColor;
	}

	/**
	 * Sets the bscDotColor.
	 *
	 * @param the
	 *            bscDotColor.
	 */
	public void setBscDotColor(String bscDotColor) {
		this.bscDotColor = bscDotColor;
	}

	/**
	 * Gets the mdlBtnCol.
	 *
	 * @return the mdlBtnCol.
	 */
	public String getMdlBtnCol() {
		return mdlBtnCol;
	}

	/**
	 * Sets the mdlBtnCol.
	 *
	 * @param the
	 *            mdlBtnCol.
	 */
	public void setMdlBtnCol(String mdlBtnCol) {
		this.mdlBtnCol = mdlBtnCol;
	}

	/**
	 * Gets the mdlBtnTxtCol.
	 *
	 * @return the mdlBtnTxtCol.
	 */
	public String getMdlBtnTxtCol() {
		return mdlBtnTxtCol;
	}

	/**
	 * Sets the mdlBtnTxtCol.
	 *
	 * @param the
	 *            mdlBtnTxtCol.
	 */
	public void setMdlBtnTxtCol(String mdlBtnTxtCol) {
		this.mdlBtnTxtCol = mdlBtnTxtCol;
	}

	/**
	 * Gets the qsnStrkFnshCol.
	 *
	 * @return the qsnStrkFnshCol.
	 */
	public String getQsnStrkFnshCol() {
		return qsnStrkFnshCol;
	}

	/**
	 * Sets the qsnStrkFnshCol.
	 *
	 * @param the
	 *            qsnStrkFnshCol.
	 */
	public void setQsnStrkFnshCol(String qsnStrkFnshCol) {
		this.qsnStrkFnshCol = qsnStrkFnshCol;
	}

	/**
	 * Gets the qsnStrkStrtngCol.
	 *
	 * @return the qsnStrkStrtngCol.
	 */
	public String getQsnStrkStrtngCol() {
		return qsnStrkStrtngCol;
	}

	/**
	 * Sets the qsnStrkStrtngCol.
	 *
	 * @param the
	 *            qsnStrkStrtngCol.
	 */
	public void setQsnStrkStrtngCol(String qsnStrkStrtngCol) {
		this.qsnStrkStrtngCol = qsnStrkStrtngCol;
	}

	/**
	 * Gets the qsnBrdrCol.
	 *
	 * @return the qsnBrdrCol.
	 */
	public String getQsnBrdrCol() {
		return qsnBrdrCol;
	}

	/**
	 * Sets the qsnBrdrCol.
	 *
	 * @param the
	 *            qsnBrdrCol.
	 */
	public void setQsnBrdrCol(String qsnBrdrCol) {
		this.qsnBrdrCol = qsnBrdrCol;
	}

	/**
	 * Gets the qsnAnsCol.
	 *
	 * @return the qsnAnsCol.
	 */
	public String getQsnAnsCol() {
		return qsnAnsCol;
	}

	/**
	 * Sets the qsnAnsCol.
	 *
	 * @param the
	 *            qsnAnsCol.
	 */
	public void setQsnAnsCol(String qsnAnsCol) {
		this.qsnAnsCol = qsnAnsCol;
	}

	/**
	 * Gets the qsnRghtAnsCol.
	 *
	 * @return the qsnRghtAnsCol.
	 */
	public String getQsnRghtAnsCol() {
		return qsnRghtAnsCol;
	}

	/**
	 * Sets the qsnRghtAnsCol.
	 *
	 * @param the
	 *            qsnRghtAnsCol.
	 */
	public void setQsnRghtAnsCol(String qsnRghtAnsCol) {
		this.qsnRghtAnsCol = qsnRghtAnsCol;
	}

	/**
	 * Gets the qsnWrngAnsCol.
	 *
	 * @return the qsnWrngAnsCol.
	 */
	public String getQsnWrngAnsCol() {
		return qsnWrngAnsCol;
	}

	/**
	 * Sets the qsnWrngAnsCol.
	 *
	 * @param the
	 *            qsnWrngAnsCol.
	 */
	public void setQsnWrngAnsCol(String qsnWrngAnsCol) {
		this.qsnWrngAnsCol = qsnWrngAnsCol;
	}

	/**
	 * Gets the qsnSelAnsTxtCol.
	 *
	 * @return the qsnSelAnsTxtCol.
	 */
	public String getQsnSelAnsTxtCol() {
		return qsnSelAnsTxtCol;
	}

	/**
	 * Sets the qsnSelAnsTxtCol.
	 *
	 * @param the
	 *            qsnSelAnsTxtCol.
	 */
	public void setQsnSelAnsTxtCol(String qsnSelAnsTxtCol) {
		this.qsnSelAnsTxtCol = qsnSelAnsTxtCol;
	}

	/**
	 * Gets the qsnRghtAnsTxtCol.
	 *
	 * @return the qsnRghtAnsTxtCol.
	 */
	public String getQsnRghtAnsTxtCol() {
		return qsnRghtAnsTxtCol;
	}

	/**
	 * Sets the qsnRghtAnsTxtCol.
	 *
	 * @param the
	 *            qsnRghtAnsTxtCol.
	 */
	public void setQsnRghtAnsTxtCol(String qsnRghtAnsTxtCol) {
		this.qsnRghtAnsTxtCol = qsnRghtAnsTxtCol;
	}

	/**
	 * Gets the qsnWrngAnstxtCol.
	 *
	 * @return the qsnWrngAnstxtCol.
	 */
	public String getQsnWrngAnstxtCol() {
		return qsnWrngAnstxtCol;
	}

	/**
	 * Sets the qsnWrngAnstxtCol.
	 *
	 * @param the
	 *            qsnWrngAnstxtCol.
	 */
	public void setQsnWrngAnstxtCol(String qsnWrngAnstxtCol) {
		this.qsnWrngAnstxtCol = qsnWrngAnstxtCol;
	}

	/**
	 * Gets the hdrCartCol.
	 *
	 * @return the hdrCartCol.
	 */
	public String getHdrCartCol() {
		return hdrCartCol;
	}

	/**
	 * Sets the hdrCartCol.
	 *
	 * @param the
	 *            hdrCartCol.
	 */
	public void setHdrCartCol(String hdrCartCol) {
		this.hdrCartCol = hdrCartCol;
	}

	/**
	 * Gets the hdrUsrCntCol.
	 *
	 * @return the hdrUsrCntCol.
	 */
	public String getHdrUsrCntCol() {
		return hdrUsrCntCol;
	}

	/**
	 * Sets the hdrUsrCntCol.
	 *
	 * @param the
	 *            hdrUsrCntCol.
	 */
	public void setHdrUsrCntCol(String hdrUsrCntCol) {
		this.hdrUsrCntCol = hdrUsrCntCol;
	}

	/**
	 * Gets the hdrLivIconHght.
	 *
	 * @return the hdrLivIconHght.
	 */
	public String getHdrLivIconHght() {
		return hdrLivIconHght;
	}

	/**
	 * Sets the hdrLivIconHght.
	 *
	 * @param the
	 *            hdrLivIconHght.
	 */
	public void setHdrLivIconHght(String hdrLivIconHght) {
		this.hdrLivIconHght = hdrLivIconHght;
	}

	/**
	 * Gets the smrFntCol.
	 *
	 * @return the smrFntCol.
	 */
	public String getSmrFntCol() {
		return smrFntCol;
	}

	/**
	 * Sets the smrFntCol.
	 *
	 * @param the
	 *            smrFntCol.
	 */
	public void setSmrFntCol(String smrFntCol) {
		this.smrFntCol = smrFntCol;
	}

	/**
	 * Gets the smrCrclCol.
	 *
	 * @return the smrCrclCol.
	 */
	public String getSmrCrclCol() {
		return smrCrclCol;
	}

	/**
	 * Sets the smrCrclCol.
	 *
	 * @param the
	 *            smrCrclCol.
	 */
	public void setSmrCrclCol(String smrCrclCol) {
		this.smrCrclCol = smrCrclCol;
	}

	/**
	 * Gets the smrBoxCol.
	 *
	 * @return the smrBoxCol.
	 */
	public String getSmrBoxCol() {
		return smrBoxCol;
	}

	/**
	 * Sets the smrBoxCol.
	 *
	 * @param the
	 *            smrBoxCol.
	 */
	public void setSmrBoxCol(String smrBoxCol) {
		this.smrBoxCol = smrBoxCol;
	}

	/**
	 * Gets the smrFntSize.
	 *
	 * @return the smrFntSize.
	 */
	public String getSmrFntSize() {
		return smrFntSize;
	}

	/**
	 * Sets the smrFntSize.
	 *
	 * @param the
	 *            smrFntSize.
	 */
	public void setSmrFntSize(String smrFntSize) {
		this.smrFntSize = smrFntSize;
	}

	/**
	 * Gets the smrFntWght.
	 *
	 * @return the smrFntWght.
	 */
	public String getSmrFntWght() {
		return smrFntWght;
	}

	/**
	 * Sets the smrFntWght.
	 *
	 * @param the
	 *            smrFntWght.
	 */
	public void setSmrFntWght(String smrFntWght) {
		this.smrFntWght = smrFntWght;
	}

	/**
	 * Gets the smrPrzFntSize.
	 *
	 * @return the smrPrzFntSize.
	 */
	public String getSmrPrzFntSize() {
		return smrPrzFntSize;
	}

	/**
	 * Sets the smrLogoX.
	 *
	 * @param the
	 *            smrLogoX.
	 */
	public void setSmrPrzFntSize(String smrPrzFntSize) {
		this.smrPrzFntSize = smrPrzFntSize;
	}

	public String getSmrLogoX() {
		return smrLogoX;
	}

	/**
	 * Sets the smrLogoX.
	 *
	 * @param the
	 *            smrLogoX.
	 */
	public void setSmrLogoX(String smrLogoX) {
		this.smrLogoX = smrLogoX;
	}

	/**
	 * Gets the smrLogoY.
	 *
	 * @return the smrLogoY.
	 */
	public String getSmrLogoY() {
		return smrLogoY;
	}

	/**
	 * Sets the smrLogoY.
	 *
	 * @param the
	 *            smrLogoY.
	 */
	public void setSmrLogoY(String smrLogoY) {
		this.smrLogoY = smrLogoY;
	}

	/**
	 * Gets the smrLogoScl.
	 *
	 * @return the smrLogoScl.
	 */
	public String getSmrLogoScl() {
		return smrLogoScl;
	}

	/**
	 * Sets the smrLogoScl.
	 *
	 * @param the
	 *            smrLogoScl.
	 */
	public void setSmrLogoScl(String smrLogoScl) {
		this.smrLogoScl = smrLogoScl;
	}

	/**
	 * Gets the smrLogoSize.
	 *
	 * @return the smrLogoSize.
	 */
	public String getSmrLogoSize() {
		return smrLogoSize;
	}

	/**
	 * Sets the smrLogoSize.
	 *
	 * @param the
	 *            smrLogoSize.
	 */
	public void setSmrLogoSize(String smrLogoSize) {
		this.smrLogoSize = smrLogoSize;
	}

	/**
	 * Gets the smrCrclFill.
	 *
	 * @return the smrCrclFill.
	 */
	public String getSmrCrclFill() {
		return smrCrclFill;
	}

	/**
	 * Sets the smrCrclFill.
	 *
	 * @param the
	 *            smrCrclFill.
	 */
	public void setSmrCrclFill(String smrCrclFill) {
		this.smrCrclFill = smrCrclFill;
	}

	/**
	 * Gets the ltryWinCol.
	 *
	 * @return the ltryWinCol.
	 */
	public String getLtryWinCol() {
		return ltryWinCol;
	}

	/**
	 * Sets the ltryWinCol.
	 *
	 * @param the
	 *            ltryWinCol.
	 */
	public void setLtryWinCol(String ltryWinCol) {
		this.ltryWinCol = ltryWinCol;
	}

	/**
	 * Gets the ltryCrclFill.
	 *
	 * @return the ltryCrclFill.
	 */
	public String getLtryCrclFill() {
		return ltryCrclFill;
	}

	/**
	 * Sets the ltryCrclFill.
	 *
	 * @param the
	 *            ltryCrclFill.
	 */
	public void setLtryCrclFill(String ltryCrclFill) {
		this.ltryCrclFill = ltryCrclFill;
	}

	/**
	 * Gets the winFntCol.
	 *
	 * @return the winFntCol.
	 */
	public String getWinFntCol() {
		return winFntCol;
	}

	/**
	 * Sets the winFntCol.
	 *
	 * @param the
	 *            winFntCol.
	 */
	public void setWinFntCol(String winFntCol) {
		this.winFntCol = winFntCol;
	}

	/**
	 * Gets the fnlstFntSize.
	 *
	 * @return the fnlstFntSize.
	 */
	public String getFnlstFntSize() {
		return fnlstFntSize;
	}

	/**
	 * Sets the fnlstFntSize.
	 *
	 * @param the
	 *            fnlstFntSize.
	 */
	public void setFnlstFntSize(String fnlstFntSize) {
		this.fnlstFntSize = fnlstFntSize;
	}

	/**
	 * Gets the winFntSize.
	 *
	 * @return the winFntSize.
	 */
	public String getWinFntSize() {
		return winFntSize;
	}

	/**
	 * Sets the winFntSize.
	 *
	 * @param the
	 *            winFntSize.
	 */
	public void setWinFntSize(String winFntSize) {
		this.winFntSize = winFntSize;
	}

	/**
	 * Gets the winFntWght.
	 *
	 * @return the winFntWght.
	 */
	public String getWinFntWght() {
		return winFntWght;
	}

	/**
	 * Sets the winFntWght.
	 *
	 * @param the
	 *            winFntWght.
	 */
	public void setWinFntWght(String winFntWght) {
		this.winFntWght = winFntWght;
	}

	/**
	 * Gets the winBoxCol.
	 *
	 * @return the winBoxCol.
	 */
	public String getWinBoxCol() {
		return winBoxCol;
	}

	/**
	 * Sets the winBoxCol.
	 *
	 * @param the
	 *            winBoxCol.
	 */
	public void setWinBoxCol(String winBoxCol) {
		this.winBoxCol = winBoxCol;
	}

	/**
	 * Gets the winCol.
	 *
	 * @return the winCol.
	 */
	public String getWinCol() {
		return winCol;
	}

	/**
	 * Sets the winCol.
	 *
	 * @param the
	 *            winCol.
	 */
	public void setWinCol(String winCol) {
		this.winCol = winCol;
	}

	/**
	 * Gets the winPrizeFntCol.
	 *
	 * @return the winPrizeFntCol.
	 */
	public String getWinPrizeFntCol() {
		return winPrizeFntCol;
	}

	/**
	 * Sets the winPrizeFntCol.
	 *
	 * @param the
	 *            winPrizeFntCol.
	 */
	public void setWinPrizeFntCol(String winPrizeFntCol) {
		this.winPrizeFntCol = winPrizeFntCol;
	}

	/**
	 * Gets the sumLogoImg.
	 *
	 * @return the sumLogoImg.
	 */
	public String getSumLogoImg() {
		return sumLogoImg;
	}

	/**
	 * Sets the sumLogoImg.
	 *
	 * @param the
	 *            sumLogoImg.
	 */
	public void setSumLogoImg(String sumLogoImg) {
		this.sumLogoImg = sumLogoImg;
	}

	/**
	 * Gets the videoPostrImg.
	 *
	 * @return the videoPostrImg.
	 */
	public String getVideoPostrImg() {
		return videoPostrImg;
	}

	/**
	 * Sets the videoPostrImg.
	 *
	 * @param the
	 *            videoPostrImg.
	 */
	public void setVideoPostrImg(String videoPostrImg) {
		this.videoPostrImg = videoPostrImg;
	}

	/**
	 * Gets the logoColImg.
	 *
	 * @return the logoColImg.
	 */
	public String getLogoColImg() {
		return logoColImg;
	}

	/**
	 * Sets the logoColImg.
	 *
	 * @param the
	 *            logoColImg.
	 */
	public void setLogoColImg(String logoColImg) {
		this.logoColImg = logoColImg;
	}

	/**
	 * Gets the logoCrclImg.
	 *
	 * @return the logoCrclImg.
	 */
	public String getLogoCrclImg() {
		return logoCrclImg;
	}

	/**
	 * Sets the logoCrclImg.
	 *
	 * @param the
	 *            logoCrclImg.
	 */
	public void setLogoCrclImg(String logoCrclImg) {
		this.logoCrclImg = logoCrclImg;
	}

	/**
	 * Gets the logoWhtImg.
	 *
	 * @return the logoWhtImg.
	 */
	public String getLogoWhtImg() {
		return logoWhtImg;
	}

	/**
	 * Sets the logoWhtImg.
	 *
	 * @param the
	 *            logoWhtImg.
	 */
	public void setLogoWhtImg(String logoWhtImg) {
		this.logoWhtImg = logoWhtImg;
	}

	/**
	 * Gets the dashbrdImg.
	 *
	 * @return the dashbrdImg.
	 */
	public String getDashbrdImg() {
		return dashbrdImg;
	}

	/**
	 * Sets the dashbrdImg.
	 *
	 * @param the
	 *            dashbrdImg.
	 */
	public void setDashbrdImg(String dashbrdImg) {
		this.dashbrdImg = dashbrdImg;
	}

	/**
	 * Gets the nameTagImg.
	 *
	 * @return the nameTagImg.
	 */
	public String getNameTagImg() {
		return nameTagImg;
	}

	/**
	 * Sets the nameTagImg.
	 *
	 * @param the
	 *            nameTagImg.
	 */
	public void setNameTagImg(String nameTagImg) {
		this.nameTagImg = nameTagImg;
	}

	/**
	 * Gets the bscBgCol.
	 *
	 * @return the bscBgCol.
	 */
	public String getCartImg() {
		return cartImg;
	}

	/**
	 * Sets the cartImg.
	 *
	 * @param the
	 *            cartImg.
	 */
	public void setCartImg(String cartImg) {
		this.cartImg = cartImg;
	}

	/**
	 * Gets the userImg.
	 *
	 * @return the userImg.
	 */
	public String getUserImg() {
		return userImg;
	}

	/**
	 * Sets the userImg.
	 *
	 * @param the
	 *            userImg.
	 */
	public void setUserImg(String userImg) {
		this.userImg = userImg;
	}

	/**
	 * Gets the cartColImg.
	 *
	 * @return the cartColImg
	 */
	public String getCartColImg() {
		return cartColImg;
	}

	/**
	 * Sets the cartColImg.
	 *
	 * @param the
	 *            cartColImg.
	 */
	public void setCartColImg(String cartColImg) {
		this.cartColImg = cartColImg;
	}

	/**
	 * Gets the userColImg.
	 *
	 * @return the userColImg
	 */
	public String getUserColImg() {
		return userColImg;
	}

	/**
	 * Sets the userColImg.
	 *
	 * @param the
	 *            userColImg.
	 */
	public void setUserColImg(String userColImg) {
		this.userColImg = userColImg;
	}

}
