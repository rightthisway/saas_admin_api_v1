/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client;

import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ClientStreamDTO.
 */
public class ClientStreamDTO extends RtfSaasBaseDTO {
	
	/** The ClientStreamDVO. */
	private ClientStreamDVO dvo;

	/**
	 * Gets the ClientStreamDVO.
	 *
	 * @return ClientStreamDVO the dvo
	 */
	public ClientStreamDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the ClientStreamDVO dvo.
	 *
	 * @param ClientStreamDVO dvo the new dvo
	 */
	public void setDvo(ClientStreamDVO dvo) {
		this.dvo = dvo;
	}

}
