/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.HashOfProtocols;
import java.io.IOException;

/**
 * The Class HashOfTotals.
 */
@ApiModel(description = "A hash of total usage, including overall total billed and used for all targets, and totals by zone for each protocol.")
public class HashOfTotals {
  
  /** The bytes billed. */
  @SerializedName("bytes_billed")
  private Integer bytesBilled = null;

  /** The bytes used. */
  @SerializedName("bytes_used")
  private Integer bytesUsed = null;

  /** The protocols. */
  @SerializedName("protocols")
  private HashOfProtocols protocols = null;

  /**
   * Bytes billed.
   *
   * @param bytesBilled the bytes billed
   * @return the hash of totals
   */
  public HashOfTotals bytesBilled(Integer bytesBilled) {
    this.bytesBilled = bytesBilled;
    return this;
  }

   /**
    * Gets the bytes billed.
    *
    * @return the bytes billed
    */
  @ApiModelProperty(example = "", value = "The amount of usage, in bytes, that was billed for all stream targets during the selected time frame.")
  public Integer getBytesBilled() {
    return bytesBilled;
  }

  /**
   * Sets the bytes billed.
   *
   * @param bytesBilled the new bytes billed
   */
  public void setBytesBilled(Integer bytesBilled) {
    this.bytesBilled = bytesBilled;
  }

  /**
   * Bytes used.
   *
   * @param bytesUsed the bytes used
   * @return the hash of totals
   */
  public HashOfTotals bytesUsed(Integer bytesUsed) {
    this.bytesUsed = bytesUsed;
    return this;
  }

   /**
    * Gets the bytes used.
    *
    * @return the bytes used
    */
  @ApiModelProperty(example = "", value = "The amount of content, in bytes, that went through all stream targets during the selected time frame.")
  public Integer getBytesUsed() {
    return bytesUsed;
  }

  /**
   * Sets the bytes used.
   *
   * @param bytesUsed the new bytes used
   */
  public void setBytesUsed(Integer bytesUsed) {
    this.bytesUsed = bytesUsed;
  }

  /**
   * Protocols.
   *
   * @param protocols the protocols
   * @return the hash of totals
   */
  public HashOfTotals protocols(HashOfProtocols protocols) {
    this.protocols = protocols;
    return this;
  }

   /**
    * Gets the protocols.
    *
    * @return the protocols
    */
  @ApiModelProperty(value = "")
  public HashOfProtocols getProtocols() {
    return protocols;
  }

  /**
   * Sets the protocols.
   *
   * @param protocols the new protocols
   */
  public void setProtocols(HashOfProtocols protocols) {
    this.protocols = protocols;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HashOfTotals hashOfTotals = (HashOfTotals) o;
    return Objects.equals(this.bytesBilled, hashOfTotals.bytesBilled) &&
        Objects.equals(this.bytesUsed, hashOfTotals.bytesUsed) &&
        Objects.equals(this.protocols, hashOfTotals.protocols);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(bytesBilled, bytesUsed, protocols);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HashOfTotals {\n");
    
    sb.append("    bytesBilled: ").append(toIndentedString(bytesBilled)).append("\n");
    sb.append("    bytesUsed: ").append(toIndentedString(bytesUsed)).append("\n");
    sb.append("    protocols: ").append(toIndentedString(protocols)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

