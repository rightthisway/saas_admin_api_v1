/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Limits;
import com.rtf.wowza.cloudsdk.client.model.ShmMetrics;
import java.io.IOException;

/**
 * The Class LimitsResponse.
 */
public class LimitsResponse {
  
  /** The current. */
  @SerializedName("current")
  private ShmMetrics current = null;

  /** The limits. */
  @SerializedName("limits")
  private Limits limits = null;

  /**
   * Current.
   *
   * @param current the current
   * @return the limits response
   */
  public LimitsResponse current(ShmMetrics current) {
    this.current = current;
    return this;
  }

   /**
    * Gets the current.
    *
    * @return the current
    */
  @ApiModelProperty(required = true, value = "")
  public ShmMetrics getCurrent() {
    return current;
  }

  /**
   * Sets the current.
   *
   * @param current the new current
   */
  public void setCurrent(ShmMetrics current) {
    this.current = current;
  }

  /**
   * Limits.
   *
   * @param limits the limits
   * @return the limits response
   */
  public LimitsResponse limits(Limits limits) {
    this.limits = limits;
    return this;
  }

   /**
    * Gets the limits.
    *
    * @return the limits
    */
  @ApiModelProperty(required = true, value = "")
  public Limits getLimits() {
    return limits;
  }

  /**
   * Sets the limits.
   *
   * @param limits the new limits
   */
  public void setLimits(Limits limits) {
    this.limits = limits;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LimitsResponse inlineResponse20043 = (LimitsResponse) o;
    return Objects.equals(this.current, inlineResponse20043.current) &&
        Objects.equals(this.limits, inlineResponse20043.limits);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(current, limits);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LimitsResponse {\n");
    
    sb.append("    current: ").append(toIndentedString(current)).append("\n");
    sb.append("    limits: ").append(toIndentedString(limits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

