 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.LiveStreams;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.auth.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.api.LiveStreamsApi;

import java.io.File;
import java.util.*;

/**
 * The Class FetchALiveStreamMetrics.
 */
public class FetchALiveStreamMetrics {

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
    	String streamId = "mfg4nkl2";
    	fetchLiveStreamMetrics(streamId);
    }
    
    /**
     * Fetch live stream metrics.
     *
     * @param streamId the stream id
     * @return the shm metrics
     * @throws Exception the exception
     */
    public static ShmMetrics fetchLiveStreamMetrics(String streamId) throws Exception {
    	
        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: wsc-access-key
        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey(defaultClient.wscaccesskey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-access-key.setApiKeyPrefix("Token");

        // Configure API key authorization: wsc-api-key
        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey(defaultClient.wscapikey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-api-key.setApiKeyPrefix("Token");

	LiveStreamsApi apiInstance = new LiveStreamsApi();
	
	try {
	    ShmMetrics result = apiInstance.showLiveStreamStats(streamId);
	    System.out.println(result);
	    return result;
	} catch (ApiException e) {
	    System.err.println("Exception when calling LiveStreamsApi#showLiveStreamStats");
	    e.printStackTrace();
	    throw e;
	}
    }
}
