/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.bind.util.ISO8601Utils;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.JsonElement;
import io.gsonfire.GsonFireBuilder;
import io.gsonfire.TypeSelector;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import com.rtf.wowza.cloudsdk.client.model.*;
import okio.ByteString;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

/**
 * The Class JSON.
 */
public class JSON {
    
    /** The gson. */
    private Gson gson;
    
    /** The is lenient on json. */
    private boolean isLenientOnJson = false;
    
    /** The date type adapter. */
    private DateTypeAdapter dateTypeAdapter = new DateTypeAdapter();
    
    /** The sql date type adapter. */
    private SqlDateTypeAdapter sqlDateTypeAdapter = new SqlDateTypeAdapter();
    
    /** The offset date time type adapter. */
    private OffsetDateTimeTypeAdapter offsetDateTimeTypeAdapter = new OffsetDateTimeTypeAdapter();
    
    /** The local date type adapter. */
    private LocalDateTypeAdapter localDateTypeAdapter = new LocalDateTypeAdapter();
    
    /** The byte array adapter. */
    private ByteArrayAdapter byteArrayAdapter = new ByteArrayAdapter();

    /**
     * Creates the gson.
     *
     * @return the gson builder
     */
    public static GsonBuilder createGson() {
        GsonFireBuilder fireBuilder = new GsonFireBuilder()
        ;
        GsonBuilder builder = fireBuilder.createGsonBuilder();
        return builder;
    }

    /**
     * Gets the discriminator value.
     *
     * @param readElement the read element
     * @param discriminatorField the discriminator field
     * @return the discriminator value
     */
    private static String getDiscriminatorValue(JsonElement readElement, String discriminatorField) {
        JsonElement element = readElement.getAsJsonObject().get(discriminatorField);
        if(null == element) {
            throw new IllegalArgumentException("missing discriminator field: <" + discriminatorField + ">");
        }
        return element.getAsString();
    }

    /**
     * Gets the class by discriminator.
     *
     * @param classByDiscriminatorValue the class by discriminator value
     * @param discriminatorValue the discriminator value
     * @return the class by discriminator
     */
    private static Class getClassByDiscriminator(Map classByDiscriminatorValue, String discriminatorValue) {
        Class clazz = (Class) classByDiscriminatorValue.get(discriminatorValue.toUpperCase());
        if(null == clazz) {
            throw new IllegalArgumentException("cannot determine model class of name: <" + discriminatorValue + ">");
        }
        return clazz;
    }

    /**
     * Instantiates a new json.
     */
    public JSON() {
        gson = createGson()
            .registerTypeAdapter(Date.class, dateTypeAdapter)
            .registerTypeAdapter(java.sql.Date.class, sqlDateTypeAdapter)
            .registerTypeAdapter(OffsetDateTime.class, offsetDateTimeTypeAdapter)
            .registerTypeAdapter(LocalDate.class, localDateTypeAdapter)
            .registerTypeAdapter(byte[].class, byteArrayAdapter)
            .create();
    }

    /**
     * Gets the gson.
     *
     * @return the gson
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * Sets the gson.
     *
     * @param gson the gson
     * @return the json
     */
    public JSON setGson(Gson gson) {
        this.gson = gson;
        return this;
    }

    /**
     * Sets the lenient on json.
     *
     * @param lenientOnJson the lenient on json
     * @return the json
     */
    public JSON setLenientOnJson(boolean lenientOnJson) {
        isLenientOnJson = lenientOnJson;
        return this;
    }

    /**
     * Serialize.
     *
     * @param obj the obj
     * @return the string
     */
    public String serialize(Object obj) {
        return gson.toJson(obj);
    }

    /**
     * Deserialize.
     *
     * @param <T> the generic type
     * @param body the body
     * @param returnType the return type
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public <T> T deserialize(String body, Type returnType) {
        try {
            if (isLenientOnJson) {
                JsonReader jsonReader = new JsonReader(new StringReader(body));
                // see https://google-gson.googlecode.com/svn/trunk/gson/docs/javadocs/com/google/gson/stream/JsonReader.html#setLenient(boolean)
                jsonReader.setLenient(true);
                return gson.fromJson(jsonReader, returnType);
            } else {
                return gson.fromJson(body, returnType);
            }
        } catch (JsonParseException e) {
            // Fallback processing when failed to parse JSON form response body:
            // return the response body string directly for the String return type;
            if (returnType.equals(String.class))
                return (T) body;
            else throw (e);
        }
    }

    /**
     * The Class ByteArrayAdapter.
     */
    public class ByteArrayAdapter extends TypeAdapter<byte[]> {

        /**
         * Write.
         *
         * @param out the out
         * @param value the value
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public void write(JsonWriter out, byte[] value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(ByteString.of(value).base64());
            }
        }

        /**
         * Read.
         *
         * @param in the in
         * @return the byte[]
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public byte[] read(JsonReader in) throws IOException {
            switch (in.peek()) {
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    String bytesAsBase64 = in.nextString();
                    ByteString byteString = ByteString.decodeBase64(bytesAsBase64);
                    return byteString.toByteArray();
            }
        }
    }

    /**
     * The Class OffsetDateTimeTypeAdapter.
     */
    public static class OffsetDateTimeTypeAdapter extends TypeAdapter<OffsetDateTime> {

        /** The formatter. */
        private DateTimeFormatter formatter;

        /**
         * Instantiates a new offset date time type adapter.
         */
        public OffsetDateTimeTypeAdapter() {
            this(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }

        /**
         * Instantiates a new offset date time type adapter.
         *
         * @param formatter the formatter
         */
        public OffsetDateTimeTypeAdapter(DateTimeFormatter formatter) {
            this.formatter = formatter;
        }

        /**
         * Sets the format.
         *
         * @param dateFormat the new format
         */
        public void setFormat(DateTimeFormatter dateFormat) {
            this.formatter = dateFormat;
        }

        /**
         * Write.
         *
         * @param out the out
         * @param date the date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public void write(JsonWriter out, OffsetDateTime date) throws IOException {
            if (date == null) {
                out.nullValue();
            } else {
                out.value(formatter.format(date));
            }
        }

        /**
         * Read.
         *
         * @param in the in
         * @return the offset date time
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public OffsetDateTime read(JsonReader in) throws IOException {
            switch (in.peek()) {
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    String date = in.nextString();
                    if (date.endsWith("+0000")) {
                        date = date.substring(0, date.length()-5) + "Z";
                    }
                    return OffsetDateTime.parse(date, formatter);
            }
        }
    }

    /**
     * The Class LocalDateTypeAdapter.
     */
    public class LocalDateTypeAdapter extends TypeAdapter<LocalDate> {

        /** The formatter. */
        private DateTimeFormatter formatter;

        /**
         * Instantiates a new local date type adapter.
         */
        public LocalDateTypeAdapter() {
            this(DateTimeFormatter.ISO_LOCAL_DATE);
        }

        /**
         * Instantiates a new local date type adapter.
         *
         * @param formatter the formatter
         */
        public LocalDateTypeAdapter(DateTimeFormatter formatter) {
            this.formatter = formatter;
        }

        /**
         * Sets the format.
         *
         * @param dateFormat the new format
         */
        public void setFormat(DateTimeFormatter dateFormat) {
            this.formatter = dateFormat;
        }

        /**
         * Write.
         *
         * @param out the out
         * @param date the date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public void write(JsonWriter out, LocalDate date) throws IOException {
            if (date == null) {
                out.nullValue();
            } else {
                out.value(formatter.format(date));
            }
        }

        /**
         * Read.
         *
         * @param in the in
         * @return the local date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public LocalDate read(JsonReader in) throws IOException {
            switch (in.peek()) {
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    String date = in.nextString();
                    return LocalDate.parse(date, formatter);
            }
        }
    }

    /**
     * Sets the offset date time format.
     *
     * @param dateFormat the date format
     * @return the json
     */
    public JSON setOffsetDateTimeFormat(DateTimeFormatter dateFormat) {
        offsetDateTimeTypeAdapter.setFormat(dateFormat);
        return this;
    }

    /**
     * Sets the local date format.
     *
     * @param dateFormat the date format
     * @return the json
     */
    public JSON setLocalDateFormat(DateTimeFormatter dateFormat) {
        localDateTypeAdapter.setFormat(dateFormat);
        return this;
    }

    /**
     * The Class SqlDateTypeAdapter.
     */
    public static class SqlDateTypeAdapter extends TypeAdapter<java.sql.Date> {

        /** The date format. */
        private DateFormat dateFormat;

        /**
         * Instantiates a new sql date type adapter.
         */
        public SqlDateTypeAdapter() {
        }

        /**
         * Instantiates a new sql date type adapter.
         *
         * @param dateFormat the date format
         */
        public SqlDateTypeAdapter(DateFormat dateFormat) {
            this.dateFormat = dateFormat;
        }

        /**
         * Sets the format.
         *
         * @param dateFormat the new format
         */
        public void setFormat(DateFormat dateFormat) {
            this.dateFormat = dateFormat;
        }

        /**
         * Write.
         *
         * @param out the out
         * @param date the date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public void write(JsonWriter out, java.sql.Date date) throws IOException {
            if (date == null) {
                out.nullValue();
            } else {
                String value;
                if (dateFormat != null) {
                    value = dateFormat.format(date);
                } else {
                    value = date.toString();
                }
                out.value(value);
            }
        }

        /**
         * Read.
         *
         * @param in the in
         * @return the java.sql. date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public java.sql.Date read(JsonReader in) throws IOException {
            switch (in.peek()) {
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    String date = in.nextString();
                    try {
                        if (dateFormat != null) {
                            return new java.sql.Date(dateFormat.parse(date).getTime());
                        }
                        return new java.sql.Date(ISO8601Utils.parse(date, new ParsePosition(0)).getTime());
                    } catch (ParseException e) {
                        throw new JsonParseException(e);
                    }
            }
        }
    }

    /**
     * The Class DateTypeAdapter.
     */
    public static class DateTypeAdapter extends TypeAdapter<Date> {

        /** The date format. */
        private DateFormat dateFormat;

        /**
         * Instantiates a new date type adapter.
         */
        public DateTypeAdapter() {
        }

        /**
         * Instantiates a new date type adapter.
         *
         * @param dateFormat the date format
         */
        public DateTypeAdapter(DateFormat dateFormat) {
            this.dateFormat = dateFormat;
        }

        /**
         * Sets the format.
         *
         * @param dateFormat the new format
         */
        public void setFormat(DateFormat dateFormat) {
            this.dateFormat = dateFormat;
        }

        /**
         * Write.
         *
         * @param out the out
         * @param date the date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public void write(JsonWriter out, Date date) throws IOException {
            if (date == null) {
                out.nullValue();
            } else {
                String value;
                if (dateFormat != null) {
                    value = dateFormat.format(date);
                } else {
                    value = ISO8601Utils.format(date, true);
                }
                out.value(value);
            }
        }

        /**
         * Read.
         *
         * @param in the in
         * @return the date
         * @throws IOException Signals that an I/O exception has occurred.
         */
        @Override
        public Date read(JsonReader in) throws IOException {
            try {
                switch (in.peek()) {
                    case NULL:
                        in.nextNull();
                        return null;
                    default:
                        String date = in.nextString();
                        try {
                            if (dateFormat != null) {
                                return dateFormat.parse(date);
                            }
                            return ISO8601Utils.parse(date, new ParsePosition(0));
                        } catch (ParseException e) {
                            throw new JsonParseException(e);
                        }
                }
            } catch (IllegalArgumentException e) {
                throw new JsonParseException(e);
            }
        }
    }

    /**
     * Sets the date format.
     *
     * @param dateFormat the date format
     * @return the json
     */
    public JSON setDateFormat(DateFormat dateFormat) {
        dateTypeAdapter.setFormat(dateFormat);
        return this;
    }

    /**
     * Sets the sql date format.
     *
     * @param dateFormat the date format
     * @return the json
     */
    public JSON setSqlDateFormat(DateFormat dateFormat) {
        sqlDateTypeAdapter.setFormat(dateFormat);
        return this;
    }

}
