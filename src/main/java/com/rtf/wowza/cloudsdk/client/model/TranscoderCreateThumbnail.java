/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamThumbnail;
import java.io.IOException;

/**
 * The Class TranscoderCreateThumbnail.
 */
public class TranscoderCreateThumbnail {
  
  /** The transcoder. */
  @SerializedName("transcoder")
  private LiveStreamThumbnail transcoder = null;

  /**
   * Transcoder.
   *
   * @param transcoder the transcoder
   * @return the transcoder create thumbnail
   */
  public TranscoderCreateThumbnail transcoder(LiveStreamThumbnail transcoder) {
    this.transcoder = transcoder;
    return this;
  }

   /**
    * Gets the transcoder thumbnail url.
    *
    * @return the transcoder thumbnail url
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStreamThumbnail getTranscoderThumbnailUrl() {
    return transcoder;
  }

  /**
   * Sets the transcoder thumbnail url.
   *
   * @param transcoder the new transcoder thumbnail url
   */
  public void setTranscoderThumbnailUrl(LiveStreamThumbnail transcoder) {
    this.transcoder = transcoder;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderCreateThumbnail inlineResponse20039 = (TranscoderCreateThumbnail) o;
    return Objects.equals(this.transcoder, inlineResponse20039.transcoder);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(transcoder);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderCreateThumbnail {\n");
    
    sb.append("    transcoder: ").append(toIndentedString(transcoder)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

