 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.LiveStreams;

import com.rtf.wowza.cloudsdk.client.ApiClient;
import com.rtf.wowza.cloudsdk.client.ApiException;
import com.rtf.wowza.cloudsdk.client.Configuration;
import com.rtf.wowza.cloudsdk.client.api.LiveStreamsApi;
import com.rtf.wowza.cloudsdk.client.auth.ApiKeyAuth;
import com.rtf.wowza.cloudsdk.client.model.LiveStream;

/**
 * The Class FetchALiveStream.
 */
public class FetchALiveStream {

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
    	String streamId = "pnbgjv1f";
    	
    	fetchLiveStream(streamId);
    }
    
    /**
     * Fetch live stream.
     *
     * @param streamId the stream id
     * @return the live stream
     * @throws Exception the exception
     */
    public static LiveStream fetchLiveStream(String streamId) throws Exception {
    
        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: wsc-access-key
        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey(defaultClient.wscaccesskey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-access-key.setApiKeyPrefix("Token");

        // Configure API key authorization: wsc-api-key
        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey(defaultClient.wscapikey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-api-key.setApiKeyPrefix("Token");

	LiveStreamsApi apiInstance = new LiveStreamsApi();
	//String streamId = "vrlp5bzj";
	try {
	    LiveStream result = apiInstance.showLiveStream(streamId);
	    System.out.println(result);
	    return result;
	} catch (ApiException e) {
	    System.err.println("Exception when calling LiveStreamsApi#listLiveStreams");
	    e.printStackTrace();
	}
	return null;
    }
}
