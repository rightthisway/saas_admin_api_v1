/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.ScheduleState;
import java.io.IOException;

/**
 * The Class ScheduleCreateState.
 */
public class ScheduleCreateState {
  
  /** The schedule. */
  @SerializedName("schedule")
  private ScheduleState schedule = null;

  /**
   * Schedule.
   *
   * @param schedule the schedule
   * @return the schedule create state
   */
  public ScheduleCreateState schedule(ScheduleState schedule) {
    this.schedule = schedule;
    return this;
  }

   /**
    * Gets the schedule state.
    *
    * @return the schedule state
    */
  @ApiModelProperty(required = true, value = "")
  public ScheduleState getScheduleState() {
    return schedule;
  }

  /**
   * Sets the schedule state.
   *
   * @param schedule the new schedule state
   */
  public void setScheduleState(ScheduleState schedule) {
    this.schedule = schedule;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduleCreateState inlineResponse20015 = (ScheduleCreateState) o;
    return Objects.equals(this.schedule, inlineResponse20015.schedule);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(schedule);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduleCreateState {\n");
    
    sb.append("    schedule: ").append(toIndentedString(schedule)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

