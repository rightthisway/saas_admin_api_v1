/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Output;
import java.io.IOException;

/**
 * The Class OutputInput.
 */
@ApiModel(description = "")
public class OutputInput {
  
  /** The output. */
  @SerializedName("output")
  private Output output = null;

  /**
   * Output.
   *
   * @param output the output
   * @return the output input
   */
  public OutputInput output(Output output) {
    this.output = output;
    return this;
  }

   /**
    * Gets the output.
    *
    * @return the output
    */
  @ApiModelProperty(required = true, value = "")
  public Output getOutput() {
    return output;
  }

  /**
   * Sets the output.
   *
   * @param output the new output
   */
  public void setOutput(Output output) {
    this.output = output;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutputInput outputInput = (OutputInput) o;
    return Objects.equals(this.output, outputInput.output);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(output);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OutputInput {\n");
    
    sb.append("    output: ").append(toIndentedString(output)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

