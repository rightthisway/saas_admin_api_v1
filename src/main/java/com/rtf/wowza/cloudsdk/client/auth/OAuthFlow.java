/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.auth;

/**
 * The Enum OAuthFlow.
 */
public enum OAuthFlow {
    
    /** The access code. */
    accessCode, 
 /** The implicit. */
 implicit, 
 /** The password. */
 password, 
 /** The application. */
 application
}
