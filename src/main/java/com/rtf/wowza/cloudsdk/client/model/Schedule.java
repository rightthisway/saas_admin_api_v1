/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;

/**
 * The Class Schedule.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class Schedule {
  
  /**
   * The Enum ActionTypeEnum.
   */
  @JsonAdapter(ActionTypeEnum.Adapter.class)
  public enum ActionTypeEnum {
    
    /** The start. */
    START("start"),
    
    /** The stop. */
    STOP("stop"),
    
    /** The start stop. */
    START_STOP("start_stop");

    /** The value. */
    private String value;

    /**
     * Instantiates a new action type enum.
     *
     * @param value the value
     */
    ActionTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the action type enum
     */
    public static ActionTypeEnum fromValue(String text) {
      for (ActionTypeEnum b : ActionTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<ActionTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final ActionTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the action type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public ActionTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ActionTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The action type. */
  @SerializedName("action_type")
  private ActionTypeEnum actionType = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The end repeat. */
  @SerializedName("end_repeat")
  private LocalDate endRepeat = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /**
   * The Enum RecurrenceDataEnum.
   */
  @JsonAdapter(RecurrenceDataEnum.Adapter.class)
  public enum RecurrenceDataEnum {
    
    /** The sunday. */
    SUNDAY("sunday"),
    
    /** The monday. */
    MONDAY("monday"),
    
    /** The tuesday. */
    TUESDAY("tuesday"),
    
    /** The wednesday. */
    WEDNESDAY("wednesday"),
    
    /** The thursday. */
    THURSDAY("thursday"),
    
    /** The friday. */
    FRIDAY("friday"),
    
    /** The saturday. */
    SATURDAY("saturday");

    /** The value. */
    private String value;

    /**
     * Instantiates a new recurrence data enum.
     *
     * @param value the value
     */
    RecurrenceDataEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the recurrence data enum
     */
    public static RecurrenceDataEnum fromValue(String text) {
      for (RecurrenceDataEnum b : RecurrenceDataEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<RecurrenceDataEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final RecurrenceDataEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the recurrence data enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public RecurrenceDataEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return RecurrenceDataEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The recurrence data. */
  @SerializedName("recurrence_data")
  private String recurrenceData = null;

  /**
   * The Enum RecurrenceTypeEnum.
   */
  @JsonAdapter(RecurrenceTypeEnum.Adapter.class)
  public enum RecurrenceTypeEnum {
    
    /** The once. */
    ONCE("once"),
    
    /** The recur. */
    RECUR("recur");

    /** The value. */
    private String value;

    /**
     * Instantiates a new recurrence type enum.
     *
     * @param value the value
     */
    RecurrenceTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the recurrence type enum
     */
    public static RecurrenceTypeEnum fromValue(String text) {
      for (RecurrenceTypeEnum b : RecurrenceTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<RecurrenceTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final RecurrenceTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the recurrence type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public RecurrenceTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return RecurrenceTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The recurrence type. */
  @SerializedName("recurrence_type")
  private RecurrenceTypeEnum recurrenceType = null;

  /** The start repeat. */
  @SerializedName("start_repeat")
  private LocalDate startRepeat = null;

  /** The start transcoder. */
  @SerializedName("start_transcoder")
  private OffsetDateTime startTranscoder = null;

  /**
   * The Enum StateEnum.
   */
  @JsonAdapter(StateEnum.Adapter.class)
  public enum StateEnum {
    
    /** The enabled. */
    ENABLED("enabled"),
    
    /** The disabled. */
    DISABLED("disabled"),
    
    /** The expired. */
    EXPIRED("expired");

    /** The value. */
    private String value;

    /**
     * Instantiates a new state enum.
     *
     * @param value the value
     */
    StateEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the state enum
     */
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StateEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the state enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StateEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StateEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The state. */
  @SerializedName("state")
  private StateEnum state = null;

  /** The stop transcoder. */
  @SerializedName("stop_transcoder")
  private OffsetDateTime stopTranscoder = null;

  /** The transcoder id. */
  @SerializedName("transcoder_id")
  private String transcoderId = null;

  /** The transcoder name. */
  @SerializedName("transcoder_name")
  private String transcoderName = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The begins at. */
  @SerializedName("begins_at")
  private OffsetDateTime beginsAt = null;

  /** The ends at. */
  @SerializedName("ends_at")
  private OffsetDateTime endsAt = null;

  /**
   * Action type.
   *
   * @param actionType the action type
   * @return the schedule
   */
  public Schedule actionType(ActionTypeEnum actionType) {
    this.actionType = actionType;
    return this;
  }

   /**
    * Gets the action type.
    *
    * @return the action type
    */
  @ApiModelProperty(example = "start_stop", value = "The type of action that the schedule should trigger on the transcoder. The default is <strong>start</strong>.")
  public ActionTypeEnum getActionType() {
    return actionType;
  }

  /**
   * Sets the action type.
   *
   * @param actionType the new action type
   */
  public void setActionType(ActionTypeEnum actionType) {
    this.actionType = actionType;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the schedule
   */
  public Schedule createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "2019-01-27 17:32:17 UTC", value = "The date and time that the schedule was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * End repeat.
   *
   * @param endRepeat the end repeat
   * @return the schedule
   */
  public Schedule endRepeat(LocalDate endRepeat) {
    this.endRepeat = endRepeat;
    return this;
  }

   /**
    * Gets the end repeat.
    *
    * @return the end repeat
    */
  @ApiModelProperty(example = "2019-02-28", value = "The month, day, and year that a recurring schedule should stop running. Specify <strong>YYYY-MM-DD</strong>.")
  public LocalDate getEndRepeat() {
    return endRepeat;
  }

  /**
   * Sets the end repeat.
   *
   * @param endRepeat the new end repeat
   */
  public void setEndRepeat(LocalDate endRepeat) {
    this.endRepeat = endRepeat;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the schedule
   */
  public Schedule id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "C0t0MW3r", value = "The unique alphanumeric string that identifies the schedule.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the schedule
   */
  public Schedule name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "Scheduled start for my camera", value = "A descriptive name for the schedule. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Recurrence data.
   *
   * @param recurrenceData the recurrence data
   * @return the schedule
   */
  public Schedule recurrenceData(String recurrenceData) {
    this.recurrenceData = recurrenceData;
    return this;
  }

   /**
    * Gets the recurrence data.
    *
    * @return the recurrence data
    */
  @ApiModelProperty(example = "monday,tuesday,wednesday,thursday,friday", value = "The day or days of the week that a recurring schedule should run.")
  public String getRecurrenceData() {
    return recurrenceData;
  }

  /**
   * Sets the recurrence data.
   *
   * @param recurrenceData the new recurrence data
   */
  public void setRecurrenceData(String recurrenceData) {
    this.recurrenceData = recurrenceData;
  }

  /**
   * Recurrence type.
   *
   * @param recurrenceType the recurrence type
   * @return the schedule
   */
  public Schedule recurrenceType(RecurrenceTypeEnum recurrenceType) {
    this.recurrenceType = recurrenceType;
    return this;
  }

   /**
    * Gets the recurrence type.
    *
    * @return the recurrence type
    */
  @ApiModelProperty(example = "recur", value = "A schedule can run one time only (<strong>once</strong>) or repeat (<strong>recur</strong>) until a specified <em>end_repeat</em> date. The default is <strong>once</strong>.")
  public RecurrenceTypeEnum getRecurrenceType() {
    return recurrenceType;
  }

  /**
   * Sets the recurrence type.
   *
   * @param recurrenceType the new recurrence type
   */
  public void setRecurrenceType(RecurrenceTypeEnum recurrenceType) {
    this.recurrenceType = recurrenceType;
  }

  /**
   * Start repeat.
   *
   * @param startRepeat the start repeat
   * @return the schedule
   */
  public Schedule startRepeat(LocalDate startRepeat) {
    this.startRepeat = startRepeat;
    return this;
  }

   /**
    * Gets the start repeat.
    *
    * @return the start repeat
    */
  @ApiModelProperty(example = "2019-01-30", value = "The month, day, and year that the recurring schedule should go into effect. Specify <strong>YYYY-MM-DD</strong>.")
  public LocalDate getStartRepeat() {
    return startRepeat;
  }

  /**
   * Sets the start repeat.
   *
   * @param startRepeat the new start repeat
   */
  public void setStartRepeat(LocalDate startRepeat) {
    this.startRepeat = startRepeat;
  }

  /**
   * Start transcoder.
   *
   * @param startTranscoder the start transcoder
   * @return the schedule
   */
  public Schedule startTranscoder(OffsetDateTime startTranscoder) {
    this.startTranscoder = startTranscoder;
    return this;
  }

   /**
    * Gets the start transcoder.
    *
    * @return the start transcoder
    */
  @ApiModelProperty(example = "2019-01-30 00:00:00 UTC", value = "The month, day, year, and time of day that the <em>action_type</em> <strong>start</strong> should occur. Specify <strong>YYYY-MM-DD HH:MM:SS</strong> where <strong>HH</strong> is a 24-hour clock in UTC.")
  public OffsetDateTime getStartTranscoder() {
    return startTranscoder;
  }

  /**
   * Sets the start transcoder.
   *
   * @param startTranscoder the new start transcoder
   */
  public void setStartTranscoder(OffsetDateTime startTranscoder) {
    this.startTranscoder = startTranscoder;
  }

  /**
   * State.
   *
   * @param state the state
   * @return the schedule
   */
  public Schedule state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
    * Gets the state.
    *
    * @return the state
    */
  @ApiModelProperty(example = "enabled", value = "A schedule must be <strong>enabled</strong> to run. Specify <strong>enabled</strong> to run the schedule or <strong>disabled</strong> to turn off the schedule so that it doesn't run.")
  public StateEnum getState() {
    return state;
  }

  /**
   * Sets the state.
   *
   * @param state the new state
   */
  public void setState(StateEnum state) {
    this.state = state;
  }

  /**
   * Stop transcoder.
   *
   * @param stopTranscoder the stop transcoder
   * @return the schedule
   */
  public Schedule stopTranscoder(OffsetDateTime stopTranscoder) {
    this.stopTranscoder = stopTranscoder;
    return this;
  }

   /**
    * Gets the stop transcoder.
    *
    * @return the stop transcoder
    */
  @ApiModelProperty(example = "2019-02-28 23:59:59 UTC", value = "The month, day, year, and time of day that the <em>action_type</em> <strong>stop</strong> should occur. Specify <strong>YYYY-MM-DD HH:MM:SS</strong> where <strong>HH</strong> is a 24-hour clock in UTC.")
  public OffsetDateTime getStopTranscoder() {
    return stopTranscoder;
  }

  /**
   * Sets the stop transcoder.
   *
   * @param stopTranscoder the new stop transcoder
   */
  public void setStopTranscoder(OffsetDateTime stopTranscoder) {
    this.stopTranscoder = stopTranscoder;
  }

  /**
   * Transcoder id.
   *
   * @param transcoderId the transcoder id
   * @return the schedule
   */
  public Schedule transcoderId(String transcoderId) {
    this.transcoderId = transcoderId;
    return this;
  }

   /**
    * Gets the transcoder id.
    *
    * @return the transcoder id
    */
  @ApiModelProperty(example = "KhbF2XPv", value = "The unique alphanumeric string that identifies the transcoder being scheduled.")
  public String getTranscoderId() {
    return transcoderId;
  }

  /**
   * Sets the transcoder id.
   *
   * @param transcoderId the new transcoder id
   */
  public void setTranscoderId(String transcoderId) {
    this.transcoderId = transcoderId;
  }

  /**
   * Transcoder name.
   *
   * @param transcoderName the transcoder name
   * @return the schedule
   */
  public Schedule transcoderName(String transcoderName) {
    this.transcoderName = transcoderName;
    return this;
  }

   /**
    * Gets the transcoder name.
    *
    * @return the transcoder name
    */
  @ApiModelProperty(example = "My Camera", value = "The name of the transcoder being scheduled.")
  public String getTranscoderName() {
    return transcoderName;
  }

  /**
   * Sets the transcoder name.
   *
   * @param transcoderName the new transcoder name
   */
  public void setTranscoderName(String transcoderName) {
    this.transcoderName = transcoderName;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the schedule
   */
  public Schedule updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

  /**
   * Begins at.
   *
   * @param beginsAt the begins at
   * @return the schedule
   */
  public Schedule beginsAt(OffsetDateTime beginsAt) {
    this.beginsAt = beginsAt;
    return this;
  }

  /**
   * Ends at.
   *
   * @param endsAt the ends at
   * @return the schedule
   */
  public Schedule endsAt(OffsetDateTime endsAt) {
    this.endsAt = endsAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "2019-01-28 17:49:37 UTC", value = "The date and time that the schedule was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the begins at.
   *
   * @param beginsAt the new begins at
   */
  public void setBeginsAt(OffsetDateTime beginsAt) {
    this.beginsAt = beginsAt;
  }

  /**
   * Gets the begins at.
   *
   * @return the begins at
   */
  public OffsetDateTime getBeginsAt() {
    return beginsAt;
  }

  /**
   * Sets the ends at.
   *
   * @param endsAt the new ends at
   */
  public void setEndsAt(OffsetDateTime endsAt) {
    this.endsAt = endsAt;
  }

  /**
   * Gets the ends at.
   *
   * @return the ends at
   */
  public OffsetDateTime getEndsAt() {
    return endsAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Schedule schedule = (Schedule) o;
    return Objects.equals(this.actionType, schedule.actionType) &&
        Objects.equals(this.createdAt, schedule.createdAt) &&
        Objects.equals(this.endRepeat, schedule.endRepeat) &&
        Objects.equals(this.id, schedule.id) &&
        Objects.equals(this.name, schedule.name) &&
        Objects.equals(this.recurrenceData, schedule.recurrenceData) &&
        Objects.equals(this.recurrenceType, schedule.recurrenceType) &&
        Objects.equals(this.startRepeat, schedule.startRepeat) &&
        Objects.equals(this.startTranscoder, schedule.startTranscoder) &&
        Objects.equals(this.state, schedule.state) &&
        Objects.equals(this.stopTranscoder, schedule.stopTranscoder) &&
        Objects.equals(this.transcoderId, schedule.transcoderId) &&
        Objects.equals(this.transcoderName, schedule.transcoderName) &&
        Objects.equals(this.updatedAt, schedule.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(actionType, createdAt, endRepeat, id, name, recurrenceData, recurrenceType, startRepeat, startTranscoder, state, stopTranscoder, transcoderId, transcoderName, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Schedule {\n");
    
    sb.append("    actionType: ").append(toIndentedString(actionType)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    endRepeat: ").append(toIndentedString(endRepeat)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    recurrenceData: ").append(toIndentedString(recurrenceData)).append("\n");
    sb.append("    recurrenceType: ").append(toIndentedString(recurrenceType)).append("\n");
    sb.append("    startRepeat: ").append(toIndentedString(startRepeat)).append("\n");
    sb.append("    startTranscoder: ").append(toIndentedString(startTranscoder)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    stopTranscoder: ").append(toIndentedString(stopTranscoder)).append("\n");
    sb.append("    transcoderId: ").append(toIndentedString(transcoderId)).append("\n");
    sb.append("    transcoderName: ").append(toIndentedString(transcoderName)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

