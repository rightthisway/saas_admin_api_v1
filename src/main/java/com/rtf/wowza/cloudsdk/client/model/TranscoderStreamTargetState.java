/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetsState;
import java.io.IOException;

/**
 * The Class TranscoderStreamTargetState.
 */
public class TranscoderStreamTargetState {
  
  /** The stream targets. */
  @SerializedName("stream_targets")
  private StreamTargetsState streamTargets = null;

  /**
   * Stream targets.
   *
   * @param streamTargets the stream targets
   * @return the transcoder stream target state
   */
  public TranscoderStreamTargetState streamTargets(StreamTargetsState streamTargets) {
    this.streamTargets = streamTargets;
    return this;
  }

   /**
    * Gets the stream target state.
    *
    * @return the stream target state
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetsState getStreamTargetState() {
    return streamTargets;
  }

  /**
   * Sets the stream target state.
   *
   * @param streamTargets the new stream target state
   */
  public void setStreamTargetState(StreamTargetsState streamTargets) {
    this.streamTargets = streamTargets;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderStreamTargetState transcoder1 = (TranscoderStreamTargetState) o;
    return Objects.equals(this.streamTargets, transcoder1.streamTargets);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargets);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderStreamTargetState {\n");
    
    sb.append("    streamTargets: ").append(toIndentedString(streamTargets)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

