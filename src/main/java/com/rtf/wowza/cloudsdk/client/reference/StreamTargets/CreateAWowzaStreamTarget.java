 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.StreamTargets;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.api.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.auth.*;

import java.util.List;

/**
 * The Class CreateAWowzaStreamTarget.
 */
public class CreateAWowzaStreamTarget {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey("YOUR API KEY");

        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey("YOUR API KEY");

	StreamTargetsApi apiInstance = new StreamTargetsApi();

        StreamTargetWowza targetWowza = new StreamTargetWowza();
        targetWowza.setName("Wowza Target SDK Java");
        targetWowza.setProvider(StreamTargetWowza.ProviderEnum.AKAMAI);
        targetWowza.setLocation(StreamTargetWowza.LocationEnum.EU_GERMANY);

	try {
		StreamTargetWowza result = apiInstance.createWowzaStreamTarget(targetWowza);
		System.out.println(result);
	} catch (ApiException e) {
	    System.err.println("Exception when calling StreamTargetsApi#createWowzaStreamTarget");
            System.err.println("Exception when calling StreamTargetsApi#Code:"+e.getCode());
            System.err.println("Exception when calling StreamTargetsApi#ResponseBody:"+e.getResponseBody());
	    e.printStackTrace();
		}
	}
}
