/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class GpuDriverVersionMetric.
 */
@ApiModel(description = "")
public class GpuDriverVersionMetric {
  
  /** The status. */
  @SerializedName("status")
  private String status = null;

  /** The text. */
  @SerializedName("text")
  private String text = null;

  /** The units. */
  @SerializedName("units")
  private String units = null;

  /** The value. */
  @SerializedName("value")
  private String value = null;

  /**
   * Status.
   *
   * @param status the status
   * @return the gpu driver version metric
   */
  public GpuDriverVersionMetric status(String status) {
    this.status = status;
    return this;
  }

   /**
    * Gets the status.
    *
    * @return the status
    */
  @ApiModelProperty(example = "normal", value = "The status of the current key. Possible values are <strong>normal</strong> (everything is fine), <strong>warning</strong> (something may be misconfigured), and <strong>no_data</strong> (no data was returned, perhaps because the instance isn't running).")
  public String getStatus() {
    return status;
  }

  /**
   * Sets the status.
   *
   * @param status the new status
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * Text.
   *
   * @param text the text
   * @return the gpu driver version metric
   */
  public GpuDriverVersionMetric text(String text) {
    this.text = text;
    return this;
  }

   /**
    * Gets the text.
    *
    * @return the text
    */
  @ApiModelProperty(example = "", value = "A message related to the value and status of the current key. Usually blank unless there's a warning status.")
  public String getText() {
    return text;
  }

  /**
   * Sets the text.
   *
   * @param text the new text
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * Units.
   *
   * @param units the units
   * @return the gpu driver version metric
   */
  public GpuDriverVersionMetric units(String units) {
    this.units = units;
    return this;
  }

   /**
    * Gets the units.
    *
    * @return the units
    */
  @ApiModelProperty(example = "", value = "The unit of the returned value, such as <strong>Kbps</strong>, <strong>bps</strong>, <strong>%</strong>, <strong>FPS</strong>, or <strong>GOP</strong>.")
  public String getUnits() {
    return units;
  }

  /**
   * Sets the units.
   *
   * @param units the new units
   */
  public void setUnits(String units) {
    this.units = units;
  }

  /**
   * Value.
   *
   * @param value the value
   * @return the gpu driver version metric
   */
  public GpuDriverVersionMetric value(String value) {
    this.value = value;
    return this;
  }

   /**
    * Gets the value.
    *
    * @return the value
    */
  @ApiModelProperty(example = "not_installed", value = "The value of the associated key.")
  public String getValue() {
    return value;
  }

  /**
   * Sets the value.
   *
   * @param value the new value
   */
  public void setValue(String value) {
    this.value = value;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GpuDriverVersionMetric gpuDriverVersionMetric = (GpuDriverVersionMetric) o;
    return Objects.equals(this.status, gpuDriverVersionMetric.status) &&
        Objects.equals(this.text, gpuDriverVersionMetric.text) &&
        Objects.equals(this.units, gpuDriverVersionMetric.units) &&
        Objects.equals(this.value, gpuDriverVersionMetric.value);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(status, text, units, value);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GpuDriverVersionMetric {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    units: ").append(toIndentedString(units)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

