/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetWowza;
import java.io.IOException;

/**
 * The Class WowzaStreamTargetInput.
 */
@ApiModel(description = "")
public class WowzaStreamTargetInput {
  
  /** The stream target wowza. */
  @SerializedName("stream_target_wowza")
  private StreamTargetWowza streamTargetWowza = null;

  /**
   * Stream target wowza.
   *
   * @param streamTargetWowza the stream target wowza
   * @return the wowza stream target input
   */
  public WowzaStreamTargetInput streamTargetWowza(StreamTargetWowza streamTargetWowza) {
    this.streamTargetWowza = streamTargetWowza;
    return this;
  }

   /**
    * Gets the stream target wowza.
    *
    * @return the stream target wowza
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetWowza getStreamTargetWowza() {
    return streamTargetWowza;
  }

  /**
   * Sets the stream target wowza.
   *
   * @param streamTargetWowza the new stream target wowza
   */
  public void setStreamTargetWowza(StreamTargetWowza streamTargetWowza) {
    this.streamTargetWowza = streamTargetWowza;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WowzaStreamTargetInput wowzaStreamTargetInput = (WowzaStreamTargetInput) o;
    return Objects.equals(this.streamTargetWowza, wowzaStreamTargetInput.streamTargetWowza);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetWowza);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WowzaStreamTargetInput {\n");
    
    sb.append("    streamTargetWowza: ").append(toIndentedString(streamTargetWowza)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

