/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class StreamTargetPropertyCreateInput.
 */
@ApiModel(description = "")
public class StreamTargetPropertyCreateInput {
  
  /** The property. */
  @SerializedName("property")
  private StreamTargetProperty property = null;

  /**
   * Property.
   *
   * @param property the property
   * @return the stream target property create input
   */
  public StreamTargetPropertyCreateInput property(StreamTargetProperty property) {
    this.property = property;
    return this;
  }

   /**
    * Gets the stream target property.
    *
    * @return the stream target property
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetProperty getStreamTargetProperty() {
    return property;
  }

  /**
   * Sets the stream target property.
   *
   * @param property the new stream target property
   */
  public void setStreamTargetProperty(StreamTargetProperty property) {
    this.property = property;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetPropertyCreateInput streamTargetStreamTargetPropertyCreateInput = (StreamTargetPropertyCreateInput) o;
    return Objects.equals(this.property, streamTargetStreamTargetPropertyCreateInput.property);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(property);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetPropertyCreateInput {\n");
    
    sb.append("    property: ").append(toIndentedString(property)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

