/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.TranscoderProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class TranscoderProperties.
 */
@ApiModel(description = "")
public class TranscoderProperties {
  
  /** The properties. */
  @SerializedName("properties")
  private List<TranscoderProperty> properties = new ArrayList<TranscoderProperty>();

  /**
   * Properties.
   *
   * @param properties the properties
   * @return the transcoder properties
   */
  public TranscoderProperties properties(List<TranscoderProperty> properties) {
    this.properties = properties;
    return this;
  }

  /**
   * Adds the properties item.
   *
   * @param propertiesItem the properties item
   * @return the transcoder properties
   */
  public TranscoderProperties addPropertiesItem(TranscoderProperty propertiesItem) {
    this.properties.add(propertiesItem);
    return this;
  }

   /**
    * Gets the properties.
    *
    * @return the properties
    */
  @ApiModelProperty(required = true, value = "")
  public List<TranscoderProperty> getProperties() {
    return properties;
  }

  /**
   * Sets the properties.
   *
   * @param properties the new properties
   */
  public void setProperties(List<TranscoderProperty> properties) {
    this.properties = properties;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderProperties transcoderProperties = (TranscoderProperties) o;
    return Objects.equals(this.properties, transcoderProperties.properties);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(properties);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderProperties {\n");
    
    sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

