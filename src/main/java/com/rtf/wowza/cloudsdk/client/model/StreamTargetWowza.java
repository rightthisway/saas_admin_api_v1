/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.threeten.bp.OffsetDateTime;

/**
 * The Class StreamTargetWowza.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-02T10:45:24.077Z")
public class StreamTargetWowza {
  
  /** The backup url. */
  @SerializedName("backup_url")
  private String backupUrl = null;

  /** The connection code. */
  @SerializedName("connection_code")
  private String connectionCode = null;

  /** The connection code expires at. */
  @SerializedName("connection_code_expires_at")
  private String connectionCodeExpiresAt = null;

  /** The created at. */
  @SerializedName("created_at")
  private String createdAt = null;

  /** The hds playback url. */
  @SerializedName("hds_playback_url")
  private String hdsPlaybackUrl = null;

  /** The hls playback url. */
  @SerializedName("hls_playback_url")
  private String hlsPlaybackUrl = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /**
   * The Enum LocationEnum.
   */
  @JsonAdapter(LocationEnum.Adapter.class)
  public enum LocationEnum {
    
    /** The asia pacific australia. */
    ASIA_PACIFIC_AUSTRALIA("asia_pacific_australia"),
    
    /** The asia pacific japan. */
    ASIA_PACIFIC_JAPAN("asia_pacific_japan"),
    
    /** The asia pacific singapore. */
    ASIA_PACIFIC_SINGAPORE("asia_pacific_singapore"),
    
    /** The asia pacific taiwan. */
    ASIA_PACIFIC_TAIWAN("asia_pacific_taiwan"),
    
    /** The eu belgium. */
    EU_BELGIUM("eu_belgium"),
    
    /** The eu germany. */
    EU_GERMANY("eu_germany"),
    
    /** The eu ireland. */
    EU_IRELAND("eu_ireland"),
    
    /** The south america brazil. */
    SOUTH_AMERICA_BRAZIL("south_america_brazil"),
    
    /** The us central iowa. */
    US_CENTRAL_IOWA("us_central_iowa"),
    
    /** The us east virginia. */
    US_EAST_VIRGINIA("us_east_virginia"),
    
    /** The us west california. */
    US_WEST_CALIFORNIA("us_west_california"),
    
    /** The us west oregon. */
    US_WEST_OREGON("us_west_oregon");

    /** The value. */
    private String value;

    /**
     * Instantiates a new location enum.
     *
     * @param value the value
     */
    LocationEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the location enum
     */
    public static LocationEnum fromValue(String text) {
      for (LocationEnum b : LocationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<LocationEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final LocationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the location enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public LocationEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LocationEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The location. */
  @SerializedName("location")
  private LocationEnum location = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The primary url. */
  @SerializedName("primary_url")
  private String primaryUrl = null;

  /** The secure ingest query param. */
  @SerializedName("secure_ingest_query_param")
  private String secureIngestQueryParam = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private String updatedAt = null;

  /** The use cors. */
  @SerializedName("use_cors")
  private Boolean useCors = null;

  /** The use secure ingest. */
  @SerializedName("use_secure_ingest")
  private Boolean useSecureIngest = null;

  /**
   * Backup url.
   *
   * @param backupUrl the backup url
   * @return the stream target wowza
   */
  public StreamTargetWowza backupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
    return this;
  }

   /**
    * Gets the backup url.
    *
    * @return the backup url
    */
  @ApiModelProperty(example = "", value = "Only for Wowza stream targets whose <em>provider</em> is <em>not</em> <strong>akamai_cupertino</strong>. The backup RTMP ingest URL of the Wowza Stream Target.")
  public String getBackupUrl() {
    return backupUrl;
  }

  /**
   * Sets the backup url.
   *
   * @param backupUrl the new backup url
   */
  public void setBackupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
  }

  /**
   * Connection code.
   *
   * @param connectionCode the connection code
   * @return the stream target wowza
   */
  public StreamTargetWowza connectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
    return this;
  }

   /**
    * Gets the connection code.
    *
    * @return the connection code
    */
  @ApiModelProperty(example = "", value = "A six-character, alphanumeric string that allows Wowza Streaming Engine to send a transcoded stream to a Wowza stream target. The code can be used once and expires 24 hours after it's created.")
  public String getConnectionCode() {
    return connectionCode;
  }

  /**
   * Sets the connection code.
   *
   * @param connectionCode the new connection code
   */
  public void setConnectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
  }

  /**
   * Connection code expires at.
   *
   * @param connectionCodeExpiresAt the connection code expires at
   * @return the stream target wowza
   */
  public StreamTargetWowza connectionCodeExpiresAt(String connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
    return this;
  }

   /**
    * Gets the connection code expires at.
    *
    * @return the connection code expires at
    */
  @ApiModelProperty(example = "", value = "The date and time that the <em>connection_code</em> expires.")
  public String getConnectionCodeExpiresAt() {
    return connectionCodeExpiresAt;
  }

  /**
   * Sets the connection code expires at.
   *
   * @param connectionCodeExpiresAt the new connection code expires at
   */
  public void setConnectionCodeExpiresAt(String connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream target wowza
   */
  public StreamTargetWowza createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the stream target was created.")
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Hds playback url.
   *
   * @param hdsPlaybackUrl the hds playback url
   * @return the stream target wowza
   */
  public StreamTargetWowza hdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hds playback url.
    *
    * @return the hds playback url
    */
  @ApiModelProperty(example = "", value = "The web address that the target uses to play Adobe HDS streams.")
  public String getHdsPlaybackUrl() {
    return hdsPlaybackUrl;
  }

  /**
   * Sets the hds playback url.
   *
   * @param hdsPlaybackUrl the new hds playback url
   */
  public void setHdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
  }

  /**
   * Hls playback url.
   *
   * @param hlsPlaybackUrl the hls playback url
   * @return the stream target wowza
   */
  public StreamTargetWowza hlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hls playback url.
    *
    * @return the hls playback url
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>provider</em> is <strong>akamai_cupertino</strong>. The web address that the target uses to play Apple HLS streams.")
  public String getHlsPlaybackUrl() {
    return hlsPlaybackUrl;
  }

  /**
   * Sets the hls playback url.
   *
   * @param hlsPlaybackUrl the new hls playback url
   */
  public void setHlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the stream target wowza
   */
  public StreamTargetWowza id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Location.
   *
   * @param location the location
   * @return the stream target wowza
   */
  public StreamTargetWowza location(LocationEnum location) {
    this.location = location;
    return this;
  }

   /**
    * Gets the location.
    *
    * @return the location
    */
  @ApiModelProperty(example = "", value = "Only for Wowza stream targets whose <em>provider</em> is <em>not</em> <strong>akamai_cupertino</strong>. Choose a location as close as possible to your video source.")
  public LocationEnum getLocation() {
    return location;
  }

  /**
   * Sets the location.
   *
   * @param location the new location
   */
  public void setLocation(LocationEnum location) {
    this.location = location;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the stream target wowza
   */
  public StreamTargetWowza name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the stream target. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Primary url.
   *
   * @param primaryUrl the primary url
   * @return the stream target wowza
   */
  public StreamTargetWowza primaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
    return this;
  }

   /**
    * Gets the primary url.
    *
    * @return the primary url
    */
  @ApiModelProperty(example = "", value = "The primary ingest URL of the target.")
  public String getPrimaryUrl() {
    return primaryUrl;
  }

  /**
   * Sets the primary url.
   *
   * @param primaryUrl the new primary url
   */
  public void setPrimaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
  }

  /**
   * The Enum ProviderEnum.
   */
  @JsonAdapter(ProviderEnum.Adapter.class)
  public enum ProviderEnum {
    
    /** The akamai. */
    AKAMAI("akamai"),

    /** The akamai cupertino. */
    AKAMAI_CUPERTINO("akamai_cupertino"),

    /** The akamai legacy rtmp. */
    AKAMAI_LEGACY_RTMP("akamai_legacy_rtmp");

    /** The value. */
    private String value;

    /**
     * Instantiates a new provider enum.
     *
     * @param value the value
     */
    ProviderEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the provider enum
     */
    public static ProviderEnum fromValue(String text) {
      for (ProviderEnum b : ProviderEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<ProviderEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final ProviderEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the provider enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public ProviderEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ProviderEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The provider. */
  @SerializedName("provider")
  private ProviderEnum provider = null;

 /**
  * Provider.
  *
  * @param provider the provider
  * @return the stream target wowza
  */
 public StreamTargetWowza provider(ProviderEnum provider) {
    this.provider = provider;
    return this;
  }

   /**
    * Gets the provider.
    *
    * @return the provider
    */
  @ApiModelProperty(example = "akamai", value = "The CDN for the target.")
  public ProviderEnum getProvider() {
    return provider;
  }

  /**
   * Sets the provider.
   *
   * @param provider the new provider
   */
  public void setProvider(ProviderEnum provider) {
    this.provider = provider;
  }


  /**
   * Secure ingest query param.
   *
   * @param secureIngestQueryParam the secure ingest query param
   * @return the stream target wowza
   */
  public StreamTargetWowza secureIngestQueryParam(String secureIngestQueryParam) {
    this.secureIngestQueryParam = secureIngestQueryParam;
    return this;
  }

   /**
    * Gets the secure ingest query param.
    *
    * @return the secure ingest query param
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>use_secure_ingest</em> is <strong>true</strong>. The query parameter needed for secure stream delivery between the transcoder and the target.")
  public String getSecureIngestQueryParam() {
    return secureIngestQueryParam;
  }

  /**
   * Sets the secure ingest query param.
   *
   * @param secureIngestQueryParam the new secure ingest query param
   */
  public void setSecureIngestQueryParam(String secureIngestQueryParam) {
    this.secureIngestQueryParam = secureIngestQueryParam;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the stream target wowza
   */
  public StreamTargetWowza streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "", value = "The name of the stream being ingested into the target.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the stream target wowza
   */
  public StreamTargetWowza updatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the stream target was updated.")
  public String getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Use cors.
   *
   * @param useCors the use cors
   * @return the stream target wowza
   */
  public StreamTargetWowza useCors(Boolean useCors) {
    this.useCors = useCors;
    return this;
  }

   /**
    * Checks if is use cors.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Only for Wowza stream targets whose <em>provider</em> is <strong>akamai_cupertino</strong>. CORS, or cross-origin resource sharing, allows streams to be safely delivered across domains.")
  public Boolean isUseCors() {
    return useCors;
  }

  /**
   * Sets the use cors.
   *
   * @param useCors the new use cors
   */
  public void setUseCors(Boolean useCors) {
    this.useCors = useCors;
  }

  /**
   * Use secure ingest.
   *
   * @param useSecureIngest the use secure ingest
   * @return the stream target wowza
   */
  public StreamTargetWowza useSecureIngest(Boolean useSecureIngest) {
    this.useSecureIngest = useSecureIngest;
    return this;
  }

   /**
    * Checks if is use secure ingest.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Only for Wowza stream targets whose <em>provider</em> is <strong>akamai_cupertino</strong>. If <strong>true</strong>, generates a <em>secure_ingest_query_param</em> to securely deliver the stream from the transcoder to the provider.")
  public Boolean isUseSecureIngest() {
    return useSecureIngest;
  }

  /**
   * Sets the use secure ingest.
   *
   * @param useSecureIngest the new use secure ingest
   */
  public void setUseSecureIngest(Boolean useSecureIngest) {
    this.useSecureIngest = useSecureIngest;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetWowza streamTargetWowza = (StreamTargetWowza) o;
    return Objects.equals(this.backupUrl, streamTargetWowza.backupUrl) &&
        Objects.equals(this.connectionCode, streamTargetWowza.connectionCode) &&
        Objects.equals(this.connectionCodeExpiresAt, streamTargetWowza.connectionCodeExpiresAt) &&
        Objects.equals(this.createdAt, streamTargetWowza.createdAt) &&
        Objects.equals(this.hdsPlaybackUrl, streamTargetWowza.hdsPlaybackUrl) &&
        Objects.equals(this.hlsPlaybackUrl, streamTargetWowza.hlsPlaybackUrl) &&
        Objects.equals(this.id, streamTargetWowza.id) &&
        Objects.equals(this.location, streamTargetWowza.location) &&
        Objects.equals(this.name, streamTargetWowza.name) &&
        Objects.equals(this.primaryUrl, streamTargetWowza.primaryUrl) &&
        Objects.equals(this.provider, streamTargetWowza.provider) &&
        Objects.equals(this.secureIngestQueryParam, streamTargetWowza.secureIngestQueryParam) &&
        Objects.equals(this.streamName, streamTargetWowza.streamName) &&
        Objects.equals(this.updatedAt, streamTargetWowza.updatedAt) &&
        Objects.equals(this.useCors, streamTargetWowza.useCors) &&
        Objects.equals(this.useSecureIngest, streamTargetWowza.useSecureIngest);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(backupUrl, connectionCode, connectionCodeExpiresAt, createdAt, hdsPlaybackUrl, hlsPlaybackUrl, id, location, name, primaryUrl, provider, secureIngestQueryParam, streamName, updatedAt, useCors, useSecureIngest);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetWowza {\n");
    
    sb.append("    backupUrl: ").append(toIndentedString(backupUrl)).append("\n");
    sb.append("    connectionCode: ").append(toIndentedString(connectionCode)).append("\n");
    sb.append("    connectionCodeExpiresAt: ").append(toIndentedString(connectionCodeExpiresAt)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    hdsPlaybackUrl: ").append(toIndentedString(hdsPlaybackUrl)).append("\n");
    sb.append("    hlsPlaybackUrl: ").append(toIndentedString(hlsPlaybackUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    primaryUrl: ").append(toIndentedString(primaryUrl)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    secureIngestQueryParam: ").append(toIndentedString(secureIngestQueryParam)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    useCors: ").append(toIndentedString(useCors)).append("\n");
    sb.append("    useSecureIngest: ").append(toIndentedString(useSecureIngest)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

