/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class LiveStreamThumbnail.
 */
public class LiveStreamThumbnail {
  
  /** The thumbnail url. */
  @SerializedName("thumbnail_url")
  private String thumbnailUrl = null;

  /**
   * Thumbnail url.
   *
   * @param thumbnailUrl the thumbnail url
   * @return the live stream thumbnail
   */
  public LiveStreamThumbnail thumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
    return this;
  }

   /**
    * Gets the thumbnail url.
    *
    * @return the thumbnail url
    */
  @ApiModelProperty(example = "https://cloud.wowza.com/proxy/stats/?target=10.11.12.13&app=app-79b8&stream=99b62146@130135.stream&media=json", value = "The URL to receive the preview thumbnail.")
  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  /**
   * Sets the thumbnail url.
   *
   * @param thumbnailUrl the new thumbnail url
   */
  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamThumbnail liveStream5 = (LiveStreamThumbnail) o;
    return Objects.equals(this.thumbnailUrl, liveStream5.thumbnailUrl);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(thumbnailUrl);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamThumbnail {\n");
    
    sb.append("    thumbnailUrl: ").append(toIndentedString(thumbnailUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

