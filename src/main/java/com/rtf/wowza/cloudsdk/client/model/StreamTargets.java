/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamTargets.
 */
@ApiModel(description = "")
public class StreamTargets {
  
  /** The stream targets. */
  @SerializedName("stream_targets")
  private List<IndexStreamTarget> streamTargets = new ArrayList<IndexStreamTarget>();

  /**
   * Stream targets.
   *
   * @param streamTargets the stream targets
   * @return the stream targets
   */
  public StreamTargets streamTargets(List<IndexStreamTarget> streamTargets) {
    this.streamTargets = streamTargets;
    return this;
  }

  /**
   * Adds the stream targets item.
   *
   * @param streamTargetsItem the stream targets item
   * @return the stream targets
   */
  public StreamTargets addStreamTargetsItem(IndexStreamTarget streamTargetsItem) {
    this.streamTargets.add(streamTargetsItem);
    return this;
  }

   /**
    * Gets the stream targets.
    *
    * @return the stream targets
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexStreamTarget> getStreamTargets() {
    return streamTargets;
  }

  /**
   * Sets the stream targets.
   *
   * @param streamTargets the new stream targets
   */
  public void setStreamTargets(List<IndexStreamTarget> streamTargets) {
    this.streamTargets = streamTargets;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargets streamTargets = (StreamTargets) o;
    return Objects.equals(this.streamTargets, streamTargets.streamTargets);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargets);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargets {\n");
    
    sb.append("    streamTargets: ").append(toIndentedString(streamTargets)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

