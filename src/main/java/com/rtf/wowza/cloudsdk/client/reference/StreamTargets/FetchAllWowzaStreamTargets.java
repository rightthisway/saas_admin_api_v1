 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.StreamTargets;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.api.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.auth.*;

import java.util.List;

/**
 * The Class FetchAllWowzaStreamTargets.
 */
public class FetchAllWowzaStreamTargets {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey(defaultClient.wscaccesskey);

        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey(defaultClient.wscapikey);

	StreamTargetsApi apiInstance = new StreamTargetsApi();
	Integer page = 1; 
	Integer perPage = 56; 
	try {
	    List<IndexWowzaStreamTarget> result = apiInstance.listWowzaStreamTargets(page, perPage);
	    System.out.println(result);
	} catch (ApiException e) {
	    System.err.println("Exception when calling StreamTargetsApi#listStreamTargets");
	    e.printStackTrace();
		}
	}
}
