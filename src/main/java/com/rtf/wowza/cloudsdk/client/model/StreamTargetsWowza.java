/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexWowzaStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamTargetsWowza.
 */
@ApiModel(description = "")
public class StreamTargetsWowza {
  
  /** The stream targets wowza. */
  @SerializedName("stream_targets_wowza")
  private List<IndexWowzaStreamTarget> streamTargetsWowza = new ArrayList<IndexWowzaStreamTarget>();

  /**
   * Stream targets wowza.
   *
   * @param streamTargetsWowza the stream targets wowza
   * @return the stream targets wowza
   */
  public StreamTargetsWowza streamTargetsWowza(List<IndexWowzaStreamTarget> streamTargetsWowza) {
    this.streamTargetsWowza = streamTargetsWowza;
    return this;
  }

  /**
   * Adds the stream targets wowza item.
   *
   * @param streamTargetsWowzaItem the stream targets wowza item
   * @return the stream targets wowza
   */
  public StreamTargetsWowza addStreamTargetsWowzaItem(IndexWowzaStreamTarget streamTargetsWowzaItem) {
    this.streamTargetsWowza.add(streamTargetsWowzaItem);
    return this;
  }

   /**
    * Gets the stream targets wowza.
    *
    * @return the stream targets wowza
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexWowzaStreamTarget> getStreamTargetsWowza() {
    return streamTargetsWowza;
  }

  /**
   * Sets the stream targets wowza.
   *
   * @param streamTargetsWowza the new stream targets wowza
   */
  public void setStreamTargetsWowza(List<IndexWowzaStreamTarget> streamTargetsWowza) {
    this.streamTargetsWowza = streamTargetsWowza;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetsWowza streamTargetsWowza = (StreamTargetsWowza) o;
    return Objects.equals(this.streamTargetsWowza, streamTargetsWowza.streamTargetsWowza);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetsWowza);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetsWowza {\n");
    
    sb.append("    streamTargetsWowza: ").append(toIndentedString(streamTargetsWowza)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

