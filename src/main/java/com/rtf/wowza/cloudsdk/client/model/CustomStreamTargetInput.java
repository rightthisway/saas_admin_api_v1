/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetCustom;
import java.io.IOException;

/**
 * The Class CustomStreamTargetInput.
 */
@ApiModel(description = "")
public class CustomStreamTargetInput {
  
  /** The stream target custom. */
  @SerializedName("stream_target_custom")
  private StreamTargetCustom streamTargetCustom = null;

  /**
   * Stream target custom.
   *
   * @param streamTargetCustom the stream target custom
   * @return the custom stream target input
   */
  public CustomStreamTargetInput streamTargetCustom(StreamTargetCustom streamTargetCustom) {
    this.streamTargetCustom = streamTargetCustom;
    return this;
  }

   /**
    * Gets the stream target custom.
    *
    * @return the stream target custom
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetCustom getStreamTargetCustom() {
    return streamTargetCustom;
  }

  /**
   * Sets the stream target custom.
   *
   * @param streamTargetCustom the new stream target custom
   */
  public void setStreamTargetCustom(StreamTargetCustom streamTargetCustom) {
    this.streamTargetCustom = streamTargetCustom;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomStreamTargetInput customStreamTargetInput = (CustomStreamTargetInput) o;
    return Objects.equals(this.streamTargetCustom, customStreamTargetInput.streamTargetCustom);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetCustom);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomStreamTargetInput {\n");
    
    sb.append("    streamTargetCustom: ").append(toIndentedString(streamTargetCustom)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

