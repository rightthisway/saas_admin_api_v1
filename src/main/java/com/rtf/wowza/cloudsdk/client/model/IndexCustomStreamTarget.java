/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class IndexCustomStreamTarget.
 */
@ApiModel(description = "")
public class IndexCustomStreamTarget {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the index custom stream target
   */
  public IndexCustomStreamTarget createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the custom stream target was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the index custom stream target
   */
  public IndexCustomStreamTarget id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the custom stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the index custom stream target
   */
  public IndexCustomStreamTarget name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the custom stream target. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the index custom stream target
   */
  public IndexCustomStreamTarget updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the custom stream target was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IndexCustomStreamTarget indexCustomStreamTarget = (IndexCustomStreamTarget) o;
    return Objects.equals(this.createdAt, indexCustomStreamTarget.createdAt) &&
        Objects.equals(this.id, indexCustomStreamTarget.id) &&
        Objects.equals(this.name, indexCustomStreamTarget.name) &&
        Objects.equals(this.updatedAt, indexCustomStreamTarget.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, id, name, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IndexCustomStreamTarget {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

