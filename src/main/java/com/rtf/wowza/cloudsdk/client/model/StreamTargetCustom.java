/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class StreamTargetCustom.
 */
@ApiModel(description = "")
public class StreamTargetCustom {
  
  /** The backup url. */
  @SerializedName("backup_url")
  private String backupUrl = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The hds playback url. */
  @SerializedName("hds_playback_url")
  private String hdsPlaybackUrl = null;

  /** The hls playback url. */
  @SerializedName("hls_playback_url")
  private String hlsPlaybackUrl = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The password. */
  @SerializedName("password")
  private String password = null;

  /** The primary url. */
  @SerializedName("primary_url")
  private String primaryUrl = null;

  /** The provider. */
  @SerializedName("provider")
  private String provider = null;

  /** The rtmp playback url. */
  @SerializedName("rtmp_playback_url")
  private String rtmpPlaybackUrl = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The username. */
  @SerializedName("username")
  private String username = null;

  /**
   * Backup url.
   *
   * @param backupUrl the backup url
   * @return the stream target custom
   */
  public StreamTargetCustom backupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
    return this;
  }

   /**
    * Gets the backup url.
    *
    * @return the backup url
    */
  @ApiModelProperty(example = "", value = "Only for custom stream targets whose <em>provider</em> is <em>not</em> **akamai_cupertino**. The backup ingest URL for a custom stream target.")
  public String getBackupUrl() {
    return backupUrl;
  }

  /**
   * Sets the backup url.
   *
   * @param backupUrl the new backup url
   */
  public void setBackupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream target custom
   */
  public StreamTargetCustom createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the custom stream target was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Hds playback url.
   *
   * @param hdsPlaybackUrl the hds playback url
   * @return the stream target custom
   */
  public StreamTargetCustom hdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hds playback url.
    *
    * @return the hds playback url
    */
  @ApiModelProperty(example = "", value = "Only for custom stream targets whose <em>provider</em> is <em>not</em> <strong>akamai_cupertino</strong>. The web address that the custom stream target uses to play Adobe HDS streams.")
  public String getHdsPlaybackUrl() {
    return hdsPlaybackUrl;
  }

  /**
   * Sets the hds playback url.
   *
   * @param hdsPlaybackUrl the new hds playback url
   */
  public void setHdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
  }

  /**
   * Hls playback url.
   *
   * @param hlsPlaybackUrl the hls playback url
   * @return the stream target custom
   */
  public StreamTargetCustom hlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hls playback url.
    *
    * @return the hls playback url
    */
  @ApiModelProperty(example = "", value = "The web address that the custom stream target uses to play Apple HLS streams.")
  public String getHlsPlaybackUrl() {
    return hlsPlaybackUrl;
  }

  /**
   * Sets the hls playback url.
   *
   * @param hlsPlaybackUrl the new hls playback url
   */
  public void setHlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the stream target custom
   */
  public StreamTargetCustom id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the custom stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the stream target custom
   */
  public StreamTargetCustom name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the custom stream target. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Password.
   *
   * @param password the password
   * @return the stream target custom
   */
  public StreamTargetCustom password(String password) {
    this.password = password;
    return this;
  }

   /**
    * Gets the password.
    *
    * @return the password
    */
  @ApiModelProperty(example = "", value = "Only for custom stream targets whose <em>provider</em> is <em>not</em> **akamai_cupertino**. A <em>username</em> must also be present. The password associated with the target username for RTMP authentication.")
  public String getPassword() {
    return password;
  }

  /**
   * Sets the password.
   *
   * @param password the new password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Primary url.
   *
   * @param primaryUrl the primary url
   * @return the stream target custom
   */
  public StreamTargetCustom primaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
    return this;
  }

   /**
    * Gets the primary url.
    *
    * @return the primary url
    */
  @ApiModelProperty(example = "", value = "The primary ingest URL of the custom stream target.")
  public String getPrimaryUrl() {
    return primaryUrl;
  }

  /**
   * Sets the primary url.
   *
   * @param primaryUrl the new primary url
   */
  public void setPrimaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
  }

  /**
   * Provider.
   *
   * @param provider the provider
   * @return the stream target custom
   */
  public StreamTargetCustom provider(String provider) {
    this.provider = provider;
    return this;
  }

   /**
    * Gets the provider.
    *
    * @return the provider
    */
  @ApiModelProperty(example = "", value = "The CDN for the target. Values can be appended with **_mock** to use in the sandbox environment.")
  public String getProvider() {
    return provider;
  }

  /**
   * Sets the provider.
   *
   * @param provider the new provider
   */
  public void setProvider(String provider) {
    this.provider = provider;
  }

  /**
   * Rtmp playback url.
   *
   * @param rtmpPlaybackUrl the rtmp playback url
   * @return the stream target custom
   */
  public StreamTargetCustom rtmpPlaybackUrl(String rtmpPlaybackUrl) {
    this.rtmpPlaybackUrl = rtmpPlaybackUrl;
    return this;
  }

   /**
    * Gets the rtmp playback url.
    *
    * @return the rtmp playback url
    */
  @ApiModelProperty(example = "", value = "The web address that the custom stream target uses to play RTMP streams.")
  public String getRtmpPlaybackUrl() {
    return rtmpPlaybackUrl;
  }

  /**
   * Sets the rtmp playback url.
   *
   * @param rtmpPlaybackUrl the new rtmp playback url
   */
  public void setRtmpPlaybackUrl(String rtmpPlaybackUrl) {
    this.rtmpPlaybackUrl = rtmpPlaybackUrl;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the stream target custom
   */
  public StreamTargetCustom streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "", value = "The name of the stream being ingested into the target.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the stream target custom
   */
  public StreamTargetCustom updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the custom stream target was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Username.
   *
   * @param username the username
   * @return the stream target custom
   */
  public StreamTargetCustom username(String username) {
    this.username = username;
    return this;
  }

   /**
    * Gets the username.
    *
    * @return the username
    */
  @ApiModelProperty(example = "", value = "Only for custom stream targets whose <em>provider</em> is <em>not</em> **akamai_cupertino**. The username or ID that the target uses for RTMP authentication.")
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username.
   *
   * @param username the new username
   */
  public void setUsername(String username) {
    this.username = username;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetCustom streamTargetCustom = (StreamTargetCustom) o;
    return Objects.equals(this.backupUrl, streamTargetCustom.backupUrl) &&
        Objects.equals(this.createdAt, streamTargetCustom.createdAt) &&
        Objects.equals(this.hdsPlaybackUrl, streamTargetCustom.hdsPlaybackUrl) &&
        Objects.equals(this.hlsPlaybackUrl, streamTargetCustom.hlsPlaybackUrl) &&
        Objects.equals(this.id, streamTargetCustom.id) &&
        Objects.equals(this.name, streamTargetCustom.name) &&
        Objects.equals(this.password, streamTargetCustom.password) &&
        Objects.equals(this.primaryUrl, streamTargetCustom.primaryUrl) &&
        Objects.equals(this.provider, streamTargetCustom.provider) &&
        Objects.equals(this.rtmpPlaybackUrl, streamTargetCustom.rtmpPlaybackUrl) &&
        Objects.equals(this.streamName, streamTargetCustom.streamName) &&
        Objects.equals(this.updatedAt, streamTargetCustom.updatedAt) &&
        Objects.equals(this.username, streamTargetCustom.username);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(backupUrl, createdAt, hdsPlaybackUrl, hlsPlaybackUrl, id, name, password, primaryUrl, provider, rtmpPlaybackUrl, streamName, updatedAt, username);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetCustom {\n");
    
    sb.append("    backupUrl: ").append(toIndentedString(backupUrl)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    hdsPlaybackUrl: ").append(toIndentedString(hdsPlaybackUrl)).append("\n");
    sb.append("    hlsPlaybackUrl: ").append(toIndentedString(hlsPlaybackUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    primaryUrl: ").append(toIndentedString(primaryUrl)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    rtmpPlaybackUrl: ").append(toIndentedString(rtmpPlaybackUrl)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

