/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Schedule;
import java.io.IOException;

/**
 * The Class ScheduleCreateInput.
 */
@ApiModel(description = "")
public class ScheduleCreateInput {
  
  /** The schedule. */
  @SerializedName("schedule")
  private Schedule schedule = null;

  /**
   * Schedule.
   *
   * @param schedule the schedule
   * @return the schedule create input
   */
  public ScheduleCreateInput schedule(Schedule schedule) {
    this.schedule = schedule;
    return this;
  }

   /**
    * Gets the schedule.
    *
    * @return the schedule
    */
  @ApiModelProperty(required = true, value = "")
  public Schedule getSchedule() {
    return schedule;
  }

  /**
   * Sets the schedule.
   *
   * @param schedule the new schedule
   */
  public void setSchedule(Schedule schedule) {
    this.schedule = schedule;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduleCreateInput scheduleCreateInput = (ScheduleCreateInput) o;
    return Objects.equals(this.schedule, scheduleCreateInput.schedule);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(schedule);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduleCreateInput {\n");
    
    sb.append("    schedule: ").append(toIndentedString(schedule)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

