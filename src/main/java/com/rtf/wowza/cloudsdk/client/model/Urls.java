/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Url;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Urls.
 */
@ApiModel(description = "")
public class Urls {
  
  /** The urls. */
  @SerializedName("urls")
  private List<Url> urls = new ArrayList<Url>();

  /**
   * Urls.
   *
   * @param urls the urls
   * @return the urls
   */
  public Urls urls(List<Url> urls) {
    this.urls = urls;
    return this;
  }

  /**
   * Adds the urls item.
   *
   * @param urlsItem the urls item
   * @return the urls
   */
  public Urls addUrlsItem(Url urlsItem) {
    this.urls.add(urlsItem);
    return this;
  }

   /**
    * Gets the urls.
    *
    * @return the urls
    */
  @ApiModelProperty(required = true, value = "")
  public List<Url> getUrls() {
    return urls;
  }

  /**
   * Sets the urls.
   *
   * @param urls the new urls
   */
  public void setUrls(List<Url> urls) {
    this.urls = urls;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Urls urls = (Urls) o;
    return Objects.equals(this.urls, urls.urls);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(urls);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Urls {\n");
    
    sb.append("    urls: ").append(toIndentedString(urls)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

