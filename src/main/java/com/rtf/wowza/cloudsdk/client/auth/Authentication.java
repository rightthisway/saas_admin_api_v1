/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.auth;

import com.rtf.wowza.cloudsdk.client.Pair;

import java.util.Map;
import java.util.List;

/**
 * The Interface Authentication.
 */
public interface Authentication {
    
    /**
     * Apply to params.
     *
     * @param queryParams the query params
     * @param headerParams the header params
     */
    void applyToParams(List<Pair> queryParams, Map<String, String> headerParams);
}
