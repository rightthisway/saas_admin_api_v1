/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.OutputStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class OutputStreamTargets.
 */
@ApiModel(description = "")
public class OutputStreamTargets {
  
  /** The output stream targets. */
  @SerializedName("output_stream_targets")
  private List<OutputStreamTarget> outputStreamTargets = new ArrayList<OutputStreamTarget>();

  /**
   * Output stream targets.
   *
   * @param outputStreamTargets the output stream targets
   * @return the output stream targets
   */
  public OutputStreamTargets outputStreamTargets(List<OutputStreamTarget> outputStreamTargets) {
    this.outputStreamTargets = outputStreamTargets;
    return this;
  }

  /**
   * Adds the output stream targets item.
   *
   * @param outputStreamTargetsItem the output stream targets item
   * @return the output stream targets
   */
  public OutputStreamTargets addOutputStreamTargetsItem(OutputStreamTarget outputStreamTargetsItem) {
    this.outputStreamTargets.add(outputStreamTargetsItem);
    return this;
  }

   /**
    * Gets the output stream targets.
    *
    * @return the output stream targets
    */
  @ApiModelProperty(required = true, value = "")
  public List<OutputStreamTarget> getOutputStreamTargets() {
    return outputStreamTargets;
  }

  /**
   * Sets the output stream targets.
   *
   * @param outputStreamTargets the new output stream targets
   */
  public void setOutputStreamTargets(List<OutputStreamTarget> outputStreamTargets) {
    this.outputStreamTargets = outputStreamTargets;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutputStreamTargets outputStreamTargets = (OutputStreamTargets) o;
    return Objects.equals(this.outputStreamTargets, outputStreamTargets.outputStreamTargets);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(outputStreamTargets);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OutputStreamTargets {\n");
    
    sb.append("    outputStreamTargets: ").append(toIndentedString(outputStreamTargets)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

