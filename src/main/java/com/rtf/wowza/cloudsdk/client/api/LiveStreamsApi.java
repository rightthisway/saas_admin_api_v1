/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.rtf.wowza.cloudsdk.client.ApiCallback;
import com.rtf.wowza.cloudsdk.client.ApiClient;
import com.rtf.wowza.cloudsdk.client.ApiException;
import com.rtf.wowza.cloudsdk.client.ApiResponse;
import com.rtf.wowza.cloudsdk.client.Configuration;
import com.rtf.wowza.cloudsdk.client.Pair;
import com.rtf.wowza.cloudsdk.client.ProgressRequestBody;
import com.rtf.wowza.cloudsdk.client.ProgressResponseBody;
import com.rtf.wowza.cloudsdk.client.model.LiveStream;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamActionState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamConnectioncode;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateActionState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateConnectioncode;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateInput;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateMetrics;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamCreateThumbnail;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamThumbnail;
import com.rtf.wowza.cloudsdk.client.model.LiveStreams;
import com.rtf.wowza.cloudsdk.client.model.ShmMetrics;

/**
 * The Class LiveStreamsApi.
 */
public class LiveStreamsApi {
    
    /** The api client. */
    private ApiClient apiClient;

    /**
     * Instantiates a new live streams api.
     */
    public LiveStreamsApi() {
        this(Configuration.getDefaultApiClient());
    }

    /**
     * Instantiates a new live streams api.
     *
     * @param apiClient the api client
     */
    public LiveStreamsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Gets the api client.
     *
     * @return the api client
     */
    public ApiClient getApiClient() {
        return apiClient;
    }

    /**
     * Sets the api client.
     *
     * @param apiClient the new api client
     */
    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Creates the live stream call.
     *
     * @param liveStream the live stream
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createLiveStreamCall(LiveStreamCreateInput liveStream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = liveStream;

        // create path and map variables
        String localVarPath = "/live_streams";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the live stream validate before call.
     *
     * @param liveStream the live stream
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createLiveStreamValidateBeforeCall(LiveStreamCreateInput liveStream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'liveStream' is set
        if (liveStream == null) {
            throw new ApiException("Missing the required parameter 'liveStream' when calling createLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = createLiveStreamCall(liveStream, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the live stream.
     *
     * @param liveStream the live stream
     * @return the live stream
     * @throws ApiException the api exception
     */
    public LiveStream createLiveStream(LiveStream liveStream) throws ApiException {
	LiveStreamCreateInput liveStreamInput = new LiveStreamCreateInput();
	liveStreamInput.setLiveStream(liveStream);
	LiveStreamCreateInput liveStreamOutput = createLiveStreamTransport(liveStreamInput);
	return liveStreamOutput.getLiveStream();
	}

    /**
     * Creates the live stream transport.
     *
     * @param liveStreamCreateInput the live stream create input
     * @return the live stream create input
     * @throws ApiException the api exception
     */
    public LiveStreamCreateInput createLiveStreamTransport(LiveStreamCreateInput liveStreamCreateInput) throws ApiException {
        ApiResponse<LiveStreamCreateInput> resp = createLiveStreamWithHttpInfo(liveStreamCreateInput);
        return resp.getData();
    }

    /**
     * Creates the live stream with http info.
     *
     * @param liveStream the live stream
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateInput> createLiveStreamWithHttpInfo(LiveStreamCreateInput liveStream) throws ApiException {
        com.squareup.okhttp.Call call = createLiveStreamValidateBeforeCall(liveStream, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the live stream async.
     *
     * @param liveStream the live stream
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createLiveStreamAsync(LiveStreamCreateInput liveStream, final ApiCallback<LiveStream> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createLiveStreamValidateBeforeCall(liveStream, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Delete live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete live stream.
     *
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteLiveStream(String id) throws ApiException {
        deleteLiveStreamWithHttpInfo(id);
    }

    /**
     * Delete live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteLiveStreamValidateBeforeCall(id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteLiveStreamAsync(String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * List live streams call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listLiveStreamsCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("page", page));
        if (perPage != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("per_page", perPage));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List live streams validate before call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listLiveStreamsValidateBeforeCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = listLiveStreamsCall(page, perPage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List live streams.
     *
     * @param page the page
     * @param perPage the per page
     * @return the live streams
     * @throws ApiException the api exception
     */
    public LiveStreams listLiveStreams(Integer page, Integer perPage) throws ApiException {
        ApiResponse<LiveStreams> resp = listLiveStreamsWithHttpInfo(page, perPage);
        return resp.getData();
    }

    /**
     * List live streams with http info.
     *
     * @param page the page
     * @param perPage the per page
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreams> listLiveStreamsWithHttpInfo(Integer page, Integer perPage) throws ApiException {
        com.squareup.okhttp.Call call = listLiveStreamsValidateBeforeCall(page, perPage, null, null);
        Type localVarReturnType = new TypeToken<LiveStreams>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List live streams async.
     *
     * @param page the page
     * @param perPage the per page
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listLiveStreamsAsync(Integer page, Integer perPage, final ApiCallback<LiveStreams> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listLiveStreamsValidateBeforeCall(page, perPage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreams>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Regenerate connection code live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call regenerateConnectionCodeLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/regenerate_connection_code"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Regenerate connection code live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call regenerateConnectionCodeLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling regenerateConnectionCodeLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = regenerateConnectionCodeLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Regenerate connection code live stream.
     *
     * @param id the id
     * @return the live stream connectioncode
     * @throws ApiException the api exception
     */
    public LiveStreamConnectioncode regenerateConnectionCodeLiveStream(String id) throws ApiException {
        LiveStreamCreateConnectioncode resp = regenerateConnectionCodeLiveStreamTransport(id);
        return resp.getLiveStreamConnectioncode();
    }

    /**
     * Regenerate connection code live stream transport.
     *
     * @param id the id
     * @return the live stream create connectioncode
     * @throws ApiException the api exception
     */
    public LiveStreamCreateConnectioncode regenerateConnectionCodeLiveStreamTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateConnectioncode> resp = regenerateConnectionCodeLiveStreamWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Regenerate connection code live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateConnectioncode> regenerateConnectionCodeLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = regenerateConnectionCodeLiveStreamValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateConnectioncode>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Regenerate connection code live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call regenerateConnectionCodeLiveStreamAsync(String id, final ApiCallback<LiveStreamCreateConnectioncode> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = regenerateConnectionCodeLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateConnectioncode>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Reset live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call resetLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/reset"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Reset live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call resetLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling resetLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = resetLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Reset live stream.
     *
     * @param id the id
     * @return the live stream action state
     * @throws ApiException the api exception
     */
    public LiveStreamActionState resetLiveStream(String id) throws ApiException {
        LiveStreamCreateActionState resp = resetLiveStreamTransport(id);
        return resp.getLiveStreamActionState();
    }

    /**
     * Reset live stream transport.
     *
     * @param id the id
     * @return the live stream create action state
     * @throws ApiException the api exception
     */
    public LiveStreamCreateActionState resetLiveStreamTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateActionState> resp = resetLiveStreamWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Reset live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateActionState> resetLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = resetLiveStreamValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Reset live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call resetLiveStreamAsync(String id, final ApiCallback<LiveStreamCreateActionState> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = resetLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = showLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show live stream.
     *
     * @param id the id
     * @return the live stream
     * @throws ApiException the api exception
     */
    public LiveStream showLiveStream(String id) throws ApiException {
        LiveStreamCreateInput output = showLiveStreamTransport(id);
        return output.getLiveStream();
    }


    /**
     * Show live stream transport.
     *
     * @param id the id
     * @return the live stream create input
     * @throws ApiException the api exception
     */
    public LiveStreamCreateInput showLiveStreamTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateInput> resp = showLiveStreamWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateInput> showLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showLiveStreamValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamAsync(String id, final ApiCallback<LiveStream> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show live stream state call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamStateCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/state"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show live stream state validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showLiveStreamStateValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showLiveStreamState(Async)");
        }
        

        com.squareup.okhttp.Call call = showLiveStreamStateCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show live stream state.
     *
     * @param id the id
     * @return the live stream state
     * @throws ApiException the api exception
     */
    public LiveStreamState showLiveStreamState(String id) throws ApiException {
        LiveStreamCreateState resp = showLiveStreamStateTransport(id);
        return resp.getLiveStreamState();
    }

    /**
     * Show live stream state transport.
     *
     * @param id the id
     * @return the live stream create state
     * @throws ApiException the api exception
     */
    public LiveStreamCreateState showLiveStreamStateTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateState> resp = showLiveStreamStateWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show live stream state with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateState> showLiveStreamStateWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showLiveStreamStateValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateState>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show live stream state async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamStateAsync(String id, final ApiCallback<LiveStreamCreateState> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showLiveStreamStateValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateState>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show live stream stats call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamStatsCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/stats"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show live stream stats validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showLiveStreamStatsValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showLiveStreamStats(Async)");
        }
        

        com.squareup.okhttp.Call call = showLiveStreamStatsCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show live stream stats.
     *
     * @param id the id
     * @return the shm metrics
     * @throws ApiException the api exception
     */
    public ShmMetrics showLiveStreamStats(String id) throws ApiException {
        LiveStreamCreateMetrics resp = showLiveStreamStatsTransport(id);
        return resp.getLiveStreamMetrics();
    }

    /**
     * Show live stream stats transport.
     *
     * @param id the id
     * @return the live stream create metrics
     * @throws ApiException the api exception
     */
    public LiveStreamCreateMetrics showLiveStreamStatsTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateMetrics> resp = showLiveStreamStatsWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show live stream stats with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateMetrics> showLiveStreamStatsWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showLiveStreamStatsValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateMetrics>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show live stream stats async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamStatsAsync(String id, final ApiCallback<LiveStreamCreateMetrics> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showLiveStreamStatsValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateMetrics>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show live stream thumbnail url call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamThumbnailUrlCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/thumbnail_url"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show live stream thumbnail url validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showLiveStreamThumbnailUrlValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showLiveStreamThumbnailUrl(Async)");
        }
        

        com.squareup.okhttp.Call call = showLiveStreamThumbnailUrlCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show live stream thumbnail url.
     *
     * @param id the id
     * @return the live stream thumbnail
     * @throws ApiException the api exception
     */
    public LiveStreamThumbnail showLiveStreamThumbnailUrl(String id) throws ApiException {
        LiveStreamCreateThumbnail liveStreamThumbnail = showLiveStreamThumbnailUrlTransport(id);
	return liveStreamThumbnail.getLiveStreamThumbnailUrl();
    }

    /**
     * Show live stream thumbnail url transport.
     *
     * @param id the id
     * @return the live stream create thumbnail
     * @throws ApiException the api exception
     */
    public LiveStreamCreateThumbnail showLiveStreamThumbnailUrlTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateThumbnail> resp = showLiveStreamThumbnailUrlWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show live stream thumbnail url with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateThumbnail> showLiveStreamThumbnailUrlWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showLiveStreamThumbnailUrlValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateThumbnail>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show live stream thumbnail url async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showLiveStreamThumbnailUrlAsync(String id, final ApiCallback<LiveStreamCreateThumbnail> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showLiveStreamThumbnailUrlValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateThumbnail>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Start live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call startLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/start"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Start live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call startLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling startLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = startLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Start live stream.
     *
     * @param id the id
     * @return the live stream action state
     * @throws ApiException the api exception
     */
    public LiveStreamActionState startLiveStream(String id) throws ApiException {
        LiveStreamCreateActionState resp = startLiveStreamWithHttpInfoTransport(id);
        return resp.getLiveStreamActionState();
    }

    /**
     * Start live stream with http info transport.
     *
     * @param id the id
     * @return the live stream create action state
     * @throws ApiException the api exception
     */
    public LiveStreamCreateActionState startLiveStreamWithHttpInfoTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateActionState> resp = startLiveStreamWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Start live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateActionState> startLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = startLiveStreamValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Start live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call startLiveStreamAsync(String id, final ApiCallback<LiveStreamCreateActionState> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = startLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Stop live stream call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call stopLiveStreamCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/live_streams/{id}/stop"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Stop live stream validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call stopLiveStreamValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling stopLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = stopLiveStreamCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Stop live stream.
     *
     * @param id the id
     * @return the live stream action state
     * @throws ApiException the api exception
     */
    public LiveStreamActionState stopLiveStream(String id) throws ApiException {
        LiveStreamCreateActionState resp = stopLiveStreamTransport(id);
        return resp.getLiveStreamActionState();
    }

    /**
     * Stop live stream transport.
     *
     * @param id the id
     * @return the live stream create action state
     * @throws ApiException the api exception
     */
    public LiveStreamCreateActionState stopLiveStreamTransport(String id) throws ApiException {
        ApiResponse<LiveStreamCreateActionState> resp = stopLiveStreamWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Stop live stream with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateActionState> stopLiveStreamWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = stopLiveStreamValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Stop live stream async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call stopLiveStreamAsync(String id, final ApiCallback<LiveStreamCreateActionState> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = stopLiveStreamValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateActionState>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update live stream call.
     *
     * @param id the id
     * @param liveStream the live stream
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateLiveStreamCall(String id, LiveStreamCreateInput liveStream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = liveStream;

        // create path and map variables
        String localVarPath = "/live_streams/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update live stream validate before call.
     *
     * @param id the id
     * @param liveStream the live stream
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateLiveStreamValidateBeforeCall(String id, LiveStreamCreateInput liveStream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling updateLiveStream(Async)");
        }
        
        // verify the required parameter 'liveStream' is set
        if (liveStream == null) {
            throw new ApiException("Missing the required parameter 'liveStream' when calling updateLiveStream(Async)");
        }
        

        com.squareup.okhttp.Call call = updateLiveStreamCall(id, liveStream, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update live stream.
     *
     * @param id the id
     * @param liveStream the live stream
     * @return the live stream
     * @throws ApiException the api exception
     */
    public LiveStream updateLiveStream(String id, LiveStream liveStream) throws ApiException {
        LiveStreamCreateInput resp = updateLiveStreamTransport(id, liveStream);
        return resp.getLiveStream();
    }

    /**
     * Update live stream transport.
     *
     * @param id the id
     * @param liveStream the live stream
     * @return the live stream create input
     * @throws ApiException the api exception
     */
    public LiveStreamCreateInput updateLiveStreamTransport(String id, LiveStream liveStream) throws ApiException {
	LiveStreamCreateInput liveStreamCreate = new LiveStreamCreateInput();
	liveStreamCreate.setLiveStream(liveStream);
        ApiResponse<LiveStreamCreateInput> resp = updateLiveStreamWithHttpInfo(id, liveStreamCreate);
        return resp.getData();
    }

    /**
     * Update live stream with http info.
     *
     * @param id the id
     * @param liveStream the live stream
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<LiveStreamCreateInput> updateLiveStreamWithHttpInfo(String id, LiveStreamCreateInput liveStream) throws ApiException {
        com.squareup.okhttp.Call call = updateLiveStreamValidateBeforeCall(id, liveStream, null, null);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update live stream async.
     *
     * @param id the id
     * @param liveStream the live stream
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateLiveStreamAsync(String id, LiveStreamCreateInput liveStream, final ApiCallback<LiveStreamCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateLiveStreamValidateBeforeCall(id, liveStream, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LiveStreamCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
