/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Uptime;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Uptimes.
 */
@ApiModel(description = "")
public class Uptimes {
  
  /** The uptimes. */
  @SerializedName("uptimes")
  private List<Uptime> uptimes = new ArrayList<Uptime>();

  /**
   * Uptimes.
   *
   * @param uptimes the uptimes
   * @return the uptimes
   */
  public Uptimes uptimes(List<Uptime> uptimes) {
    this.uptimes = uptimes;
    return this;
  }

  /**
   * Adds the uptimes item.
   *
   * @param uptimesItem the uptimes item
   * @return the uptimes
   */
  public Uptimes addUptimesItem(Uptime uptimesItem) {
    this.uptimes.add(uptimesItem);
    return this;
  }

   /**
    * Gets the uptimes.
    *
    * @return the uptimes
    */
  @ApiModelProperty(required = true, value = "")
  public List<Uptime> getUptimes() {
    return uptimes;
  }

  /**
   * Sets the uptimes.
   *
   * @param uptimes the new uptimes
   */
  public void setUptimes(List<Uptime> uptimes) {
    this.uptimes = uptimes;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Uptimes uptimes = (Uptimes) o;
    return Objects.equals(this.uptimes, uptimes.uptimes);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(uptimes);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Uptimes {\n");
    
    sb.append("    uptimes: ").append(toIndentedString(uptimes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

