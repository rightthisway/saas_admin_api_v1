/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamActionState;
import java.io.IOException;

/**
 * The Class LiveStreamCreateActionState.
 */
public class LiveStreamCreateActionState {
  
  /** The live stream action state. */
  @SerializedName("live_stream")
  private LiveStreamActionState liveStreamActionState = null;

  /**
   * Live stream action state.
   *
   * @param liveStreamActionState the live stream action state
   * @return the live stream create action state
   */
  public LiveStreamCreateActionState liveStreamActionState(LiveStreamActionState liveStreamActionState) {
    this.liveStreamActionState = liveStreamActionState;
    return this;
  }

   /**
    * Gets the live stream action state.
    *
    * @return the live stream action state
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStreamActionState getLiveStreamActionState() {
    return liveStreamActionState;
  }

  /**
   * Sets the live stream action state.
   *
   * @param liveStreamActionState the new live stream action state
   */
  public void setLiveStreamActionState(LiveStreamActionState liveStreamActionState) {
    this.liveStreamActionState = liveStreamActionState;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateActionState inlineResponse2003 = (LiveStreamCreateActionState) o;
    return Objects.equals(this.liveStreamActionState, inlineResponse2003.liveStreamActionState);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreamActionState);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateActionState {\n");
    
    sb.append("    liveStreamActionState: ").append(toIndentedString(liveStreamActionState)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

