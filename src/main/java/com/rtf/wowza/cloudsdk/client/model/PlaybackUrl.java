/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class PlaybackUrl.
 */
public class PlaybackUrl {
  
  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The output id. */
  @SerializedName("output_id")
  private String outputId = null;

  /** The url. */
  @SerializedName("url")
  private String url = null;

  /**
   * Name.
   *
   * @param name the name
   * @return the playback url
   */
  public PlaybackUrl name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "hls", value = "The name of the playback URL: <strong>source</strong>, <strong>default</strong>, <strong>passthrough</strong>, or the output rendition's resolution.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Output id.
   *
   * @param outputId the output id
   * @return the playback url
   */
  public PlaybackUrl outputId(String outputId) {
    this.outputId = outputId;
    return this;
  }

   /**
    * Gets the output id.
    *
    * @return the output id
    */
  @ApiModelProperty(example = "dcxq5q6c", value = "Only for output rendition-based playback URLs, not source playback URLs. The unique alphanumeric string that identifies the output rendition.")
  public String getOutputId() {
    return outputId;
  }

  /**
   * Sets the output id.
   *
   * @param outputId the new output id
   */
  public void setOutputId(String outputId) {
    this.outputId = outputId;
  }

  /**
   * Url.
   *
   * @param url the url
   * @return the playback url
   */
  public PlaybackUrl url(String url) {
    this.url = url;
    return this;
  }

   /**
    * Gets the url.
    *
    * @return the url
    */
  @ApiModelProperty(example = "https://abcdef.dev.entrypoint.cloud.wowza.com/app-16ea/ngrp:43a23e5a_all/playlist.m3u8", value = "The playback URL for the source or output rendition.")
  public String getUrl() {
    return url;
  }

  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(String url) {
    this.url = url;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PlaybackUrl playbackUrl = (PlaybackUrl) o;
    return Objects.equals(this.name, playbackUrl.name) &&
        Objects.equals(this.outputId, playbackUrl.outputId) &&
        Objects.equals(this.url, playbackUrl.url);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, outputId, url);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PlaybackUrl {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    outputId: ").append(toIndentedString(outputId)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

