/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class StreamTargetConnectioncode.
 */
public class StreamTargetConnectioncode {
  
  /** The connection code. */
  @SerializedName("connection_code")
  private String connectionCode = null;

  /**
   * Connection code.
   *
   * @param connectionCode the connection code
   * @return the stream target connectioncode
   */
  public StreamTargetConnectioncode connectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
    return this;
  }

   /**
    * Gets the connection code.
    *
    * @return the connection code
    */
  @ApiModelProperty(example = "0cd2e8", value = "A six-character, alphanumeric string that allows select encoders, such as Wowza Streaming Engine or the Wowza GoCoder app, to send an encoded stream to a stream target in Wowza Streaming Cloud. The code can be used once and expires 24 hours after it's created.")
  public String getConnectionCode() {
    return connectionCode;
  }

  /**
   * Sets the connection code.
   *
   * @param connectionCode the new connection code
   */
  public void setConnectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetConnectioncode streamTarget1 = (StreamTargetConnectioncode) o;
    return Objects.equals(this.connectionCode, streamTarget1.connectionCode);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(connectionCode);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetConnectioncode {\n");
    
    sb.append("    connectionCode: ").append(toIndentedString(connectionCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

