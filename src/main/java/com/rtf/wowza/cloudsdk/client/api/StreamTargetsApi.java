/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.api;

import com.rtf.wowza.cloudsdk.client.ApiCallback;
import com.rtf.wowza.cloudsdk.client.ApiClient;
import com.rtf.wowza.cloudsdk.client.ApiException;
import com.rtf.wowza.cloudsdk.client.ApiResponse;
import com.rtf.wowza.cloudsdk.client.Configuration;
import com.rtf.wowza.cloudsdk.client.Pair;
import com.rtf.wowza.cloudsdk.client.ProgressRequestBody;
import com.rtf.wowza.cloudsdk.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import com.rtf.wowza.cloudsdk.client.model.CustomStreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.GeoblockCreateInput;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetCustom;
import com.rtf.wowza.cloudsdk.client.model.CustomStreamTargetInput;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetUll;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetWowza;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetCreateConnectioncode;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetConnectioncode;
import com.rtf.wowza.cloudsdk.client.model.Geoblock;
import com.rtf.wowza.cloudsdk.client.model.TokenAuth;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetMetricsResponse;
import com.rtf.wowza.cloudsdk.client.model.IndexStreamTarget;
import com.rtf.wowza.cloudsdk.client.model.StreamTarget;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetProperties;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetPropertyCreateInput;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.StreamTargets;
import com.rtf.wowza.cloudsdk.client.model.IndexCustomStreamTarget;
import com.rtf.wowza.cloudsdk.client.model.IndexUllStreamTarget;
import com.rtf.wowza.cloudsdk.client.model.IndexWowzaStreamTarget;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetsCustom;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetsUll;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetsWowza;
import com.rtf.wowza.cloudsdk.client.model.TokenAuthCreateInput;
import com.rtf.wowza.cloudsdk.client.model.UllStreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.UllStreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.WowzaStreamTargetCreateInput;
import com.rtf.wowza.cloudsdk.client.model.WowzaStreamTargetCreateInput;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class StreamTargetsApi.
 */
public class StreamTargetsApi {
    
    /** The api client. */
    private ApiClient apiClient;

    /**
     * Instantiates a new stream targets api.
     */
    public StreamTargetsApi() {
        this(Configuration.getDefaultApiClient());
    }

    /**
     * Instantiates a new stream targets api.
     *
     * @param apiClient the api client
     */
    public StreamTargetsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Gets the api client.
     *
     * @return the api client
     */
    public ApiClient getApiClient() {
        return apiClient;
    }

    /**
     * Sets the api client.
     *
     * @param apiClient the new api client
     */
    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Creates the custom stream target call.
     *
     * @param streamTargetCustom the stream target custom
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createCustomStreamTargetCall(CustomStreamTargetCreateInput streamTargetCustom, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetCustom;

        // create path and map variables
        String localVarPath = "/stream_targets/custom";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the custom stream target validate before call.
     *
     * @param streamTargetCustom the stream target custom
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createCustomStreamTargetValidateBeforeCall(CustomStreamTargetCreateInput streamTargetCustom, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetCustom' is set
        if (streamTargetCustom == null) {
            throw new ApiException("Missing the required parameter 'streamTargetCustom' when calling createCustomStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = createCustomStreamTargetCall(streamTargetCustom, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the custom stream target.
     *
     * @param streamTargetCustom the stream target custom
     * @return the stream target custom
     * @throws ApiException the api exception
     */
    public StreamTargetCustom createCustomStreamTarget(StreamTargetCustom streamTargetCustom) throws ApiException {
	CustomStreamTargetCreateInput input = new CustomStreamTargetCreateInput();
	input.setStreamTargetCustom(streamTargetCustom);
        CustomStreamTargetInput resp = createCustomStreamTargetTransport(input);
        return resp.getStreamTargetCustom();
    }

    /**
     * Creates the custom stream target transport.
     *
     * @param streamTargetCustom the stream target custom
     * @return the custom stream target input
     * @throws ApiException the api exception
     */
    public CustomStreamTargetInput createCustomStreamTargetTransport(CustomStreamTargetCreateInput streamTargetCustom) throws ApiException {
        ApiResponse<CustomStreamTargetInput> resp = createCustomStreamTargetWithHttpInfo(streamTargetCustom);
        return resp.getData();
    }

    /**
     * Creates the custom stream target with http info.
     *
     * @param streamTargetCustom the stream target custom
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<CustomStreamTargetInput> createCustomStreamTargetWithHttpInfo(CustomStreamTargetCreateInput streamTargetCustom) throws ApiException {
        com.squareup.okhttp.Call call = createCustomStreamTargetValidateBeforeCall(streamTargetCustom, null, null);
        Type localVarReturnType = new TypeToken<CustomStreamTargetInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the custom stream target async.
     *
     * @param streamTargetCustom the stream target custom
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createCustomStreamTargetAsync(CustomStreamTargetCreateInput streamTargetCustom, final ApiCallback<CustomStreamTargetInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createCustomStreamTargetValidateBeforeCall(streamTargetCustom, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CustomStreamTargetInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the stream target call.
     *
     * @param streamTarget the stream target
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetCall(StreamTargetCreateInput streamTarget, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTarget;

        // create path and map variables
        String localVarPath = "/stream_targets ";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the stream target validate before call.
     *
     * @param streamTarget the stream target
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createStreamTargetValidateBeforeCall(StreamTargetCreateInput streamTarget, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTarget' is set
        if (streamTarget == null) {
            throw new ApiException("Missing the required parameter 'streamTarget' when calling createStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = createStreamTargetCall(streamTarget, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the stream target.
     *
     * @param streamTarget the stream target
     * @return the stream target
     * @throws ApiException the api exception
     */
    public StreamTarget createStreamTarget(StreamTargetCreateInput streamTarget) throws ApiException {
        ApiResponse<StreamTarget> resp = createStreamTargetWithHttpInfo(streamTarget);
        return resp.getData();
    }

    /**
     * Creates the stream target with http info.
     *
     * @param streamTarget the stream target
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTarget> createStreamTargetWithHttpInfo(StreamTargetCreateInput streamTarget) throws ApiException {
        com.squareup.okhttp.Call call = createStreamTargetValidateBeforeCall(streamTarget, null, null);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the stream target async.
     *
     * @param streamTarget the stream target
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetAsync(StreamTargetCreateInput streamTarget, final ApiCallback<StreamTarget> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createStreamTargetValidateBeforeCall(streamTarget, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the stream target geoblock call.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetGeoblockCall(String streamTargetId, GeoblockCreateInput geoblock, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = geoblock;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/geoblock"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the stream target geoblock validate before call.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createStreamTargetGeoblockValidateBeforeCall(String streamTargetId, GeoblockCreateInput geoblock, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling createStreamTargetGeoblock(Async)");
        }
        
        // verify the required parameter 'geoblock' is set
        if (geoblock == null) {
            throw new ApiException("Missing the required parameter 'geoblock' when calling createStreamTargetGeoblock(Async)");
        }
        

        com.squareup.okhttp.Call call = createStreamTargetGeoblockCall(streamTargetId, geoblock, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the stream target geoblock.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the geoblock
     * @throws ApiException the api exception
     */
    public Geoblock createStreamTargetGeoblock(String streamTargetId, Geoblock geoblock) throws ApiException {
	GeoblockCreateInput input = new GeoblockCreateInput();
	input.setGeoblock(geoblock);
        GeoblockCreateInput resp = createStreamTargetGeoblockTransport(streamTargetId, input);
        return resp.getGeoblock();
    }

    /**
     * Creates the stream target geoblock transport.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the geoblock create input
     * @throws ApiException the api exception
     */
    public GeoblockCreateInput createStreamTargetGeoblockTransport(String streamTargetId, GeoblockCreateInput geoblock) throws ApiException {
        ApiResponse<GeoblockCreateInput> resp = createStreamTargetGeoblockWithHttpInfo(streamTargetId, geoblock);
        return resp.getData();
    }

    /**
     * Creates the stream target geoblock with http info.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<GeoblockCreateInput> createStreamTargetGeoblockWithHttpInfo(String streamTargetId, GeoblockCreateInput geoblock) throws ApiException {
        com.squareup.okhttp.Call call = createStreamTargetGeoblockValidateBeforeCall(streamTargetId, geoblock, null, null);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the stream target geoblock async.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetGeoblockAsync(String streamTargetId, GeoblockCreateInput geoblock, final ApiCallback<GeoblockCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createStreamTargetGeoblockValidateBeforeCall(streamTargetId, geoblock, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the stream target property call.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetPropertyCall(String streamTargetId, StreamTargetPropertyCreateInput property, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = property;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/properties"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the stream target property validate before call.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createStreamTargetPropertyValidateBeforeCall(String streamTargetId, StreamTargetPropertyCreateInput property, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling createStreamTargetProperty(Async)");
        }
        
        // verify the required parameter 'property' is set
        if (property == null) {
            throw new ApiException("Missing the required parameter 'property' when calling createStreamTargetProperty(Async)");
        }
        

        com.squareup.okhttp.Call call = createStreamTargetPropertyCall(streamTargetId, property, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the stream target property.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @return the stream target property
     * @throws ApiException the api exception
     */
    public StreamTargetProperty createStreamTargetProperty(String streamTargetId, StreamTargetProperty property) throws ApiException {
	StreamTargetPropertyCreateInput input = new StreamTargetPropertyCreateInput();
	input.setStreamTargetProperty(property);
        StreamTargetPropertyCreateInput resp = createStreamTargetPropertyTransport(streamTargetId, input);
        return resp.getStreamTargetProperty();
    }


    /**
     * Creates the stream target property transport.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @return the stream target property create input
     * @throws ApiException the api exception
     */
    public StreamTargetPropertyCreateInput createStreamTargetPropertyTransport(String streamTargetId, StreamTargetPropertyCreateInput property) throws ApiException {
        ApiResponse<StreamTargetPropertyCreateInput> resp = createStreamTargetPropertyWithHttpInfo(streamTargetId, property);
        return resp.getData();
    }

    /**
     * Creates the stream target property with http info.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetPropertyCreateInput> createStreamTargetPropertyWithHttpInfo(String streamTargetId, StreamTargetPropertyCreateInput property) throws ApiException {
        com.squareup.okhttp.Call call = createStreamTargetPropertyValidateBeforeCall(streamTargetId, property, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetPropertyCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the stream target property async.
     *
     * @param streamTargetId the stream target id
     * @param property the property
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetPropertyAsync(String streamTargetId, StreamTargetPropertyCreateInput property, final ApiCallback<StreamTargetPropertyCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createStreamTargetPropertyValidateBeforeCall(streamTargetId, property, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetPropertyCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the stream target token auth call.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetTokenAuthCall(String streamTargetId, TokenAuthCreateInput tokenAuth, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = tokenAuth;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/token_auth"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the stream target token auth validate before call.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createStreamTargetTokenAuthValidateBeforeCall(String streamTargetId, TokenAuthCreateInput tokenAuth, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling createStreamTargetTokenAuth(Async)");
        }
        
        // verify the required parameter 'tokenAuth' is set
        if (tokenAuth == null) {
            throw new ApiException("Missing the required parameter 'tokenAuth' when calling createStreamTargetTokenAuth(Async)");
        }
        

        com.squareup.okhttp.Call call = createStreamTargetTokenAuthCall(streamTargetId, tokenAuth, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the stream target token auth.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the token auth
     * @throws ApiException the api exception
     */
    public TokenAuth createStreamTargetTokenAuth(String streamTargetId, TokenAuth tokenAuth) throws ApiException {
	TokenAuthCreateInput input = new TokenAuthCreateInput();
	input.setTokenAuth(tokenAuth);
        TokenAuthCreateInput resp = createStreamTargetTokenAuthTransport(streamTargetId, input);
        return resp.getTokenAuth();
    }

    /**
     * Creates the stream target token auth transport.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the token auth create input
     * @throws ApiException the api exception
     */
    public TokenAuthCreateInput createStreamTargetTokenAuthTransport(String streamTargetId, TokenAuthCreateInput tokenAuth) throws ApiException {
        ApiResponse<TokenAuthCreateInput> resp = createStreamTargetTokenAuthWithHttpInfo(streamTargetId, tokenAuth);
        return resp.getData();
    }

    /**
     * Creates the stream target token auth with http info.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<TokenAuthCreateInput> createStreamTargetTokenAuthWithHttpInfo(String streamTargetId, TokenAuthCreateInput tokenAuth) throws ApiException {
        com.squareup.okhttp.Call call = createStreamTargetTokenAuthValidateBeforeCall(streamTargetId, tokenAuth, null, null);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the stream target token auth async.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createStreamTargetTokenAuthAsync(String streamTargetId, TokenAuthCreateInput tokenAuth, final ApiCallback<TokenAuthCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createStreamTargetTokenAuthValidateBeforeCall(streamTargetId, tokenAuth, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the ull stream target call.
     *
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createUllStreamTargetCall(UllStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetUll;

        // create path and map variables
        String localVarPath = "/stream_targets/ull";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the ull stream target validate before call.
     *
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createUllStreamTargetValidateBeforeCall(UllStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetUll' is set
        if (streamTargetUll == null) {
            throw new ApiException("Missing the required parameter 'streamTargetUll' when calling createUllStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = createUllStreamTargetCall(streamTargetUll, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the ull stream target.
     *
     * @param streamTargetCustom the stream target custom
     * @return the stream target ull
     * @throws ApiException the api exception
     */
    public StreamTargetUll createUllStreamTarget(StreamTargetUll streamTargetCustom) throws ApiException {
        UllStreamTargetCreateInput input = new UllStreamTargetCreateInput();
        input.setStreamTargetUll(streamTargetCustom);
        UllStreamTargetCreateInput resp = createUllStreamTargetTransport(input);
        return resp.getStreamTargetUll();
    }


    /**
     * Creates the ull stream target transport.
     *
     * @param streamTargetUll the stream target ull
     * @return the ull stream target create input
     * @throws ApiException the api exception
     */
    public UllStreamTargetCreateInput createUllStreamTargetTransport(UllStreamTargetCreateInput streamTargetUll) throws ApiException {
        ApiResponse<UllStreamTargetCreateInput> resp = createUllStreamTargetWithHttpInfo(streamTargetUll);
        return resp.getData();
    }

    /**
     * Creates the ull stream target with http info.
     *
     * @param streamTargetUll the stream target ull
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<UllStreamTargetCreateInput> createUllStreamTargetWithHttpInfo(UllStreamTargetCreateInput streamTargetUll) throws ApiException {
        com.squareup.okhttp.Call call = createUllStreamTargetValidateBeforeCall(streamTargetUll, null, null);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the ull stream target async.
     *
     * @param streamTargetUll the stream target ull
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createUllStreamTargetAsync(UllStreamTargetCreateInput streamTargetUll, final ApiCallback<UllStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createUllStreamTargetValidateBeforeCall(streamTargetUll, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Creates the wowza stream target call.
     *
     * @param streamTargetWowza the stream target wowza
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createWowzaStreamTargetCall(WowzaStreamTargetCreateInput streamTargetWowza, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetWowza;

        // create path and map variables
        String localVarPath = "/stream_targets/wowza";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Creates the wowza stream target validate before call.
     *
     * @param streamTargetWowza the stream target wowza
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createWowzaStreamTargetValidateBeforeCall(WowzaStreamTargetCreateInput streamTargetWowza, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetWowza' is set
        if (streamTargetWowza == null) {
            throw new ApiException("Missing the required parameter 'streamTargetWowza' when calling createWowzaStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = createWowzaStreamTargetCall(streamTargetWowza, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates the wowza stream target.
     *
     * @param streamTargetWowza the stream target wowza
     * @return the stream target wowza
     * @throws ApiException the api exception
     */
    public StreamTargetWowza createWowzaStreamTarget(StreamTargetWowza streamTargetWowza) throws ApiException {
	WowzaStreamTargetCreateInput input = new WowzaStreamTargetCreateInput();
	input.setStreamTargetWowza(streamTargetWowza);
        WowzaStreamTargetCreateInput resp = createWowzaStreamTargetTransport(input);
        return resp.getStreamTargetWowza();
    }

    /**
     * Creates the wowza stream target transport.
     *
     * @param streamTargetWowza the stream target wowza
     * @return the wowza stream target create input
     * @throws ApiException the api exception
     */
    public WowzaStreamTargetCreateInput createWowzaStreamTargetTransport(WowzaStreamTargetCreateInput streamTargetWowza) throws ApiException {
        ApiResponse<WowzaStreamTargetCreateInput> resp = createWowzaStreamTargetWithHttpInfo(streamTargetWowza);
        return resp.getData();
    }

    /**
     * Creates the wowza stream target with http info.
     *
     * @param streamTargetWowza the stream target wowza
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<WowzaStreamTargetCreateInput> createWowzaStreamTargetWithHttpInfo(WowzaStreamTargetCreateInput streamTargetWowza) throws ApiException {
        com.squareup.okhttp.Call call = createWowzaStreamTargetValidateBeforeCall(streamTargetWowza, null, null);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates the wowza stream target async.
     *
     * @param streamTargetWowza the stream target wowza
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call createWowzaStreamTargetAsync(WowzaStreamTargetCreateInput streamTargetWowza, final ApiCallback<WowzaStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createWowzaStreamTargetValidateBeforeCall(streamTargetWowza, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Delete custom stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteCustomStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/custom/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete custom stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteCustomStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteCustomStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteCustomStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete custom stream target.
     *
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteCustomStreamTarget(String id) throws ApiException {
        deleteCustomStreamTargetWithHttpInfo(id);
    }

    /**
     * Delete custom stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteCustomStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteCustomStreamTargetValidateBeforeCall(id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete custom stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteCustomStreamTargetAsync(String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteCustomStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * Delete stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete stream target.
     *
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteStreamTarget(String id) throws ApiException {
        deleteStreamTargetWithHttpInfo(id);
    }

    /**
     * Delete stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteStreamTargetValidateBeforeCall(id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteStreamTargetAsync(String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * Delete stream target property call.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteStreamTargetPropertyCall(String streamTargetId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/properties/{id}"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()))
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete stream target property validate before call.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteStreamTargetPropertyValidateBeforeCall(String streamTargetId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling deleteStreamTargetProperty(Async)");
        }
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteStreamTargetProperty(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteStreamTargetPropertyCall(streamTargetId, id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete stream target property.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteStreamTargetProperty(String streamTargetId, String id) throws ApiException {
        deleteStreamTargetPropertyWithHttpInfo(streamTargetId, id);
    }

    /**
     * Delete stream target property with http info.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteStreamTargetPropertyWithHttpInfo(String streamTargetId, String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteStreamTargetPropertyValidateBeforeCall(streamTargetId, id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete stream target property async.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteStreamTargetPropertyAsync(String streamTargetId, String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteStreamTargetPropertyValidateBeforeCall(streamTargetId, id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * Delete ull stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteUllStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/ull/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete ull stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteUllStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteUllStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteUllStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete ull stream target.
     *
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteUllStreamTarget(String id) throws ApiException {
        deleteUllStreamTargetWithHttpInfo(id);
    }

    /**
     * Delete ull stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteUllStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteUllStreamTargetValidateBeforeCall(id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete ull stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteUllStreamTargetAsync(String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteUllStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * Delete wowza stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteWowzaStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/wowza/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Delete wowza stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteWowzaStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling deleteWowzaStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = deleteWowzaStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete wowza stream target.
     *
     * @param id the id
     * @throws ApiException the api exception
     */
    public void deleteWowzaStreamTarget(String id) throws ApiException {
        deleteWowzaStreamTargetWithHttpInfo(id);
    }

    /**
     * Delete wowza stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<Void> deleteWowzaStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = deleteWowzaStreamTargetValidateBeforeCall(id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Delete wowza stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call deleteWowzaStreamTargetAsync(String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteWowzaStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    
    /**
     * List custom stream targets call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listCustomStreamTargetsCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/custom";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("page", page));
        if (perPage != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("per_page", perPage));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List custom stream targets validate before call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listCustomStreamTargetsValidateBeforeCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = listCustomStreamTargetsCall(page, perPage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List custom stream targets.
     *
     * @param page the page
     * @param perPage the per page
     * @return the list
     * @throws ApiException the api exception
     */
    public List<IndexCustomStreamTarget> listCustomStreamTargets(Integer page, Integer perPage) throws ApiException {
        StreamTargetsCustom resp = listCustomStreamTargetsTransport(page, perPage);
        return resp.getStreamTargetsCustom();
    }

    /**
     * List custom stream targets transport.
     *
     * @param page the page
     * @param perPage the per page
     * @return the stream targets custom
     * @throws ApiException the api exception
     */
    public StreamTargetsCustom listCustomStreamTargetsTransport(Integer page, Integer perPage) throws ApiException {
        ApiResponse<StreamTargetsCustom> resp = listCustomStreamTargetsWithHttpInfo(page, perPage);
        return resp.getData();
    }

    /**
     * List custom stream targets with http info.
     *
     * @param page the page
     * @param perPage the per page
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetsCustom> listCustomStreamTargetsWithHttpInfo(Integer page, Integer perPage) throws ApiException {
        com.squareup.okhttp.Call call = listCustomStreamTargetsValidateBeforeCall(page, perPage, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetsCustom>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List custom stream targets async.
     *
     * @param page the page
     * @param perPage the per page
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listCustomStreamTargetsAsync(Integer page, Integer perPage, final ApiCallback<StreamTargetsCustom> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listCustomStreamTargetsValidateBeforeCall(page, perPage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetsCustom>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * List stream target properties call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listStreamTargetPropertiesCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/properties"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List stream target properties validate before call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listStreamTargetPropertiesValidateBeforeCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling listStreamTargetProperties(Async)");
        }
        

        com.squareup.okhttp.Call call = listStreamTargetPropertiesCall(streamTargetId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List stream target properties.
     *
     * @param streamTargetId the stream target id
     * @return the list
     * @throws ApiException the api exception
     */
    public List<StreamTargetProperty> listStreamTargetProperties(String streamTargetId) throws ApiException {
        StreamTargetProperties resp = listStreamTargetPropertiesTransport(streamTargetId);
        return resp.getProperties();
    }

    /**
     * List stream target properties transport.
     *
     * @param streamTargetId the stream target id
     * @return the stream target properties
     * @throws ApiException the api exception
     */
    public StreamTargetProperties listStreamTargetPropertiesTransport(String streamTargetId) throws ApiException {
        ApiResponse<StreamTargetProperties> resp = listStreamTargetPropertiesWithHttpInfo(streamTargetId);
        return resp.getData();
    }

    /**
     * List stream target properties with http info.
     *
     * @param streamTargetId the stream target id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetProperties> listStreamTargetPropertiesWithHttpInfo(String streamTargetId) throws ApiException {
        com.squareup.okhttp.Call call = listStreamTargetPropertiesValidateBeforeCall(streamTargetId, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetProperties>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List stream target properties async.
     *
     * @param streamTargetId the stream target id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listStreamTargetPropertiesAsync(String streamTargetId, final ApiCallback<StreamTargetProperties> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listStreamTargetPropertiesValidateBeforeCall(streamTargetId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetProperties>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * List stream targets call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listStreamTargetsCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("page", page));
        if (perPage != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("per_page", perPage));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List stream targets validate before call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listStreamTargetsValidateBeforeCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = listStreamTargetsCall(page, perPage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List stream targets.
     *
     * @param page the page
     * @param perPage the per page
     * @return the list
     * @throws ApiException the api exception
     */
    public List<IndexStreamTarget> listStreamTargets(Integer page, Integer perPage) throws ApiException {
        StreamTargets resp = listStreamTargetsTransport(page, perPage);
        return resp.getStreamTargets();
    }

    /**
     * List stream targets transport.
     *
     * @param page the page
     * @param perPage the per page
     * @return the stream targets
     * @throws ApiException the api exception
     */
    public StreamTargets listStreamTargetsTransport(Integer page, Integer perPage) throws ApiException {
        ApiResponse<StreamTargets> resp = listStreamTargetsWithHttpInfo(page, perPage);
        return resp.getData();
    }

    /**
     * List stream targets with http info.
     *
     * @param page the page
     * @param perPage the per page
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargets> listStreamTargetsWithHttpInfo(Integer page, Integer perPage) throws ApiException {
        com.squareup.okhttp.Call call = listStreamTargetsValidateBeforeCall(page, perPage, null, null);
        Type localVarReturnType = new TypeToken<StreamTargets>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List stream targets async.
     *
     * @param page the page
     * @param perPage the per page
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listStreamTargetsAsync(Integer page, Integer perPage, final ApiCallback<StreamTargets> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listStreamTargetsValidateBeforeCall(page, perPage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargets>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * List ull stream targets call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listUllStreamTargetsCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/ull";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("page", page));
        if (perPage != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("per_page", perPage));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List ull stream targets validate before call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listUllStreamTargetsValidateBeforeCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = listUllStreamTargetsCall(page, perPage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List ull stream targets.
     *
     * @param page the page
     * @param perPage the per page
     * @return the list
     * @throws ApiException the api exception
     */
    public List<IndexUllStreamTarget> listUllStreamTargets(Integer page, Integer perPage) throws ApiException {
        StreamTargetsUll resp = listUllStreamTargetsTransport(page, perPage);
        return resp.getStreamTargetsUll();
    }

    /**
     * List ull stream targets transport.
     *
     * @param page the page
     * @param perPage the per page
     * @return the stream targets ull
     * @throws ApiException the api exception
     */
    public StreamTargetsUll listUllStreamTargetsTransport(Integer page, Integer perPage) throws ApiException {
        ApiResponse<StreamTargetsUll> resp = listUllStreamTargetsWithHttpInfo(page, perPage);
        return resp.getData();
    }

    /**
     * List ull stream targets with http info.
     *
     * @param page the page
     * @param perPage the per page
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetsUll> listUllStreamTargetsWithHttpInfo(Integer page, Integer perPage) throws ApiException {
        com.squareup.okhttp.Call call = listUllStreamTargetsValidateBeforeCall(page, perPage, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetsUll>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List ull stream targets async.
     *
     * @param page the page
     * @param perPage the per page
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listUllStreamTargetsAsync(Integer page, Integer perPage, final ApiCallback<StreamTargetsUll> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listUllStreamTargetsValidateBeforeCall(page, perPage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetsUll>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * List wowza stream targets call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listWowzaStreamTargetsCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/wowza";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("page", page));
        if (perPage != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("per_page", perPage));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * List wowza stream targets validate before call.
     *
     * @param page the page
     * @param perPage the per page
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listWowzaStreamTargetsValidateBeforeCall(Integer page, Integer perPage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = listWowzaStreamTargetsCall(page, perPage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * List wowza stream targets.
     *
     * @param page the page
     * @param perPage the per page
     * @return the list
     * @throws ApiException the api exception
     */
    public List<IndexWowzaStreamTarget> listWowzaStreamTargets(Integer page, Integer perPage) throws ApiException {
        StreamTargetsWowza resp = listWowzaStreamTargetsTransport(page, perPage);
        return resp.getStreamTargetsWowza();
    }

    /**
     * List wowza stream targets transport.
     *
     * @param page the page
     * @param perPage the per page
     * @return the stream targets wowza
     * @throws ApiException the api exception
     */
    public StreamTargetsWowza listWowzaStreamTargetsTransport(Integer page, Integer perPage) throws ApiException {
        ApiResponse<StreamTargetsWowza> resp = listWowzaStreamTargetsWithHttpInfo(page, perPage);
        return resp.getData();
    }

    /**
     * List wowza stream targets with http info.
     *
     * @param page the page
     * @param perPage the per page
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetsWowza> listWowzaStreamTargetsWithHttpInfo(Integer page, Integer perPage) throws ApiException {
        com.squareup.okhttp.Call call = listWowzaStreamTargetsValidateBeforeCall(page, perPage, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetsWowza>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * List wowza stream targets async.
     *
     * @param page the page
     * @param perPage the per page
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call listWowzaStreamTargetsAsync(Integer page, Integer perPage, final ApiCallback<StreamTargetsWowza> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listWowzaStreamTargetsValidateBeforeCall(page, perPage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetsWowza>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Regenerate connection code stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call regenerateConnectionCodeStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}/regenerate_connection_code"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Regenerate connection code stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call regenerateConnectionCodeStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling regenerateConnectionCodeStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = regenerateConnectionCodeStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Regenerate connection code stream target.
     *
     * @param id the id
     * @return the stream target connectioncode
     * @throws ApiException the api exception
     */
    public StreamTargetConnectioncode regenerateConnectionCodeStreamTarget(String id) throws ApiException {
        StreamTargetCreateConnectioncode resp = regenerateConnectionCodeStreamTargetTransport(id);
        return resp.getStreamTargetConnectioncode();
    }

    /**
     * Regenerate connection code stream target transport.
     *
     * @param id the id
     * @return the stream target create connectioncode
     * @throws ApiException the api exception
     */
    public StreamTargetCreateConnectioncode regenerateConnectionCodeStreamTargetTransport(String id) throws ApiException {
        ApiResponse<StreamTargetCreateConnectioncode> resp = regenerateConnectionCodeStreamTargetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Regenerate connection code stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetCreateConnectioncode> regenerateConnectionCodeStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = regenerateConnectionCodeStreamTargetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetCreateConnectioncode>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Regenerate connection code stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call regenerateConnectionCodeStreamTargetAsync(String id, final ApiCallback<StreamTargetCreateConnectioncode> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = regenerateConnectionCodeStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetCreateConnectioncode>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show custom stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showCustomStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/custom/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show custom stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showCustomStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showCustomStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = showCustomStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show custom stream target.
     *
     * @param id the id
     * @return the stream target custom
     * @throws ApiException the api exception
     */
    public StreamTargetCustom showCustomStreamTarget(String id) throws ApiException {
        CustomStreamTargetCreateInput resp = showCustomStreamTargetTransport(id);
        return resp.getStreamTargetCustom();
    }

    /**
     * Show custom stream target transport.
     *
     * @param id the id
     * @return the custom stream target create input
     * @throws ApiException the api exception
     */
    public CustomStreamTargetCreateInput showCustomStreamTargetTransport(String id) throws ApiException {
        ApiResponse<CustomStreamTargetCreateInput> resp = showCustomStreamTargetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show custom stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<CustomStreamTargetCreateInput> showCustomStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showCustomStreamTargetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<CustomStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show custom stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showCustomStreamTargetAsync(String id, final ApiCallback<CustomStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showCustomStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CustomStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target.
     *
     * @param id the id
     * @return the stream target
     * @throws ApiException the api exception
     */
    public StreamTarget showStreamTarget(String id) throws ApiException {
        ApiResponse<StreamTarget> resp = showStreamTargetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTarget> showStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetAsync(String id, final ApiCallback<StreamTarget> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target geoblock call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetGeoblockCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/geoblock"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target geoblock validate before call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetGeoblockValidateBeforeCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling showStreamTargetGeoblock(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetGeoblockCall(streamTargetId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target geoblock.
     *
     * @param streamTargetId the stream target id
     * @return the geoblock
     * @throws ApiException the api exception
     */
    public Geoblock showStreamTargetGeoblock(String streamTargetId) throws ApiException {
        GeoblockCreateInput resp = showStreamTargetGeoblockTransport(streamTargetId);
        return resp.getGeoblock();
    }

    /**
     * Show stream target geoblock transport.
     *
     * @param streamTargetId the stream target id
     * @return the geoblock create input
     * @throws ApiException the api exception
     */
    public GeoblockCreateInput showStreamTargetGeoblockTransport(String streamTargetId) throws ApiException {
        ApiResponse<GeoblockCreateInput> resp = showStreamTargetGeoblockWithHttpInfo(streamTargetId);
        return resp.getData();
    }

    /**
     * Show stream target geoblock with http info.
     *
     * @param streamTargetId the stream target id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<GeoblockCreateInput> showStreamTargetGeoblockWithHttpInfo(String streamTargetId) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetGeoblockValidateBeforeCall(streamTargetId, null, null);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target geoblock async.
     *
     * @param streamTargetId the stream target id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetGeoblockAsync(String streamTargetId, final ApiCallback<GeoblockCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetGeoblockValidateBeforeCall(streamTargetId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target metrics current call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetMetricsCurrentCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}/metrics/current"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target metrics current validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetMetricsCurrentValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showStreamTargetMetricsCurrent(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetMetricsCurrentCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target metrics current.
     *
     * @param id the id
     * @return the stream target metrics response
     * @throws ApiException the api exception
     */
    public StreamTargetMetricsResponse showStreamTargetMetricsCurrent(String id) throws ApiException {
        ApiResponse<StreamTargetMetricsResponse> resp = showStreamTargetMetricsCurrentWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show stream target metrics current with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetMetricsResponse> showStreamTargetMetricsCurrentWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetMetricsCurrentValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetMetricsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target metrics current async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetMetricsCurrentAsync(String id, final ApiCallback<StreamTargetMetricsResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetMetricsCurrentValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetMetricsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target metrics historic call.
     *
     * @param id the id
     * @param from the from
     * @param to the to
     * @param interval the interval
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetMetricsHistoricCall(String id, String from, String to, String interval, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}/metrics/historic"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (from != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("from", from));
        if (to != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("to", to));
        if (interval != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("interval", interval));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target metrics historic validate before call.
     *
     * @param id the id
     * @param from the from
     * @param to the to
     * @param interval the interval
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetMetricsHistoricValidateBeforeCall(String id, String from, String to, String interval, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showStreamTargetMetricsHistoric(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetMetricsHistoricCall(id, from, to, interval, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target metrics historic.
     *
     * @param id the id
     * @param from the from
     * @param to the to
     * @param interval the interval
     * @return the stream target metrics response
     * @throws ApiException the api exception
     */
    public StreamTargetMetricsResponse showStreamTargetMetricsHistoric(String id, String from, String to, String interval) throws ApiException {
        ApiResponse<StreamTargetMetricsResponse> resp = showStreamTargetMetricsHistoricWithHttpInfo(id, from, to, interval);
        return resp.getData();
    }

    /**
     * Show stream target metrics historic with http info.
     *
     * @param id the id
     * @param from the from
     * @param to the to
     * @param interval the interval
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetMetricsResponse> showStreamTargetMetricsHistoricWithHttpInfo(String id, String from, String to, String interval) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetMetricsHistoricValidateBeforeCall(id, from, to, interval, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetMetricsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target metrics historic async.
     *
     * @param id the id
     * @param from the from
     * @param to the to
     * @param interval the interval
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetMetricsHistoricAsync(String id, String from, String to, String interval, final ApiCallback<StreamTargetMetricsResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetMetricsHistoricValidateBeforeCall(id, from, to, interval, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetMetricsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target property call.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetPropertyCall(String streamTargetId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/properties/{id}"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()))
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target property validate before call.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetPropertyValidateBeforeCall(String streamTargetId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling showStreamTargetProperty(Async)");
        }
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showStreamTargetProperty(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetPropertyCall(streamTargetId, id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target property.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @return the stream target property
     * @throws ApiException the api exception
     */
    public StreamTargetProperty showStreamTargetProperty(String streamTargetId, String id) throws ApiException {
        StreamTargetPropertyCreateInput resp = showStreamTargetPropertyTransport(streamTargetId, id);
        return resp.getStreamTargetProperty();
    }

    /**
     * Show stream target property transport.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @return the stream target property create input
     * @throws ApiException the api exception
     */
    public StreamTargetPropertyCreateInput showStreamTargetPropertyTransport(String streamTargetId, String id) throws ApiException {
        ApiResponse<StreamTargetPropertyCreateInput> resp = showStreamTargetPropertyWithHttpInfo(streamTargetId, id);
        return resp.getData();
    }

    /**
     * Show stream target property with http info.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTargetPropertyCreateInput> showStreamTargetPropertyWithHttpInfo(String streamTargetId, String id) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetPropertyValidateBeforeCall(streamTargetId, id, null, null);
        Type localVarReturnType = new TypeToken<StreamTargetPropertyCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target property async.
     *
     * @param streamTargetId the stream target id
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetPropertyAsync(String streamTargetId, String id, final ApiCallback<StreamTargetPropertyCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetPropertyValidateBeforeCall(streamTargetId, id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTargetPropertyCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show stream target token auth call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetTokenAuthCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/token_auth"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show stream target token auth validate before call.
     *
     * @param streamTargetId the stream target id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showStreamTargetTokenAuthValidateBeforeCall(String streamTargetId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling showStreamTargetTokenAuth(Async)");
        }
        

        com.squareup.okhttp.Call call = showStreamTargetTokenAuthCall(streamTargetId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show stream target token auth.
     *
     * @param streamTargetId the stream target id
     * @return the token auth
     * @throws ApiException the api exception
     */
    public TokenAuth showStreamTargetTokenAuth(String streamTargetId) throws ApiException {
        TokenAuthCreateInput resp = showStreamTargetTokenAuthTransport(streamTargetId);
        return resp.getTokenAuth();
    }

    /**
     * Show stream target token auth transport.
     *
     * @param streamTargetId the stream target id
     * @return the token auth create input
     * @throws ApiException the api exception
     */
    public TokenAuthCreateInput showStreamTargetTokenAuthTransport(String streamTargetId) throws ApiException {
        ApiResponse<TokenAuthCreateInput> resp = showStreamTargetTokenAuthWithHttpInfo(streamTargetId);
        return resp.getData();
    }

    /**
     * Show stream target token auth with http info.
     *
     * @param streamTargetId the stream target id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<TokenAuthCreateInput> showStreamTargetTokenAuthWithHttpInfo(String streamTargetId) throws ApiException {
        com.squareup.okhttp.Call call = showStreamTargetTokenAuthValidateBeforeCall(streamTargetId, null, null);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show stream target token auth async.
     *
     * @param streamTargetId the stream target id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showStreamTargetTokenAuthAsync(String streamTargetId, final ApiCallback<TokenAuthCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showStreamTargetTokenAuthValidateBeforeCall(streamTargetId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show ull stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showUllStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/ull/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show ull stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showUllStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showUllStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = showUllStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show ull stream target.
     *
     * @param id the id
     * @return the stream target ull
     * @throws ApiException the api exception
     */
    public StreamTargetUll showUllStreamTarget(String id) throws ApiException {
        UllStreamTargetCreateInput resp = showUllStreamTargetTransport(id);
        return resp.getStreamTargetUll();
    }

    /**
     * Show ull stream target transport.
     *
     * @param id the id
     * @return the ull stream target create input
     * @throws ApiException the api exception
     */
    public UllStreamTargetCreateInput showUllStreamTargetTransport(String id) throws ApiException {
        ApiResponse<UllStreamTargetCreateInput> resp = showUllStreamTargetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show ull stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<UllStreamTargetCreateInput> showUllStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showUllStreamTargetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show ull stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showUllStreamTargetAsync(String id, final ApiCallback<UllStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showUllStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Show wowza stream target call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showWowzaStreamTargetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/stream_targets/wowza/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show wowza stream target validate before call.
     *
     * @param id the id
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call showWowzaStreamTargetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling showWowzaStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = showWowzaStreamTargetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Show wowza stream target.
     *
     * @param id the id
     * @return the stream target wowza
     * @throws ApiException the api exception
     */
    public StreamTargetWowza showWowzaStreamTarget(String id) throws ApiException {
        WowzaStreamTargetCreateInput resp = showWowzaStreamTargetTransport(id);
        return resp.getStreamTargetWowza();
    }

    /**
     * Show wowza stream target transport.
     *
     * @param id the id
     * @return the wowza stream target create input
     * @throws ApiException the api exception
     */
    public WowzaStreamTargetCreateInput showWowzaStreamTargetTransport(String id) throws ApiException {
        ApiResponse<WowzaStreamTargetCreateInput> resp = showWowzaStreamTargetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Show wowza stream target with http info.
     *
     * @param id the id
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<WowzaStreamTargetCreateInput> showWowzaStreamTargetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = showWowzaStreamTargetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show wowza stream target async.
     *
     * @param id the id
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call showWowzaStreamTargetAsync(String id, final ApiCallback<WowzaStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = showWowzaStreamTargetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update custom stream target call.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateCustomStreamTargetCall(String id, CustomStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetUll;

        // create path and map variables
        String localVarPath = "/stream_targets/custom/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update custom stream target validate before call.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateCustomStreamTargetValidateBeforeCall(String id, CustomStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling updateCustomStreamTarget(Async)");
        }
        
        // verify the required parameter 'streamTargetUll' is set
        if (streamTargetUll == null) {
            throw new ApiException("Missing the required parameter 'streamTargetUll' when calling updateCustomStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = updateCustomStreamTargetCall(id, streamTargetUll, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update custom stream target.
     *
     * @param id the id
     * @param streamTargetCustom the stream target custom
     * @return the stream target custom
     * @throws ApiException the api exception
     */
    public StreamTargetCustom updateCustomStreamTarget(String id, StreamTargetCustom streamTargetCustom) throws ApiException {
	CustomStreamTargetCreateInput input = new CustomStreamTargetCreateInput();
	input.setStreamTargetCustom(streamTargetCustom);
        CustomStreamTargetCreateInput resp = updateCustomStreamTargetTransport(id, input);
        return resp.getStreamTargetCustom();
    }

    /**
     * Update custom stream target transport.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @return the custom stream target create input
     * @throws ApiException the api exception
     */
    public CustomStreamTargetCreateInput updateCustomStreamTargetTransport(String id, CustomStreamTargetCreateInput streamTargetUll) throws ApiException {
        ApiResponse<CustomStreamTargetCreateInput> resp = updateCustomStreamTargetWithHttpInfo(id, streamTargetUll);
        return resp.getData();
    }

    /**
     * Update custom stream target with http info.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<CustomStreamTargetCreateInput> updateCustomStreamTargetWithHttpInfo(String id, CustomStreamTargetCreateInput streamTargetUll) throws ApiException {
        com.squareup.okhttp.Call call = updateCustomStreamTargetValidateBeforeCall(id, streamTargetUll, null, null);
        Type localVarReturnType = new TypeToken<CustomStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update custom stream target async.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateCustomStreamTargetAsync(String id, CustomStreamTargetCreateInput streamTargetUll, final ApiCallback<CustomStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateCustomStreamTargetValidateBeforeCall(id, streamTargetUll, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CustomStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update stream target call.
     *
     * @param id the id
     * @param streamTarget the stream target
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetCall(String id, StreamTargetCreateInput streamTarget, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTarget;

        // create path and map variables
        String localVarPath = "/stream_targets/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update stream target validate before call.
     *
     * @param id the id
     * @param streamTarget the stream target
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateStreamTargetValidateBeforeCall(String id, StreamTargetCreateInput streamTarget, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling updateStreamTarget(Async)");
        }
        
        // verify the required parameter 'streamTarget' is set
        if (streamTarget == null) {
            throw new ApiException("Missing the required parameter 'streamTarget' when calling updateStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = updateStreamTargetCall(id, streamTarget, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update stream target.
     *
     * @param id the id
     * @param streamTarget the stream target
     * @return the stream target
     * @throws ApiException the api exception
     */
    public StreamTarget updateStreamTarget(String id, StreamTargetCreateInput streamTarget) throws ApiException {
        ApiResponse<StreamTarget> resp = updateStreamTargetWithHttpInfo(id, streamTarget);
        return resp.getData();
    }

    /**
     * Update stream target with http info.
     *
     * @param id the id
     * @param streamTarget the stream target
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<StreamTarget> updateStreamTargetWithHttpInfo(String id, StreamTargetCreateInput streamTarget) throws ApiException {
        com.squareup.okhttp.Call call = updateStreamTargetValidateBeforeCall(id, streamTarget, null, null);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update stream target async.
     *
     * @param id the id
     * @param streamTarget the stream target
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetAsync(String id, StreamTargetCreateInput streamTarget, final ApiCallback<StreamTarget> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateStreamTargetValidateBeforeCall(id, streamTarget, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StreamTarget>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update stream target geoblock call.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetGeoblockCall(String streamTargetId, GeoblockCreateInput geoblock, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = geoblock;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/geoblock"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update stream target geoblock validate before call.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateStreamTargetGeoblockValidateBeforeCall(String streamTargetId, GeoblockCreateInput geoblock, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling updateStreamTargetGeoblock(Async)");
        }
        
        // verify the required parameter 'geoblock' is set
        if (geoblock == null) {
            throw new ApiException("Missing the required parameter 'geoblock' when calling updateStreamTargetGeoblock(Async)");
        }
        

        com.squareup.okhttp.Call call = updateStreamTargetGeoblockCall(streamTargetId, geoblock, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update stream target geoblock.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the geoblock
     * @throws ApiException the api exception
     */
    public Geoblock updateStreamTargetGeoblock(String streamTargetId, Geoblock geoblock) throws ApiException {
	GeoblockCreateInput input = new GeoblockCreateInput();
	input.setGeoblock(geoblock);
        GeoblockCreateInput resp = updateStreamTargetGeoblockTransport(streamTargetId, input);
        return resp.getGeoblock();
    }

    /**
     * Update stream target geoblock transport.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the geoblock create input
     * @throws ApiException the api exception
     */
    public GeoblockCreateInput updateStreamTargetGeoblockTransport(String streamTargetId, GeoblockCreateInput geoblock) throws ApiException {
        ApiResponse<GeoblockCreateInput> resp = updateStreamTargetGeoblockWithHttpInfo(streamTargetId, geoblock);
        return resp.getData();
    }

    /**
     * Update stream target geoblock with http info.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<GeoblockCreateInput> updateStreamTargetGeoblockWithHttpInfo(String streamTargetId, GeoblockCreateInput geoblock) throws ApiException {
        com.squareup.okhttp.Call call = updateStreamTargetGeoblockValidateBeforeCall(streamTargetId, geoblock, null, null);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update stream target geoblock async.
     *
     * @param streamTargetId the stream target id
     * @param geoblock the geoblock
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetGeoblockAsync(String streamTargetId, GeoblockCreateInput geoblock, final ApiCallback<GeoblockCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateStreamTargetGeoblockValidateBeforeCall(streamTargetId, geoblock, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GeoblockCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update stream target token auth call.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetTokenAuthCall(String streamTargetId, TokenAuthCreateInput tokenAuth, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = tokenAuth;

        // create path and map variables
        String localVarPath = "/stream_targets/{stream_target_id}/token_auth"
            .replaceAll("\\{" + "stream_target_id" + "\\}", apiClient.escapeString(streamTargetId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update stream target token auth validate before call.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateStreamTargetTokenAuthValidateBeforeCall(String streamTargetId, TokenAuthCreateInput tokenAuth, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'streamTargetId' is set
        if (streamTargetId == null) {
            throw new ApiException("Missing the required parameter 'streamTargetId' when calling updateStreamTargetTokenAuth(Async)");
        }
        
        // verify the required parameter 'tokenAuth' is set
        if (tokenAuth == null) {
            throw new ApiException("Missing the required parameter 'tokenAuth' when calling updateStreamTargetTokenAuth(Async)");
        }
        

        com.squareup.okhttp.Call call = updateStreamTargetTokenAuthCall(streamTargetId, tokenAuth, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update stream target token auth.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the token auth
     * @throws ApiException the api exception
     */
    public TokenAuth updateStreamTargetTokenAuth(String streamTargetId, TokenAuth tokenAuth) throws ApiException {
	TokenAuthCreateInput input = new TokenAuthCreateInput();
	input.setTokenAuth(tokenAuth);
        TokenAuthCreateInput resp = updateStreamTargetTokenAuthTransport(streamTargetId, input);
        return resp.getTokenAuth();
    }


    /**
     * Update stream target token auth transport.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the token auth create input
     * @throws ApiException the api exception
     */
    public TokenAuthCreateInput updateStreamTargetTokenAuthTransport(String streamTargetId, TokenAuthCreateInput tokenAuth) throws ApiException {
        ApiResponse<TokenAuthCreateInput> resp = updateStreamTargetTokenAuthWithHttpInfo(streamTargetId, tokenAuth);
        return resp.getData();
    }

    /**
     * Update stream target token auth with http info.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<TokenAuthCreateInput> updateStreamTargetTokenAuthWithHttpInfo(String streamTargetId, TokenAuthCreateInput tokenAuth) throws ApiException {
        com.squareup.okhttp.Call call = updateStreamTargetTokenAuthValidateBeforeCall(streamTargetId, tokenAuth, null, null);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update stream target token auth async.
     *
     * @param streamTargetId the stream target id
     * @param tokenAuth the token auth
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateStreamTargetTokenAuthAsync(String streamTargetId, TokenAuthCreateInput tokenAuth, final ApiCallback<TokenAuthCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateStreamTargetTokenAuthValidateBeforeCall(streamTargetId, tokenAuth, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<TokenAuthCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update ull stream target call.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateUllStreamTargetCall(String id, UllStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetUll;

        // create path and map variables
        String localVarPath = "/stream_targets/ull/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update ull stream target validate before call.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateUllStreamTargetValidateBeforeCall(String id, UllStreamTargetCreateInput streamTargetUll, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling updateUllStreamTarget(Async)");
        }
        
        // verify the required parameter 'streamTargetUll' is set
        if (streamTargetUll == null) {
            throw new ApiException("Missing the required parameter 'streamTargetUll' when calling updateUllStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = updateUllStreamTargetCall(id, streamTargetUll, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update ull stream target.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @return the stream target ull
     * @throws ApiException the api exception
     */
    public StreamTargetUll updateUllStreamTarget(String id, StreamTargetUll streamTargetUll) throws ApiException {
        UllStreamTargetCreateInput input = new UllStreamTargetCreateInput();
        input.setStreamTargetUll(streamTargetUll);
        UllStreamTargetCreateInput resp = updateUllStreamTargetTransport(id, input);
        return resp.getStreamTargetUll();
    }


    /**
     * Update ull stream target transport.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @return the ull stream target create input
     * @throws ApiException the api exception
     */
    public UllStreamTargetCreateInput updateUllStreamTargetTransport(String id, UllStreamTargetCreateInput streamTargetUll) throws ApiException {
        ApiResponse<UllStreamTargetCreateInput> resp = updateUllStreamTargetWithHttpInfo(id, streamTargetUll);
        return resp.getData();
    }

    /**
     * Update ull stream target with http info.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<UllStreamTargetCreateInput> updateUllStreamTargetWithHttpInfo(String id, UllStreamTargetCreateInput streamTargetUll) throws ApiException {
        com.squareup.okhttp.Call call = updateUllStreamTargetValidateBeforeCall(id, streamTargetUll, null, null);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update ull stream target async.
     *
     * @param id the id
     * @param streamTargetUll the stream target ull
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateUllStreamTargetAsync(String id, UllStreamTargetCreateInput streamTargetUll, final ApiCallback<UllStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateUllStreamTargetValidateBeforeCall(id, streamTargetUll, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<UllStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    
    /**
     * Update wowza stream target call.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateWowzaStreamTargetCall(String id, WowzaStreamTargetCreateInput streamTargetWowza, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = streamTargetWowza;

        // create path and map variables
        String localVarPath = "/stream_targets/wowza/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "wsc-access-key", "wsc-api-key", "wsc-signature", "wsc-timestamp" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Update wowza stream target validate before call.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @param progressListener the progress listener
     * @param progressRequestListener the progress request listener
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateWowzaStreamTargetValidateBeforeCall(String id, WowzaStreamTargetCreateInput streamTargetWowza, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling updateWowzaStreamTarget(Async)");
        }
        
        // verify the required parameter 'streamTargetWowza' is set
        if (streamTargetWowza == null) {
            throw new ApiException("Missing the required parameter 'streamTargetWowza' when calling updateWowzaStreamTarget(Async)");
        }
        

        com.squareup.okhttp.Call call = updateWowzaStreamTargetCall(id, streamTargetWowza, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Update wowza stream target.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @return the stream target wowza
     * @throws ApiException the api exception
     */
    public StreamTargetWowza updateWowzaStreamTarget(String id, StreamTargetWowza streamTargetWowza) throws ApiException {
	WowzaStreamTargetCreateInput input = new WowzaStreamTargetCreateInput();
	input.setStreamTargetWowza(streamTargetWowza);
        WowzaStreamTargetCreateInput resp = updateWowzaStreamTargetTransport(id, input);
        return resp.getStreamTargetWowza();
    }

    /**
     * Update wowza stream target transport.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @return the wowza stream target create input
     * @throws ApiException the api exception
     */
    public WowzaStreamTargetCreateInput updateWowzaStreamTargetTransport(String id, WowzaStreamTargetCreateInput streamTargetWowza) throws ApiException {
        ApiResponse<WowzaStreamTargetCreateInput> resp = updateWowzaStreamTargetWithHttpInfo(id, streamTargetWowza);
        return resp.getData();
    }

    /**
     * Update wowza stream target with http info.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @return the api response
     * @throws ApiException the api exception
     */
    public ApiResponse<WowzaStreamTargetCreateInput> updateWowzaStreamTargetWithHttpInfo(String id, WowzaStreamTargetCreateInput streamTargetWowza) throws ApiException {
        com.squareup.okhttp.Call call = updateWowzaStreamTargetValidateBeforeCall(id, streamTargetWowza, null, null);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update wowza stream target async.
     *
     * @param id the id
     * @param streamTargetWowza the stream target wowza
     * @param callback the callback
     * @return the com.squareup.okhttp. call
     * @throws ApiException the api exception
     */
    public com.squareup.okhttp.Call updateWowzaStreamTargetAsync(String id, WowzaStreamTargetCreateInput streamTargetWowza, final ApiCallback<WowzaStreamTargetCreateInput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = updateWowzaStreamTargetValidateBeforeCall(id, streamTargetWowza, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<WowzaStreamTargetCreateInput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
