/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamSourceAkamai;
import java.io.IOException;

/**
 * The Class AkamaiStreamSourceCreateInput.
 */
@ApiModel(description = "")
public class AkamaiStreamSourceCreateInput {
  
  /** The stream source akamai. */
  @SerializedName("stream_source_akamai")
  private StreamSourceAkamai streamSourceAkamai = null;

  /**
   * Stream source akamai.
   *
   * @param streamSourceAkamai the stream source akamai
   * @return the akamai stream source create input
   */
  public AkamaiStreamSourceCreateInput streamSourceAkamai(StreamSourceAkamai streamSourceAkamai) {
    this.streamSourceAkamai = streamSourceAkamai;
    return this;
  }

   /**
    * Gets the stream source akamai.
    *
    * @return the stream source akamai
    */
  @ApiModelProperty(required = true, value = "")
  public StreamSourceAkamai getStreamSourceAkamai() {
    return streamSourceAkamai;
  }

  /**
   * Sets the stream source akamai.
   *
   * @param streamSourceAkamai the new stream source akamai
   */
  public void setStreamSourceAkamai(StreamSourceAkamai streamSourceAkamai) {
    this.streamSourceAkamai = streamSourceAkamai;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AkamaiStreamSourceCreateInput akamaiStreamSourceCreateInput = (AkamaiStreamSourceCreateInput) o;
    return Objects.equals(this.streamSourceAkamai, akamaiStreamSourceCreateInput.streamSourceAkamai);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamSourceAkamai);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AkamaiStreamSourceCreateInput {\n");
    
    sb.append("    streamSourceAkamai: ").append(toIndentedString(streamSourceAkamai)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

