/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.dto.ClientUserDTO;
import com.rtf.saas.service.SaaSDataMigrationService;
import com.rtf.saas.sql.dvo.WebServiceApiTrackingDVO;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class MigrateTrackingToSQLServlet.
 */
@WebServlet("/migratetrackingstosql.json")
public class MigrateTrackingToSQLServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6178988742625945791L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Migrate contest tracking details to sql from cassandra
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientUserDTO respDTO = new ClientUserDTO();
		respDTO.setSts(0);
		try {
			List<WebServiceApiTrackingDVO> trackings = SaaSDataMigrationService.getTrackingDataToMigrate();
			for (WebServiceApiTrackingDVO track : trackings) {
				if (SaaSDataMigrationService.migrateTrackingRecord(track)) {
					SaaSDataMigrationService.deleteMigratedData(track);
				}
			}
			respDTO.setSts(1);
			setClientMessage(respDTO, null, trackings.size() + " Records migrated successfully.");
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
