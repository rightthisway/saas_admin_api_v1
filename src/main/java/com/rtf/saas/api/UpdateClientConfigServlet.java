/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.dto.ClientConfigDTO;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class UpdateClientConfigServlet.
 */
@WebServlet("/updateclientconfig.json")
public class UpdateClientConfigServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3159661061885615043L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String prodId = request.getParameter("prodId");
		String key = request.getParameter("keyId");
		String val = request.getParameter("keyVal");
		String cau = request.getParameter("cau");

		ClientConfigDTO respDTO = new ClientConfigDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (prodId == null || prodId.isEmpty()) {
				setClientMessage(respDTO, SAASMessages.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (key == null || key.isEmpty()) {
				setClientMessage(respDTO, SAASMessages.INVALID_KEY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (val == null || val.isEmpty()) {
				setClientMessage(respDTO, SAASMessages.INVALID_KEY_VALUE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientConfigDVO config = new ClientConfigDVO();
			config.setClId(clId);
			config.setProdId(prodId);
			config.setKey(key);
			config.setValue(val);
			config.setuBy(cau);
			Boolean isUpdated = ClientConfigService.updateContestConfig(config);
			if (!isUpdated) {
				setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Boolean isChat = false;
			if (key != null && key.equalsIgnoreCase(Constant.LIVE_TRIVIA_CHAT_SETTING)) {
				if (val != null && val.equalsIgnoreCase( SAASMessages.CHAT_ON_VALUE)) {
					isChat = true;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, SAASMessages.FIRESTORE_CONFIG_NOT_FOUND, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateChattingOnOff(isChat);
				}
			}else if (key != null && key.equalsIgnoreCase(Constant.LIVE_TRIVIA_TOTAL_ANSWER_COUNT)) {
				if (val != null && val.equalsIgnoreCase( SAASMessages.CHAT_ON_VALUE)) {
					isChat = true;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, SAASMessages.FIRESTORE_CONFIG_NOT_FOUND, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateAnswerCountOnOff(isChat);
				}
			}else if (key != null && key.equalsIgnoreCase(Constant.LIVE_TRIVIA_TOTAL_USER_COUNT)) {
				if (val != null && val.equalsIgnoreCase( SAASMessages.CHAT_ON_VALUE)) {
					isChat = true;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, SAASMessages.FIRESTORE_CONFIG_NOT_FOUND, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateTotalUserCountOnOff(isChat);
				}
			}else if (key != null && key.equalsIgnoreCase(Constant.LIVE_TRIVIA_QUESTION_TIMER)) {
				try {
					Integer.parseInt(val);
				} catch (Exception e) {
					setClientMessage(respDTO, SAASMessages.INVALID_KEY_VALUE, null);
					generateResponse(request, response, respDTO);
					return;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, SAASMessages.FIRESTORE_CONFIG_NOT_FOUND, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateQuestionTimer(val);
				}
			}else if (key != null && key.equalsIgnoreCase(Constant.LIVE_TRIVIA_ANSWER_TIMER)) {
				try {
					Integer.parseInt(val);
				} catch (Exception e) {
					setClientMessage(respDTO, SAASMessages.INVALID_KEY_VALUE, null);
					generateResponse(request, response, respDTO);
					return;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, SAASMessages.FIRESTORE_CONFIG_NOT_FOUND, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateAnswerTimer(val);
				}
			}

			respDTO.setMsg(SAASMessages.SELECT_KEY_VALUE_UPDATE_SUCCESS_MSG);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;

	}

}
