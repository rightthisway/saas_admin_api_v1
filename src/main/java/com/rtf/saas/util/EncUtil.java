/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Class EncUtil.
 */
public class EncUtil {

	/** The workload. */
	// RTFSaaSBcrypt workload to use when generating password hashes. 10-31 is a
	// valid value.
	private static int workload = 12;

	/**
	 * Gets the hash password.
	 *
	 * @param password_plaintext the password plaintext
	 * @return the hash password
	 */
	public static String getHashPassword(String password_plaintext) {
		String salt = RTFSaaSBcrypt.gensalt(workload);
		String hashed_password = RTFSaaSBcrypt.hashpw(password_plaintext, salt);

		return (hashed_password);
	}

	/**
	 * Check password.
	 *
	 * @param password_plaintext the password plaintext
	 * @param stored_hash        the stored hash
	 * @return true, if successful
	 */
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;

		if (null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = RTFSaaSBcrypt.checkpw(password_plaintext, stored_hash);

		return (password_verified);
	}



}