/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Enum StatusEnums.
 */
public enum StatusEnums {

	/** The true. */
	TRUE,
	/** The false. */
	FALSE,
	/** The active. */
	ACTIVE,
	/** The deleted. */
	DELETED,

	/** The duplicate. */
	DUPLICATE
}
