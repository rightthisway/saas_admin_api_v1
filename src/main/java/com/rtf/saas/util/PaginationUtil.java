/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class PaginationUtil.
 */
public class PaginationUtil {

	/** The Constant PAGESIZE. */
	public static final Integer PAGESIZE = 10;

	/**
	 * Calculate pagination parameter.
	 *
	 * @param count  the count
	 * @param pageNo the page no
	 * @return the pagination DTO
	 */
	public static PaginationDTO calculatePaginationParameter(Integer count, String pageNo) {
		PaginationDTO paginationDTO = new PaginationDTO();
		try {
			if (pageNo == null || pageNo.isEmpty()) {
				pageNo = "0";
			}
			if (count == null) {
				count = 0;
			}
			paginationDTO.setPageNum(pageNo);
			paginationDTO.setPageSize(PAGESIZE);
			paginationDTO.setTotalPages(Math.ceil((double) count / (double) PAGESIZE));
			paginationDTO.setTotalRows(count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationDTO;
	}

	/**
	 * Gets the pagination query.
	 *
	 * @param pageNo the page no
	 * @return the pagination query
	 */
	public static String getPaginationQuery(String pageNo) {
		String query = "";
		if (pageNo != null && !pageNo.isEmpty()) {
			query = " OFFSET (" + pageNo + "-1)*" + PaginationUtil.PAGESIZE + " ROWS FETCH NEXT "
					+ PaginationUtil.PAGESIZE + "  ROWS ONLY";
		}
		return query;
	}
}
