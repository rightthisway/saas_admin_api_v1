/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Class Constant.
 */
public class Constant {

	/** The Constant OTT. */
	public static final String OTT = "OTT";

	/** The Constant SNW. */
	public static final String SNW = "SNW";

	/** The Constant SHOP. */
	public static final String SHOP = "SVT";

	/** The Constant LIVETRIVIA. */
	public static final String LIVETRIVIA = "LVT";

	/** The Constant COMN. */
	public static final String COMN = "COMN";

	/** The Constant CONTEST_INTERVAL_TIME. */
	public static final String CONTEST_INTERVAL_TIME = "CONTEST_INTERVAL_TIMER";

	/** The Constant QUESTION_WAIT_TIMER. */
	public static final String QUESTION_WAIT_TIMER = "QUESTION_WAIT_TIMER";

	/** The Constant QUESTION_TIMER. */
	public static final String QUESTION_TIMER = "QUESTION_TIMER";

	/** The Constant ANSWER_TIMER. */
	public static final String ANSWER_TIMER = "ANSWER_TIMER";

	/** The Constant ANSWER_PRIZE_POPUP_TIMER. */
	public static final String ANSWER_PRIZE_POPUP_TIMER = "ANSWER_PRIZE_POPUP_TIMER";

	/** The Constant CONTEST_QUESTION_MODE_FIXED. */
	public static final String CONTEST_QUESTION_MODE_FIXED = "FIXED";

	/** The Constant CONTEST_QUESTION_MODE_RANDOM. */
	public static final String CONTEST_QUESTION_MODE_RANDOM = "RANDOM";

	/** The Constant CONTEST_TRACK_STARTED. */
	public static final String CONTEST_TRACK_STARTED = "STARTED";

	/** The Constant CONTEST_TRACK_COMPLETED. */
	public static final String CONTEST_TRACK_COMPLETED = "COMPLETED";

	/** The Constant CONTEST_TRACK_PTYPE_QUESTION. */
	public static final String CONTEST_TRACK_PTYPE_QUESTION = "Q";

	/** The Constant CONTEST_TRACK_PTYPE_ANSWER. */
	public static final String CONTEST_TRACK_PTYPE_ANSWER = "A";

	/** The Constant TRUE. */
	public static final String TRUE = "TRUE";

	/** The Constant LIVE_TRIVIA_CHAT_SETTING. */
	// LIVE TRIVIA
	public static final String LIVE_TRIVIA_CHAT_SETTING = "LIVE_TRIVIA_CHAT_SETTING";

	/** The Constant LIVE_TRIVIA_USERID_AUTO_GENERATED. */
	// LIVE TRIVIA
	public static final String LIVE_TRIVIA_USERID_AUTO_GENERATED = "IS_USERID_AUTO_GENERATED";

	/** The Constant LIVE_TRIVIA_TOTAL_ANSWER_COUNT. */
	public static final String LIVE_TRIVIA_TOTAL_ANSWER_COUNT = "TOTAL_ANSWER_COUNT";

	/** The Constant LIVE_TRIVIA_TOTAL_USER_COUNT. */
	public static final String LIVE_TRIVIA_TOTAL_USER_COUNT = "TOTAL_USER_COUNT";

	/** The Constant LIVE_TRIVIA_CONTEST_RESTRICTED_DURATION_HRS. */
	public static final String LIVE_TRIVIA_CONTEST_RESTRICTED_DURATION_HRS = "LIVE_TRIVIA_CONTEST_RESTRICTED_DURATION_HRS";
	
	/** The Constant LIVE_TRIVIA_CONTEST_SCRIPT_RESTRICTED_DURATION_HRS. */
	public static final String LIVE_TRIVIA_CONTEST_SCRIPT_RESTRICTED_DURATION_HRS = "LIVE_TRIVIA_CONTEST_SCRIPT_RESTRICTED_DURATION_HRS";
	
	/** The Constant LIVE_TRIVIA_QUESTION_TIMER. */
	public static final String LIVE_TRIVIA_QUESTION_TIMER = "LIVE_TRIVIA_QUESTION_TIMER";
	
	/** The Constant LIVE_TRIVIA_ANSWER_TIMER. */
	public static final String LIVE_TRIVIA_ANSWER_TIMER = "LIVE_TRIVIA_ANSWER_TIMER";

	/** The No of records per screen. */
	public static Integer NoOfRecordsPerScreen = 20;

	/** The reward icon upld folder path key. */
	public static String REWARD_ICON_UPLD_FOLDER_PATH_KEY = "rewardicons.upload.location";

	/** The Constant REWARD_ICON_CONTEXT_PATH_KEY. */
	public static final String REWARD_ICON_CONTEXT_PATH_KEY = "rewardicons.webapp.context";

	/** The Constant REWARD_ICON_IMAGE_SERVER. */
	public static final String REWARD_ICON_IMAGE_SERVER = "rewardicons.webapp.serverurl";

	/** The Constant ALL_SUB_CATEGORY. */
	public static final String ALL_SUB_CATEGORY = "ALL";

	/** The Constant ALL_SUB_CATEGORY_DESC. */
	public static final String ALL_SUB_CATEGORY_DESC = "All from  ";

}
