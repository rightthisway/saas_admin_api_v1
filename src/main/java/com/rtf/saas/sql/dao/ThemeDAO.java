/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ThemeDVO;

/**
 * The Class ThemeDAO.
 */
public class ThemeDAO {

	/**
	 * Gets the all themes.
	 *
	 * @param clientid the clientid
	 * @return the all themes
	 * @throws Exception the exception
	 */
	public static List<ThemeDVO> getAllThemes(String clientid) throws Exception {
		List<ThemeDVO> themes = new ArrayList<ThemeDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ThemeDVO theme = null;
		StringBuffer sql = new StringBuffer("SELECT * FROM sd_bg_theme_bank WHERE clintid = ? ORDER BY bgthemname");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			rs = ps.executeQuery();

			while (rs.next()) {
				theme = new ThemeDVO();
				theme.setClId(rs.getString("clintid"));
				theme.setThmId(rs.getInt("bgthembankid"));
				theme.setThmName(rs.getString("bgthemname"));
				theme.setThmColor(rs.getString("bgthemcolor"));
				theme.setThmImgDesk(rs.getString("bgthemimgurl"));
				theme.setThmImgMob(rs.getString("bgthemimgurlmob"));
				theme.setPlayGameImg(rs.getString("playimgurl"));
				theme.setIsAct(rs.getBoolean("isactive"));
				theme.setCreby(rs.getString("creby"));
				theme.setCredate(rs.getDate("credate"));
				theme.setUpdby(rs.getString("updby"));
				theme.setUpddate(rs.getDate("upddate"));
				themes.add(theme);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return themes;
	}

	/**
	 * Gets the theme by id.
	 *
	 * @param clId the cl id
	 * @param id   the id
	 * @return the theme by id
	 * @throws Exception the exception
	 */
	public static ThemeDVO getThemeById(String clId, Integer id) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ThemeDVO theme = null;
		StringBuffer sql = new StringBuffer("SELECT * FROM sd_bg_theme_bank WHERE clintid=? and bgthembankid = ?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			ps.setInt(2, id);
			rs = ps.executeQuery();

			while (rs.next()) {
				theme = new ThemeDVO();
				theme.setClId(rs.getString("clintid"));
				theme.setThmId(rs.getInt("bgthembankid"));
				theme.setThmName(rs.getString("bgthemname"));
				theme.setThmColor(rs.getString("bgthemcolor"));
				theme.setThmImgDesk(rs.getString("bgthemimgurl"));
				theme.setThmImgMob(rs.getString("bgthemimgurlmob"));
				theme.setPlayGameImg(rs.getString("playimgurl"));
				theme.setIsAct(rs.getBoolean("isactive"));
				theme.setCreby(rs.getString("creby"));
				theme.setCredate(rs.getDate("credate"));
				theme.setUpdby(rs.getString("updby"));
				theme.setUpddate(rs.getDate("upddate"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return theme;
	}

}
