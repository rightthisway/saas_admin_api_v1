/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class ClientConfigSQLDAO.
 */
public class ClientConfigSQLDAO {

	/**
	 * Gets the all contest connfigs.
	 *
	 * @param clientid the clientid
	 * @param filter   the filter
	 * @param pgNo     the pg no
	 * @return the all contest connfigs
	 * @throws Exception the exception
	 */
	public static List<ClientConfigDVO> getAllContestConnfigs(String clientid, String filter, String pgNo)
			throws Exception {
		List<ClientConfigDVO> configs = new ArrayList<ClientConfigDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClientConfigDVO config = null;
		StringBuffer sql = new StringBuffer("SELECT * FROM pa_commn_client_config WHERE clintid = ? AND isdisplayable=? ");
		try {
			String filterQuery = GridHeaderFilterUtil.getContestConfigFilterQuery(filter);
			//String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			sql.append(filterQuery);
			sql.append(" ORDER BY descr");
			//sql.append(paginationQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();
			Date date = null;
			while (rs.next()) {
				config = new ClientConfigDVO();
				config.setClId(rs.getString("clintid"));
				config.setKey(rs.getString("keyid"));
				config.setProdId(rs.getString("prodrkeytype"));
				config.setPropName(rs.getString("keyname"));
				config.setValue(rs.getString("keyvalue"));
				config.setDesc(rs.getString("descr"));
				date = rs.getTimestamp("upddate");
				config.setuDate(date != null ? date.getTime() : null);
				config.setuBy(rs.getString("updby"));
				config.setdType(rs.getString("keyiddattype"));
				configs.add(config);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return configs;
	}

	/**
	 * Gets the all contest connfigs count.
	 *
	 * @param clientid the clientid
	 * @param filter   the filter
	 * @return the all contest connfigs count
	 * @throws Exception the exception
	 */
	public static Integer getAllContestConnfigsCount(String clientid, String filter) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		StringBuffer sql = new StringBuffer("SELECT count(*) FROM pa_commn_client_config WHERE clintid = ?  AND isdisplayable=?");
		try {
			String filterQuery = GridHeaderFilterUtil.getContestConfigFilterQuery(filter);
			sql.append(filterQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the contest connfig by key.
	 *
	 * @param clientid the clientid
	 * @param prodId   the prod id
	 * @param key      the key
	 * @return the contest connfig by key
	 * @throws Exception the exception
	 */
	public static ClientConfigDVO getContestConnfigByKey(String clientid, String prodId, String key) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClientConfigDVO config = null;
		StringBuffer sql = new StringBuffer("SELECT * FROM pa_commn_client_config WHERE clintid = ? AND prodrkeytype=? AND keyid =?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, prodId);
			ps.setString(3, key);
			rs = ps.executeQuery();

			while (rs.next()) {
				config = new ClientConfigDVO();
				config.setClId(rs.getString("clintid"));
				config.setProdId(rs.getString("prodrkeytype"));
				config.setPropName(rs.getString("keyname"));
				config.setKey(rs.getString("keyid"));
				config.setValue(rs.getString("keyvalue"));
				config.setDesc(rs.getString("descr"));
				config.setuBy(rs.getString("updby"));
				return config;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return config;
	}

	/**
	 * Update contest config.
	 *
	 * @param config the config
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestConfig(ClientConfigDVO config) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		try {
			StringBuffer sql = new StringBuffer("update pa_commn_client_config  set keyvalue=?,updby=?,upddate=getDate()  WHERE clintid = ? AND prodrkeytype= ? AND keyid =?");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, config.getValue());
			ps.setString(2, config.getuBy());
			ps.setString(3, config.getClId());
			ps.setString(4, config.getProdId());
			ps.setString(5, config.getKey());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
}
