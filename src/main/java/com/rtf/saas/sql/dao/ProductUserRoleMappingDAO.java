/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ProductUserRoleMappingDAO.
 */
public class ProductUserRoleMappingDAO {
	
	/**
	 * Gets the user role products.
	 *
	 * @param usrRole the usr role
	 * @return the user role products
	 * @throws Exception the exception
	 */
	public static List<String> getUserRoleProducts(String usrRole) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;	
		List<String> products = new ArrayList<String>();
		
		StringBuffer sql = new StringBuffer("select prodcode from pd_prod_user_mapping WHERE userrole=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, usrRole);			
			rs = ps.executeQuery();
			while (rs.next()) {	
				products.add(rs.getString("prodcode"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return products;
	}

}
