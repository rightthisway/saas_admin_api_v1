/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.io.Serializable;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ClientConfigDVO.
 */
public class ClientConfigDVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1316140323738279157L;

	/** The cl id. */
	private String clId;
	
	/** The prod id. */
	private String prodId;
	
	/** The prop name. */
	private String propName;
	
	/** The key. */
	private String key;
	
	/** The value. */
	private String value;
	
	/** The desc. */
	private String desc;
	
	/** The u by. */
	private String uBy;
	
	/** The u date. */
	private Long uDate;
	
	/** The d type. */
	private String dType;
	
	/** The upd date time str. */
	private String updDateTimeStr;
	
	
	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	/**
	 * Gets the u by.
	 *
	 * @return the u by
	 */
	public String getuBy() {
		return uBy;
	}
	
	/**
	 * Sets the u by.
	 *
	 * @param uBy the new u by
	 */
	public void setuBy(String uBy) {
		this.uBy = uBy;
	}
	
	/**
	 * Gets the u date.
	 *
	 * @return the u date
	 */
	public Long getuDate() {
		return uDate;
	}
	
	/**
	 * Sets the u date.
	 *
	 * @param uDate the new u date
	 */
	public void setuDate(Long uDate) {
		this.uDate = uDate;
	}
	
	/**
	 * Gets the upd date time str.
	 *
	 * @return the upd date time str
	 */
	public String getUpdDateTimeStr() {
		updDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(uDate);
		return updDateTimeStr;
	}
	
	/**
	 * Sets the upd date time str.
	 *
	 * @param updDateTimeStr the new upd date time str
	 */
	public void setUpdDateTimeStr(String updDateTimeStr) {
		this.updDateTimeStr = updDateTimeStr;
	}
	
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public String getProdId() {
		return prodId;
	}
	
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	
	/**
	 * Gets the prop name.
	 *
	 * @return the prop name
	 */
	public String getPropName() {
		return propName;
	}
	
	/**
	 * Sets the prop name.
	 *
	 * @param propName the new prop name
	 */
	public void setPropName(String propName) {
		this.propName = propName;
	}
	
	/**
	 * Gets the d type.
	 *
	 * @return the d type
	 */
	public String getdType() {
		return dType;
	}
	
	/**
	 * Sets the d type.
	 *
	 * @param dType the new d type
	 */
	public void setdType(String dType) {
		this.dType = dType;
	}
}
