/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.util.Date;

/**
 * The Class ThemeDVO.
 */
public class ThemeDVO {

	/** The cl id. */
	private String clId;

	/** The thm id. */
	private Integer thmId;

	/** The thm name. */
	private String thmName;

	/** The thm color. */
	private String thmColor;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The play game img. */
	private String playGameImg;

	/** The is act. */
	private Boolean isAct;

	/** The creby. */
	private String creby;

	/** The updby. */
	private String updby;

	/** The credate. */
	private Date credate;

	/** The upddate. */
	private Date upddate;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the thm id.
	 *
	 * @return the thm id
	 */
	public Integer getThmId() {
		return thmId;
	}

	/**
	 * Sets the thm id.
	 *
	 * @param thmId the new thm id
	 */
	public void setThmId(Integer thmId) {
		this.thmId = thmId;
	}

	/**
	 * Gets the thm name.
	 *
	 * @return the thm name
	 */
	public String getThmName() {
		return thmName;
	}

	/**
	 * Sets the thm name.
	 *
	 * @param thmName the new thm name
	 */
	public void setThmName(String thmName) {
		this.thmName = thmName;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}
}
