/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

/**
 * The Class DatabaseConnections.
 */
public class DatabaseConnections {

	public static String saasPlayDbUrl;

	public static String saasPlayDbUserName;

	public static String saasPlayDbPassword;

	public static String saasRegDbUrl;

	public static String saasRegDbUserName;

	public static String saasRegDbPassword;

	public static String saasReportDbUrl;

	public static String saasReportDbUserName;

	public static String saasReportDbPassword;

	public static String saasPlayLinkedServerInReg;

	public static String saasRegLinkedServerInPlay;

	public static String sqlDriverName;

	public static String getSaasAdminLinkedServer(String clientId) {
		return saasPlayLinkedServerInReg;	
	}

	public static String getSaasClientRegLinkedServer() {
		return saasRegLinkedServerInPlay;	
	
	
	}

	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		saasPlayDbUrl = resourceBundle.getString("saas.play.sql.db.url");
		saasPlayDbUserName = resourceBundle.getString("saas.play.sql.db.user.name");
		saasPlayDbPassword = resourceBundle.getString("saas.play.sql.db.password");

		saasRegDbUrl = resourceBundle.getString("saas.reg.sql.db.url");
		saasRegDbUserName = resourceBundle.getString("saas.reg.sql.db.user.name");
		saasRegDbPassword = resourceBundle.getString("saas.reg.sql.db.password");
		
		saasReportDbUrl = resourceBundle.getString("saas.report.sql.db.url");
		saasReportDbUserName = resourceBundle.getString("saas.report.sql.db.user.name");
		saasReportDbPassword = resourceBundle.getString("saas.report.sql.db.password");

		saasPlayLinkedServerInReg = resourceBundle.getString("saas.play.lined.server.in.reg");
		saasRegLinkedServerInPlay = resourceBundle.getString("saas.reg.lined.server.in.play");

		sqlDriverName = resourceBundle.getString("sql.driver.name");

	}

	/**
	 * Close connection.
	 *
	 * @param con the con
	 */
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Gets the saas admin connection.
	 *
	 * @return the saas admin connection
	 * @throws Exception the exception
	 */
	public static Connection getSaasAdminConnection() throws Exception {

		try {		
			String driver = sqlDriverName;
			Class.forName(driver);
			return DriverManager.getConnection(saasPlayDbUrl, saasPlayDbUserName, saasPlayDbPassword);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Gets the saas client details connection.
	 *
	 * @return the saas client details connection
	 * @throws Exception the exception
	 */
	public static Connection getSaasClientDetailsConnection() throws Exception {
		try {
			
			String driver = sqlDriverName;
			Class.forName(driver);	
			return DriverManager.getConnection(saasRegDbUrl, saasRegDbUserName, saasRegDbPassword);
			
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Gets the saas admin report DB connection.
	 *
	 * @return the saas admin report DB connection
	 * @throws Exception the exception
	 */
	public static Connection getSaasAdminReportDBConnection() throws Exception {

		try {
			sqlDriverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			String driver = sqlDriverName;
			Class.forName(driver);
			return DriverManager.getConnection(saasReportDbUrl, saasReportDbUserName, saasReportDbPassword);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Gets the result set.
	 *
	 * @param statement the statement
	 * @param sql       the sql
	 * @return the result set
	 */
	public static ResultSet getResultSet(Statement statement, String sql) {
		try {
			return statement.executeQuery(sql);
		} catch (SQLException sqlEx) {
			return null;
		}
	}

	/**
	 * Gets the callable statement.
	 *
	 * @param connnection the connnection
	 * @param sql         the sql
	 * @return the callable statement
	 */
	public static CallableStatement getCallableStatement(Connection connnection, String sql) {
		try {
			return connnection.prepareCall(sql);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			return null;
		}

	}

}
