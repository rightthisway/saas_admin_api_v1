/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.exception;

/**
 * The Class ContestConfigException.
 */
public class ContestConfigException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5012642588925719222L;

	/**
	 * Instantiates a new contest config exception.
	 */
	public ContestConfigException() {		
	}
	
	/**
	 * Instantiates a new contest config exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public ContestConfigException(String msg,Exception ex) 	{
		 //Write to DB for tracking 
		ex.printStackTrace();
	}
	
	/**
	 * Instantiates a new contest config exception.
	 *
	 * @param msg the msg
	 */
	public ContestConfigException(String msg) 	{
		 //Write to DB for tracking 
		
	}
}
