/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.exception;

/**
 * The Class RTFServiceAccessException.
 */
public class RTFServiceAccessException extends Exception {
	
	
	private static final long serialVersionUID = 5187693158807635589L;

	/**
	 * Instantiates a new RTF service access exception.
	 */
	public RTFServiceAccessException() {		
	}
	
	/**
	 * Instantiates a new RTF service access exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public RTFServiceAccessException(String msg,Exception ex) 	{	
		 // Do something with Exception
	}
	
	
	public RTFServiceAccessException(String msg) {		
	}

}
