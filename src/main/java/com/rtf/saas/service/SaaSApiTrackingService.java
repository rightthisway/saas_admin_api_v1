/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.service;

import com.rtf.saas.sql.dao.WebServiceApiTrackingDAO;
import com.rtf.saas.sql.dvo.LiveContestPlayTrackingDVO;

/**
 * The Class SaaSApiTrackingService.
 */
public class SaaSApiTrackingService {

	/**
	 * Save contest tracking.
	 *
	 * @param clientId    the client id
	 * @param conid       the conid
	 * @param apiName     the api name
	 * @param userName    the user name
	 * @param platform    the platform
	 * @param ipAddress   the ip address
	 * @param action      the action
	 * @param description the description
	 * @param respStatus  the resp status
	 * @param qNo         the q no
	 */
	public static void saveContestTracking(String clientId, String conid, String apiName, String userName,
			String platform, String ipAddress, String action, String description, Integer respStatus, Integer qNo) {

		try {
			LiveContestPlayTrackingDVO dvo = new LiveContestPlayTrackingDVO();
			clientId = clientId != null ? clientId : "-1";
			dvo.setClintid(clientId);
			dvo.setApiname(apiName);
			dvo.setConid(conid);
			//dvo.setIpAddress(ipAddress);
			dvo.setPlatform(platform);
			dvo.setDescription(description);
			dvo.setResstatus(respStatus);
			dvo.setAction(action);
			dvo.setqNo(qNo);
			dvo.setUserName(userName);
			WebServiceApiTrackingDAO.saveContestTracking(dvo);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
