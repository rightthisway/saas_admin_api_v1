/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.service;

import java.util.List;

import com.rtf.saas.cass.dao.WebServiceApiTrackingDAO;
import com.rtf.saas.sql.dvo.WebServiceApiTrackingDVO;

/**
 * The Class SaaSDataMigrationService.
 */
public class SaaSDataMigrationService {

	/**
	 * Gets the tracking data to migrate.
	 *
	 * @return the tracking data to migrate
	 */
	public static List<WebServiceApiTrackingDVO> getTrackingDataToMigrate() {
		List<WebServiceApiTrackingDVO> list = null;
		try {
			list = WebServiceApiTrackingDAO.getAllWebServiceApiTrackingRecords();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Delete migrated data.
	 *
	 * @param dvo the dvo
	 * @return true, if successful
	 */
	public static boolean deleteMigratedData(WebServiceApiTrackingDVO dvo) {
		Boolean isDelete = false;
		try {
			isDelete = WebServiceApiTrackingDAO.deleteWebServiceApiTrackingRecord(dvo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isDelete;
	}

	/**
	 * Migrate tracking record.
	 *
	 * @param dvo the dvo
	 * @return true, if successful
	 */
	public static boolean migrateTrackingRecord(WebServiceApiTrackingDVO dvo) {
		try {
			com.rtf.saas.sql.dao.WebServiceApiTrackingDAO.saveWebServiceApiTracking(dvo);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
