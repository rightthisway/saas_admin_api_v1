/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.service;

import java.util.List;

import com.rtf.saas.sql.dao.ClientFirestoreConfigSQLDAO;
import com.rtf.saas.sql.dao.ClientProductsDAO;
import com.rtf.saas.sql.dao.ProductUserRoleMappingDAO;
import com.rtf.saas.sql.dvo.ClientFirestoreConfigDVO;
import com.rtf.saas.sql.dvo.ClientProductsDVO;

/**
 * The Class ClientService.
 */
public class ClientService {

	/**
	 * Gets the client config.
	 *
	 * @param clientId the client id
	 * @return the client config
	 */
	public static List<ClientFirestoreConfigDVO> getClientConfig(String clientId) {
		List<ClientFirestoreConfigDVO> configs = null;
		try {
			configs = ClientFirestoreConfigSQLDAO.getClientConfig(clientId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return configs;
	}

	/**
	 * Gets the client products.
	 *
	 * @param clId the cl id
	 * @return the client products
	 */
	public static List<ClientProductsDVO> getClientProducts(String clId) {
		List<ClientProductsDVO> products = null;
		try {
			products = ClientProductsDAO.getClientProducts(clId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return products;
	}

	/**
	 * Gets the product list by role mapping.
	 *
	 * @param usrRole the usr role
	 * @return the product list by role mapping
	 */
	public static List<String> getProductListByRoleMapping(String usrRole) {
		List<String> roleBasedproducts = null;
		try {
			roleBasedproducts = ProductUserRoleMappingDAO.getUserRoleProducts(usrRole);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roleBasedproducts;
	}

}
