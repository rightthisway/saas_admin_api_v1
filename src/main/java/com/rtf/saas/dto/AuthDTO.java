/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class AuthDTO.
 */
public class AuthDTO extends RtfSaasBaseDTO {	

}
