/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class ChildCategoryDVO.
 */
public class ChildCategoryDVO implements Serializable {

	private static final long serialVersionUID = 7606880741587393160L;

	/** The c name. */
	private String cName;

	/** The name. */
	private String name;

	/** The desc. */
	private String desc;

	/** The is act. */
	private Boolean isAct;

	/** The c by. */
	private String cBy;

	/** The u by. */
	private String uBy;

	/** The c date. */
	private Long cDate;

	/** The u date. */
	private Long uDate;

	/**
	 * Gets the c name.
	 *
	 * @return the c name
	 */
	public String getcName() {
		return cName;
	}

	/**
	 * Sets the c name.
	 *
	 * @param cName
	 *            the new c name
	 */
	public void setcName(String cName) {
		this.cName = cName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Sets the desc.
	 *
	 * @param desc
	 *            the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct
	 *            the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the c by.
	 *
	 * @return the c by
	 */
	public String getcBy() {
		return cBy;
	}

	/**
	 * Sets the c by.
	 *
	 * @param cBy
	 *            the new c by
	 */
	public void setcBy(String cBy) {
		this.cBy = cBy;
	}

	/**
	 * Gets the u by.
	 *
	 * @return the u by
	 */
	public String getuBy() {
		return uBy;
	}

	/**
	 * Sets the u by.
	 *
	 * @param uBy
	 *            the new u by
	 */
	public void setuBy(String uBy) {
		this.uBy = uBy;
	}

	/**
	 * Gets the c date.
	 *
	 * @return the c date
	 */
	public Long getcDate() {
		return cDate;
	}

	/**
	 * Sets the c date.
	 *
	 * @param cDate
	 *            the new c date
	 */
	public void setcDate(Long cDate) {
		this.cDate = cDate;
	}

	/**
	 * Gets the u date.
	 *
	 * @return the u date
	 */
	public Long getuDate() {
		return uDate;
	}

	/**
	 * Sets the u date.
	 *
	 * @param uDate
	 *            the new u date
	 */
	public void setuDate(Long uDate) {
		this.uDate = uDate;
	}

}
