/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ContestDVO.
 */
public class ContestDVO implements Serializable {

	private static final long serialVersionUID = -422612602287858543L;

	/** The co id. */
	private String coId;

	/** The cl id. */
	private String clId;

	/** The img U. */
	private String imgU;

	/** The seq no. */
	private Integer seqNo;

	/** The cat. */
	private String cat;

	/** The sub cat. */
	private String subCat;

	/** The cl img U. */
	private String clImgU;

	/** The st date. */
	private Long stDate;

	/** The en date. */
	private Long enDate;

	/** The name. */
	private String name;

	/** The cr by. */
	private String crBy;

	/** The cr date. */
	private Long crDate;

	/** The is cl img. */
	private String isClImg;

	/** The is act. */
	private Integer isAct;

	/** The q size. */
	private Integer qSize;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/** The ans type. */
	private String ansType;

	/** The is elimination. */
	private Boolean isElimination;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The q mode. */
	private String qMode;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The play game img. */
	private String playGameImg;

	/** The thm id. */
	private Integer thmId;

	/** The thm color. */
	private String thmColor;

	/** The st date time str. */
	private String stDateTimeStr;

	/** The en date time str. */
	private String enDateTimeStr;

	/** The st date str. */
	private String stDateStr;

	/** The ed date str. */
	private String edDateStr;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/** The status. */
	private String status;

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU
	 *            the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param seqNo
	 *            the new seq no
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * Gets the cat.
	 *
	 * @return the cat
	 */
	public String getCat() {
		return cat;
	}

	/**
	 * Sets the cat.
	 *
	 * @param cat
	 *            the new cat
	 */
	public void setCat(String cat)

	{
		this.cat = cat;
	}

	/**
	 * Gets the sub cat.
	 *
	 * @return the sub cat
	 */
	public String getSubCat() {
		return subCat;
	}

	/**
	 * Sets the sub cat.
	 *
	 * @param subCat
	 *            the new sub cat
	 */
	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	/**
	 * Gets the cl img U.
	 *
	 * @return the cl img U
	 */
	public String getClImgU() {
		return clImgU;
	}

	/**
	 * Sets the cl img U.
	 *
	 * @param clImgU
	 *            the new cl img U
	 */
	public void setClImgU(String clImgU) {
		this.clImgU = clImgU;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy
	 *            the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Gets the checks if is cl img.
	 *
	 * @return the checks if is cl img
	 */
	public String getIsClImg() {
		return isClImg;
	}

	/**
	 * Sets the checks if is cl img.
	 *
	 * @param isClImg
	 *            the new checks if is cl img
	 */
	public void setIsClImg(String isClImg) {
		this.isClImg = isClImg;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Integer getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct
	 *            the new checks if is act
	 */
	public void setIsAct(Integer isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize
	 *            the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy
	 *            the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

	/**
	 * Gets the ans type.
	 *
	 * @return the ans type
	 */
	public String getAnsType() {
		return ansType;
	}

	/**
	 * Sets the ans type.
	 *
	 * @param ansType
	 *            the new ans type
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	/**
	 * Gets the checks if is elimination.
	 *
	 * @return the checks if is elimination
	 */
	public Boolean getIsElimination() {
		if (isElimination == null)
			isElimination = Boolean.FALSE;
		return isElimination;
	}

	/**
	 * Sets the checks if is elimination.
	 *
	 * @param isElimination
	 *            the new checks if is elimination
	 */
	public void setIsElimination(Boolean isElimination) {
		this.isElimination = isElimination;
	}

	/**
	 * Gets the q mode.
	 *
	 * @return the q mode
	 */
	public String getqMode() {
		return qMode;
	}

	/**
	 * Sets the q mode.
	 *
	 * @param qMode
	 *            the new q mode
	 */
	public void setqMode(String qMode) {
		this.qMode = qMode;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType
	 *            the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal
	 *            the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate
	 *            the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Gets the en date.
	 *
	 * @return the en date
	 */
	public Long getEnDate() {
		return enDate;
	}

	/**
	 * Sets the en date.
	 *
	 * @param enDate
	 *            the new en date
	 */
	public void setEnDate(Long enDate) {
		this.enDate = enDate;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate
	 *            the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate
	 *            the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob
	 *            the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk
	 *            the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Gets the thm id.
	 *
	 * @return the thm id
	 */
	public Integer getThmId() {
		return thmId;
	}

	/**
	 * Sets the thm id.
	 *
	 * @param thmId
	 *            the new thm id
	 */
	public void setThmId(Integer thmId) {
		this.thmId = thmId;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor
	 *            the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the st date time str.
	 *
	 * @return the st date time str
	 */
	public String getStDateTimeStr() {
		stDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(stDate);
		return stDateTimeStr;
	}

	/**
	 * Sets the st date time str.
	 *
	 * @param stDateTimeStr
	 *            the new st date time str
	 */
	public void setStDateTimeStr(String stDateTimeStr) {
		this.stDateTimeStr = stDateTimeStr;
	}

	/**
	 * Gets the en date time str.
	 *
	 * @return the en date time str
	 */
	public String getEnDateTimeStr() {
		enDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(enDate);
		return enDateTimeStr;
	}

	/**
	 * Sets the en date time str.
	 *
	 * @param enDateTimeStr
	 *            the new en date time str
	 */
	public void setEnDateTimeStr(String enDateTimeStr) {
		this.enDateTimeStr = enDateTimeStr;
	}

	/**
	 * Gets the st date str.
	 *
	 * @return the st date str
	 */
	public String getStDateStr() {
		stDateStr = DateFormatUtil.getMMDDYYYYString(stDate);
		return stDateStr;
	}

	/**
	 * Sets the st date str.
	 *
	 * @param stDateStr
	 *            the new st date str
	 */
	public void setStDateStr(String stDateStr) {
		this.stDateStr = stDateStr;
	}

	/**
	 * Gets the ed date str.
	 *
	 * @return the ed date str
	 */
	public String getEdDateStr() {
		edDateStr = DateFormatUtil.getMMDDYYYYString(enDate);
		return edDateStr;
	}

	/**
	 * Sets the ed date str.
	 *
	 * @param edDateStr
	 *            the new ed date str
	 */
	public void setEdDateStr(String edDateStr) {
		this.edDateStr = edDateStr;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(crDate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr
	 *            the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(upDate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr
	 *            the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg
	 *            the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
