/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ContestRewardDVO.
 */
public class ContestRewardDVO implements Serializable {

	private static final long serialVersionUID = 2158038238585684897L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The type. */
	private String type;

	/** The rwd val. */
	private Double rwdVal;

	/** The is act. */
	private Boolean isAct;

	/** The c by. */
	private String cBy;

	/** The u by. */
	private String uBy;

	/** The c date. */
	private Long cDate;

	/** The u date. */
	private Long uDate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal
	 *            the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct
	 *            the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the c by.
	 *
	 * @return the c by
	 */
	public String getcBy() {
		return cBy;
	}

	/**
	 * Sets the c by.
	 *
	 * @param cBy
	 *            the new c by
	 */
	public void setcBy(String cBy) {
		this.cBy = cBy;
	}

	/**
	 * Gets the u by.
	 *
	 * @return the u by
	 */
	public String getuBy() {
		return uBy;
	}

	/**
	 * Sets the u by.
	 *
	 * @param uBy
	 *            the new u by
	 */
	public void setuBy(String uBy) {
		this.uBy = uBy;
	}

	/**
	 * Gets the c date.
	 *
	 * @return the c date
	 */
	public Long getcDate() {
		return cDate;
	}

	/**
	 * Sets the c date.
	 *
	 * @param cDate
	 *            the new c date
	 */
	public void setcDate(Long cDate) {
		this.cDate = cDate;
	}

	/**
	 * Gets the u date.
	 *
	 * @return the u date
	 */
	public Long getuDate() {
		return uDate;
	}

	/**
	 * Sets the u date.
	 *
	 * @param uDate
	 *            the new u date
	 */
	public void setuDate(Long uDate) {
		this.uDate = uDate;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(cDate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr
	 *            the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(uDate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr
	 *            the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

}
