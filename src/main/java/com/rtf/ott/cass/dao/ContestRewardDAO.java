/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dao;

import java.util.List;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.cass.dvo.ContestRewardDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestRewardDAO.
 */
public class ContestRewardDAO {

	/**
	 * Update contest rewards.
	 *
	 * @param rwds
	 *            the rwds
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestRewards(List<ContestRewardDVO> rwds, String coId) throws Exception {
		Boolean isUpdated = false;
		try {
			for (ContestRewardDVO r : rwds) {
				StringBuffer sql = new StringBuffer();
				if (r.getCoId() == null) {
					sql.append("INSERT INTO pa_ofltx_contest_rewards (clintid,conid,rwdtype,isactive,creby,credate,rwdval) values (?,?,?,?,?,toTimestamp(now()),?)");
					CassandraConnector.getSession().execute(sql.toString(),
							new Object[] { r.getClId(), coId, r.getType(), Boolean.TRUE, r.getcBy(), r.getRwdVal() });
				} else {
					sql.append("update pa_ofltx_contest_rewards set rwdval=?,updby=?,upddate=toTimestamp(now()) WHERE clintid=? and conid=? and  rwdtype=?");
					CassandraConnector.getSession().execute(sql.toString(),
							new Object[] { r.getRwdVal(), r.getuBy(), r.getClId(), r.getCoId(), r.getType() });
				}
			}
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return isUpdated;
	}

}
