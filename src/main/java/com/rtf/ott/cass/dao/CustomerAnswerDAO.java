/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.cass.dvo.CustomerAnswerDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class CustomerAnswerDAO.
 */
public class CustomerAnswerDAO {

	/**
	 * Insert customer quen answer details.
	 *
	 * @param obj
	 *            the obj
	 * @return the customer answer DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustomerAnswerDVO insertCustomerQuenAnswerDetails(CustomerAnswerDVO obj) throws Exception {

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" INSERT INTO pt_ofltx_customer_answer  ")
			.append("(conid,custid,custplayid, custans, conname ,qsnbnkans ,rwdtype, qsnbnkid,  ")
			.append(" qsnseqno ,  consrtdate ,credate , upddate ,   iscrtans, rwdval) VALUES ")
			.append(" (?,?, ?, ?,  ?,?,?,?,  ?, toTimestamp(now()), toTimestamp(now()), ?, ?,?) ");;
			CassandraConnector.getSession().execute(sql.toString(), obj.getConid(),
					obj.getCustid(), obj.getCustplayid(), obj.getCustans(), obj.getConname(), obj.getQsnbnkans(),
					obj.getRwdtype(), obj.getQsnbnkid(), obj.getQsnseqno(), obj.getUpddate(), obj.isIscrtans(),
					obj.getRwdvalue());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

	/**
	 * Update customer quen answer details.
	 *
	 * @param obj
	 *            the obj
	 * @return the customer answer DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustomerAnswerDVO updateCustomerQuenAnswerDetails(CustomerAnswerDVO obj) throws Exception {

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" UPDATE pt_ofltx_customer_answer set ")
			.append(" custans = ? , iscrtans = ? , ")
			.append(" rwdtype = ?  , rwdval = ? , upddate = toTimestamp(now())  where ")
			.append(" qsnbnkid = ?  and custid = ? and custplayid = ? and conid = ? ");
			CassandraConnector.getSession().execute(sql.toString(),
					obj.getCustans(), obj.isIscrtans(), obj.getRwdtype(), obj.getRwdvalue(), obj.getQsnbnkid(),
					obj.getCustid(), obj.getCustplayid(), obj.getConid());
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return obj;
	}

	

	/**
	 * Gets the customer correct answers.
	 *
	 * @param contestId
	 *            the contest id
	 * @param playId
	 *            the play id
	 * @param custId
	 *            the cust id
	 * @return the customer correct answers
	 * @throws Exception
	 *             the exception
	 */
	public static List<CustomerAnswerDVO> getCustomerCorrectAnswers(String contestId, String playId, String custId)
			throws Exception {
		ResultSet resultSet = null;
		List<CustomerAnswerDVO> customerAnswers = new ArrayList<CustomerAnswerDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_answer  " + " WHERE custid=? and conid=? and custplayid=?",
					custId, contestId, playId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					Boolean isCrct = row.getBool("iscrtans");
					if (isCrct != null && isCrct) {
						CustomerAnswerDVO customerAnswer = new CustomerAnswerDVO();
						customerAnswer.setConid(row.getString("conid"));
						customerAnswer.setCustid(row.getString("custid"));
						customerAnswer.setQsnbnkid(row.getInt("qsnbnkid"));
						customerAnswer.setCustans(row.getString("custans"));
						customerAnswer.setCustplayid(row.getString("custplayid"));
						customerAnswer.setIscrtans(isCrct);
						customerAnswer.setQsnbnkans(row.getString("qsnbnkans"));
						customerAnswer.setQsnseqno(row.getInt("qsnseqno"));
						customerAnswer.setRwdtype(row.getString("rwdtype"));
						customerAnswer.setRwdvalue(row.getDouble("rwdval"));
						customerAnswers.add(customerAnswer);
					}
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return customerAnswers;
	}
}
