/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dvo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ContestQuestionDVO.
 */
public class ContestQuestionDVO implements Serializable {

	private static final long serialVersionUID = 8299740487122561236L;

	/** The clintid. */
	private String clintid;

	/** The conqsnid. */
	private Integer conqsnid;

	/** The conid. */
	private String conid;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/** The isactive. */
	@JsonIgnore
	private Boolean isactive;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;
	// FEEDBACK OR REGULAR
	// private String anstype;
	/** The corans. */
	// private String cattyp;
	private String corans;

	/** The creby. */
	private String creby;

	/** The credate. */
	private Date credate;

	/** The qbid. */
	// private String isvalidated;
	private Integer qbid;
	// FIXED OR RANDOM

	/** The qsnmode. */
	@JsonIgnore
	private String qsnmode;

	/** The qsnorient. */
	// DISPLAY ANSWER OPTIONS HORIZONTAL OR VERTICAL
	private String qsnorient;

	/** The qsntext. */
	private String qsntext;

	/** The rwdtype. */
	private String rwdtype;

	/** The rwdvalue. */
	private Double rwdvalue;

	/** The updby. */
	// private String subcattyp;
	private String updby;

	/** The upddate. */
	private Date upddate;

	/** The credate str. */
	private String credateStr;

	/** The upddate str. */
	private String upddateStr;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid
	 *            the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid
	 *            the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid
	 *            the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno
	 *            the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive
	 *            the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa
	 *            the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb
	 *            the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc
	 *            the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd
	 *            the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Gets the corans.
	 *
	 * @return the corans
	 */
	/*
	 * public String getAnstype() { if(anstype==null) { anstype="REGULAR"; }
	 * return anstype; } public void setAnstype(String anstype) { this.anstype =
	 * anstype; } public String getCattyp() { if(cattyp == null) { cattyp =
	 * "GENERAL"; } return cattyp; } public void setCattyp(String cattyp) {
	 * this.cattyp = cattyp; }
	 */
	public String getCorans() {
		return corans;
	}

	/**
	 * Sets the corans.
	 *
	 * @param corans
	 *            the new corans
	 */
	public void setCorans(String corans) {
		this.corans = corans;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby
	 *            the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate
	 *            the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	/*
	 * public String getIsvalidated() { //if(isvalidated == null) {
	 * isvalidated="VALID"; //} return isvalidated; } public void
	 * setIsvalidated(String isvalidated) { this.isvalidated = isvalidated; }
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid
	 *            the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Gets the qsnmode.
	 *
	 * @return the qsnmode
	 */
	public String getQsnmode() {
		qsnmode = "RANDOM";
		return qsnmode;
	}

	/**
	 * Sets the qsnmode.
	 *
	 * @param qsnmode
	 *            the new qsnmode
	 */
	public void setQsnmode(String qsnmode) {
		this.qsnmode = qsnmode;
	}

	/**
	 * Gets the qsnorient.
	 *
	 * @return the qsnorient
	 */
	public String getQsnorient() {
		return qsnorient;
	}

	/**
	 * Sets the qsnorient.
	 *
	 * @param qsnorient
	 *            the new qsnorient
	 */
	public void setQsnorient(String qsnorient) {
		this.qsnorient = qsnorient;
	}

	/**
	 * Gets the qsntext.
	 *
	 * @return the qsntext
	 */
	public String getQsntext() {
		return qsntext;
	}

	/**
	 * Sets the qsntext.
	 *
	 * @param qsntext
	 *            the new qsntext
	 */
	public void setQsntext(String qsntext) {
		this.qsntext = qsntext;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		rwdtype = "POINTS";
		return rwdtype;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype
	 *            the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		if (rwdvalue == null) {
			rwdvalue = 0.0;
		}
		return rwdvalue;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue
	 *            the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	/*
	 * public String getSubcattyp() { if(subcattyp == null) { subcattyp =
	 * "GENERAL"; } return subcattyp; } public void setSubcattyp(String
	 * subcattyp) { this.subcattyp = subcattyp; }
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby
	 *            the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate
	 *            the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the credate str.
	 *
	 * @return the credate str
	 */
	public String getCredateStr() {
		credateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(credate);
		return credateStr;
	}

	/**
	 * Sets the credate str.
	 *
	 * @param credateStr
	 *            the new credate str
	 */
	public void setCredateStr(String credateStr) {
		this.credateStr = credateStr;
	}

	/**
	 * Gets the upddate str.
	 *
	 * @return the upddate str
	 */
	public String getUpddateStr() {
		upddateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(upddate);
		return upddateStr;
	}

	/**
	 * Sets the upddate str.
	 *
	 * @param upddateStr
	 *            the new upddate str
	 */
	public void setUpddateStr(String upddateStr) {
		this.upddateStr = upddateStr;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContestQuestionDVO [clintid=" + clintid + ", conqsnid=" + conqsnid + ", conid=" + conid + ", qsnseqno="
				+ qsnseqno + ", isactive=" + isactive + ", opa=" + opa + ", opb=" + opb + ", opc=" + opc + ", opd="
				+ opd + ", corans=" + corans + ", creby=" + creby + ", credate=" + credate + ", qbid=" + qbid
				+ ", qsnmode=" + qsnmode + ", qsnorient=" + qsnorient + ", qsntext=" + qsntext + ", rwdtype=" + rwdtype
				+ ", rwdvalue=" + rwdvalue + ", updby=" + updby + ", upddate=" + upddate + "]";
	}

}
