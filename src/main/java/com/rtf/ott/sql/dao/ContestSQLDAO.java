/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class ContestSQLDAO.
 */
public class ContestSQLDAO {

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("SELECT * from  pa_ofltx_contest_mstr  WHERE clintid = ? AND conid = ? ORDER BY consrtdate");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setRwdType(rs.getString("rwdtype"));
				contestDVO.setRwdVal(rs.getDouble("rwdval"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the all contest by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestByStatusandFilter(String clId, String status, String filter,
			String pgNo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT c.clintid AS clintid,c.conid AS conid ")
				.append(",c.anstype AS anstype,c.brndimgurl AS brndimgurl ")
				.append(",c.cardimgurl AS cardimgurl,c.cardseqno AS cardseqno ")
				.append(",c.cattype AS cattype,c.conenddate AS conenddate ")
				.append(",c.conimgurl AS conimgurl,c.conname AS conname ,c.consrtdate AS consrtdate,c.creby AS creby ")
				.append(",c.credate AS credate,c.isconactive AS isconactive ")
				.append(",c.iselimtype AS iselimtype,c.noofqns AS noofqns ,c.qsnmode AS qsnmode,c.rwdtype AS rwdtype ")
				.append(",c.rwdval AS rwdval,c.subcattype AS subcattype ,c.updby AS updby,c.upddate AS upddate ")
				.append(",c.bgthemcolor AS bgthemcolor,c.bgthemimgurl AS bgthemimgurl ")
				.append(",c.bgthemimgurlmob AS bgthemimgurlmob,c.bgthembankid AS bgthembankid ")
				.append(",c.playimgurl AS playimgurl,s.isconactivetext AS isconactivetext ")
				.append("FROM pa_ofltx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ")
				.append("WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			String sortingSql = GridSortingUtil.getOTTContestSortingQuery(filter);
			if(sortingSql!=null && !sortingSql.trim().isEmpty()){
				sql.append(sortingSql);
			}else{
				sql.append(" ORDER BY c.consrtdate");
			}
			
			sql.append(paginationQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setStatus(rs.getString("isconactivetext"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setRwdType(rs.getString("rwdtype"));
				contestDVO.setRwdVal(rs.getDouble("rwdval"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest data to export.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestDataToExport(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT c.clintid AS clintid,c.conid AS conid ")
				.append(",c.anstype AS anstype,c.brndimgurl AS brndimgurl ")
				.append(",c.cardimgurl AS cardimgurl,c.cardseqno AS cardseqno ")
				.append(",c.cattype AS cattype,c.conenddate AS conenddate ")
				.append(",c.conimgurl AS conimgurl,c.conname AS conname ")
				.append(",c.consrtdate AS consrtdate,c.creby AS creby ")
				.append(",c.credate AS credate,c.isconactive AS isconactive ")
				.append(",c.iselimtype AS iselimtype,c.noofqns AS noofqns ")
				.append(",c.qsnmode AS qsnmode,c.rwdtype AS rwdtype ")
				.append(",c.rwdval AS rwdval,c.subcattype AS subcattype ")
				.append(",c.updby AS updby,c.upddate AS upddate ")
				.append(",c.bgthemcolor AS bgthemcolor,c.bgthemimgurl AS bgthemimgurl ")
				.append(",c.bgthemimgurlmob AS bgthemimgurlmob,c.bgthembankid AS bgthembankid ")
				.append(",c.playimgurl AS playimgurl,s.isconactivetext AS isconactivetext ")
				.append("FROM pa_ofltx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ")
				.append("WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY c.consrtdate");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setStatus(rs.getString("isconactivetext"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setRwdType(rs.getString("rwdtype"));
				contestDVO.setRwdVal(rs.getDouble("rwdval"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest count by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest count by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllContestCountByStatusandFilter(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT count(*) from  pa_ofltx_contest_mstr  c join sd_contest_isactive s on c.isconactive=s.isconactive WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append( " AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Save contest SQL.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("insert into pa_ofltx_contest_mstr (clintid,conid,anstype,cardseqno,cattype,conenddate,conname,consrtdate,creby,credate");
		sql.append(",isconactive,iselimtype,noofqns,qsnmode,rwdtype,rwdval,subcattype,bgthemcolor,bgthemimgurl,bgthemimgurlmob,bgthembankid,playimgurl,updby,upddate) ");
		sql.append("values (?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,?,?,?,getDate())");
		Boolean isInserted = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getClId());
			ps.setString(2, co.getCoId());
			ps.setString(3, co.getAnsType());
			ps.setInt(4, co.getSeqNo());
			ps.setString(5, co.getCat());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getEnDate()));
			ps.setString(7, co.getName());
			ps.setTimestamp(8, new java.sql.Timestamp(co.getStDate()));
			ps.setString(9, co.getCrBy());
			ps.setInt(10, co.getIsAct());
			ps.setBoolean(11, co.getIsElimination());
			ps.setInt(12, co.getqSize());
			ps.setString(13, co.getqMode());
			ps.setString(14, co.getRwdType());
			ps.setDouble(15, co.getRwdVal());
			ps.setString(16, co.getSubCat());
			ps.setString(17, co.getThmColor());
			ps.setString(18, co.getThmImgDesk());
			ps.setString(19, co.getThmImgMob());
			ps.setInt(20, co.getThmId());
			ps.setString(21, co.getPlayGameImg());
			ps.setString(22, co.getCrBy());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
			return isInserted;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Update contest SQL.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_ofltx_contest_mstr set ")
		.append("anstype=?,cardseqno=?,cattype=?,")
		.append("conenddate=?,conname=?,consrtdate=?,")
		.append("updby=?,upddate=getDate(),")
		.append("isconactive=?,iselimtype=?,noofqns=?,qsnmode=?,rwdtype=?,")
		.append("rwdval=?,subcattype=?,bgthemcolor=?,bgthemimgurl=?, bgthemimgurlmob=?,bgthembankid=?,playimgurl=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getAnsType());
			ps.setInt(2, co.getSeqNo());
			ps.setString(3, co.getCat());
			ps.setTimestamp(4, new java.sql.Timestamp(co.getEnDate()));
			ps.setString(5, co.getName());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getStDate()));
			ps.setString(7, co.getUpBy());
			ps.setInt(8, co.getIsAct());
			ps.setBoolean(9, co.getIsElimination());
			ps.setInt(10, co.getqSize());
			ps.setString(11, co.getqMode());
			ps.setString(12, co.getRwdType());
			ps.setDouble(13, co.getRwdVal());
			ps.setString(14, co.getSubCat());
			ps.setString(15, co.getThmColor());
			ps.setString(16, co.getThmImgDesk());
			ps.setString(17, co.getThmImgMob());
			ps.setInt(18, co.getThmId());
			ps.setString(19, co.getPlayGameImg());
			ps.setString(20, co.getClId());
			ps.setString(21, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update contest status.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestStatus(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_ofltx_contest_mstr set ")
		.append("isconactive=?,upddate=getDate(),updby=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, co.getIsAct());
			ps.setString(2, co.getUpBy());
			ps.setString(3, co.getClId());
			ps.setString(4, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Delete contest SQL.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContestSQL(String clientId, String coId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer("update  pa_ofltx_contest_mstr set isconactive=? WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, 4);
			ps.setString(2, clientId);
			ps.setString(3, coId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
