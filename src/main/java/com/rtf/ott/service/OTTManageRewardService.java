/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.dto.OTTRerwardTypeDTO;
import com.rtf.ott.sql.dao.SDRewardTypeSQLDAO;
import com.rtf.ott.sql.dvo.RewardTypeDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class OTTManageRewardService.
 */
public class OTTManageRewardService {

	/**
	 * Fetch OTT rewardk list with filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param pgNo
	 *            the pg no
	 * @param filter
	 *            the filter
	 * @return the OTT rerward type DTO
	 */
	public static OTTRerwardTypeDTO fetchOTTRewardkListWithFilter(String clientId, OTTRerwardTypeDTO dto, String pgNo,
			String filter) {
		List<RewardTypeDVO> rwdList = null;
		try {

			String filterQuery = GridHeaderFilterUtil.getAllRewardTypesFilterQuery(filter);

			rwdList = SDRewardTypeSQLDAO.getAllRewardTypesWithFilter(clientId, pgNo, filterQuery,filter);
			Integer count = SDRewardTypeSQLDAO.getAllRewardTypesCountwithFilter(clientId, filterQuery);
			if (rwdList == null || rwdList.size() == 0) {
				dto.setSts(0);
				dto.setMsg(SAASMessages.ADMIN_QB_NOREWARDS);

			}
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setRewardList(rwdList);

			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the reward type data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filter
	 *            the filter
	 * @return the reward type data to export
	 */
	public static List<RewardTypeDVO> getRewardTypeDataToExport(String clientId, String filter) {
		List<RewardTypeDVO> rwdList = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getAllRewardTypesFilterQuery(filter);
			rwdList = SDRewardTypeSQLDAO.getRewardTypeDataToExport(clientId, filterQuery);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return rwdList;
	}

	/**
	 * Creates the reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @param userId
	 *            the user id
	 * @return the OTT rerward type DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTRerwardTypeDTO createRewardType(String clientid, OTTRerwardTypeDTO dto, String userId)
			throws Exception {
		try {
			RewardTypeDVO dvo = dto.getReward();
			Integer sts = SDRewardTypeSQLDAO.createRewardType(clientid, dvo, userId);
			dto.setSts(sts);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Update reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @param userId
	 *            the user id
	 * @return the OTT rerward type DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTRerwardTypeDTO updateRewardType(String clientid, OTTRerwardTypeDTO dto, String userId)
			throws Exception {
		dto.setSts(0);
		Integer updCnt = 0;
		try {
			RewardTypeDVO dvo = dto.getReward();
			if (GenUtil.isNullOrEmpty(dvo.getRwdimgurl())) {
				updCnt = SDRewardTypeSQLDAO.updateRewardTypewithNoImage(clientid, dvo, userId);

			} else {
				updCnt = SDRewardTypeSQLDAO.updateRewardType(clientid, dvo, userId);
			}
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Deletee reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param rewardType
	 *            the reward type
	 * @param userId
	 *            the user id
	 * @param dto
	 *            the dto
	 * @return the OTT rerward type DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTRerwardTypeDTO deleteeRewardType(String clientid, String rewardType, String userId,
			OTTRerwardTypeDTO dto) throws Exception {
		try {
			dto.setSts(0);
			Integer updCnt = SDRewardTypeSQLDAO.deleteRewardType(clientid, rewardType, userId);
			if (updCnt == 1) {
				dto.setSts(1);
			}
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

}
