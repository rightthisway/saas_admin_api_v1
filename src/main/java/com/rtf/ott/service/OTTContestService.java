/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.cass.dao.ContestDAO;
import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.sql.dao.ContestSQLDAO;

/**
 * The Class OTTContestService.
 */
public class OTTContestService {

	/**
	 * Gets the SQL contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the SQL contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getSQLContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = ContestSQLDAO.getContestByContestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the all contest by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestByStatusandFilter(String clId, String status, String filter,
			String pgNo) throws Exception {
		List<ContestDVO> list = null;
		try {
			list = ContestSQLDAO.getAllContestByStatusandFilter(clId, status, filter, pgNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the all contest data to export.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestDataToExport(String clId, String status, String filter)
			throws Exception {
		List<ContestDVO> list = null;
		try {
			list = ContestSQLDAO.getAllContestDataToExport(clId, status, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the all contest count by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest count by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllContestCountByStatusandFilter(String clId, String status, String filter)
			throws Exception {
		Integer count = 0;
		try {
			count = ContestSQLDAO.getAllContestCountByStatusandFilter(clId, status, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Save contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContest(ContestDVO contest) throws Exception {
		boolean isInserted = false;
		try {
			isInserted = ContestSQLDAO.saveContestSQL(contest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isInserted;
	}

	/**
	 * Update contest status.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 */
	public static boolean updateContestStatus(ContestDVO contest) {
		boolean isInserted = false;
		try {
			isInserted = ContestSQLDAO.updateContestStatus(contest);
			if (isInserted && (contest.getIsAct() == 2 || contest.getIsAct().equals(2))) {
				isInserted = ContestDAO.saveContestCass(contest);
			} else if (isInserted && (contest.getIsAct() == 3 || contest.getIsAct().equals(3))) {
				isInserted = ContestDAO.deleteContestCass(contest.getClId(), contest.getCoId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isInserted;
	}

	/**
	 * Update contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContest(ContestDVO contest) throws Exception {
		boolean isUpdated = false;
		try {
			isUpdated = ContestSQLDAO.updateContestSQL(contest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}

	/**
	 * Delete contest.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContest(String clId, String coId) throws Exception {
		boolean isDeleted = false;
		try {
			isDeleted = ContestSQLDAO.deleteContestSQL(clId, coId);
			if (isDeleted) {
				isDeleted = ContestDAO.deleteContestCass(clId, coId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isDeleted;
	}
}
