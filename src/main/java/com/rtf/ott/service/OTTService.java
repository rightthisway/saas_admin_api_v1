/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.Collections;
import java.util.List;

import com.rtf.ott.cass.dao.ContestDAO;
import com.rtf.ott.cass.dao.ContestPlayerTrackingDAO;
import com.rtf.ott.cass.dao.ContestWinnerDAO;
import com.rtf.ott.cass.dao.CustomerAnswerDAO;
import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.cass.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.cass.dvo.ContestWinnerDVO;
import com.rtf.ott.cass.dvo.CustomerAnswerDVO;
import com.rtf.ott.util.ContestPlayerTrackingComp;

/**
 * The Class OTTService.
 */
public class OTTService {
	
	/**
	 * Gets the last contest tracking by customer.
	 *
	 * @param coId the co id
	 * @param cuId the cu id
	 * @return the last contest tracking by customer
	 */
	public static ContestPlayerTrackingDVO getLastContestTrackingByCustomer(String coId,String cuId){
		try {
			List<ContestPlayerTrackingDVO> trackings = ContestPlayerTrackingDAO.getContestTrackingByCustomer(coId, cuId);
			if(trackings != null && !trackings.isEmpty()){
				Collections.sort(trackings, new ContestPlayerTrackingComp());
				ContestPlayerTrackingDVO track = trackings.get(0);
				return track;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the contest tracking by play id.
	 *
	 * @param coId the co id
	 * @param cuId the cu id
	 * @param playId the play id
	 * @return the contest tracking by play id
	 */
	public static ContestPlayerTrackingDVO getContestTrackingByPlayId(String coId,String cuId,String playId){
		try {
			ContestPlayerTrackingDVO tracking = ContestPlayerTrackingDAO.getContestTrackingByPlayId(coId, cuId,playId);
			return tracking;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the customer correct answers.
	 *
	 * @param coId the co id
	 * @param cuId the cu id
	 * @param playId the play id
	 * @return the customer correct answers
	 */
	public static List<CustomerAnswerDVO> getCustomerCorrectAnswers(String coId,String cuId,String playId){
		try {
			List<CustomerAnswerDVO> trackings = CustomerAnswerDAO.getCustomerCorrectAnswers(coId, cuId,playId);
			return trackings;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * Save contest winner.
	 *
	 * @param winner the winner
	 * @return the boolean
	 */
	public static Boolean saveContestWinner(ContestWinnerDVO winner){
		try {
			ContestWinnerDAO.saveContestWinner(winner);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Save contest tracking.
	 *
	 * @param tracking the tracking
	 * @return the boolean
	 */
	public static Boolean saveContestTracking(ContestPlayerTrackingDVO tracking){
		try {
			ContestPlayerTrackingDAO.saveContestTracking(tracking);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	

	/**
	 * Fetch OTT contest info.
	 *
	 * @param clientId the client id
	 * @param contestId the contest id
	 * @return the contest DVO
	 * @throws Exception the exception
	 */
	public static ContestDVO fetchOTTContestInfo(String clientId,String contestId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
		contestMstrDVO = ContestDAO.getContestByContestId(clientId,contestId);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}
	
	
	
	/**
	 * Gets the rounded rwd val.
	 *
	 * @param val the val
	 * @return the rounded rwd val
	 */
	public static String getRoundedRwdVal(Double val) {
		
		Integer intVal= val.intValue();
		if(val > intVal) {
			return String.format("%.2f", val);
		} else {
			return ""+intVal;
		}
		
	}
}
