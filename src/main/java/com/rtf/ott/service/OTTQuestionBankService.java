/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.cass.dao.QuestionBankDAO;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class OTTQuestionBankService.
 */
public class OTTQuestionBankService {

	/**
	 * Fetch OTT question bank list.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param pgNo
	 *            the pg no
	 * @param filter
	 *            the filter
	 * @return the OTT question DTO
	 */
	public static OTTQuestionDTO fetchOTTQuestionBankList(String clientId, OTTQuestionDTO dto, String pgNo,
			String filter) {
		List<QuestionBankDVO> qbList = null;
		try {
			Integer count = 0;
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = QuestionBankSQLDAO.getAllOLTXQuestionsFromQBankWithFilter(clientId, pgNo, filterQuery, filter);
			if (qbList == null || qbList.size() == 0) {
				dto.setSts(0);
				dto.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = QuestionBankSQLDAO.getAllOLTXQuestionsCountFromQBankWithFilter(clientId, filterQuery);
			}
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setQbList(qbList);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Fetch OTT question bank listfor contest id.
	 *
	 * @param respDTO
	 *            the resp DTO
	 * @param filter
	 *            the filter
	 * @param catType
	 *            the cat type
	 * @param subCatType
	 *            the sub cat type
	 * @param pgNo
	 *            the pg no
	 * @return the OTT question DTO
	 */
	public static OTTQuestionDTO fetchOTTQuestionBankListforContestId(OTTQuestionDTO respDTO, String filter,
			String catType, String subCatType, String pgNo) {
		List<QuestionBankDVO> qbList = null;
		try {

			Integer count = 0;
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = QuestionBankSQLDAO.getAllOLTXQuestionsFromQBankByContestIdWithFilter(respDTO.getClId(),
					respDTO.getCoId(), catType, subCatType, filterQuery, pgNo,filter);
			if (qbList == null || qbList.size() == 0) {
				respDTO.setSts(0);
				respDTO.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = QuestionBankSQLDAO.getAllOLTXQuestionsCountFromQBankByContestIdWithFilter(respDTO.getClId(),
						respDTO.getCoId(), catType, subCatType, filterQuery);
			}
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setQbList(qbList);
			respDTO.setSts(1);
		} catch (Exception ex) {
			respDTO.setSts(0);
			ex.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Creates the question.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT question DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTQuestionDTO createQuestion(String clientid, OTTQuestionDTO dto) throws Exception {
		try {
			QuestionBankDVO dvo = dto.getOttqbdo();
			dvo = QuestionBankSQLDAO.createQuestion(clientid, dvo);
			if (dvo.getQsnbnkid() == null) {
				dto.setSts(0);
			} else {
				dto.setSts(1);
			}
			if (1 == dto.getSts()) {
				dvo.setClintid(clientid);
				QuestionBankDAO.saveQuestionBank(dvo);
			}

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Update question.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT question DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTQuestionDTO updateQuestion(String clientid, OTTQuestionDTO dto) throws Exception {
		dto.setSts(0);
		try {
			QuestionBankDVO dvo = dto.getOttqbdo();
			Integer updCnt = QuestionBankSQLDAO.updateQuestion(clientid, dvo);
			dto.setSts(updCnt);
			if (1 == updCnt) {
				dvo.setClintid(clientid);
				QuestionBankDAO.updateQuestionBank(dvo);
			}
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}
	
	
	
	/**
	 * Update question fact.
	 *
	 * @param clientid
	 *            the clientid
	 * @param qbId
	 *            the qbId
	 * @param isChecked
	 *            the isChecked
	 * @return the Boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateQuestionFact(String clientid,  Integer qbId,Boolean isChecked) throws Exception {
		Boolean isUpd = false;
		try {
			isUpd = QuestionBankSQLDAO.updateQuestionFact(clientid, qbId,isChecked);
		} catch (Exception ex) {
			isUpd = false;
		}
		return isUpd;
	}

	/**
	 * Delete question.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT question DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTQuestionDTO deleteQuestion(String clientid, OTTQuestionDTO dto) throws Exception {
		try {
			dto.setSts(0);
			QuestionBankDVO dvo = dto.getOttqbdo();
			Integer updCnt = QuestionBankSQLDAO.deleteQuestion(clientid, dvo);
			if (updCnt == 1) {
				dto.setSts(1);
				dvo.setClintid(clientid);
				QuestionBankDAO.deleteQuestionBank(dvo);
			}

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the questions count by category subcategory and question type.
	 *
	 * @param clientId
	 *            the client id
	 * @param category
	 *            the category
	 * @param subCat
	 *            the sub cat
	 * @param qMode
	 *            the q mode
	 * @return the questions count by category subcategory and question type
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getQuestionsCountByCategorySubcategoryAndQuestionType(String clientId, String category,
			String subCat, String qMode) throws Exception {
		Integer count = 0;
		try {
			count = QuestionBankSQLDAO.getQuestionsCountByCategorySubcategoryAndQuestionType(clientId, category, subCat,
					qMode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

}
