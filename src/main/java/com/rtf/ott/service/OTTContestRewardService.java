/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.cass.dvo.ContestRewardDVO;
import com.rtf.ott.sql.dao.ContestRewardDAO;
import com.rtf.saas.sql.dao.RewardTypeDAO;
import com.rtf.saas.sql.dvo.RewardTypeDVO;

/**
 * The Class OTTContestRewardService.
 */
public class OTTContestRewardService {

	/**
	 * Ge all contest rewards.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return the list
	 */
	public static List<ContestRewardDVO> geAllContestRewards(String clId, String coId) {
		try {
			List<ContestRewardDVO> rewards = ContestRewardDAO.geAlltContestRewards(clId, coId);
			return rewards;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Ge all reward types.
	 *
	 * @param clId
	 *            the cl id
	 * @return the list
	 */
	public static List<RewardTypeDVO> geAllRewardTypes(String clId) {
		try {
			List<RewardTypeDVO> rewards = RewardTypeDAO.geAllRewardTypes(clId);
			return rewards;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all contest rewards.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return the all contest rewards
	 */
	public static List<ContestRewardDVO> getAllContestRewards(String clId, String coId) {
		try {
			List<ContestRewardDVO> rewards = ContestRewardDAO.getAllContestRewards(clId, coId);
			return rewards;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Update contest rewards.
	 *
	 * @param cr
	 *            the cr
	 * @param coId
	 *            the co id
	 * @return the boolean
	 */
	public static Boolean updateContestRewards(List<ContestRewardDVO> cr, String coId) {
		Boolean isUpdate = false;
		try {
			isUpdate = ContestRewardDAO.updateContestRewards(cr, coId);
			if (isUpdate) {
				isUpdate = com.rtf.ott.cass.dao.ContestRewardDAO.updateContestRewards(cr, coId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdate;
	}

}
