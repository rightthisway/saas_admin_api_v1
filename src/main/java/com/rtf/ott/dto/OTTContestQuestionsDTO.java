/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class OTTContestQuestionsDTO.
 */
public class OTTContestQuestionsDTO extends RtfSaasBaseDTO {

	/** The cont questions. */
	private ContestQuestionDVO contQuestions;

	/** The list. */
	private List<ContestQuestionDVO> list;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the cont questions.
	 *
	 * @return the cont questions
	 */
	public ContestQuestionDVO getContQuestions() {
		return contQuestions;
	}

	/**
	 * Sets the cont questions.
	 *
	 * @param contQuestions
	 *            the new cont questions
	 */
	public void setContQuestions(ContestQuestionDVO contQuestions) {
		this.contQuestions = contQuestions;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<ContestQuestionDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list
	 *            the new list
	 */
	public void setList(List<ContestQuestionDVO> list) {
		this.list = list;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
