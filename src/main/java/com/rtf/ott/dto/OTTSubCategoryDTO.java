/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.sql.dvo.SubCategoryDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class OTTSubCategoryDTO.
 */
public class OTTSubCategoryDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private SubCategoryDVO dvo;

	/** The pagination. */
	private PaginationDTO pagination;

	/** The s catlist. */
	private List<SubCategoryDVO> sCatlist;

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public SubCategoryDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo
	 *            the new dvo
	 */
	public void setDvo(SubCategoryDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the s catlist.
	 *
	 * @return the s catlist
	 */
	public List<SubCategoryDVO> getsCatlist() {
		return sCatlist;
	}

	/**
	 * Sets the s catlist.
	 *
	 * @param sCatlist
	 *            the new s catlist
	 */
	public void setsCatlist(List<SubCategoryDVO> sCatlist) {
		this.sCatlist = sCatlist;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
