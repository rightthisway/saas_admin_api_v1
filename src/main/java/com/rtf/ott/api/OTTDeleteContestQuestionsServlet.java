/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTDeleteContestQuestionsServlet.
 */
@WebServlet("/ottDeleteContQuest.json")
public class OTTDeleteContestQuestionsServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5527141175378199141L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Delete Contest question
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsnIdsStr = request.getParameter("qIds");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			String userName = "";
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsnIdsStr == null || qsnIdsStr.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			String[] qIdsArr = qsnIdsStr.split(",");
			if (qIdsArr.length == 0) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<ContestQuestionDVO> contQuestions = new ArrayList<ContestQuestionDVO>();
			for (String qidStr : qIdsArr) {
				try {
					Integer qId = Integer.parseInt(qidStr);
					ContestQuestionDVO contQuestion = OTTContestQuestionsService.getcontestquestionsByQuestionId(clId,
							contId, qId);
					if (contQuestion == null) {
						setClientMessage(respDTO, OTTMessageConstant.QUESTION_ID_NOT_EXIT, null);
						generateResponse(request, response, respDTO);
						return;
					}
					contQuestions.add(contQuestion);
				} catch (Exception e) {
					setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			List<ContestQuestionDVO> deletedList = new ArrayList<ContestQuestionDVO>();
			for (ContestQuestionDVO contQuestion : contQuestions) {
				Integer updateCnt = OTTContestQuestionsService.deleteContestQuestions(clId, contQuestion);
				if (updateCnt != null && updateCnt > 0) {
					deletedList.add(contQuestion);
				}
			}
			List<ContestQuestionDVO> updatedList = new ArrayList<ContestQuestionDVO>();
			List<ContestQuestionDVO> list = OTTContestQuestionsService.getcontestquestionsBycontestId(clId, contId);
			if (list != null && !list.isEmpty()) {
				int seqNo = 1;
				for (ContestQuestionDVO contestQuestion : list) {
					if (!contestQuestion.getQsnseqno().equals(seqNo)) {
						contestQuestion.setQsnseqno(seqNo);
						contestQuestion.setUpdby(userName);
						contestQuestion.setUpddate(new Date());

						updatedList.add(contestQuestion);
					}
					seqNo++;
				}
			}
			if (!updatedList.isEmpty()) {
				OTTContestQuestionsService.updateContestQuestionsSeqno(clId, updatedList);
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, OTTMessageConstant.CONTEST_QUEST_DEETE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
