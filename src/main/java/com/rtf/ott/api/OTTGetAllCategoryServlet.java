/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.dto.OTTCategoryDTO;
import com.rtf.ott.service.OTTCategoryService;
import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTGetAllCategoryServlet.
 */
@WebServlet("/ottgetallcategory.json")
public class OTTGetAllCategoryServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -3836899376972391935L;

	/**
	 * Generate Http Response for OTT contest category Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get all category list based on filter
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String sts = request.getParameter("sts");
		String pNo = request.getParameter("pgNo");
		String filter = request.getParameter("hfilter");

		OTTCategoryDTO respDTO = new OTTCategoryDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(sts)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONT_STATUS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(pNo)) {
				pNo = "1";
			}

			respDTO = OTTCategoryService.fetchOTTCategorListWithFilter(clId, respDTO, pNo, filter);
			if (respDTO.getSts() == 0) {
				if (respDTO.getMsg() == null || respDTO.getMsg().isEmpty()) {
					setClientMessage(respDTO, null, null);
					generateResponse(request, response, respDTO);
					return;
				}

			}
			List<CategoryDVO> catList = respDTO.getCatlist();
			if (respDTO.getSts() == 1) {
				if (catList == null || catList.size() == 0)
					respDTO.setMsg(SAASMessages.ADMIN_QB_NOCATEGORY);
			}
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
