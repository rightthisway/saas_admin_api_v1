/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.service.OTTQuestionBankService;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTStartContestServlet.
 */
@WebServlet("/ottstartcontest.json")
public class OTTStartContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -7850169573989063216L;

	/**
	 * Generate Http Response for OTT contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Update Contest status as Started
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cau = request.getParameter("cau");
		OTTContestDTO respDTO = new OTTContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() == 2) {
				setClientMessage(respDTO, OTTMessageConstant.SELECTED_CONTEST_IS_ALREADY_LIVE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() != 1 || !contest.getIsAct().equals(1)) {
				setClientMessage(respDTO, OTTMessageConstant.ONLY_ACTIVE_CONTEST_CAN_BE_STARTED, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Date today = new Date();
			if (contest.getStDate() > today.getTime()) {
				setClientMessage(respDTO, OTTMessageConstant.CANT_START_FUTURE_DATE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getEnDate() < today.getTime()) {
				setClientMessage(respDTO, OTTMessageConstant.CANT_START_PAST_END_DATE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getqMode().equalsIgnoreCase(OTTMessageConstant.CONT_QUEST_MODE_FIXED)) {
				Integer count = OTTContestQuestionsService.getContestQuestionsCount(contest.getClId(),
						contest.getCoId());
				if (count == null || count == 0) {
					setClientMessage(respDTO, OTTMessageConstant.ADD_QUESTION_TO_CONTEST_TO_START, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (count != contest.getqSize() || !count.equals(contest.getqSize())) {
					setClientMessage(respDTO,
							OTTMessageConstant.CONT_NO_OF_QUESTION_MISMATCHES,
							null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else if (contest.getqMode().equalsIgnoreCase(OTTMessageConstant.CONT_QUEST_MODE_RANDOM)) {
				Integer count = OTTQuestionBankService.getQuestionsCountByCategorySubcategoryAndQuestionType(
						contest.getClId(), contest.getCat(), contest.getSubCat(), contest.getAnsType());
				if (count < contest.getqSize()) {
					setClientMessage(respDTO,
							OTTMessageConstant.QUEST_BANK_NOT_HAVE_CONT_NO_OF_QUESTIONS,
							null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			contest.setUpBy(cau);
			contest.setIsAct(2);
			Boolean isUpdated = OTTContestService.updateContestStatus(contest);
			respDTO.setContestMstrDVO(contest);
			if (isUpdated) {
				respDTO.setSts(1);
				respDTO.setMsg(OTTMessageConstant.CONTEST_UPDATE_TO_ACTIVE_SUCCESS_MSG);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(0);
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;

	}

}
