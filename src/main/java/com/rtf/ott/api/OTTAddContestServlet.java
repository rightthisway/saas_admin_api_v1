/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestDTO;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.util.DateFormatUtil;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTAddContestServlet.
 */
@WebServlet("/ottaddcontest.json")
public class OTTAddContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -6261179278981378207L;

	/**
	 * Generate Http Response for OTT contest Response data is sent in JSON format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Ad Contest to system
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String name = request.getParameter("name");
		String stDate = request.getParameter("stDate");
		String enDate = request.getParameter("enDate");
		String cat = request.getParameter("cat");
		String subCat = request.getParameter("subCat");
		String qMode = request.getParameter("qMode");
		String qType = request.getParameter("qType");
		String elmType = request.getParameter("elmType");
		String rwdType = request.getParameter("rwdType");
		String rwdVal = request.getParameter("rwdVal");
		String qSize = request.getParameter("qSize");
		String dSeq = request.getParameter("dSeq");

		OTTContestDTO respDTO = new OTTContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			String userName = "";
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_START_DATE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cat == null || cat.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CATEGORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (subCat == null || subCat.trim().isEmpty()) {
				subCat = null;
			}
			if (qMode == null || qMode.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_MODE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qType == null || qType.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_MODE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (elmType == null || elmType.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_ELIMINATION_TYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qSize == null || qSize.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.NO_OF_QUESTION_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (rwdType == null || rwdType == OTTMessageConstant.REWARD_TYPE_0
					|| rwdType.equalsIgnoreCase(OTTMessageConstant.REWARD_TYPE_0) || rwdType.isEmpty()) {
				rwdType = null;
			}
			if ((rwdType != null && !rwdType.isEmpty()) && (rwdVal == null || rwdVal.isEmpty())) {
				setClientMessage(respDTO, OTTMessageConstant.WINNER_PRIZE_VALUE_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if ((rwdType == null || rwdType.isEmpty()) && (rwdVal != null && !rwdVal.isEmpty())) {
				setClientMessage(respDTO, OTTMessageConstant.WINNER_PRIZE_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if ((rwdType != null && !rwdType.isEmpty()) && (rwdVal == OTTMessageConstant.REWARD_TYPE_0
					|| rwdVal.equalsIgnoreCase(OTTMessageConstant.REWARD_TYPE_0))) {
				setClientMessage(respDTO, OTTMessageConstant.WINNER_PRIZE_VALUE_VALID_NUMBER, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date sDate = null;
			Date eDate = null;
			Double rwdValue = 0.0;
			Integer queSize = 0;
			Integer seq = 0;

			String stDate1 = stDate + " 00:00:00";
			String enDate1 = enDate + " 23:59:00";

			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			eDate = DateFormatUtil.getDateWithTwentyFourHourFormat(enDate1);

			if (rwdVal != null && !rwdVal.isEmpty()) {
				try {
					rwdValue = Double.parseDouble(rwdVal);
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO, OTTMessageConstant.INVALID_WINNER_PRIZE_VALUE, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			try {
				queSize = Integer.parseInt(qSize);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, OTTMessageConstant.INVALID_NO_OF_QUEST, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (dSeq != null && !dSeq.isEmpty()) {
				try {
					seq = Integer.parseInt(dSeq);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Date now = new Date();
			if (eDate.before(now)) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_DATE_SHOULD_BE_TODAY_OR_FUTURE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = new ContestDVO();
			contest.setAnsType(qType);
			contest.setCat(cat);
			contest.setCrBy(cau);
			contest.setClId(clId);
			contest.setClImgU(null);
			contest.setCoId(clId + now.getTime());
			contest.setCrBy(userName);
			contest.setCrDate(now.getTime());
			contest.setEnDate(eDate.getTime());
			contest.setImgU(null);
			contest.setIsAct(1);
			contest.setIsClImg(null);
			contest.setIsElimination(elmType.equalsIgnoreCase("true") ? true : false);
			contest.setName(name);
			contest.setqMode(qMode);
			contest.setqSize(queSize);
			contest.setRwdType(rwdType);
			contest.setRwdVal(rwdValue);
			contest.setSeqNo(seq);
			contest.setStDate(sDate.getTime());
			contest.setSubCat(subCat);
			contest.setThmId(0);

			boolean isSaved = OTTContestService.saveContest(contest);
			if (isSaved) {
				respDTO.setSts(1);
				respDTO.setMsg(OTTMessageConstant.CONTEST_CREATED_SUCCESS_MSG);
				respDTO.setContestMstrDVO(contest);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
