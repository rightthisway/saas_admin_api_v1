/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTGetAllContestQuestionsServlet.
 */
@WebServlet("/ottgetAllContQust.json")
public class OTTGetAllContestQuestionsServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -5896460547040819972L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get all contest questions list based on filter
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String filter = request.getParameter("hfilter");
		String pgNo = request.getParameter("pgNo");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		respDTO.setCoId(coId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pgNo == null || pgNo.isEmpty()) {
				pgNo = "1";
			}

			respDTO = OTTContestQuestionsService.getAllContestQuestionsByContestIdandFilter(respDTO, filter, pgNo);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
