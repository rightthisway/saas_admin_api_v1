/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.utils;

/**
 * The Class NoReprtRecsFoundExeption.
 */
public class NoReprtRecsFoundExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3778794073795276518L;

	/** The msg. */
	private String msg;

	/** The report name. */
	private String reportName;

	/** The cl id. */
	private String clId;

	/**
	 * Instantiates a new no reprt recs found exeption.
	 */
	public NoReprtRecsFoundExeption() {
		super();
	}

	/**
	 * Instantiates a new no reprt recs found exeption.
	 *
	 * @param reportName the report name
	 * @param clId       the cl id
	 * @param msg        the msg
	 */
	public NoReprtRecsFoundExeption(String reportName, String clId, String msg) {
		super();
		this.msg = msg;
		this.clId = clId;
		this.reportName = reportName;

	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Gets the report name.
	 *
	 * @return the report name
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * Sets the report name.
	 *
	 * @param reportName the new report name
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

}
