/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class CustContestStatsnBsnsReport.
 */
@WebServlet("/custcontstatsbsnsreport.json")
public class CustContestStatsnBsnsReport extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -7324294247395461108L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * get customer contest stats report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;

		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(ReportConstants.CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT_SHEETNAME);
			String reportName = ReportConstants.CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT;

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", reportName);

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);

			startRowCnt = startRowCnt + 2;

			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Heading");
			headerList.add("Value");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			List<ContestWinnerDVO> reportList = new ArrayList<ContestWinnerDVO>();

			if (reportList == null || reportList.size() == 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}

			int j = 0;
			int rowCount = startRowCnt + 1;
			for (ContestWinnerDVO dvo : reportList) {
				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dvo.getCuId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCuId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getRwdType(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getRwdVal(), j, rowhead);
				j++;

			}
			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}

	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		String xlsFileName = ReportConstants.CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT_FNAME + ".";
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment; filename=" + xlsFileName + ReportsUtil.EXCEL_EXTENSION); // Shiva
		workbook.write(response.getOutputStream());
		workbook.close();
	}
}
