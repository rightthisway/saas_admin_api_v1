/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.reports.dto.NoOfTriviaGamesGrpDTO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class NoOfTriviaGamesGraph.
 */
@WebServlet("/nooftriviagamegrp.json")
public class NoOfTriviaGamesGraph extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get No Of Trivia Games conducted Graph
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		NoOfTriviaGamesGrpDTO respDTO = new NoOfTriviaGamesGrpDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

}
