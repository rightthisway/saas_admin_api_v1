/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportMessageConstant;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class MonthlyParticipantsCountStatisticsReport.
 */
@WebServlet("/monthlyparticipantscountreport.json")
public class MonthlyParticipantsCountStatisticsReport extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get Monthly contest participants count report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (fromDate == null || fromDate.isEmpty()) {
				msg = ReportMessageConstant.INVALID_FROM_DATE;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (toDate == null || toDate.isEmpty()) {
				msg = ReportMessageConstant.INVALID_TO_DATE;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			// For Sheet 1
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Month");
			headerList1.add("No. Of Games Played");
			headerList1.add("Participants Count");
			headerList1.add("Unique Participants Count");
			headerList1.add("Old User Participants Count");
			headerList1.add("Average Viewership");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("MonthlyParticipantsStatistics");

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", "Monthly Participants Statistics");

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet1, workbook, startRowCnt);
			startRowCnt = startRowCnt + 2;

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Integer totalNoOfGamesPlayed = 0;
			Integer overAllParticipantsCount = 0;

			List<Object[]> dataList = LivtContestReportsDAO
					.getMonthlyParticipantsCountStatisticsReportDataForSheetOne(clId);
			if (dataList == null || dataList.size() <= 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet1, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
			} else {
				ExcelUtil.generateExcelHeaderRowWithStyles(headerList1, ssSheet1, workbook, startRowCnt);

				int j = 0;
				int rowCount = startRowCnt + 1;

				for (Object[] dataObj : dataList) {

					Row rowhead = ssSheet1.createRow(rowCount);
					rowCount++;
					j = 0;
					ReportsUtil.getExcelStringCell(dataObj[0], j, rowhead);
					j++;
					ReportsUtil.getExcelIntegerCell(dataObj[1], j, rowhead);
					j++;
					ReportsUtil.getExcelIntegerCell(dataObj[2], j, rowhead);
					j++;
					ReportsUtil.getExcelIntegerCell(dataObj[3], j, rowhead);
					j++;
					ReportsUtil.getExcelIntegerCell(dataObj[4], j, rowhead);
					j++;
					ReportsUtil.getExcelDecimalCell(dataObj[5], j, rowhead);
					j++;

					if (dataObj[1] != null) {
						totalNoOfGamesPlayed += Integer.parseInt(dataObj[1].toString());
					}
					if (dataObj[2] != null) {
						overAllParticipantsCount += Integer.parseInt(dataObj[2].toString());
					}

				}

				if (totalNoOfGamesPlayed > 0) {
					rowCount++;
					ssSheet1.createRow(rowCount);
					Row rowheadA = ssSheet1.createRow(rowCount);
					ReportsUtil.getExcelStringCell("Total No. Of Games Played", 0, rowheadA);
					ReportsUtil.getExcelIntegerCell(totalNoOfGamesPlayed, 1, rowheadA);
				}
				if (overAllParticipantsCount > 0) {
					rowCount++;
					ssSheet1.createRow(rowCount);
					Row rowheadB = ssSheet1.createRow(rowCount);
					ReportsUtil.getExcelStringCell("Overall Participants Count", 0, rowheadB);
					ReportsUtil.getExcelIntegerCell(overAllParticipantsCount, 1, rowheadB);
				}
			}

			// For Sheet 2
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("Month");
			headerList2.add("Contest Name");
			headerList2.add("Contest Date");
			headerList2.add("No. Of Participants");

			Sheet ssSheet2 = workbook.createSheet("ContestWiseParticipantsCount");
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);
			startRowCnt = 0;
			startRowCnt = startRowCnt + 2;

			List<Object[]> dataListb = LivtContestReportsDAO
					.getMonthlyParticipantsCountStatisticsReportDataForSheetTwo(clId, fromDate, toDate);
			if (dataList == null || dataList.size() <= 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet2, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
			} else {
				ExcelUtil.generateExcelHeaderRowWithStyles(headerList2, ssSheet2, workbook, startRowCnt);

				int k = 0;
				int rowCountk = startRowCnt + 1;
				int totNoOfGamesPlayed = 0;
				overAllParticipantsCount = 0;

				for (Object[] dataObj : dataListb) {

					Row rowhead = ssSheet2.createRow(rowCountk);
					rowCountk++;
					k = 0;
					ReportsUtil.getExcelStringCell(dataObj[0], k, rowhead);
					k++;
					ReportsUtil.getExcelStringCell(dataObj[1], k, rowhead);
					k++;
					ReportsUtil.getExcelDateCell(dataObj[2], k, rowhead, cellStyleWithHourMinute);
					k++;
					ReportsUtil.getExcelIntegerCell(dataObj[3], k, rowhead);
					k++;

					if (dataObj[1] != null) {
						totNoOfGamesPlayed++;
					}
					if (dataObj[3] != null) {
						overAllParticipantsCount += Integer.parseInt(dataObj[3].toString());
					}

				}
				if (totNoOfGamesPlayed > 0) {
					rowCountk++;
					ssSheet2.createRow(rowCountk);
					Row rowheadA = ssSheet2.createRow(rowCountk);
					ReportsUtil.getExcelStringCell("Total No. Of Games Played", 0, rowheadA);
					ReportsUtil.getExcelIntegerCell(totNoOfGamesPlayed, 1, rowheadA);
				}
				if (overAllParticipantsCount > 0) {
					rowCountk++;
					ssSheet2.createRow(rowCountk);
					Row rowheadB = ssSheet2.createRow(rowCountk);
					ReportsUtil.getExcelStringCell("Overall Participants Count", 0, rowheadB);
					ReportsUtil.getExcelIntegerCell(overAllParticipantsCount, 1, rowheadB);
				}
			}
			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MonthlyParticipantsStatistics." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}
}
