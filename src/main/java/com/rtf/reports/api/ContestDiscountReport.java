/*
 * *************************************************************************************************
 * ** Copyright (c) 2021 RTFLIVE. All rights reserved. SaaS Product Line Release : 1.00 Author :
 * RTFLIVE
 * *************************************************************************************************
 * **
 */
package com.rtf.reports.api;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import com.rtf.reports.dto.GenReportDTO;
import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportMessageConstant;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ContestSummaryWinnerReport.
 */
@WebServlet("/consdiscreport.json")
public class ContestDiscountReport extends RtfSaasBaseServlet {

  private static final long serialVersionUID = 891590446276645826L;

  /**
   * Generate Http Response for Rtf base Dto data is sent in JSON format.
   *
   * @param request the request
   * @param response the response
   * @param rtfSaasBaseDTO the rtf saas base DTO
   * @throws ServletException the servlet exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @Override
  public void generateResponse(HttpServletRequest request, HttpServletResponse response,
      RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
    Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
    map.put("resp", rtfSaasBaseDTO);
    String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
    PrintWriter out = response.getWriter();
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    response.addHeader("Access-Control-Allow-Origin", "*");
    response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
    response.addHeader("Access-Control-Allow-Headers",
        "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
    out.print(ottrespStr);
    out.flush();
  }

  /**
   * get all contest summary winners list report
   *
   * @param request the request
   * @param response the response
   * @throws ServletException the servlet exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @Override
  public void processRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String clId = request.getParameter("clId");
    String coId = request.getParameter("coId");
    String cau = request.getParameter("cau");
    // String fromDate = request.getParameter("fromDate");
    // String toDate = request.getParameter("toDate");

    RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
    respDTO.setSts(0);
    String msg = null;
    try {
      if (clId == null || clId.isEmpty()) {
        msg = UserMsgConstants.INVALID_CLIENT;
        setClientMessage(respDTO, msg, null);
        generateResponse(request, response, respDTO);
        return;
      }
      /*
       * if (fromDate == null || fromDate.isEmpty()) { msg =
       * ReportMessageConstant.INVALID_FROM_DATE; setClientMessage(respDTO, msg, null);
       * generateResponse(request, response, respDTO); return; }
       */
      /*
       * if (toDate == null || toDate.isEmpty()) { msg = ReportMessageConstant.INVALID_TO_DATE;
       * setClientMessage(respDTO, msg, null); generateResponse(request, response, respDTO); return;
       * }
       */
      if (coId == null || coId.isEmpty()) {
        msg = ReportMessageConstant.INVALID_CONTEST_ID;
        setClientMessage(respDTO, msg, null);
        generateResponse(request, response, respDTO);
        return;
      }
      if (cau == null || cau.isEmpty()) {
        msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
        setClientMessage(respDTO, msg, null);
        generateResponse(request, response, respDTO);
        return;
      }

      SXSSFWorkbook workbook = new SXSSFWorkbook();
      Sheet ssSheet = workbook.createSheet("ContestDiscount");
      CreationHelper createHelper = workbook.getCreationHelper();
      CellStyle cellStyle = workbook.createCellStyle();
      CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
      cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
      cellStyleWithHourMinute
          .setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

      Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
      reportNameMap.put("Report Name", "Contest Discount Report");

      int startRowCnt = 0;
      ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);
      startRowCnt = startRowCnt + 2;

      List<String> lst = LivtContestReportsDAO.getContestNamenDateDets(clId, coId);
      Map<String, String> reportSummaryDataNameMap = new LinkedHashMap<String, String>();
      reportSummaryDataNameMap.put("Contest Name", lst.get(0));
      reportSummaryDataNameMap.put("Contest Date", lst.get(1));

      ExcelUtil.addReportSummaryDataTitle(reportSummaryDataNameMap, ssSheet, workbook, startRowCnt);
      startRowCnt = startRowCnt + 2;
      GenReportDTO dto = null;
      try {
        dto =
            LivtContestReportsDAO.fetchcontestDiscDetailsForReport(clId, coId, new GenReportDTO());
        List<String> headerList = dto.getColNameLst();
        ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);
      } catch (Exception ex) {
        ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt,
            ReportConstants.NO_RECS_FOUND);
        generateExcelResponse(response, workbook);
        return;
      }
      List<List<String>> dataList = dto.getDatasArryList();
      if (dataList == null || dataList.size() == 0) {
        ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt,
            ReportConstants.NO_RECS_FOUND);
        generateExcelResponse(response, workbook);
        return;
      }

      int j = 0;
      int rowCount = startRowCnt + 1;
      for (List<String> subList : dataList) {
        Row rowhead = ssSheet.createRow(rowCount);
        rowCount++;
        j = 0;

        for (int k = 0; k < subList.size(); k++) {
          ReportsUtil.getExcelStringCell(subList.get(k), k, rowhead);
        }


      }

      System.out.println("Excel file has been generated successfully.");

      generateExcelResponse(response, workbook);

    } catch (Exception e) {
      e.printStackTrace();
      setClientMessage(respDTO, null, null);
      generateResponse(request, response, respDTO);

    }
  }

  /**
   * Generate excel response.
   *
   * @param response the response
   * @param workbook the workbook
   * @throws Exception the exception
   */
  public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook)
      throws Exception {
    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    response.setHeader("Content-Type", "application/vnd.ms-excel");
    response.setHeader("Content-disposition",
        "attachment; filename=ContestDiscountDets." + ReportsUtil.EXCEL_EXTENSION);
    workbook.write(response.getOutputStream());
    workbook.close();
  }

  public static void main(String[] args) throws Exception {
    try {
      String clId = "AMIT202002CID";

      String coId = "AMIT202002CID1624550666267";

      SXSSFWorkbook workbook = new SXSSFWorkbook();
      Sheet ssSheet = workbook.createSheet("ContestDiscount");
      CreationHelper createHelper = workbook.getCreationHelper();
      CellStyle cellStyle = workbook.createCellStyle();
      CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
      cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
      cellStyleWithHourMinute
          .setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

      Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
      reportNameMap.put("Report Name", "Contest Discount Report");

      int startRowCnt = 0;
      ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);
      startRowCnt = startRowCnt + 2;

      GenReportDTO dto =
          LivtContestReportsDAO.fetchcontestDiscDetailsForReport(clId, coId, new GenReportDTO());
      List<String> headerList = dto.getColNameLst();
      List<String> dynaList = dto.getDynamicColList();
      ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

      List<List<String>> dataList = dto.getDatasArryList();
      if (dataList == null || dataList.size() == 0) {
        ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt,
            ReportConstants.NO_RECS_FOUND);
        // generateExcelResponse(response, workbook);
        return;
      }

      int j = 0;
      int rowCount = startRowCnt + 1;
      for (List<String> subList : dataList) {
        Row rowhead = ssSheet.createRow(rowCount);
        rowCount++;
        j = 0;

        for (int k = 0; k < subList.size(); k++) {
          ReportsUtil.getExcelStringCell(subList.get(k), k, rowhead);
        }


      }

      System.out.println("Excel file has been generated successfully.");


      FileOutputStream fileOut = new FileOutputStream("E://TMP/EXCEL/disc01122.xlsx");
      workbook.write(fileOut);
      // closing the Stream
      fileOut.close();
      // closing the workbook
      workbook.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }



}
