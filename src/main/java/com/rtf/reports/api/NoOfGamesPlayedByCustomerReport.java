/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class NoOfGamesPlayedByCustomerReport.
 */
@WebServlet("/noofgamesplayedbycustomerreport.json")
public class NoOfGamesPlayedByCustomerReport extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get No Of Games Played By Customer Report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			int noOfGames = 0;
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Customer UserID");
			headerList.add("Customer Email");
			headerList.add("No oF Games Played");
			headerList.add("Average Percentage Game Played by Customer");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("NoOfGamePlayedByCustomer");

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", "No Of Games Played By Customer");

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);
			startRowCnt = startRowCnt + 2;

			List<Object[]> dataList = LivtContestReportsDAO.getAvgNoOfGamesPlayedByCustomerReportData(clId);
			if (dataList == null || dataList.size() == 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);
			int j = 0;
			int rowCount = startRowCnt + 1;
			int totalCustomerCount = 0;
			int totalGames = 0;
			boolean isFirstRow = true;
			for (Object[] dataObj : dataList) {

				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;

				ReportsUtil.getExcelStringCell(dataObj[0], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[1], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[2], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[3], j, rowhead);
				j++;
				if (dataObj[3] != null) {
					totalGames += Integer.parseInt(dataObj[3].toString());
				}
				ReportsUtil.getExcelDecimalCell(dataObj[4], j, rowhead);
				j++;
				totalCustomerCount++;
				if (isFirstRow) {
					if (dataObj[5] != null) {
						noOfGames = Integer.parseInt(dataObj[5].toString());
						isFirstRow = false;
					}
				}

			}

			ssSheet.createRow(rowCount);
			rowCount++;
			Row rowhead1 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("Total No.Of Customers Who Have Played Game: (A1)", 0, rowhead1);
			ReportsUtil.getExcelIntegerCell(totalCustomerCount, 1, rowhead1);

			rowCount++;
			Row rowhead2 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("Total No.Of Contest Played Till Date: (B1)", 0, rowhead2);
			ReportsUtil.getExcelIntegerCell(noOfGames, 1, rowhead2);

			rowCount++;
			Double totalGamesAsDouble = Double.parseDouble(String.valueOf(totalGames));
			Double totalGamesAsDouble1 = Double.parseDouble(String.valueOf(noOfGames));
			Row rowhead3 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("Overall Average No Of Customers Who Have Played Game: (A1/B1)", 0,
					rowhead3);
			ReportsUtil.getExcelDecimalCell((totalCustomerCount / totalGamesAsDouble1), 1, rowhead3);

			rowCount++;
			Row rowhead4 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("Total No. Of Contest Played by All Customers: (C1) (sum of Column E)", 0,
					rowhead4);
			ReportsUtil.getExcelIntegerCell(totalGames, 1, rowhead4);

			rowCount++;
			Row rowhead5 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("Overall Average No Of Games Participated Per Customer: (C1/A1)", 0,
					rowhead5);
			ReportsUtil.getExcelDecimalCell((totalGamesAsDouble / totalCustomerCount), 1, rowhead5);

			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=NoOfGamePlayedByCustomer." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();

	}
}
