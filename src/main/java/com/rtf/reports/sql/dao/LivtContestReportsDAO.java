/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.sql.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.rtf.livt.dvo.ContestListDVO;
import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.reports.dto.CustAnswerDTO;
import com.rtf.reports.dto.GenReportDTO;
import com.rtf.reports.dvo.CustomerAnswerReportDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class LivtContestReportsDAO.
 */
public class LivtContestReportsDAO {

	/**
	 * Gets the contest list data for report.
	 *
	 * @param clId
	 *            the cl id
	 * @return the contest list data for report
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestListDVO> getContestListDataForReport(String clId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestListDVO> list = new ArrayList<ContestListDVO>();
		StringBuffer sql = new StringBuffer();
		
		sql.append(" select top 5 conid , conname ");
		sql.append(" , (CONVERT(VARCHAR(20), consrtdate, 101) + RIGHT(CONVERT(VARCHAR(20), consrtdate, 100), 8)) ContestDate " ) ;
		sql.append(" from pa_livvx_contest_mstr where clintid=? " ) ; 
		sql.append(" order by consrtdate desc " ) ;		
		try {

			System.out.println("************************************** " + sql.toString());
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestListDVO contestDVO = new ContestListDVO();
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setName(rs.getString("conname"));
				contestDVO.setStDateTimeStr(rs.getString("ContestDate"));
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest participants by co id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the all contest participants by co id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestParticipantsDVO> getAllContestParticipantsByCoId(String clientId, String contestId)
			throws Exception {
		List<ContestParticipantsDVO> participants = new ArrayList<ContestParticipantsDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select cp.custid,cc.userid,cc.custemail from pt_livvx_contest_participants cp")
				.append(" inner join pu_client_customer cc on cc.clintid=cp.clintid and cc.custid=cp.custid")
				.append(" where cp.clintid=? and conid=?");
		try {
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();
			ContestParticipantsDVO participant = null;
			while (rs.next()) {
				participant = new ContestParticipantsDVO();
				participant.setCuId(rs.getString("custid"));
				participant.setuId(rs.getString("userid"));
				participant.setEmail(rs.getString("custemail"));

				participants.add(participant);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return participants;
	}

	/**
	 * Gets the contest question wise user answer count report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the contest question wise user answer count report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getContestQuestionWiseUserAnswerCountReportData(String clientId, String fromDate,
			String toDate) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append(
					"	SELECT  [contestID], [contestName], [contestStartDateTime], [No.OfParticipantsParticipated], [No.OfParticipantsAttempted] ")
					.append(", [FirstQuestionAttempted], [FirstQuestionAnswered], [%FirstQuestionAttempted], [%FirstQuestionAnswered] ")
					.append(", [SecondQuestionAttempted], [SecondQuestionAnswered], [%SecondQuestionAttempted], [%SecondQuestionAnswered] ")
					.append(", [ThirdQuestionAttempted], [ThirdQuestionAnswered], [%ThirdQuestionAttempted], [%ThirdQuestionAnswered] ")
					.append(", [FourthQuestionAttempted], [FourthQuestionAnswered], [%FourthQuestionAttempted], [%FourthQuestionAnswered] ")
					.append(", [FivethQuestionAttempted], [FivethQuestionAnswered], [%FivethQuestionAttempted], [%FivethQuestionAnswered] ")
					.append(", [SixthQuestionAttempted], [SixthQuestionAnswered], [%SixthQuestionAttempted], [%SixthQuestionAnswered] ")
					.append(", [SeventhQuestionAttempted], [SeventhQuestionAnswered], [%SeventhQuestionAttempted], [%SeventhQuestionAnswered] ")
					.append(", [EighthQuestionAttempted], [EighthQuestionAnswered], [%EighthQuestionAttempted], [%EighthQuestionAnswered] ")
					.append(", [NinethQuestionAttempted], [NinethQuestionAnswered], [%NinethQuestionAttempted], [%NinethQuestionAnswered] ")
					.append(", [TenthQuestionAttempted], [TenthQuestionAnswered], [%TenthQuestionAttempted], [%TenthQuestionAnswered] ")
					.append(", [TotalQuestionAnswered], [TotalNoOfQuestionsAttempted], [Overall%Answered], [OriginalContestStartDatetime] ")
					.append("	FROM rep_contest_wise_participants_question_answer_count_report_vw with(nolock) ")
					.append("	WHERE clintid =? ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate += " 00:00:00.000";
				sql.append("	AND CONVERT(datetime,OriginalContestStartDatetime) >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate += " 23:59:59:000";
				sql.append("	AND CONVERT(datetime,OriginalContestStartDatetime) <= '").append(toDate).append("' ");
			}
			sql.append(" ORDER BY OriginalContestStartDatetime desc");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[48];
				int index = 0;
				for (int i = 1; i <= 48; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the avg questions answered by customer report data.
	 *
	 * @param clientId
	 *            the client id
	 * @return the avg questions answered by customer report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getAvgQuestionsAnsweredByCustomerReportData(String clientId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("	SELECT  CustomerName, CustomerUserID, CustomerEmail ,NoOfGamesPlayedByAnswering,")
					.append(" TotalNoOfQuestionsFromPlayedGames,TotalNoOfQuestionsAnswered,")
					.append(" AveragePercentageAnsweredByCustomer")
					.append(" FROM rep_average_noof_question_answered_by_customers_report_vw ")
					.append(" where clintid=?").append(" ORDER BY NoOfGamesPlayedByAnswering desc, CustomerUserID");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[7];
				int index = 0;
				for (int i = 1; i <= 7; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the avg no of games played by customer report data.
	 *
	 * @param clientId
	 *            the client id
	 * @return the avg no of games played by customer report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getAvgNoOfGamesPlayedByCustomerReportData(String clientId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append(
					"	SELECT  CustomerName, CustomerUserID, CustomerEmail, NoOfGamesPlayedByCustomer, AveragePercentageGamePlayedByCustomer,totnoofcon")
					.append(" from rep_average_noof_games_played_by_customers_reportt_vw").append(" where clintid=?")
					.append(" order by NoOfGamesPlayedByCustomer desc,CustomerUserID ; ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[6];
				int index = 0;
				for (int i = 1; i <= 6; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest wise first time customer report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the contest wise first time customer report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getContestWiseFirstTimeCustomerReportData(String clientId, String fromDate,
			String toDate) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("	SELECT ContestID, ContestName, contestStartDateTimeString,")
					.append(" FirstParticipantsCount, TotalParticipants,")
					.append(" FirstParticipantsPlayedPercentage , SignUpCount , contestStartDateTime")
					.append(" FROM rep_contest_wise_first_time_participants_count_report_vw")
					.append(" where clintid=? ");

			if (fromDate != null && !fromDate.isEmpty()) {
				sql.append("	and convert(datetime,contestStartDateTime) >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				sql.append(" and convert(datetime,contestStartDateTime) <= '").append(toDate).append("' ");
			}
			sql = sql.append(" ORDER BY contestStartDateTime desc;");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[7];
				int index = 0;
				for (int i = 1; i <= 7; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the monthly customer participants count breakdown statistics.
	 *
	 * @param clientId
	 *            the client id
	 * @return the monthly customer participants count breakdown statistics
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getMonthlyCustomerParticipantsCountBreakdownStatistics(String clientId)
			throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append(
					" SELECT   [Month],RegisteredUsers,NoOfShows,[NeverPlayedOrViewed%],NeverPlayedOrViewed,[OneShowThenInActive%],OneShowThenInActive,")
					.append(" [OneShowPerMonth%],OneShowPerMonth,[OneShowPerWeek%],OneShowPerWeek,[OneShowPerDay%],OneShowPerDay,conyear,conmnth")
					.append(" from rep_monthly_customer_participants_count_breakdown_statistics_report_vw")
					.append(" where clintid=?  order by conyear desc, conmnth desc;      ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[13];
				int index = 0;
				for (int i = 1; i <= 13; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the moth wise user registration count report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the moth wise user registration count report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getMothWiseUserRegistrationCountReportData(String clientId, String fromDate,
			String toDate) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(
					"select DATENAME(MONTH,rc.contest_customer_signup_date) + ' '+ DATENAME(YEAR,rc.contest_customer_signup_date) as Month1 ")
					.append(", count(rc.contest_customer_id) as DandR, DATEPART(YEAR,rc.contest_customer_signup_date) ")
					.append(", DATEPART(MONTH,rc.contest_customer_signup_date) ")
					.append("from customer c with(nolock) ")
					.append("INNER JOIN contest_registered_customer rc with(nolock) on c.id = rc.contest_customer_id ")
					.append("where 1=1 ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate = fromDate + " 00:00:00.000";
				sb.append("and rc.contest_customer_signup_date >= '" + fromDate + "' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate = toDate + " 23:59:00.000";
				sb.append("and rc.contest_customer_signup_date <= '" + toDate + "' ");
			}
			sb.append("GROUP BY DATEPART(YEAR,rc.contest_customer_signup_date) ");
			sb.append(", DATEPART(MONTH,rc.contest_customer_signup_date) ");
			sb.append(", DATENAME(MONTH,rc.contest_customer_signup_date) ");
			sb.append(", DATENAME(YEAR,rc.contest_customer_signup_date) ");
			sb.append("order by 3 desc, 4 desc;");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[2];
				int index = 0;
				for (int i = 1; i <= 2; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the moth wise user registration count report data.
	 *
	 * @param clientId
	 *            the client id
	 * @return the moth wise user registration count report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getMothWiseUserRegistrationCountReportData(String clientId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("select [Month] ,DownloadedAndRegistered, conyear,conmnth")
					.append(" from rep_monthly_registered_customer_count_report_vw").append(" where clintid=?")
					.append(" order by conyear desc,conmnth desc");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[2];
				int index = 0;
				for (int i = 1; i <= 2; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the monthly participants count statistics report data for sheet one.
	 *
	 * @param clientId
	 *            the client id
	 * @return the monthly participants count statistics report data for sheet
	 *         one
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getMonthlyParticipantsCountStatisticsReportDataForSheetOne(String clientId)
			throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append(
					" SELECT   [Month],NoOfGamesPlayed,ParticipantsCount,UniqueParticipantsCount,OldUserParticipantsCount,")
					.append(" AverageViewership,conyear,conmnth")
					.append(" from rep_monthly_customer_participants_count_statistics_report_vw ")
					.append(" where clintid=?").append("  order by conyear desc, conmnth desc;");
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[6];
				int index = 0;
				for (int i = 1; i <= 6; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the monthly participants count statistics report data for sheet two.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the monthly participants count statistics report data for sheet
	 *         two
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getMonthlyParticipantsCountStatisticsReportDataForSheetTwo(String clientId,
			String fromDate, String toDate) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("  SELECT MonthYear, ContestName, contestStartDateTimeString, ")
					.append("  [No.OfParticipantsParticipated],contestStartDateTime")
					.append(" FROM rep_contest_wise_participants_question_answer_count_report_vw")
					.append(" where clintid=? ");

			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate = fromDate + " 00:00:00.000";
				sql.append("  and contestStartDateTime >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate = toDate + " 23:59:00.000";
				sql.append("  and contestStartDateTime <= '").append(toDate).append("' ");
			}
			sql.append(" ORDER BY contestStartDateTime desc;  ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[4];
				int index = 0;
				for (int i = 1; i <= 4; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the customer contest statisticsfor business.
	 *
	 * @param clientId
	 *            the client id
	 * @return the customer contest statisticsfor business
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getCustomerContestStatisticsforBusiness(String clientId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer(" select Heading,Value from rep_rtf_customer_and_contest_statistics_report_for_business where clintid=? order by ord ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[2];
				int index = 0;
				for (int i = 1; i <= 2; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the total no of contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the total no of contest
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getTotalNoOfContest(String clientId, String fromDate, String toDate) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer contCount = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append(
					"select count(distinct id) as contestcount from contest co where co.is_test_contest = 0 and co.status = 'EXPIRED' ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate += " 00:00:00.000";
				sql.append("and convert(datetime,co.contest_start_datetime) >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate += " 23:59:59:000";
				sql.append("and convert(datetime,co.contest_start_datetime) <= '").append(toDate).append("' ");
			}

			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contCount = rs.getInt("contestcount");

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contCount;
	}

	/**
	 * Gets the customer average time takenper contest statistics.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the customer average time takenper contest statistics
	 */
	public static List<Object[]> getCustomerAverageTimeTakenperContestStatistics(String clientId, String fromDate,
			String toDate) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Object[]> list = null;
		StringBuffer sql = new StringBuffer();
		try {

			sql.append(" select  conid,  ContestName ,ContestDate ,")
					.append(" OverallParticipantsCount , OverallAverageMinutesSpentOnContest,")
					.append(" ParticipantsCountWhoSpent0to5Mins,AverageParticipantsCountWhoSpent0to5Mins , ")
					.append(" ParticipantsCountWhoSpent5to10Mins ,AverageParticipantsCountWhoSpent5to10Mins  ,")
					.append(" ParticipantsCountWhoSpent10to15Mins, AverageParticipantsCountWhoSpent10to15Mins  ,")
					.append(" ParticipantsCountWhoSpentAbove15Mins, AverageParticipantsCountWhoSpentAbove15Mins ,")
					.append(" ParticipantsCountWhoSpent0to11Mins ,AverageParticipantsCountWhoSpent0to11Mins ,")
					.append(" ParticipantsCountWhoSpentAbove11Mins,AverageParticipantsCountWhoSpentAbove11Mins ")
					.append(" from rep_customer_average_time_taken_per_contest_statistics_report_vw  ")
					.append(" where 1=1 and clintid=? ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate += " 00:00:00.000";
				sql.append(" and convert(datetime,consrtdate) >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate += " 23:59:59:000";
				sql.append(" and convert(datetime,consrtdate) <= '").append(toDate).append("' ");
			}

			sql.append("    order by contestDate desc  ");
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[17];
				int index = 0;
				for (int i = 1; i <= 17; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest participant by contest name filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return the contest participant by contest name filter
	 */
	public static List<Object[]> getContestParticipantByContestNameFilter(String clientId, String coId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Object[]> list = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select customerid, customername , customeremail ")
					.append(" from rep_contest_participants_details_report_vw ").append(" where clintid=? and conid=?")
					.append(" order by customername");
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			ps.setString(2, coId);
			rs = ps.executeQuery();
			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[3];
				int index = 0;
				for (int i = 1; i <= 3; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the avg time taken per contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the avg time taken per contest
	 */
	public static List<Object[]> getAvgTimeTakenPerContest(String clientId, String fromDate, String toDate) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Object[]> list = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select conid, contestname , ContestDate,CustomerAveragetimeinminutes ")
					.append("from rep_customer_average_time_taken_per_contest_report_vw ")
					.append("where 1=1 and clintid=? ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate += " 00:00:00.000";
				sb.append(" and convert(datetime,consrtdate) >= '").append(fromDate).append("' ");
				;
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate += " 23:59:59:000";
				sb.append(" and convert(datetime,consrtdate) <= '").append(toDate).append("' ");
				;
			}
			sb.append(" order by  consrtdate desc");
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[4];
				int index = 0;
				for (int i = 1; i <= 4; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest summary winner report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conId1
	 *            the con id
	 * @return the contest summary winner report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getContestSummaryWinnerReportData(String clientId, String conId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer(" select CustomerID,RewardType,RewardValue from rep_contest_winner_details_report_vw  where clintid=? and conid=?  order by CustomerID");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, conId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[3];
				int index = 0;
				for (int i = 1; i <= 3; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest grand winner report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conId
	 *            the con id
	 * @return the contest grand winner report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getContestGrandWinnerReportData(String clientId, String conId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer("	select CustomeriD,RewardType,RewardValue from rep_contest_grand_winner_details_report_vw  where clintid=? and conid=? order by CustomeriD;");
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, conId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[3];
				int index = 0;
				for (int i = 1; i <= 3; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the customer time spent on each contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the customer time spent on each contest
	 */
	public static List<Object[]> getCustomerTimeSpentOnEachContest(String clientId, String fromDate, String toDate) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Object[]> list = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select conid,contestname,contestdate,customerid,customeremailid,")
					.append("minutesspentoncontest ").append("from rep_customer_time_spent_on_each_contest_report_vw ")
					.append("where 1=1 and clintid=? ");
			if (fromDate != null && !fromDate.isEmpty()) {
				fromDate += " 00:00:00.000";
				sb.append(" and convert(datetime,consrtdate) >= '").append(fromDate).append("' ");
			}
			if (toDate != null && !toDate.isEmpty()) {
				toDate += " 23:59:59:000";
				sb.append(" and convert(datetime,consrtdate) <= '").append(toDate).append("' ");
			}
			sb.append(" order by  consrtdate desc,contestname ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[6];
				int index = 0;
				for (int i = 1; i <= 6; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	
	/**
	 * Gets the contest summary winner report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conId
	 *            the con id
	 * @return the contest summary winner report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Object[]> getContestAnswerReportData(String clientId, String conId) throws Exception {
		List<Object[]> list = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer("	select CustomerName,CustomerEmail,RewardType,RewardValue from rep_contest_winner_details_report_vw  where clintid=? and conid=?  order by CustomerName");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, conId);
			rs = ps.executeQuery();

			list = new ArrayList<Object[]>();
			Object[] object = null;
			while (rs.next()) {
				object = new Object[4];
				int index = 0;
				for (int i = 1; i <= 4; i++) {
					object[index] = rs.getObject(i);
					index++;
				}
				list.add(object);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	public static CustAnswerDTO fetchAnswerOptionDetailsForReport1(String clId, String conId,CustAnswerDTO dto) throws Exception {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;	
		try {			
			String query = "{call spGetAudienceParticipationAnswerDetailsByShow(?,?,?)}";
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			stmt = conn.prepareCall(query);
			stmt.setString(1, clId);			
			stmt.setString(2, conId);
			stmt.setInt(3, 1);
			rs = stmt.executeQuery();
			
		ResultSetMetaData rsmetadata = 	rs.getMetaData();
		int    columnCount    =    rsmetadata.getColumnCount();	
		List<CustomerAnswerReportDVO> dataList = new ArrayList<CustomerAnswerReportDVO>();
		List<String> colNames = new ArrayList<String>();
		String dynamicColname = "Customer Answer";
		for    (int    i=1;   i<=columnCount;    i++ ) {
		   String name   =   rsmetadata.getColumnName(i);	
		   colNames.add(name);
		}
		while (rs.next()) {
			CustomerAnswerReportDVO dvo = new CustomerAnswerReportDVO();
			dvo.setClId(rs.getString("Contest Name"));
			dvo.setCoStrDate(rs.getString("Contest Date"));
			dvo.setCustId(rs.getString("Customer Id"));
			dvo.setTotcrtAns(rs.getString("Total Answered Correct"));
			dvo.setTotqAttmpt(rs.getString("Total Questions Attempted"));		    
			for(int i = 1 ; i <= 10; i ++ ) {
				String colname = "Q" + i + " " +  dynamicColname;
				System.out.println(colname);
				String beanProp = "a" + i;
				if(colNames.contains(colname)) {
					BeanUtils.setProperty(dvo,beanProp,rs.getString(colname));	
				}				
			}			
			dataList.add(dvo);		
		}
		dto.setLst(dataList);
		dto.setColNameLst(colNames);
		
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}
	public static CustAnswerDTO fetchAnswerOptionDetailsForReport(String clId, String conId,CustAnswerDTO dto) throws Exception {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;	
		try {			
			String query = "{call spGetAudienceParticipationAnswerDetailsByShow(?,?,?)}";
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			stmt = conn.prepareCall(query);
			stmt.setString(1, clId);			
			stmt.setString(2, conId);
			stmt.setInt(3, 1);
			rs = stmt.executeQuery();
			
		ResultSetMetaData rsmetadata = 	rs.getMetaData();
		int    columnCount    =    rsmetadata.getColumnCount();	
		List<List<String>> dataList = new ArrayList<List<String>>();
		List<String> colNames = new ArrayList<String>();
		String dynamicColname = "Customer Answer";
		for    (int    i=1;   i<=columnCount;    i++ ) {
		   String name   =   rsmetadata.getColumnName(i);	
		   colNames.add(name);
		}
		while (rs.next()) {
			List<String> subList = new ArrayList<String> ();
			//subList.add(rs.getString("Contest Name"));
			//subList.add(rs.getString("Contest Date"));
			subList.add(rs.getString("Customer Id"));			
			    
			for(int i = 1 ; i <= 10; i ++ ) {
				String colname = "Q" + i + " " +  dynamicColname;
				System.out.println(colname);				
				if(colNames.contains(colname)) {
					subList.add(rs.getString(colname));				
				}				
			}
			subList.add(rs.getString("Total Questions Attempted"));
			subList.add(rs.getString("Total Answered Correct"));
			dataList.add(subList);
		}
		dto.setDatasArryList(dataList);		
		dto.setColNameLst(colNames);	
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}
	public static GenReportDTO fetchcontestcartDetailsForReport(String clId, String conId,GenReportDTO dto) throws Exception {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		String coName = null;
		String coDate = null;
		try {			
			String query = "{call spGetAudienceParticipationShoppingCartDetails(?,?,?)}";
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			stmt = conn.prepareCall(query);
			stmt.setString(1, clId);			
			stmt.setString(2, conId);
			stmt.setInt(3, 1);
			rs = stmt.executeQuery();
			
		ResultSetMetaData rsmetadata = 	rs.getMetaData();
		int    columnCount    =    rsmetadata.getColumnCount();	
		List<List<String>> dataList = new ArrayList<List<String>>();
		List<String> colNames = new ArrayList<String>();
		List<String> prodNameColList = new ArrayList<String>();
		
		String dynamicColname = "ProdName";
		for    (int    i=1;   i<=columnCount;    i++ ) {
		   String name   =   rsmetadata.getColumnName(i);
		   if(name.contains(dynamicColname)) {
			   prodNameColList.add(name);
		   }else {
			   if(name.equals("conname")) {
				   colNames.add("Contest Name");  
			   }
			   else if(name.equals("consrtdatestr")) {
				   colNames.add("Contest Date"); 				   
			   }
			   else if(name.equals("custid")) {
				   colNames.add("Customer ID"); 				   
			   }
			   else {
				   //do nothing..
				   //  Contest Name	Contest Date	Customer ID
			   }		
			  
		   }
		}
		while (rs.next()) {
			List<String> subList = new ArrayList<String> ();			
			//subList.add(rs.getString("conname"));
			//subList.add(rs.getString("consrtdatestr"));
			subList.add(rs.getString("custid"));			
			    
			for(int i = 0,j=1 ; i < prodNameColList.size(); i ++,j++ ) {
				String colname = dynamicColname +j;
				System.out.println(colname);
				subList.add(rs.getString(colname));				
						
			}			
			dataList.add(subList);
		}
		dto.setDatasArryList(dataList);	
		dto.setColNameLst(colNames);
		dto.setDynamicColList(prodNameColList);
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}
	
	public static GenReportDTO fetchcontestDiscDetailsForReport(String clId, String conId,GenReportDTO dto) throws Exception {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;	
		try {			
			String query = "{call spGetAudienceParticipationDiscountDetailsByShow(?,?,?)}";
			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			stmt = conn.prepareCall(query);
			stmt.setString(1, clId);			
			stmt.setString(2, conId);
			stmt.setInt(3, 1);
			rs = stmt.executeQuery();
			
		ResultSetMetaData rsmetadata = 	rs.getMetaData();
		int    columnCount    =    rsmetadata.getColumnCount();	
		List<List<String>> dataList = new ArrayList<List<String>>();
		List<String> colNames = new ArrayList<String>();
		List<String> dynaColList = new ArrayList<String>();		
		String dynamicColname = "Discount Code";
		for    (int    i=1;   i<=columnCount;    i++ ) {
		   String name   =   rsmetadata.getColumnName(i);
		   if(name.contains(dynamicColname)) {
			  dynaColList.add(name);
		   }
		   colNames.add(name);
		}			
		
		while (rs.next()) {
			List<String> subList = new ArrayList<String> ();
			//subList.add(rs.getString("Contest Name"));
			//subList.add(rs.getString("Contest Date"));
			subList.add(rs.getString("Customer Id"));		
			    
			for(int i = 1 ; i <= 10; i ++ ) {
				String colname = dynamicColname + " " + i ;
				System.out.println(colname);				
				if(dynaColList.contains(colname)) {
					subList.add(rs.getString(colname));				
				}				
			}
			dataList.add(subList);
		}
		dto.setDatasArryList(dataList);	
		dto.setColNameLst(colNames);
		dto.setDynamicColList(dynaColList);
		return dto;
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(rs != null) {rs.close(); }
				if(stmt != null) {stmt.close(); }
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}
	
	
	
	/**
	 * Gets the contest summary winner report data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conId
	 *            the con id
	 * @return the contest summary winner report data
	 * @throws Exception
	 *             the exception
	 */
	public static List<String> getContestNamenDateDets(String clientId, String conId) throws Exception {
		List<String> list = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuffer sql = new StringBuffer("select conname, "
					+ "(CONVERT(VARCHAR(20), consrtdate, 101) + RIGHT(CONVERT(VARCHAR(20), consrtdate, 100), 8)) condate"  
					+ " from pa_livvx_contest_mstr where clintid= ? " + 
					" and conid=? ");

			conn = DatabaseConnections.getSaasAdminReportDBConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, conId);
			rs = ps.executeQuery();			
			while (rs.next()) {
				list.add(rs.getString("conname"));
				list.add(rs.getString("condate"));
				}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	
	
	
	public static void main(String[] args) throws Exception {
		/*
		 * CustAnswerDTO dto = new CustAnswerDTO(); dto =
		 * fetchAnswerOptionDetailsForReport1("AMIT202002CID", "148", dto);
		 * System.out.println(dto);
		 */
		
		
		GenReportDTO dto = fetchcontestcartDetailsForReport("AMIT202002CID", "AMIT202002CID1620311038178", new GenReportDTO());;
		System.out.println(dto.getDatasArryList().size());
	}

}
