/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.NoOfShoppableGamesGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class NoOfShoppableGamesGrpDTO.
 */
public class NoOfShoppableGamesGrpDTO extends RtfSaasBaseDTO {
	
	/** The list. */
	List<NoOfShoppableGamesGrpDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<NoOfShoppableGamesGrpDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<NoOfShoppableGamesGrpDVO> list) {
		this.list = list;
	}
	
}
