/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.CustomerAnswerReportDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class CustAnswerDTO.
 */
public class CustAnswerDTO extends RtfSaasBaseDTO {

	
	private String coId;
	private String clId ;
	private List<CustomerAnswerReportDVO> lst;	
	private List<String> colNameLst;
	private List<List<String>> datasArryList;
	
	public String getCoId() {
		return coId;
	}
	public void setCoId(String coId) {
		this.coId = coId;
	}
	public String getClId() {
		return clId;
	}
	public void setClId(String clId) {
		this.clId = clId;
	}
	public List<CustomerAnswerReportDVO> getLst() {
		return lst;
	}
	public void setLst(List<CustomerAnswerReportDVO> lst) {
		this.lst = lst;
	}
	public List<String> getColNameLst() {
		return colNameLst;
	}
	public void setColNameLst(List<String> colNameLst) {
		this.colNameLst = colNameLst;
	}
	public List<List<String>> getDatasArryList() {
		return datasArryList;
	}
	public void setDatasArryList(List<List<String>> datasArryList) {
		this.datasArryList = datasArryList;
	}
	
}
