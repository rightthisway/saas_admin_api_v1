/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;
import java.util.Map;

import com.rtf.reports.dvo.NoOfShoppableGamesGrpDVO;
import com.rtf.reports.dvo.ParticipantsShopEngageGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ShoppalbeGamesGrpDTO.
 */
public class ShoppalbeGamesGrpDTO extends RtfSaasBaseDTO {

	/** The trivslist. */
	List<NoOfShoppableGamesGrpDVO> trivslist;

	/** The prtslist. */
	List<ParticipantsShopEngageGrpDVO> prtslist;

	/** The svt engage rate. */
	Map<String, String> svtEngageRate;

	/** The shop gams dmgrpy. */
	List<Map<String, String>> shopGamsDmgrpy;

	/**
	 * Gets the trivslist.
	 *
	 * @return the trivslist
	 */
	public List<NoOfShoppableGamesGrpDVO> getTrivslist() {
		return trivslist;
	}

	/**
	 * Sets the trivslist.
	 *
	 * @param trivslist the new trivslist
	 */
	public void setTrivslist(List<NoOfShoppableGamesGrpDVO> trivslist) {
		this.trivslist = trivslist;
	}

	/**
	 * Gets the prtslist.
	 *
	 * @return the prtslist
	 */
	public List<ParticipantsShopEngageGrpDVO> getPrtslist() {
		return prtslist;
	}

	/**
	 * Sets the prtslist.
	 *
	 * @param prtslist the new prtslist
	 */
	public void setPrtslist(List<ParticipantsShopEngageGrpDVO> prtslist) {
		this.prtslist = prtslist;
	}

	/**
	 * Gets the svt engage rate.
	 *
	 * @return the svt engage rate
	 */
	public Map<String, String> getSvtEngageRate() {
		return svtEngageRate;
	}

	/**
	 * Sets the svt engage rate.
	 *
	 * @param svtEngageRate the svt engage rate
	 */
	public void setSvtEngageRate(Map<String, String> svtEngageRate) {
		this.svtEngageRate = svtEngageRate;
	}

	/**
	 * Gets the shop gams dmgrpy.
	 *
	 * @return the shop gams dmgrpy
	 */
	public List<Map<String, String>> getShopGamsDmgrpy() {
		return shopGamsDmgrpy;
	}

	/**
	 * Sets the shop gams dmgrpy.
	 *
	 * @param shopGamsDmgrpy the shop gams dmgrpy
	 */
	public void setShopGamsDmgrpy(List<Map<String, String>> shopGamsDmgrpy) {
		this.shopGamsDmgrpy = shopGamsDmgrpy;
	}

}
