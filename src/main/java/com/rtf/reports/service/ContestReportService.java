/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.service;

import java.util.List;

import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.sql.dao.LivtContestGrandWinnerDAO;
import com.rtf.livt.sql.dao.LivtContestWinnerDAO;

/**
 * The Class ContestReportService.
 */
public class ContestReportService {

	/**
	 * Fetch contest grand winner details.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<ContestGrandWinnerDVO> fetchContestGrandWinnerDetails(String clientId, String contestId)
			throws Exception {
		List<ContestGrandWinnerDVO> repLst = null;
		try {
			repLst = LivtContestGrandWinnerDAO.getAllWinnersByCoId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return repLst;
	}

	/**
	 * Fetch contest summary winner details.
	 *
	 * @param clId the client Id
	 * @param coId the contest Id
	 * @return the list
	 */
	public static List<ContestWinnerDVO> fetchContestSummaryWinnerDetails(String clId, String coId) {

		List<ContestWinnerDVO> repLst = null;
		try {
			repLst = LivtContestWinnerDAO.getAllWinnersByCoId(clId, coId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return repLst;

	}
}
