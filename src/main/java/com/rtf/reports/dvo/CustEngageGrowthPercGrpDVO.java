/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dvo;

import java.io.Serializable;

/**
 * The Class CustEngageGrowthPercGrpDVO.
 */
public class CustEngageGrowthPercGrpDVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6990988446128928250L;

	/** The name. */
	private String name;

	/** The subscribers. */
	private Double subscribers;

	/** The participants. */
	private Double participants;

	/** The players. */
	private Double players;

	/** The finishers. */
	private Double finishers;

	/** The winners. */
	private Double winners;

	/** The games. */
	private Double games;

	/** The passive viewers. */
	private Double passiveViewers;

	/** The finalists. */
	private Double finalists;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the subscribers.
	 *
	 * @return the subscribers
	 */
	public Double getSubscribers() {
		return subscribers;
	}

	/**
	 * Sets the subscribers.
	 *
	 * @param subscribers the new subscribers
	 */
	public void setSubscribers(Double subscribers) {
		this.subscribers = subscribers;
	}

	/**
	 * Gets the participants.
	 *
	 * @return the participants
	 */
	public Double getParticipants() {
		return participants;
	}

	/**
	 * Sets the participants.
	 *
	 * @param participants the new participants
	 */
	public void setParticipants(Double participants) {
		this.participants = participants;
	}

	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public Double getPlayers() {
		return players;
	}

	/**
	 * Sets the players.
	 *
	 * @param players the new players
	 */
	public void setPlayers(Double players) {
		this.players = players;
	}

	/**
	 * Gets the finishers.
	 *
	 * @return the finishers
	 */
	public Double getFinishers() {
		return finishers;
	}

	/**
	 * Sets the finishers.
	 *
	 * @param finishers the new finishers
	 */
	public void setFinishers(Double finishers) {
		this.finishers = finishers;
	}

	/**
	 * Gets the winners.
	 *
	 * @return the winners
	 */
	public Double getWinners() {
		return winners;
	}

	/**
	 * Sets the winners.
	 *
	 * @param winners the new winners
	 */
	public void setWinners(Double winners) {
		this.winners = winners;
	}

	/**
	 * Gets the games.
	 *
	 * @return the games
	 */
	public Double getGames() {
		return games;
	}

	/**
	 * Sets the games.
	 *
	 * @param games the new games
	 */
	public void setGames(Double games) {
		this.games = games;
	}

	/**
	 * Gets the passive viewers.
	 *
	 * @return the passive viewers
	 */
	public Double getPassiveViewers() {
		return passiveViewers;
	}

	/**
	 * Sets the passive viewers.
	 *
	 * @param passiveViewers the new passive viewers
	 */
	public void setPassiveViewers(Double passiveViewers) {
		this.passiveViewers = passiveViewers;
	}

	/**
	 * Gets the finalists.
	 *
	 * @return the finalists
	 */
	public Double getFinalists() {
		return finalists;
	}

	/**
	 * Sets the finalists.
	 *
	 * @param finalists the new finalists
	 */
	public void setFinalists(Double finalists) {
		this.finalists = finalists;
	}

}
