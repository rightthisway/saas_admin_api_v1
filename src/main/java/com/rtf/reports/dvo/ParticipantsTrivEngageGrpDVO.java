/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dvo;

import java.io.Serializable;

/**
 * The Class ParticipantsTrivEngageGrpDVO.
 */
public class ParticipantsTrivEngageGrpDVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2346395251661525627L;

	/** The name. */
	private String name;

	/** The viewers. */
	private Double viewers;

	/** The players. */
	private Double players;

	/** The finishers. */
	private Double finishers;

	/** The passive viewers. */
	private Double passiveViewers;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the viewers.
	 *
	 * @return the viewers
	 */
	public Double getViewers() {
		return viewers;
	}

	/**
	 * Sets the viewers.
	 *
	 * @param viewers the new viewers
	 */
	public void setViewers(Double viewers) {
		this.viewers = viewers;
	}

	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public Double getPlayers() {
		return players;
	}

	/**
	 * Sets the players.
	 *
	 * @param players the new players
	 */
	public void setPlayers(Double players) {
		this.players = players;
	}

	/**
	 * Gets the finishers.
	 *
	 * @return the finishers
	 */
	public Double getFinishers() {
		return finishers;
	}

	/**
	 * Sets the finishers.
	 *
	 * @param finishers the new finishers
	 */
	public void setFinishers(Double finishers) {
		this.finishers = finishers;
	}

	/**
	 * Gets the passive viewers.
	 *
	 * @return the passive viewers
	 */
	public Double getPassiveViewers() {
		return passiveViewers;
	}

	/**
	 * Sets the passive viewers.
	 *
	 * @param passiveViewers the new passive viewers
	 */
	public void setPassiveViewers(Double passiveViewers) {
		this.passiveViewers = passiveViewers;
	}

}
