/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dvo;

import java.io.Serializable;

/**
 * The Class NoOfTriviaGamesGrpDVO.
 */
public class NoOfTriviaGamesGrpDVO implements Serializable {

	/** The name. */
	private String name;
	
	/** The notrivs. */
	private Integer notrivs;
	
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the notrivs.
	 *
	 * @return the notrivs
	 */
	public Integer getNotrivs() {
		return notrivs;
	}
	
	/**
	 * Sets the notrivs.
	 *
	 * @param notrivs the new notrivs
	 */
	public void setNotrivs(Integer notrivs) {
		this.notrivs = notrivs;
	}
	

}
