/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.aws;

/**
 * The Class AwsS3Response.
 */
public class AwsS3Response {
	
	/** The desc. */
	private String desc;
	
	/** The e code. */
	private String eCode;
	
	/** The status. */
	private Integer status;
	
	/** The full file path. */
	private String fullFilePath;
	
	/**
	 * Gets the full file path.
	 *
	 * @return the full file path
	 */
	public String getFullFilePath() {
		return fullFilePath;
	}
	
	/**
	 * Sets the full file path.
	 *
	 * @param fullFilePath the new full file path
	 */
	public void setFullFilePath(String fullFilePath) {
		this.fullFilePath = fullFilePath;
	}
	
	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Gets the e code.
	 *
	 * @return the e code
	 */
	public String geteCode() {
		return eCode;
	}
	
	/**
	 * Sets the e code.
	 *
	 * @param eCode the new e code
	 */
	public void seteCode(String eCode) {
		this.eCode = eCode;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Error [des=" + desc + "]";
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

}
