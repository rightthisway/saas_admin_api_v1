/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.aws;

import java.io.File;

import org.apache.commons.io.FileUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.util.Messages;

/**
 * AWS file service utility class for performing file operation(upload/download/delete) on S3.
 */
public class AWSFileService {

	/** The Constant TMP_DIR. */
	public static final String TMP_DIR = System.getProperty("java.io.tmpdir");

	/** The ak. */
	private static final String AK = "AKIARJICTKFNN5SWY6VE";

	/** The asa. */
	private static final String ASA = "jF1WVZqR7n4wNkPJB3bLIqpg9BgcpxvbDX+JplOp";

	/**
	 * Upload file to amazon S3 to specified bucket and folder.
	 *
	 * @param bucketName String
	 * @param fileName String
	 * @param folderName String
	 * @param fileObj File
	 * @return AwsS3Response
	 */
	public static AwsS3Response upLoadFileToS3(String bucketName, String fileName, String folderName, File fileObj) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();
			String fileNameToUpload = folderName + "/" + fileName;
			s3client.putObject(new PutObjectRequest(bucketName, fileNameToUpload, fileObj));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(fileNameToUpload);
			awsS3Response.setDesc("File has been Uploaded Successfully");

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.UPLOAD_FILE_EXC + ase.getMessage());

		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.UPLOAD_FILE_EXC );
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.UPLOAD_FILE_EXC + ex.getLocalizedMessage());
			ex.printStackTrace();
		}
		return awsS3Response;

	}

	/**
	 * Download file from amazon S3 based on specified bucket and folder.
	 *
	 * @param bucketName String
	 * @param fileName String
	 * @param folderName String
	 * @return tAwsS3Response
	 */
	public static AwsS3Response downLoadFileFromS3(String bucketName, String fileName, String folderName) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = SaaSAdminAppCache.TMP_FOLDER + File.separator+fileName;

			File fileSaveDir = new File(TMP_DIR);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, folderName + "/" + fileName));
			FileUtils.copyInputStreamToFile(fileObject.getObjectContent(), new File(downLoadFullPath));
			fileObject.close();
			s3client.shutdown();
			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(downLoadFullPath);

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);

		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);

		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);
			ex.printStackTrace();
		}
		return awsS3Response;

	}

	/**
	 * Permanently delete file from S3 from mentioned bucket and folder.
	 *
	 * @param bucketName String
	 * @param fileName String
	 * @param folderName String
	 * @return AwsS3Response
	 */
	public static AwsS3Response deleteFilesFromS3(String bucketName, String fileName, String folderName) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			s3client.deleteObject(new DeleteObjectRequest(bucketName, folderName + "/" + fileName));

			awsS3Response.setStatus(1);

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);

		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc(Messages.DOWNLOAD_FILE_EXC);
			ex.printStackTrace();
		}
		return awsS3Response;

	}

}