package com.rtf.livt.dvo;

import java.io.Serializable;
/**
 * The Class LiveContestUserIdConfDVO.
 */
public class LiveContestUserIdConfDVO implements Serializable{

	/** The Client ID. */
	private String clId;
	
	/** The Node ID. */
	private String nodeId;
	
	/** The User Id counter. */
	private Integer counter;
	
	/**
	 * Gets the clId.
	 *
	 * @return the clId
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the clId.
	 *
	 * @param clId
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the nodeId.
	 *
	 * @return the nodeId
	 */
	public String getNodeId() {
		return nodeId;
	}
	
	/**
	 * Sets the nodeId.
	 *
	 * @param nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	/**
	 * Gets the counter.
	 *
	 * @return the counter
	 */
	public Integer getCounter() {
		return counter;
	}
	
	/**
	 * Sets the counter.
	 *
	 * @param counter
	 */
	public void setCounter(Integer counter) {
		this.counter = counter;
	}
	
	
	
}
