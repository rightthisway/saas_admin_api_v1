/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientCustomerDVO.
 */
public class ClientCustomerDVO implements Serializable {

	private static final long serialVersionUID = -7991617525782046088L;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The email. */
	private String email;

	/** The fname. */
	private String fname;

	/** The lname. */
	private String lname;

	/** The phone. */
	private String phone;

	/** The purl. */
	private String purl;

	/** The uplby. */
	private String uplby;

	/** The upldate. */
	private Date upldate;

	/** The password. */
	private String password;

	/** The sign up type. */
	private String signUpType;

	/** The token id. */
	private String tokenId;

	/** The school name. */
	private String schoolName;

	/** The user id. */
	private String userId;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the fname.
	 *
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * Sets the fname.
	 *
	 * @param fname
	 *            the new fname
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * Gets the lname.
	 *
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * Sets the lname.
	 *
	 * @param lname
	 *            the new lname
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone
	 *            the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the purl.
	 *
	 * @return the purl
	 */
	public String getPurl() {
		return purl;
	}

	/**
	 * Sets the purl.
	 *
	 * @param purl
	 *            the new purl
	 */
	public void setPurl(String purl) {
		this.purl = purl;
	}

	/**
	 * Gets the uplby.
	 *
	 * @return the uplby
	 */
	public String getUplby() {
		return uplby;
	}

	/**
	 * Sets the uplby.
	 *
	 * @param uplby
	 *            the new uplby
	 */
	public void setUplby(String uplby) {
		this.uplby = uplby;
	}

	/**
	 * Gets the upldate.
	 *
	 * @return the upldate
	 */
	public Date getUpldate() {
		return upldate;
	}

	/**
	 * Sets the upldate.
	 *
	 * @param upldate
	 *            the new upldate
	 */
	public void setUpldate(Date upldate) {
		this.upldate = upldate;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the sign up type.
	 *
	 * @return the sign up type
	 */
	public String getSignUpType() {
		return signUpType;
	}

	/**
	 * Sets the sign up type.
	 *
	 * @param signUpType
	 *            the new sign up type
	 */
	public void setSignUpType(String signUpType) {
		this.signUpType = signUpType;
	}

	/**
	 * Gets the token id.
	 *
	 * @return the token id
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * Sets the token id.
	 *
	 * @param tokenId
	 *            the new token id
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	/**
	 * Gets the school name.
	 *
	 * @return the school name
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * Sets the school name.
	 *
	 * @param schoolName
	 *            the new school name
	 */
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		if (fname != null && !fname.isEmpty()) {
			return fname;
		}
		return cuId;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		/*
		 * if(fname != null && !fname.isEmpty()) { return fname; } return cuId;
		 */
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ClientCustomerDVO [clId=" + clId + ", cuId=" + cuId + ", email=" + email + ", fname=" + fname
				+ ", lname=" + lname + ", phone=" + phone + ", purl=" + purl + ", uplby=" + uplby + ", upldate="
				+ upldate + ", password=" + password + ", signUpType=" + signUpType + ", tokenId=" + tokenId
				+ ", schoolName=" + schoolName + ", userId=" + userId + "]";
	}

}
