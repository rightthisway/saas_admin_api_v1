/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class ClusterNodeConfigDVO.
 */
public class ClusterNodeConfigDVO implements Serializable {

	private static final long serialVersionUID = 748696352895529314L;

	/** The id. */
	private Integer id;
	
	/** The cl id. */
	private String clId;
	
	/** The url. */
	private String url;
	
	/** The status. */
	private Integer status;
	
	/** The dir path. */
	private String dirPath;
	
	/** The is primary. */
	private Boolean isPrimary;
	
	/** The Node ID. */
	private String nodeId;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the dir path.
	 *
	 * @return the dir path
	 */
	public String getDirPath() {
		return dirPath;
	}
	
	/**
	 * Sets the dir path.
	 *
	 * @param dirPath the new dir path
	 */
	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}
	
	/**
	 * Gets the checks if is primary.
	 *
	 * @return the checks if is primary
	 */
	public Boolean getIsPrimary() {
		return isPrimary;
	}
	
	/**
	 * Sets the checks if is primary.
	 *
	 * @param isPrimary the new checks if is primary
	 */
	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	/**
	 * Gets the nodeId.
	 *
	 * @return the nodeId.
	 */
	public String getNodeId() {
		return nodeId;
	}

	/**
	 * Sets the nodeId.
	 *
	 * @param nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
}
