/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class ContestGrandWinnerDVO.
 */
public class ContestGrandWinnerDVO implements Serializable {

	private static final long serialVersionUID = 5107464767458270276L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The phone. */
	private String phone;

	/** The name. */
	private String fName;

	/** The l name. */
	private String lName;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The cr date. */
	private Long crDate;

	/** The u id. */
	private String uId;

	/** The img U. */
	private String imgU;

	/** The email. */
	private String email;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType
	 *            the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal
	 *            the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate
	 *            the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		return uId;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId
	 *            the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU
	 *            the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone
	 *            the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the f name.
	 *
	 * @return the f name
	 */
	public String getfName() {
		return fName;
	}

	/**
	 * Sets the f name.
	 *
	 * @param fName
	 *            the new f name
	 */
	public void setfName(String fName) {
		this.fName = fName;
	}

	/**
	 * Gets the l name.
	 *
	 * @return the l name
	 */
	public String getlName() {
		return lName;
	}

	/**
	 * Sets the l name.
	 *
	 * @param lName
	 *            the new l name
	 */
	public void setlName(String lName) {
		this.lName = lName;
	}

	/**
	 * Instantiates a new contest grand winner DVO.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @param cuId
	 *            the cu id
	 * @param rwdType
	 *            the rwd type
	 * @param rwdVal
	 *            the rwd val
	 * @param fName
	 *            the f name
	 * @param lName
	 *            the l name
	 * @param uId
	 *            the u id
	 * @param email
	 *            the email
	 * @param phone
	 *            the phone
	 */
	public ContestGrandWinnerDVO(String clId, String coId, String cuId, String rwdType, Double rwdVal, String fName,
			String lName, String uId, String email, String phone) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rwdType = rwdType;
		this.rwdVal = rwdVal;
		this.email = email;
		this.fName = fName;
		this.lName = lName;
		this.uId = uId;
		this.phone = phone;
	}

	/**
	 * Instantiates a new contest grand winner DVO.
	 */
	public ContestGrandWinnerDVO() {

	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContestGrandWinnerDVO [clId=" + clId + ", coId=" + coId + ", cuId=" + cuId + ", rwdType=" + rwdType
				+ ", rwdVal=" + rwdVal + ", crDate=" + crDate + ", uId=" + uId + ", imgU=" + imgU + ", email=" + email
				+ "]";
	}
}
