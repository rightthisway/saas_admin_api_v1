/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.firestore;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.LiveContestAnswerCountDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.LiveTriviaUtil;
import com.rtf.saas.sql.dvo.ClientFirestoreConfigDVO;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.dvo.ContestProductSetsDVO;

/**
 * The Class LiveFirestore.
 */
public class LiveFirestore {

	/** The collection. */
	private CollectionReference collection = null;

	/** The client id. */
	private String clientId = null;

	/** The question. */
	private static DocumentReference question = null;

	/** The contest details. */
	private static DocumentReference contestDetails = null;

	/** The answer count. */
	private static DocumentReference answerCount = null;

	/** The total user count. */
	private static DocumentReference totalUserCount = null;

	/** The summary winners. */
	private static DocumentReference summaryWinners = null;

	/** The summary winner count. */
	private static DocumentReference summaryWinnerCount = null;

	/** The grand winners. */
	private static DocumentReference grandWinners = null;

	/** The contest info. */
	private static DocumentReference contestInfo = null;

	/** The jackpot winner. */
	private static DocumentReference jackpotWinner = null;

	/** The products. */
	private static DocumentReference products = null;

	/** The app settings. */
	private static DocumentReference appSettings = null;

	/** The host node. */
	private static DocumentReference hostNode = null;

	/** The prev que. */
	private static DocumentReference prevQue = null;

	/** The fire store log. */
	private static final Logger fireStoreLog = LoggerFactory.getLogger(LiveFirestore.class);

	/**
	 * Initialise fire store.
	 *
	 * @param config
	 *            the config
	 * @return true, if successful
	 */
	public boolean initialiseFireStore(ClientFirestoreConfigDVO config) {
		boolean isInit = false;
		try {
			clientId = config.getClId();
			FileInputStream serviceAccount = new FileInputStream(config.getFirestoreAdminConfig());
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();
			FirebaseApp saasFireStore = FirebaseApp.initializeApp(options, String.valueOf(new Date().getTime()));
			Firestore firestore = FirestoreClient.getFirestore(saasFireStore);
			collection = firestore.collection(config.getClId());
			isInit = true;
			fireStoreLog.info("FIRESTORE INITIALISED FOR CLIENT : " + config.getClId());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE INITIALIZING FIRESTORE FOR CLIENT : " + config.getClId());
		}
		return isInit;
	}

	/**
	 * Update question.
	 *
	 * @param q
	 *            the q
	 */
	public void updateQuestion(LivtContestQuestionDVO q) {
		Date timeStamp = new Date();
		try {
			question = collection.document("question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getConqsnid());
			qMap.put("qNo", q.getQsnseqno());
			qMap.put("text", q.getQsntext());
			qMap.put("optionA", q.getOpa());
			qMap.put("optionB", q.getOpb());
			qMap.put("optionC", q.getOpc());
			qMap.put("optionD", q.getOpd());
			qMap.put("answer", q.getCorans());
			qMap.put("ansPrize", q.getAnsRwdVal());
			qMap.put("qOrientation", q.getQsnorient());
			qMap.put("timeStamp", DateFormatUtil.getISOTimeStamp());
			qMap.put("qenck", q.getEncKey());
			qMap.put("cpncde", q.getCoupcde());
			question.set(qMap);
			fireStoreLog.info("QUESTION NODE UPDATED COID : " + q.getConid() + " | QNO : " + q.getQsnseqno()
					+ " | ANSWER : " + q.getCorans());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE QUESTION NODE UPDATED COID : " + q.getConid() + " | QNO : "
					+ q.getQsnseqno() + " | ANSWER : " + q.getCorans());
		}
	}

	/**
	 * Update question action.
	 *
	 * @param action
	 *            the action
	 */
	public void updateQuestionAction(String action) {
		Date timeStamp = new Date();
		try {
			question = collection.document("question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("answer", action);
			qMap.put("timeStamp", DateFormatUtil.getISOTimeStamp());
			question.update(qMap);
			fireStoreLog.info("QUESTION NODE UPDATED ACTION : " + action);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE QUESTION NODE UPDATED ACTION : " + action);
		}
	}

	/**
	 * Update contest details Q no.
	 *
	 * @param q
	 *            the q
	 */
	public void updateContestDetailsQNo(LivtContestQuestionDVO q) {
		try {
			contestDetails = collection.document("contestdetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", q.getQsnseqno());
			contestDetails.update(cMap);
			fireStoreLog.info("CONTEST DETAILS NODE UPDATED QNO : " + q.getQsnseqno());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CONTEST DETAILS NODE QNO : " + q.getQsnseqno());
		}
	}

	/**
	 * Sets the answer count.
	 *
	 * @param a
	 *            the new answer count
	 */
	public void setAnswerCount(LiveContestAnswerCountDVO a) {
		Date timeStamp = new Date();
		try {
			answerCount = collection.document("answercounts");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", a.getOptACount());
			aMap.put("optionBCount", a.getOptBCount());
			aMap.put("optionCCount", a.getOptCCount());
			aMap.put("optionDCount", a.getOptDCount());
			aMap.put("answerPrize", a.getRwdVal());
			aMap.put("timeStamp", timeStamp.getTime());
			answerCount.set(aMap);
			fireStoreLog.info("ANSWER COUNT NODE UPDATED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE  UPDATING ANSWER COUNT NODE");
		}
	}

	/**
	 * Sets the jackpot winners.
	 *
	 * @param jackpotWinners
	 *            the new jackpot winners
	 */
	public void setJackpotWinners(List<ContestJackpotWinnerDVO> jackpotWinners) {
		try {
			jackpotWinner = collection.document("jackpotwinners");
			if (jackpotWinners != null && !jackpotWinners.isEmpty()) {
				List<Map<String, Object>> jackWinList = new ArrayList<Map<String, Object>>();
				Map<String, Object> sMap = null;
				for (ContestJackpotWinnerDVO w : jackpotWinners) {
					sMap = new HashMap<String, Object>();
					sMap.put("coId", w.getCoId());
					//sMap.put("cuId", w.getCuId());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					//sMap.put("email", w.getEmail());
					if (w.getRwdVal() != null && w.getRwdVal() > 0 && w.getRwdType() != null
							&& !w.getRwdType().isEmpty()) {
						sMap.put("prize", w.getRwdVal() + " " + w.getRwdType());
					} else {
						sMap.put("prize", "");
					}
					jackWinList.add(sMap);
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("winner", jackWinList);
				jackpotWinner.set(map);
			}
			fireStoreLog.info("JACKPOT WINNER NODE UPDATED SIZE : " + jackpotWinners.size());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE  UPDATING JACKPOT WINNER NODE");
		}
	}

	/**
	 * Sets the summary winners.
	 *
	 * @param summaryWinner
	 *            the summary winner
	 * @param count
	 *            the count
	 */
	public void setSummaryWinners(List<ContestWinnerDVO> summaryWinner, Integer count) {
		try {
			summaryWinners = collection.document("summarywinners");
			summaryWinnerCount = collection.document("summarywinnercounts");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				List<Map<String, Object>> summaryList = new ArrayList<Map<String, Object>>();
				Map<String, Object> sMap = null;

				for (ContestWinnerDVO w : summaryWinner) {
					sMap = new HashMap<String, Object>();
					sMap.put("coId", w.getCoId());
					//sMap.put("cuId", w.getCuId());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					if (w.getRwdVal() != null && w.getRwdVal() > 0 && w.getRwdType() != null
							&& !w.getRwdType().isEmpty()) {
						sMap.put("prize", w.getRwdVal() + " " + w.getRwdType());
					} else {
						sMap.put("prize", "");
					}

					summaryList.add(sMap);
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("winners", summaryList);
				summaryWinnerCount.set(aMap);
				summaryWinners.set(map);
			} else {
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				summaryWinnerCount.set(aMap);
			}
			fireStoreLog.info("SUMMARY WINNER NODE UPDATED SIZE : " + count);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE  UPDATING SUMMARY WINNER NODE");
		}
	}

	/**
	 * Sets the grand winners.
	 *
	 * @param summaryWinner
	 *            the new grand winners
	 */
	public void setGrandWinners(List<ContestGrandWinnerDVO> summaryWinner) {
		try {
			grandWinners = collection.document("grandwinners");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				List<Map<String, Object>> winList = new ArrayList<Map<String, Object>>();
				Map<String, Object> sMap = null;
				for (ContestGrandWinnerDVO w : summaryWinner) {
					sMap = new HashMap<String, Object>();
					sMap.put("coId", w.getCoId());
					//sMap.put("cuId", w.getCuId());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					//sMap.put("email", w.getEmail());
					if (w.getRwdVal() != null && w.getRwdVal() > 0 && w.getRwdType() != null
							&& !w.getRwdType().isEmpty()) {
						sMap.put("prize", w.getRwdVal() + " " + w.getRwdType());
					} else {
						sMap.put("prize", "");
					}
					winList.add(sMap);
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("winners", winList);
				grandWinners.set(map);
			}
			fireStoreLog.info("GRAND WINNER NODE UPDATED SIZE : " + summaryWinner.size());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE  UPDATING GRAND WINNER NODE");
		}
	}

	/**
	 * Update total user count.
	 *
	 * @param count
	 *            the count
	 */
	public void updateTotalUserCount(Integer count) {
		try {
			totalUserCount = collection.document("totalusercounts");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("count", count);
			totalUserCount.set(aMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * End contest.
	 */
	public void endContest() {
		Date timeStamp = new Date();
		try {
			question = collection.document("question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", 0);
			qMap.put("qNo", 0);
			qMap.put("text", "");
			qMap.put("optionA", "");
			qMap.put("optionB", "");
			qMap.put("optionC", "");
			qMap.put("optionD", "");
			qMap.put("answer", "END");
			qMap.put("ansPrize", 0);
			qMap.put("timeStamp", DateFormatUtil.getISOTimeStamp());
			qMap.put("eraseOption", "");

			prevQue = collection.document("prevque");
			Map<String, Object> pqMap = new HashMap<String, Object>();
			pqMap.put("qNo", 0);
			pqMap.put("answer", "");
			pqMap.put("ansRwd", 0.00);

			answerCount = collection.document("answercounts");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", 0);
			aMap.put("optionBCount", 0);
			aMap.put("optionCCount", 0);
			aMap.put("optionDCount", 0);
			aMap.put("answerPrize", 0.0);
			aMap.put("timeStamp", timeStamp.getTime());

			contestDetails = collection.document("contestdetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", "");
			cMap.put("contestName", "");
			cMap.put("extendedName", "");
			cMap.put("questionSize", "");
			cMap.put("rewardPrize", "");
			cMap.put("queRwdImg", "");
			cMap.put("conThmImg", "");
			cMap.put("queType", "");
			cMap.put("id", "");
			cMap.put("queRwdType", "");
			cMap.put("runningCount", "");
			cMap.put("isElm", true);

			Map<String, Object> wMap = new HashMap<String, Object>();
			wMap.put("winners", new ArrayList<Map<String, Object>>());

			Map<String, Object> sMap = new HashMap<String, Object>();
			sMap.put("count", 0);

			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("products", new ArrayList<Map<String, Object>>());
			pMap.put("isShowProd", false);
			pMap.put("sellerType", "");
			pMap.put("storeURL", "");

			summaryWinnerCount = collection.document("summarywinnercounts");
			jackpotWinner = collection.document("jackpotwinners");
			summaryWinners = collection.document("summarywinners");
			grandWinners = collection.document("grandwinners");
			products = collection.document("shopingproducts");
			totalUserCount = collection.document("totalusercounts");

			question.set(qMap);
			prevQue.set(pqMap);
			answerCount.set(aMap);
			contestDetails.set(cMap);
			summaryWinners.set(wMap);
			grandWinners.set(wMap);
			jackpotWinner.set(wMap);
			summaryWinnerCount.set(sMap);
			totalUserCount.set(sMap);
			products.update(pMap);
			fireStoreLog.info("CONTEST ENDED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE END CONTEST");
		}
	}

	/**
	 * Sets the contest details.
	 *
	 * @param c
	 *            the c
	 * @param queRwdImg
	 *            the que rwd img
	 * @param isShowProd
	 *            the is show prod
	 */
	public void setContestDetails(ContestDVO c, String queRwdImg, Boolean isShowProd) {
		try {
			contestDetails = collection.document("contestdetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", 0);
			cMap.put("contestName", c.getName());
			cMap.put("extendedName", c.getExtName());
			cMap.put("questionSize", c.getqSize());
			if (c.getWinRwdVal() != null && c.getWinRwdVal() > 0 && c.getWinRwdType() != null
					&& !c.getWinRwdType().isEmpty()) {
				cMap.put("rewardPrize", c.getWinRwdVal() + " " + c.getWinRwdType());
			} else {
				cMap.put("rewardPrize", "");
			}

			cMap.put("queRwdType", c.getQueRwdType());
			cMap.put("id", c.getCoId());
			cMap.put("runningCount", c.getRunCount());
			cMap.put("queRwdImg", queRwdImg);
			cMap.put("conThmImg", c.getThmImgDesk());
			cMap.put("queType", c.getAnsType());
			cMap.put("isElm", c.getIsElimination());
			cMap.put("isShoppable", isShowProd);
			contestDetails.set(cMap);
			fireStoreLog.info("CONTEST DETAILS NODE SET");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE CONTEST DETAILS NODE SET");
		}
	}

	/**
	 * Update live stream url.
	 *
	 * @param url
	 *            the url
	 */
	public void updateLiveStreamUrl(String url) {
		try {
			contestInfo = collection.document("dashboardinfo");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("videoUrl", url);
			contestInfo.update(vMap);
			fireStoreLog.info("VIDEO URL UPDATED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING VIDEO URL");
		}
	}

	/**
	 * Sets the contest started.
	 *
	 * @param isStarted
	 *            the new contest started
	 */
	public void setContestStarted(Boolean isStarted) {
		try {
			contestInfo = collection.document("dashboardinfo");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("isContestStarted", isStarted);
			contestInfo.update(vMap);
			fireStoreLog.info("CONTEST STARTED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING START CONTEST");
		}
	}

	/**
	 * Update upcoming contest details.
	 */
	public void updateUpcomingContestDetails() {
		try {
			contestInfo = collection.document("dashboardinfo");

			List<ContestDVO> contests = LiveContestService.getAllContestByStatusandFilter(clientId, "ACTIVE", null,
					"1");
			ContestDVO liveContest = LiveContestService.getContestByStatus(clientId, 2);
			List<Map<String, Object>> contestMapList = new ArrayList<Map<String, Object>>();
			if (contests != null && !contests.isEmpty()) {
				ContestDVO c = contests.get(0);
				Map<String, Object> cMap = new HashMap<String, Object>();
				cMap.put("isContestStarted", liveContest == null ? false : true);
				cMap.put("nextContestId", c.getCoId());
				cMap.put("isPassReq", c.getIsPwd());

				for (ContestDVO contest : contests) {
					if (contestMapList.size() < 4) {
						Map<String, Object> contestMap = new HashMap<String, Object>();
						contestMap.put("contestId", contest.getCoId());
						contestMap.put("contestName", contest.getName());
						contestMap.put("type", contest.getCat());
						contestMap.put("imageUrl", "");
						contestMap.put("contestTime",
								DateFormatUtil.getNextContestStartTimeMsg(new Date(contest.getStDate())));
						contestMapList.add(contestMap);
					}
				}

				if (contestMapList.isEmpty()) {
					Map<String, Object> contestMap = new HashMap<String, Object>();
					contestMap.put("contestId", -1);
					contestMap.put("contestName", "Next Contest will be Announced Shortly.");
					contestMap.put("type", "NONE");
					contestMap.put("imageUrl", "");
					contestMap.put("contestTime", "TBD");
					contestMap.put("shareText", "");
					contestMapList.add(contestMap);
				}

				cMap.put("contestArray", contestMapList);
				contestInfo.update(cMap);
			} else {
				Map<String, Object> contestMap = new HashMap<String, Object>();
				contestMap.put("contestId", -1);
				contestMap.put("contestName", "Next Contest will be Announced Shortly.");
				contestMap.put("type", "NONE");
				contestMap.put("imageUrl", "");
				contestMap.put("contestTime", "TBD");
				contestMap.put("shareText", "");
				contestMapList.add(contestMap);

				Map<String, Object> cMap = new HashMap<String, Object>();
				cMap.put("isContestStarted", liveContest == null ? false : true);
				cMap.put("nextContestId", 0);

				cMap.put("contestArray", contestMapList);
				contestInfo.update(cMap);
			}
			fireStoreLog.info("UPCOMING CONTEST DETAILS UPDATED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING UPCOMING CONTEST DETAILS");
		}
	}

	/**
	 * Sets the shoping products.
	 *
	 * @param prods
	 *            the prods
	 * @param isDec
	 *            the is dec
	 */
	public void setShopingProducts(List<ContestProductSetsDVO> prods, Boolean isDec,Map<String, Object> sellerinfoMap ) {
		try {
			products = collection.document("shopingproducts");
			if (prods != null && !prods.isEmpty()) {
				Date tStamp = new Date();
				List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
				Map<String, Object> pMap = null;
				for (ContestProductSetsDVO p : prods) {
					pMap = new HashMap<String, Object>();
					pMap.put("coId", p.getConId());
					pMap.put("merchId", p.getMercId());
					pMap.put("imgU", p.getProdImgUrl());
					String sPrice = null;
					String rPrice = null;
					if (isDec) {
						sPrice = LiveTriviaUtil.getRoundedValueString(p.getPricePerUnit());
						rPrice = LiveTriviaUtil.getRoundedValueString(p.getRegPrice());
						if (sPrice == rPrice || sPrice.equals(rPrice)) {
							rPrice = "0";
						}
					} else {
						sPrice = LiveTriviaUtil.getIntegerValueRoundedUpString(p.getPricePerUnit());
						rPrice = LiveTriviaUtil.getIntegerValueRoundedUpString(p.getRegPrice());
						if (sPrice == rPrice || sPrice.equals(rPrice)) {
							rPrice = "0";
						}
					}
					pMap.put("price", sPrice);
					pMap.put("regPrice", rPrice);
					pMap.put("prodPriceDtl", p.getProdPriceDtl());
					pMap.put("isDecimal", isDec);
					pMap.put("priceType", p.getPriceType() != null ? p.getPriceType() : "");
					pMap.put("prodDesc", p.getProdDesc());
					pMap.put("prodId", p.getMercProdid());
					pMap.put("prodName", p.getProdName());
					pMap.put("timeStamp", tStamp.getTime());
					pMap.put("merchantProdRefId",p.getMerexternalRefProdId() );
					pList.add(pMap);
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("products", pList);
				map.put("isShowProd", true);
				map.put("sellerType", sellerinfoMap.get("sellerType"));
				map.put("storeURL", sellerinfoMap.get("storeURL"));
				products.set(map);
			}
			fireStoreLog.info("SHOPPING PRODUCTS UPDATED SIZE : " + prods.size());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING SHOPPING PRODUCTS");
		}
	}

	/**
	 * Clear shoping products.
	 */
	public void clearShopingProducts() {
		try {
			products = collection.document("shopingproducts");
			List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("products", pList);
			map.put("isShowProd", false);
			products.set(map);
			fireStoreLog.info("SHOPPING PRODUCTS CLEARED ");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE CLEARING SHOPPING PRODUCTS");
		}
	}

	/**
	 * Update host tracker.
	 *
	 * @param state
	 *            the state
	 * @param desc
	 *            the desc
	 * @param ans
	 *            the ans
	 * @param ansCount
	 *            the ans count
	 * @param winCount
	 *            the win count
	 * @param sumWinners
	 *            the sum winners
	 * @param grandWinners
	 *            the grand winners
	 */
	private void updateHostTracker(String state, String desc, String ans, Integer ansCount, Integer winCount,
			List<ContestWinnerDVO> sumWinners, List<ContestGrandWinnerDVO> grandWinners) {
		try {
			hostNode = collection.document("hostnode");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("ans", ans);
			tMap.put("ansCount", ansCount);
			tMap.put("tracker", state);
			tMap.put("description", desc);

			if (state.equalsIgnoreCase(LivePlayContest.SUMMARY.toString())) {
				List<Map<String, Object>> summaryList = new ArrayList<Map<String, Object>>();
				String prize = null;
				if (sumWinners != null && !sumWinners.isEmpty()) {
					for (ContestWinnerDVO w : sumWinners) {
						Map<String, Object> sMap = new HashMap<String, Object>();
						sMap.put("coId", w.getCoId());
						//sMap.put("cuId", w.getCuId());
						sMap.put("uId", w.getuId());
						prize = (w.getRwdVal() != null && w.getRwdVal() > 0) ? w.getRwdVal() + " " + w.getRwdType()
								: " ";
						summaryList.add(sMap);
					}
				}
				tMap.put("sWinner", summaryList);
				tMap.put("sPrize", prize);
				tMap.put("winCount", winCount);
			}

			if (state.equalsIgnoreCase(LivePlayContest.DCLWINNER.toString())) {
				List<Map<String, Object>> grandWinnersList = new ArrayList<Map<String, Object>>();
				String prize = null;
				if (grandWinners != null && !grandWinners.isEmpty()) {
					for (ContestGrandWinnerDVO w : grandWinners) {
						Map<String, Object> sMap = new HashMap<String, Object>();
						sMap.put("coId", w.getCoId());
						//sMap.put("cuId", w.getCuId());
						sMap.put("uId", w.getuId());
						prize = (w.getRwdVal() != null && w.getRwdVal() > 0) ? w.getRwdVal() + " " + w.getRwdType()
								: " ";
						grandWinnersList.add(sMap);
					}
				}
				tMap.put("gWinner", grandWinnersList);
				tMap.put("gPrize", prize);
				tMap.put("winCount", winCount);
			}

			if (state.equalsIgnoreCase(LivePlayContest.END.toString())) {
				tMap.put("gWinner", new ArrayList<Map<String, Object>>());
				tMap.put("sWinner", new ArrayList<Map<String, Object>>());
				tMap.put("winCount", winCount);
				tMap.put("gPrize", "");
				tMap.put("sPrize", "");
				tMap.put("ans", "");
				tMap.put("ansCount", "");
				tMap.put("isShowProd", false);
			}

			hostNode.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update product for host.
	 *
	 * @param state
	 *            the state
	 */
	public void updateProductForHost(Boolean state) {
		try {
			hostNode = collection.document("hostnode");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("isShowProd", state);
			hostNode.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update chatting on off.
	 *
	 * @param isChats
	 *            the is chats
	 */
	public void updateChattingOnOff(Boolean isChats) {
		try {
			appSettings = collection.document("appSettings");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("isChat", isChats);
			appSettings.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update answer count on off.
	 *
	 * @param isOn
	 *            the is on
	 */
	public void updateAnswerCountOnOff(Boolean isOn) {
		try {
			appSettings = collection.document("appSettings");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("dispACount", isOn);
			appSettings.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update total user count on off.
	 *
	 * @param isOn
	 *            the is on
	 */
	public void updateTotalUserCountOnOff(Boolean isOn) {
		try {
			appSettings = collection.document("appSettings");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("dispTCount", isOn);
			appSettings.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Update  question timer count.
	 *
	 * @param val
	 */
	public void updateQuestionTimer(String val) {
		try {
			appSettings = collection.document("appSettings");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("qTime", val);
			appSettings.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Update answer timer count.
	 *
	 * @param val
	 */
	public void updateAnswerTimer(String val) {
		try {
			appSettings = collection.document("appSettings");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("aTime", val);
			appSettings.update(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update host node.
	 *
	 * @param state
	 *            the state
	 * @param answer
	 *            the answer
	 * @param answerCount
	 *            the answer count
	 * @param queNo
	 *            the que no
	 * @param winnerCount
	 *            the winner count
	 * @param sumWinners
	 *            the sum winners
	 * @param grandWinners
	 *            the grand winners
	 */
	public void updateHostNode(LivePlayContest state, String answer, Integer answerCount, Integer queNo,
			Integer winnerCount, List<ContestWinnerDVO> sumWinners, List<ContestGrandWinnerDVO> grandWinners) {
		try {
			String description = "";
			Integer qNo = null;
			Integer ansCount = null;
			Integer winCount = null;
			String ans = null;
			if (state == null) {
				return;
			}
			if (state.equals(LivePlayContest.START)) {
				description = "CONTEST STARTED, READY FOR Q1";
			} else if (state.equals(LivePlayContest.QUESTION)) {
				qNo = queNo;
				description = "Q" + qNo + " DISPLAYED";
			} else if (state.equals(LivePlayContest.COUNT)) {
				qNo = queNo;
				ans = answer;
				ansCount = answerCount;
				description = "TELL A" + qNo;
			} else if (state.equals(LivePlayContest.ANSWER)) {
				qNo = queNo;
				ansCount = answerCount;
				ans = answer;
				description = "A" + qNo + " DISPLAYED";
			} else if (state.equals(LivePlayContest.SUMMARY)) {
				winCount = winnerCount;
				description = "READY FOR SUMMARY";
			} else if (state.equals(LivePlayContest.DSPSUMMARY)) {
				winCount = winnerCount;
				description = "SUMMARY DISPLAYED";
			} else if (state.equals(LivePlayContest.LOTTERY)) {
				description = "RUNNING LOTTERY";
			} else if (state.equals(LivePlayContest.DCLWINNER)) {
				description = "READY FOR WINNER";
			} else if (state.equals(LivePlayContest.DSPWINNER)) {
				description = "WINNER DISPLAYED";
			} else if (state.equals(LivePlayContest.END)) {
				description = "CONTEST ENDED";
			}
			updateHostTracker(state.toString(), description, ans, ansCount, winCount, sumWinners, grandWinners);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update prev question.
	 *
	 * @param q
	 *            the q
	 */
	public void updatePrevQuestion(LivtContestQuestionDVO q) {
		try {
			if (q == null) {
				return;
			}
			prevQue = collection.document("prevque");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qNo", q.getQsnseqno());
			qMap.put("answer", q.getCorans());
			qMap.put("ansRwd", q.getAnsRwdVal());
			String coupCode = GenUtil.getEmptyStringIfNull(q.getCoupcde());
			qMap.put("disCode", coupCode);
			prevQue.set(qMap);
			fireStoreLog.info("PREV QUESTION NODE UPDATED COID : " + q.getConid() + " | QNO : " + q.getQsnseqno()
					+ " | ANSWER : " + q.getCorans());
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE PREV QUESTION NODE UPDATED COID : " + q.getConid() + " | QNO : "
					+ q.getQsnseqno() + " | ANSWER : " + q.getCorans());
		}
	}
	
	
	/**
	 * Update product for host.
	 *
	 * @param state
	 *            the state
	 */
	public void updateSellerDets(Map<String, Object> dataMap) { 
		try {
			products = collection.document("shopingproducts");			
			products.update(dataMap);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public void setContestPlayBackURL(String playBackUrl) {
		try {
			contestInfo = collection.document("dashboardinfo");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("videoUrl", playBackUrl);
			contestInfo.update(vMap);
			fireStoreLog.info("PLAY BACK URL ADDED");
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING PLAY BACK URL");
		}
	}

}
