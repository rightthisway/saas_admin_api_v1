/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LiveContestHost;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.LiveTriviaUtil;
import com.rtf.livt.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveUpdateContestServlet.
 */
@WebServlet("/liveupdatecontests.json")
public class LiveUpdateContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -2358467883869182348L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		String name = request.getParameter("name");
		String extName = request.getParameter("extName");
		String stDate = request.getParameter("stDate");
		String stHour = request.getParameter("stHour");
		String stMinute = request.getParameter("stMinutes");
		String cat = request.getParameter("cat");
		String coType = request.getParameter("coType");
		String subCat = request.getParameter("subCat");
		String qType = request.getParameter("qType");
		String elmType = request.getParameter("elmType");
		String isSummSplit = request.getParameter("isSummSplit");
		String sumRwdType = request.getParameter("sumRwdType");
		String sumRwdVal = request.getParameter("sumRwdVal");
		String partRwdType = request.getParameter("partRwdType");
		String partRwdVal = request.getParameter("partRwdVal");
		String queRwdType = request.getParameter("queRwdType");
		String isLottery = request.getParameter("isLottery");
		String winRwdType = request.getParameter("winRwdType");
		String winRwdVal = request.getParameter("winRwdVal");
		String winCount = request.getParameter("winCount");
		String qSize = request.getParameter("qSize");
		String isPwdStr = request.getParameter("isPwd");
		String conPwd = request.getParameter("conPwd");
		String expPartCntStr = request.getParameter("expPartCnt");
		String hostIdStr = request.getParameter("hostId");

		LiveContestDTO respDTO = new LiveContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (partRwdType == null || partRwdType.trim().isEmpty()) {
				partRwdType = null;
			}
			if (queRwdType == null || queRwdType.trim().isEmpty()) {
				queRwdType = null;
			}
			if (sumRwdType == null || sumRwdType.trim().isEmpty()) {
				sumRwdType = null;
			}
			if (winRwdType == null || winRwdType.trim().isEmpty()) {
				winRwdType = null;
			}
			if (subCat == null || subCat.isEmpty()) {
				subCat = null;
			}
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, "Invalid contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, "Invalid start date.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stHour == null || stHour.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stMinute == null || stMinute.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cat == null || cat.isEmpty()) {
				setClientMessage(respDTO, "Invalid category.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qType == null || qType.isEmpty()) {
				setClientMessage(respDTO, "Invalid nature of question.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (elmType == null || elmType.isEmpty()) {
				setClientMessage(respDTO, "Invalid elimination type.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qSize == null || qSize.isEmpty()) {
				setClientMessage(respDTO, "Invalid No. of question.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (expPartCntStr == null || expPartCntStr.isEmpty()) {
				setClientMessage(respDTO, "Expected participants count is mendatory", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if ((sumRwdType != null && !sumRwdType.isEmpty()) && (sumRwdVal == null || sumRwdVal.isEmpty())) {
				setClientMessage(respDTO,
						"Contest winners prize value is mendatory, when Contest winners prize type is selected.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if ((sumRwdType == null || sumRwdType.isEmpty()) && (sumRwdVal != null && !sumRwdVal.isEmpty())) {
				setClientMessage(respDTO,
						"Contest winners prize type is mendatory, when Contest winners prize value is provided.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coType == null || coType.isEmpty()) {
				setClientMessage(respDTO, "Contest type is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (isSummSplit == null || isSummSplit.isEmpty()) {
				setClientMessage(respDTO, "Contest winners prize split option is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (isLottery == null || isLottery.isEmpty()) {
				setClientMessage(respDTO, "Run lottery option is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			Boolean isLot = false;
			try {
				isLot = Boolean.parseBoolean(isLottery);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid run lottery option.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date sDate = null;
			Boolean isSplitSumm = false;
			Double sumRwdValue = 0.0;
			Double winRwdValue = 0.0;
			Double partiRwdVal = 0.0;
			Integer winCounts = 0;
			Integer queSize = 0;
			Integer hour = 0;
			Integer minute = 0;
			Integer expPartCnt = 0;
			Integer hostId = null;
			LiveContestHost host = null;
			if (isLot) {
				if (winRwdVal == null || winRwdVal.isEmpty()) {
					winRwdVal = "0";
				}
				if (winCount == null || winCount.isEmpty()) {
					setClientMessage(respDTO, "No. of lottery winners is mendatory when lottery option is yes.", null);
					generateResponse(request, response, respDTO);
					return;
				}

				if (winRwdVal != null && !winRwdVal.isEmpty()) {
					try {
						winRwdValue = Double.parseDouble(winRwdVal);
					} catch (Exception e) {
						e.printStackTrace();
						setClientMessage(respDTO, "Invalid lottery winners prize value", null);
						generateResponse(request, response, respDTO);
						return;
					}
				}

				try {
					winCounts = Integer.parseInt(winCount);
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO, "Invalid no. of lottery winners.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			if (partRwdType != null && !partRwdType.isEmpty()) {
				if (partRwdVal == null || partRwdVal.isEmpty()) {
					setClientMessage(respDTO, "Invalid participantion prize value.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				try {
					partiRwdVal = Double.parseDouble(partRwdVal);
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO, "Invalid participantion prize value.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			
			if(hostIdStr!=null && !hostIdStr.isEmpty()){
				try {
					hostId = Integer.parseInt(hostIdStr);
					host = LiveContestService.getLiveContestHostById(clId, hostId);
					if(host == null){
						setClientMessage(respDTO,  MessageConstant.INVALID_CONTEST_HOST, null);
						generateResponse(request, response, respDTO);
						return;
					}
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO,  MessageConstant.INVALID_CONTEST_HOST, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			try {
				hour = Integer.parseInt(stHour);
				minute = Integer.parseInt(stMinute);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			String stDate1 = stDate + " " + hour + ":" + minute + ":00";
			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			if (sDate == null) {
				setClientMessage(respDTO, "Invalid Contest start date and time.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				isSplitSumm = Boolean.parseBoolean(isSummSplit);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid contest winners prize split option value", null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (sumRwdVal != null && !sumRwdVal.isEmpty()) {
				try {
					sumRwdValue = Double.parseDouble(sumRwdVal);
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO, "Invalid contest winners prize value.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			try {
				queSize = Integer.parseInt(qSize);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid no of question value.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				expPartCnt = Integer.parseInt(expPartCntStr);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid expected participants count.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Boolean isPwd = Boolean.FALSE;
			if (isPwdStr != null && !isPwdStr.isEmpty()) {
				try {
					isPwd = Boolean.parseBoolean(isPwdStr);
				} catch (Exception e) {
					e.printStackTrace();
					setClientMessage(respDTO, "Invalid value for Is password protected game.", null);
					generateResponse(request, response, respDTO);
					return;
				}

				if (isPwd && (conPwd == null || conPwd.isEmpty())) {
					setClientMessage(respDTO, "password is mendatory for pasword protected game.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (!isPwd) {
					conPwd = null;
				}
			} else {
				conPwd = null;
			}

			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, "Invalid contest identifier.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() == 2 || contest.getIsAct().equals(2)) {
				setClientMessage(respDTO, "Not Allowed to update Live contest, Please end it first.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientConfigDVO config = ClientConfigService.getContestConnfigByKey(clId, Constant.LIVETRIVIA,
					Constant.LIVE_TRIVIA_CONTEST_RESTRICTED_DURATION_HRS);
			Integer restrictHours = 6;
			try {
				if (config != null) {
					restrictHours = Integer.parseInt(config.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, restrictHours);
			if (sDate.before(cal.getTime())) {
				setClientMessage(respDTO, "Contest start date time should be future date, Minimum " + restrictHours
						+ " after current time.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Boolean flag = LiveTriviaUtil.calculateExpectedParticipantCount(stDate, hour, minute, expPartCnt);
			if (!flag) {
				setClientMessage(respDTO, "This time-slot has been booked completely, Please choose another timeslot.",
						null);
				generateResponse(request, response, respDTO);
				return;
			}

			contest.setAnsType(qType);
			contest.setLastAction(null);
			contest.setLastQue(0);
			contest.setExpPartCount(expPartCnt);
			contest.setIsPwd(isPwd);
			contest.setConPwd(conPwd);
			contest.setPartiRwdType(partRwdType);
			contest.setPartiRwdVal(partiRwdVal);
			contest.setQueRwdType(queRwdType);
			contest.setCoType(coType);
			contest.setCat(cat);
			contest.setUpBy(cau);
			contest.setClId(clId);
			contest.setClImgU(null);
			contest.setCoId(coId);
			contest.setImgU(null);
			contest.setIsAct(1);
			contest.setIsElimination(elmType.equalsIgnoreCase("true") ? true : false);
			contest.setName(name);
			contest.setExtName(extName);
			contest.setqSize(queSize);
			contest.setIsSplitSummary(isSplitSumm);
			contest.setSumRwdType(sumRwdType);
			contest.setSumRwdVal(sumRwdValue);
			contest.setIsLotEnbl(isLot);
			contest.setWinnerCount(winCounts);
			contest.setWinRwdType(winRwdType);
			contest.setWinRwdVal(winRwdValue);
			contest.setSeqNo(1);
			contest.setStDate(sDate.getTime());
			contest.setSubCat(subCat);
			contest.setThmId(0);
			contest.setHstImgUrl(host!=null?host.getHostImageUrl():null);
			contest.setHostedBy(host!=null?host.getHostName():null);

			boolean isUpdated = LiveContestService.updateContest(contest);
			if (isUpdated) {
				LiveContestService.saveContestAudit(contest);
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateUpcomingContestDetails();
				}
				respDTO.setSts(1);
				respDTO.setMsg("Contest data updated successfully.");
				respDTO.setContest(contest);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, "Something went wrong, Please try again.", null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
