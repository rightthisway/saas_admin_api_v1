/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.util.LiveTriviaUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveValidateContestTimeslotServlet.
 */
@WebServlet("/livevalidatecontesttimeslot.json")
public class LiveValidateContestTimeslotServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -3906838434318673705L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String stDate = request.getParameter("stDate");
		String stHour = request.getParameter("stHour");
		String stMinute = request.getParameter("stMinutes");
		String expPartCntStr = request.getParameter("expPartCnt");
		LiveContestDTO respDTO = new LiveContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		try {

			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, "Invalid start date.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stHour == null || stHour.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stMinute == null || stMinute.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (expPartCntStr == null || expPartCntStr.isEmpty()) {
				setClientMessage(respDTO, "Expected participants count is mendatory", null);
				generateResponse(request, response, respDTO);
				return;
			}
			Date sDate = null;
			Integer hour = 0;
			Integer minute = 0;
			Integer expPartCnt = 0;

			try {
				hour = Integer.parseInt(stHour);
				minute = Integer.parseInt(stMinute);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			String stDate1 = stDate + " " + hour + ":" + minute + ":00";
			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			if (sDate == null) {
				setClientMessage(respDTO, "Invalid Contest start date and time.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				expPartCnt = Integer.parseInt(expPartCntStr);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid expected participants count.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Boolean flag = LiveTriviaUtil.calculateExpectedParticipantCount(stDate, hour, minute, expPartCnt);
			if (!flag) {
				setClientMessage(respDTO, "This time-slot has been booked completely, Please choose another timeslot.",
						null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, "Something went wrong please try again.", null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
