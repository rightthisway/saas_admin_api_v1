/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtUpdateContestQuestionsSeqNoServlet.
 */
@WebServlet("/livtUpdateContQuestSeqNo.json")
public class LivtUpdateContestQuestionsSeqNoServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 8907586120928796949L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsnIdStr = request.getParameter("qId");
		String moveType = request.getParameter("mtype");
		String userName = request.getParameter("cau");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, "Invalid User Name.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (moveType == null || moveType.isEmpty()) {
				setClientMessage(respDTO, "Invalid question move Type.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!moveType.equals("UP") && !moveType.equals("DOWN")) {
				setClientMessage(respDTO, "Invalid question move Type.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qId = null;
			try {
				qId = Integer.parseInt(qsnIdStr);

			} catch (Exception e) {
				setClientMessage(respDTO, "Invalid Question id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			LivtContestQuestionDVO contQuestion = LivtContestQuestionsService.getcontestquestionsByQuestionId(clId,
					contId, qId);
			if (contQuestion == null) {
				setClientMessage(respDTO, "question id not exists.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest id not exists", null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<LivtContestQuestionDVO> updatedList = new ArrayList<LivtContestQuestionDVO>();
			if (moveType.equals("UP")) {
				if (contQuestion.getQsnseqno().equals(1)) {
					setClientMessage(respDTO, "You can't move first question to Upside.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				Integer newSeqno = contQuestion.getQsnseqno() - 1;
				contQuestion.setQsnseqno(newSeqno);
				contQuestion.setUpdby(userName);
				contQuestion.setUpddate(new Date());
				updatedList.add(contQuestion);

				LivtContestQuestionDVO newseqNocontestQuestion = LivtContestQuestionsService
						.getcontestquestionsByContestIdandQuestionSeqNo(clId, contId, newSeqno);
				if (newseqNocontestQuestion == null) {
					setClientMessage(respDTO, "You can't move first question to Upside.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				newseqNocontestQuestion.setQsnseqno(newSeqno + 1);
				newseqNocontestQuestion.setUpdby(userName);
				newseqNocontestQuestion.setUpddate(new Date());
				updatedList.add(newseqNocontestQuestion);

			} else if (moveType.equals("DOWN")) {
				Integer newSeqno = contQuestion.getQsnseqno() + 1;
				contQuestion.setQsnseqno(newSeqno);
				contQuestion.setUpdby(userName);
				contQuestion.setUpddate(new Date());
				updatedList.add(contQuestion);

				LivtContestQuestionDVO newseqNocontestQuestion = LivtContestQuestionsService
						.getcontestquestionsByContestIdandQuestionSeqNo(clId, contId, newSeqno);
				if (newseqNocontestQuestion == null) {
					setClientMessage(respDTO, "You can't move last question to Downside.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				newseqNocontestQuestion.setQsnseqno(newSeqno - 1);
				newseqNocontestQuestion.setUpdby(userName);
				newseqNocontestQuestion.setUpddate(new Date());
				updatedList.add(newseqNocontestQuestion);
			}
			if (!updatedList.isEmpty()) {
				LivtContestQuestionsService.updateContestQuestionsSeqno(clId, updatedList);
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, "Questions seqNo updated successfully.");
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, "Something went wrong, please try again sometime later", null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
