package com.rtf.livt.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.util.ShopMessageConstant;

@WebServlet("/upldliveconscript.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class LiveUploadContestScriptServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -7290370181349285051L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON
	 * format.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Upload contest script file to S3
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String userId = request.getParameter("cau");

		LiveContestDTO dto = new LiveContestDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(dto, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}

			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(dto, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}

			ClientConfigDVO config = ClientConfigService.getContestConnfigByKey(clId, Constant.LIVETRIVIA,
					Constant.LIVE_TRIVIA_CONTEST_SCRIPT_RESTRICTED_DURATION_HRS);
			Integer restrictHours = 24;
			try {
				if (config != null) {
					restrictHours = Integer.parseInt(config.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			Date stDate = new Date(contest.getStDate());
			cal.add(Calendar.HOUR, restrictHours);
			if (stDate.before(cal.getTime())) {
				setClientMessage(dto,
						MessageConstant.CONTEST_SCRIPT_MSG1 + restrictHours + MessageConstant.CONTEST_SCRIPT_MSG2,
						null);
				generateResponse(request, response, dto);
				return;
			}

			String TMP_DIR = SaaSAdminAppCache.TMP_FOLDER;
			TMP_DIR = TMP_DIR + File.separator + clId + File.separator;

			File fileSaveDir = new File(TMP_DIR);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			String origFileName = null;
			try {
				for (Part part : request.getParts()) {
					origFileName = FileUtil.getFileName(part);
				}
			} catch (Exception ex) {
				dto.setSts(0);
				setClientMessage(dto, MessageConstant.INVALID_CONTEST_SCRIPT_FILE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(origFileName)) {
				dto.setSts(0);
				setClientMessage(dto, MessageConstant.INVALID_CONTEST_SCRIPT_FILE, null);
				generateResponse(request, response, dto);
				return;
			}

			String EXTENSION = FileUtil.getFileExtension(origFileName);
			String fileName = clId + "_" + coId + "." + EXTENSION;
			try {
				for (Part part : request.getParts()) {
					part.write(TMP_DIR + File.separator + fileName);
				}
			} catch (Exception ex) {
				dto.setSts(0);
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			String s3bucket = SaaSAdminAppCache.s3SecuredBucket;
			String scriptFolder = SaaSAdminAppCache.envfoldertype + "/" + SaaSAdminAppCache.s3ContestScriptFolder + "/" + clId;
			File tmpfile = new File(TMP_DIR + File.separator + fileName);
			AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, fileName, scriptFolder, tmpfile);
			if (null != awsRsponse && awsRsponse.getStatus() != 1) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}

			contest.setScrptFile(fileName);
			contest.setUpBy(userId);
			LiveContestService.updateContestScriptFile(contest);

			dto.setSts(1);
			dto.setMsg(MessageConstant.CONTEST_SCRIPT_FILE_UPLOADED);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
	}

}
