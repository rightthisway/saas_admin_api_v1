/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.constants.LiveContestConst;
import com.rtf.livt.constants.SAASAPI;
import com.rtf.livt.dto.LivePlayContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.enums.LiveContestStatus;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.SaaSApiTrackingService;
import com.rtf.saas.sql.dao.RewardTypeDAO;
import com.rtf.saas.sql.dvo.RewardTypeDVO;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveStartContestServlet.
 */
@WebServlet("/livestartcontest.json")
public class LiveStartContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -5475176003510502694L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cau = request.getParameter("cau");
		LivePlayContestDTO respDTO = new LivePlayContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		String msg = null;

		try {
			if (clId == null || clId.isEmpty()) {
				msg = "Invalid client id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				msg = "Invalid contest id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = "Invalid loggedin user.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO migContest = LiveContestService.getContestByStatus(clId, 5);
			if (migContest != null) {
				msg = "Not allowed to start contest, Last played contest data are still migrating.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				msg = "Contest not found in system.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			String queRwdImg = "";
			if (contest.getQueRwdType() != null && !contest.getQueRwdType().isEmpty()) {
				RewardTypeDVO reward = RewardTypeDAO.getRewardType(clId, contest.getQueRwdType());
				if (reward != null) {
					queRwdImg = reward.getRwdImgUrl();
				}
			}
			contest.setUpBy(cau);
			contest.setLastAction(null);
			contest.setLastQue(0);
			contest.setIsAct(1);
			LiveContestService.resetContest(contest);
			if (!SAASAPI.resetContest(contest)) {
				msg = "Error occured while saas api resetting data before starting contest.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (contest.getRunCount() == null) {
				contest.setRunCount(0);
			}
			contest.setRunCount(contest.getRunCount() + 1);
			contest.setIsAct(2);
			contest.setUpBy(cau);
			contest.setLastAction(LivePlayContest.START.toString());
			contest.setLastQue(0);
			Boolean isStarted = LiveContestService.startContest(contest);
			if (!isStarted) {
				msg = "Something went wrong while starting contest";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			isStarted = LiveContestService.updateContestState(contest);
			if (isStarted) {
				Integer prodCount = LiveContestService.getContestProductsetCount(contest.getClId(), contest.getCoId());
				Boolean isShowProd = false;
				if (prodCount != null && prodCount > 0) {
					isShowProd = true;
				}
				LiveContestService.saveContestAudit(contest);
				LivtContestQuestionDVO que = LivtContestQuestionsService
						.getcontestquestionsByContestIdandQuestionSeqNo(contest.getClId(), contest.getCoId(), 1);

				if (!SAASAPI.startContest(contest)) {

					contest.setUpBy(cau);
					contest.setLastAction(null);
					contest.setLastQue(0);
					contest.setIsAct(1);
					LiveContestService.resetContest(contest);

					msg = "Error occured while saas api start contest.";
					setClientMessage(respDTO, msg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
					generateResponse(request, response, respDTO);
					return;
				}

				for (LiveFirestore firestore : firestores) {
					firestore.setContestDetails(contest, queRwdImg, isShowProd);
					firestore.setContestStarted(true);
					firestore.updateQuestionAction(LiveContestStatus.LIVE.toString());
					firestore.updateHostNode(LivePlayContest.START, null, null, null, null, null, null);
				}
				respDTO.setSts(1);
				respDTO.setQuestion(que);
				respDTO.setIsProdConfig(prodCount > 0 ? true : false);
				respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - 1");
				respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
				if (contest.getqSize() <= 0) {
					respDTO.setBtnText(LiveContestConst.END_CONTEST_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_END_API);
				}
				msg = "Contest started.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				msg = "Error occured while starting contest data.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			msg = "Error occured while starting contest data.";
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			SaaSApiTrackingService.saveContestTracking(clId, coId, "livestartcontest.json", cau, "WEB",
					request.getRemoteAddr(), LivePlayContest.START.toString(), msg, respDTO.getSts(), 0);
		}
		return;
	}
}
