/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dto.LivtContestQuestionsDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtDeleteContestQuestionsServlet.
 */
@WebServlet("/livtDeleteContQuest.json")
public class LivtDeleteContestQuestionsServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 8327829809099787185L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsnIdsStr = request.getParameter("qIds");
		String userName = request.getParameter("cau");

		LivtContestQuestionsDTO respDTO = new LivtContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, "Invalid User Name.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsnIdsStr == null || qsnIdsStr.isEmpty()) {
				setClientMessage(respDTO, "Invalid Question ids.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			String[] qIdsArr = qsnIdsStr.split(",");
			if (qIdsArr.length == 0) {
				setClientMessage(respDTO, "Invalid Question ids.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest id not exists", null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<LivtContestQuestionDVO> contQuestions = new ArrayList<LivtContestQuestionDVO>();
			for (String qidStr : qIdsArr) {
				try {
					Integer qId = Integer.parseInt(qidStr);
					LivtContestQuestionDVO contQuestion = LivtContestQuestionsService
							.getcontestquestionsByQuestionId(clId, contId, qId);
					if (contQuestion == null) {
						setClientMessage(respDTO, "Question id not exists", null);
						generateResponse(request, response, respDTO);
						return;
					}
					contQuestions.add(contQuestion);
				} catch (Exception e) {
					setClientMessage(respDTO, "Invalid Question ids.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			List<LivtContestQuestionDVO> deletedList = new ArrayList<LivtContestQuestionDVO>();
			for (LivtContestQuestionDVO contQuestion : contQuestions) {
				Integer updateCnt = LivtContestQuestionsService.deleteContestQuestions(clId, contQuestion);
				if (updateCnt != null && updateCnt > 0) {
					deletedList.add(contQuestion);
				}
			}
			List<LivtContestQuestionDVO> updatedList = new ArrayList<LivtContestQuestionDVO>();
			List<LivtContestQuestionDVO> list = LivtContestQuestionsService.getcontestquestionsBycontestId(clId,
					contId);
			if (list != null && !list.isEmpty()) {
				int seqNo = 1;
				for (LivtContestQuestionDVO contestQuestion : list) {
					if (!contestQuestion.getQsnseqno().equals(seqNo)) {
						contestQuestion.setQsnseqno(seqNo);
						contestQuestion.setUpdby(userName);
						contestQuestion.setUpddate(new Date());

						updatedList.add(contestQuestion);
					}
					seqNo++;
				}
			}
			if (!updatedList.isEmpty()) {
				LivtContestQuestionsService.updateContestQuestionsSeqno(clId, updatedList);
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, "Questions Deleted successfully.");
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, "Something went wrong, please try again sometime later", null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
