/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.constants.LiveContestConst;
import com.rtf.livt.constants.SAASAPI;
import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.MigrationAPIInvokerThread;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.SaaSApiTrackingService;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveEndContestServlet.
 */
@WebServlet("/liveendcontest.json")
public class LiveEndContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 6483574096784650768L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		LiveContestDTO respDTO = new LiveContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		String msg = null;
		ContestDVO contest = null;

		try {
			if (clId == null || clId.isEmpty()) {
				msg = "Invalid client id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				msg = "Invalid contest id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = "Invalid loggedin user.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				msg = "Contest not found with given identifier.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			contest.setIsAct(3);
			contest.setUpBy(cau);

			List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
			if (firestores == null || firestores.isEmpty()) {
				setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			for (LiveFirestore firestore : firestores) {
				firestore.endContest();
				firestore.setContestStarted(false);
				firestore.updateHostNode(LivePlayContest.END, null, null, null, null, null, null);
			}

			if (!SAASAPI.endContest(contest)) {
				msg = "Error occured while saas api end contest.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Boolean isEnded = LiveContestService.updateContestStatus(contest);
			if (isEnded) {
				LiveContestService.saveContestAudit(contest);
				for (LiveFirestore firestore : firestores) {
					firestore.updateUpcomingContestDetails();
					firestore.setContestStarted(false);
				}
				MigrationAPIInvokerThread migration = new MigrationAPIInvokerThread(clId, coId);
				Thread thread = new Thread(migration);
				thread.start();
				msg = "Contest ended.";
				respDTO.setSts(1);
				setClientMessage(respDTO, null, msg);
				generateResponse(request, response, respDTO);
				return;
			} else {
				msg = "Error occured while ending contest.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			msg = "Error occured while ending contest.";
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			SaaSApiTrackingService.saveContestTracking(clId, coId, LiveContestConst.CONTEST_END_API, cau, "WEB",
					request.getRemoteAddr(), LivePlayContest.END.toString(), msg, respDTO.getSts(),
					contest != null ? contest.getLastQue() : 0);
		}
		return;
	}
}
