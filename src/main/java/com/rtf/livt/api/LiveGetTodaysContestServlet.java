/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.constants.LiveContestConst;
import com.rtf.livt.dto.LivePlayContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveGetTodaysContestServlet.
 */
@WebServlet("/livegettodayscontest.json")
public class LiveGetTodaysContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 425511071742399258L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		LivePlayContestDTO respDTO = new LivePlayContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getTodaysContest(clId);
			if (contest == null) {
				setClientMessage(respDTO, "There is no contest schedule for today.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<LivtContestQuestionDVO> questions = LivtContestQuestionsService.getcontestquestionsBycontestId(clId,
					contest.getCoId());
			if (questions == null) {
				questions = new ArrayList<LivtContestQuestionDVO>();
			}
			if (contest.getqSize() > 0 && questions.isEmpty()) {
				setClientMessage(respDTO, "Not Allowed to start contest, Questions are not added to contest.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer questionCount = questions.size();
			if (questionCount != contest.getqSize() || !questionCount.equals(contest.getqSize())) {
				setClientMessage(respDTO,
						"Not Allowed to start contest, No. of question declared at contest level and actual questions in contest are mismatching.",
						null);
				generateResponse(request, response, respDTO);
				return;
			}

			for (LivtContestQuestionDVO que : questions) {
				if (contest.getAnsType().equalsIgnoreCase("REGULAR")
						&& (que.getCorans() == null || que.getCorans().isEmpty())) {
					setClientMessage(respDTO,
							"Not Allowed to start contest, Question type REGULAR is configured at contest level and there are one or more FEEDBACK type(without answer) of questions in contest.",
							null);
					generateResponse(request, response, respDTO);
					return;
				} else if (contest.getAnsType().equalsIgnoreCase("FEEDBACK")
						&& (que.getCorans() != null && !que.getCorans().isEmpty())) {
					setClientMessage(respDTO,
							"Not Allowed to start contest, Question type FEEDBACK configured at contest level and there are one or more REGULAR type(with answer) of questions in contest.",
							null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			Integer prodCount = LiveContestService.getContestProductsetCount(contest.getClId(), contest.getCoId());
			Boolean isShowProd = false;
			if (prodCount != null && prodCount > 0) {
				isShowProd = true;
			}
			respDTO.setIsProdConfig(isShowProd);
			if (contest.getIsAct() == 2) {
				if (contest.getLastAction() == null) {
					setClientMessage(respDTO, "Cannot resume contest, contest last state was not captured.", null);
					generateResponse(request, response, respDTO);
					return;
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.START.toString())) {
					respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - 1");
					respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
					if (contest.getqSize() <= 0) {
						respDTO.setBtnText(LiveContestConst.END_CONTEST_BTN);
						respDTO.setNxtApi(LiveContestConst.CONTEST_END_API);
					}
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.QUESTION.toString())) {
					respDTO.setBtnText(LiveContestConst.COUNT_BTN + " - " + contest.getLastQue());
					respDTO.setNxtApi(LiveContestConst.CONTEST_ANSWER_COUNT_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.COUNT.toString())) {
					respDTO.setBtnText(LiveContestConst.ANSWER_BTN + " - " + contest.getLastQue());
					respDTO.setNxtApi(LiveContestConst.CONTEST_ANSWER_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.ANSWER.toString())) {
					if (contest.getLastQue() == contest.getqSize()) {
						respDTO.setBtnText(LiveContestConst.SUMMARY_BTN);
						respDTO.setNxtApi(LiveContestConst.CONTEST_SIMMARY_API);
					} else {
						respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - " + (contest.getLastQue() + 1));
						respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
					}

				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.SUMMARY.toString())) {
					respDTO.setBtnText(LiveContestConst.DSP_SUMMARY_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_SHOW_SUMMARY_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DSPSUMMARY.toString())) {
					if (contest.getIsLotEnbl()) {
						respDTO.setBtnText(LiveContestConst.LOTTERY_BTN);
						respDTO.setNxtApi(LiveContestConst.CONTEST_LOTTERY_API);
					} else {
						respDTO.setBtnText(LiveContestConst.END_CONTEST_BTN);
						respDTO.setNxtApi(LiveContestConst.CONTEST_END_API);
					}
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.LOTTERY.toString())) {
					respDTO.setBtnText(LiveContestConst.DL_WINNER_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_WINNER_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DCLWINNER.toString())) {
					respDTO.setBtnText(LiveContestConst.DSP_WINNER_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_WINNER_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DSPWINNER.toString())) {
					respDTO.setBtnText(LiveContestConst.END_CONTEST_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_END_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DCLMINIJACKPOT.toString())) {
					respDTO.setBtnText(LiveContestConst.DSP_MINIJACKPOT_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_SHOW_MINIJACKPOT_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DSPMINIJACKPOT.toString())) {
					respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - " + (contest.getLastQue() + 1));
					respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DCLMEGAJACKPOT.toString())) {
					respDTO.setBtnText(LiveContestConst.DSP_MEGAJACKPOT_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_MEGAJACKPOT_API);
				} else if (contest.getLastAction().equalsIgnoreCase(LivePlayContest.DSPMEGAJACKPOT.toString())) {
					respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - " + (contest.getLastQue() + 1));
					respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
				}

			} else if (contest.getIsAct() == 1) {
				respDTO.setBtnText(LiveContestConst.START_CONTEST_BTN);
				respDTO.setNxtApi(LiveContestConst.START_CONTEST_API);
			}
			respDTO.setContest(contest);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
}
