/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.constants.LiveContestConst;
import com.rtf.livt.dto.LivePlayContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LiveContestStatsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.SaaSApiTrackingService;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveGetGrandWinnerServlet.
 */
@WebServlet("/livegetcontestwinner.json")
public class LiveGetGrandWinnerServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -5496869754482559373L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		LivePlayContestDTO respDTO = new LivePlayContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		String msg = null;
		ContestDVO contest = null;

		try {
			if (clId == null || clId.isEmpty()) {
				msg = "Invalid client id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				msg = "Invalid contest id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = "Invalid loggedin user.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				msg = "Contest not found with given identifier.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			contest.setUpBy(cau);
			contest.setLastAction(LivePlayContest.DCLWINNER.toString());
			Boolean isUpdated = LiveContestService.updateContestState(contest);

			if (isUpdated) {
				List<ContestGrandWinnerDVO> gwLst = LiveContestStatsService.getContestGrandWinnersList(contest);
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateHostNode(LivePlayContest.DCLWINNER, null, null, null, gwLst.size(), null, gwLst);
				}
				respDTO.setGrandWinners(gwLst);
				respDTO.setBtnText(LiveContestConst.DSP_WINNER_BTN);
				respDTO.setNxtApi(LiveContestConst.CONTEST_SHOW_WINNER_API);
				msg = "Grand winner declared.";
				respDTO.setSts(1);
				generateResponse(request, response, respDTO);
				return;
			} else {
				msg = "Error occured while Declaring Winner.";
				setClientMessage(respDTO, "Error occured while Declaring Winner.", null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			msg = "Error occured while Declaring Winner.";
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			SaaSApiTrackingService.saveContestTracking(clId, coId, LiveContestConst.CONTEST_WINNER_API, cau, "WEB",
					request.getRemoteAddr(), LivePlayContest.DCLWINNER.toString(), msg, respDTO.getSts(),
					contest != null ? contest.getLastQue() : 0);
		}
		return;
	}

}
