/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.RandomGenerator;
import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.livt.util.LiveTriviaUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.service.ShopContestProductSetService;
import com.rtf.shop.util.ShopMessageConstant;

/**
 * The Class LiveUpdateContestToActiveServlet.
 */
@WebServlet("/livemovecontesttoactive.json")
public class LiveUpdateContestToActiveServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 6291298216823118629L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		String stDate = request.getParameter("stDate");
		String stHour = request.getParameter("stHour");
		String stMinute = request.getParameter("stMinutes");
		String expPartCntStr = request.getParameter("expPartCnt");

		LiveContestDTO respDTO = new LiveContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, "Invalid contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, "Invalid Start date.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stHour == null || stHour.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stMinute == null || stMinute.isEmpty()) {
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (expPartCntStr == null || expPartCntStr.isEmpty()) {
				setClientMessage(respDTO, "Expected participant count is mendatory..", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date sDate = null;
			Integer hour = 0;
			Integer minute = 0;
			Integer expPartCnt = 0;

			try {
				hour = Integer.parseInt(stHour);
				minute = Integer.parseInt(stMinute);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Contest start time is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				expPartCnt = Integer.parseInt(expPartCntStr);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Invalid expected participant count.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			String stDate1 = stDate + " " + hour + ":" + minute + ":00";
			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			if (sDate == null) {
				setClientMessage(respDTO, "Invalid Contest start date and time.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, "Invalid contest identifier.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!contest.getIsAct().equals(3)) {
				setClientMessage(respDTO, "Only Expired contest can be moved to active.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			ClientConfigDVO config = ClientConfigService.getContestConnfigByKey(clId, Constant.LIVETRIVIA,
					Constant.LIVE_TRIVIA_CONTEST_RESTRICTED_DURATION_HRS);
			Integer restrictHours = 6;
			try {
				if (config != null) {
					restrictHours = Integer.parseInt(config.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, restrictHours);

			if (sDate.before(cal.getTime())) {
				setClientMessage(respDTO, "Contest start date time should be future date, Minimum " + restrictHours
						+ " after current time.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Boolean flag = LiveTriviaUtil.calculateExpectedParticipantCount(stDate, hour, minute,
					contest.getExpPartCount());
			if (!flag) {
				setClientMessage(respDTO, "This time-slot has been booked completely, Please choose another timeslot.",
						null);
				generateResponse(request, response, respDTO);
				return;
			}

			/*
			 * contest.setUpBy(cau); contest.setClId(clId);
			 * contest.setStDate(sDate.getTime()); contest.setIsAct(1);
			 * contest.setLastAction(null); contest.setLastQue(null);
			 * contest.setExpPartCount(expPartCnt);
			 */

			// NEW
			Date now = new Date();
			ContestDVO co = new ContestDVO();
			co.setLastAction(null);
			co.setLastQue(0);
			co.setExpPartCount(expPartCnt);
			co.setIsPwd(contest.getIsPwd());
			co.setConPwd(contest.getConPwd());
			co.setPartiRwdType(contest.getPartiRwdType());
			co.setPartiRwdVal(contest.getPartiRwdVal());
			co.setQueRwdType(contest.getQueRwdType());
			co.setAnsType(contest.getAnsType());
			co.setCoType(contest.getCoType());
			co.setCat(contest.getCat());
			co.setCrBy(cau);
			co.setClId(clId);
			co.setClImgU(null);
			co.setCoId(clId + now.getTime());
			co.setCrDate(now.getTime());
			co.setImgU(null);
			co.setIsAct(1);
			co.setIsElimination(contest.getIsElimination());
			co.setName(contest.getName());
			co.setExtName(contest.getExtName());
			co.setqSize(contest.getqSize());
			co.setIsSplitSummary(contest.getIsSplitSummary());
			co.setSumRwdType(contest.getSumRwdType());
			co.setSumRwdVal(contest.getSumRwdVal());
			co.setIsLotEnbl(contest.getIsLotEnbl());
			co.setWinnerCount(contest.getWinnerCount());
			co.setWinRwdType(contest.getWinRwdType());
			co.setWinRwdVal(contest.getWinRwdVal());
			co.setSeqNo(1);
			co.setStDate(sDate.getTime());
			co.setSubCat(contest.getSubCat());
			co.setThmId(0);
			co.setRunCount(0);
			co.setHstImgUrl(contest.getHstImgUrl());
			co.setHostedBy(contest.getHostedBy());

			boolean isSaved = LiveContestService.saveContest(co);

			List<LivtContestQuestionDVO> questions = LivtContestQuestionsService.getcontestquestionsBycontestId(contest.getClId(), contest.getCoId());
			for (LivtContestQuestionDVO que : questions) {
				LivtContestQuestionDVO contQuestion = new LivtContestQuestionDVO();
				contQuestion.setClintid(clId);
				contQuestion.setConid(co.getCoId());
				contQuestion.setQsntext(que.getQsntext());
				contQuestion.setOpa(que.getOpa());
				contQuestion.setOpb(que.getOpb());
				contQuestion.setOpc(que.getOpc());
				contQuestion.setOpd(que.getOpd());
				contQuestion.setCorans(que.getCorans());
				contQuestion.setQsnorient(que.getQsnorient());
				contQuestion.setCreby(cau);
				contQuestion.setCredate(new Date());
				contQuestion.setQsnseqno(que.getQsnseqno());
				contQuestion.setMjRwdType(que.getMjRwdType());
				contQuestion.setMjRwdVal(que.getMjRwdVal());
				contQuestion.setMjWinnersCount(que.getMjWinnersCount());
				contQuestion.setAnsRwdVal(que.getAnsRwdVal());
				contQuestion.setqDifLevel(que.getqDifLevel());
				contQuestion.setEncKey(RandomGenerator.getAlphaNumericString(6));
				contQuestion.setCoupcde(que.getCoupcde());
				contQuestion.setCoupondiscType(que.getCoupondiscType());
				Integer id = LivtContestQuestionsService.saveContestQuestions(clId, contQuestion);
			}

			List<ContestProductSetsDVO> products = ShopContestProductSetService.getContestProductSetsByContestId(clId,coId, null);
			for (ContestProductSetsDVO pro : products) {
				ContestProductSetsDVO cPro = new ContestProductSetsDVO();
				cPro.setClintId(pro.getClintId());
				cPro.setConId(co.getCoId());
				cPro.setCreby(cau);
				cPro.setDispSeq(pro.getDispSeq());
				cPro.setDispStatus(ShopMessageConstant.PRODUCT_STATUS_ACTIVE);
				cPro.setPriceType(pro.getPriceType());
				cPro.setMercId(pro.getMercId());
				cPro.setMercProdid(pro.getMercProdid());
				cPro.setPricePerUnit(pro.getPricePerUnit());
				cPro.setProdDesc(pro.getProdDesc());
				cPro.setProdImgUrl(pro.getProdImgUrl());
				cPro.setProdName(pro.getProdName());
				cPro.setRegPrice(pro.getRegPrice());
				cPro.setProdPriceDtl(pro.getProdPriceDtl());
				cPro.setUnitType(pro.getUnitType());
				cPro.setUnitValue(pro.getUnitValue());
				ShopContestProductSetService.saveContestProduct(cPro);
			}
			
			boolean isUpdated = LiveContestService.deleteContest(contest.getClId(), contest.getCoId(), cau);

			if (isSaved) {
				LiveContestService.saveContestAudit(co);
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateUpcomingContestDetails();
				}
				respDTO.setSts(1);
				respDTO.setMsg("Contest updated to active successfully.");
				respDTO.setContest(co);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, "Something went wrong, Please try again.", null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
