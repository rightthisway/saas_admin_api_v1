/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.rtf.livt.cass.dao.CassContestWinnersDAO;
import com.rtf.livt.cass.dao.ClientCustomerCassDAO;
import com.rtf.livt.cass.dao.ContestGrandWinnerCassDAO;
import com.rtf.livt.cass.dao.LiveContestAnswerDAO;
import com.rtf.livt.cass.dao.LiveContestJackpotWinnerCassDAO;
import com.rtf.livt.dvo.ClientCustomerDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.LiveContestAnswerDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.sql.dao.LivtContestGrandWinnerDAO;
import com.rtf.livt.util.LivtContestSessionUtil;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;

/**
 * The Class LiveContestStatsService.
 */
public class LiveContestStatsService {

	/**
	 * Gets the contest winners list.
	 *
	 * @param contest
	 *            the contest
	 * @return the contest winners list
	 */
	public static List<ContestWinnerDVO> getContestWinnersList(ContestDVO contest) {
		List<ContestWinnerDVO> winnerListForUpdate = null;
		try {
			Map<String, ContestWinnerDVO> winnerMap = CassContestWinnersDAO
					.getContestWinnersMapByContestId(contest.getClId(), contest.getCoId());

			if (winnerMap == null || winnerMap.size() == 0)
				return null;

			Integer winnersCount = winnerMap.size();
			Boolean isRewardSplittable = contest.getIsSplitSummary();
			Double sumRewardVal = contest.getSumRwdVal();
			String rewardType = contest.getSumRwdType();
			Double rewardsPerWinner = sumRewardVal;

			if (rewardType == null) {
				rewardsPerWinner = 0.0;
			} else if (isRewardSplittable) {
				rewardsPerWinner = Math.floor((sumRewardVal / winnersCount) * 100) / 100;
			}
			winnerListForUpdate = updatedWinnerDVOListWtihRewards(winnerMap, rewardsPerWinner, rewardType);
			CassContestWinnersDAO.batchUpsertWinnerDetails(winnerListForUpdate, rewardsPerWinner, rewardType);
			winnerListForUpdate = updatedWinnerDVOListWtihUserDets(winnerMap, contest.getClId(), contest.getCoId());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return winnerListForUpdate;
	}

	/**
	 * Gets the contest winners list for display.
	 *
	 * @param contest
	 *            the contest
	 * @return the contest winners list for display
	 */
	public static List<ContestWinnerDVO> getContestWinnersListForDisplay(ContestDVO contest) {
		List<ContestWinnerDVO> winnerListForUpdate = null;

		try {
			winnerListForUpdate = LivtContestSessionUtil.contestWinnerMap.get(contest.getCoId());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return winnerListForUpdate;
	}

	/**
	 * Gets the contest grand winners list.
	 *
	 * @param contest
	 *            the contest
	 * @return the contest grand winners list
	 */
	public static List<ContestGrandWinnerDVO> getContestGrandWinnersList(ContestDVO contest) {
		List<ContestGrandWinnerDVO> winnerListForUpdate = null;
		List<ContestGrandWinnerDVO> finalGrandwinnerList = null;
		try {
			Map<String, ContestWinnerDVO> winnerMap = CassContestWinnersDAO
					.getContestWinnersMapByContestId(contest.getClId(), contest.getCoId());

			if (winnerMap == null || winnerMap.size() == 0)
				return null;
			Double rwdVal = contest.getWinRwdVal();
			String rwdType = contest.getWinRwdType();
			Integer noOfWinnersConfigured = contest.getWinnerCount();
			Integer noOfActualWinnersize = winnerMap.size();

			List<ContestWinnerDVO> granndWinnerList = new ArrayList<ContestWinnerDVO>();
			List<ContestWinnerDVO> winnerList = null;
			if (noOfActualWinnersize <= noOfWinnersConfigured) {
				winnerList = winnerMap.values().stream().collect(Collectors.toList());
				granndWinnerList = winnerList;
			} else {
				List<ContestGrandWinnerDVO> oldGWinners = LivtContestGrandWinnerDAO
						.getAllWinnersByClientId(contest.getClId());
				for (ContestGrandWinnerDVO win : oldGWinners) {
					int wSize = winnerMap.size();
					if (winnerMap.containsKey(win.getCuId()) && wSize > noOfWinnersConfigured) {
						winnerMap.remove(win.getCuId());
					}
				}
				winnerList = winnerMap.values().stream().collect(Collectors.toList());
				granndWinnerList = getRandomWinners(winnerList, noOfWinnersConfigured);
			}

			List<ContestGrandWinnerDVO> lotterWinnerList = populateLotterywinnersVOList(granndWinnerList, rwdType,
					rwdVal);

			ContestGrandWinnerCassDAO.saveAll(lotterWinnerList);

			Map<String, ContestGrandWinnerDVO> lotWinnerMap = ContestGrandWinnerCassDAO
					.getContestGrandWinnersMapByContestId(contest.getClId(), contest.getCoId());

			finalGrandwinnerList = updatelotteryWinnerDVOListWtihUserDets(lotWinnerMap, contest.getClId());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return finalGrandwinnerList;
	}

	/**
	 * Display contest grand winners list.
	 *
	 * @param contest
	 *            the contest
	 * @return the list
	 */
	public static List<ContestGrandWinnerDVO> displayContestGrandWinnersList(ContestDVO contest) {

		List<ContestGrandWinnerDVO> finalGrandwinnerList = null;
		try {

			Map<String, ContestGrandWinnerDVO> lotWinnerMap = ContestGrandWinnerCassDAO
					.getContestGrandWinnersMapByContestId(contest.getClId(), contest.getCoId());

			finalGrandwinnerList = updatelotteryWinnerDVOListWtihUserDets(lotWinnerMap, contest.getClId());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return finalGrandwinnerList;
	}

	/**
	 * Updatelottery winner DVO list wtih user dets.
	 *
	 * @param winnerMap
	 *            the winner map
	 * @param clintId
	 *            the clint id
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	private static List<ContestGrandWinnerDVO> updatelotteryWinnerDVOListWtihUserDets(
			Map<String, ContestGrandWinnerDVO> winnerMap, String clintId) throws Exception {

		List<String> winnerIdList = winnerMap.keySet().stream().collect(Collectors.toList());
		Map<String, ClientCustomerDVO> cliCustMap = ClientCustomerCassDAO.getClientCustomerByCustomerId(clintId,
				winnerIdList);
		ClientConfigDVO clientConfigDVO = ClientConfigService.getContestConnfigByKey(clintId, Constant.LIVETRIVIA,
				Constant.LIVE_TRIVIA_USERID_AUTO_GENERATED);
		boolean isAuto = Boolean.FALSE;
		if(clientConfigDVO != null && clientConfigDVO.getValue()!=null && clientConfigDVO.getValue().equalsIgnoreCase("YES")){
			isAuto = Boolean.TRUE;
		}
		for (String cuId : winnerMap.keySet()) {
			ContestGrandWinnerDVO dvo = winnerMap.get(cuId);
			ClientCustomerDVO dvoMstr = cliCustMap.get(cuId);
			if (dvoMstr != null) {
				dvo.setEmail(dvoMstr.getEmail());
				dvo.setImgU(dvoMstr.getPurl());
				dvo.setuId(isAuto==true?dvoMstr.getUserId():dvoMstr.getCuId());
			}
		}
		return winnerMap.values().stream().collect(Collectors.toList());

	}

	/**
	 * Populate lotterywinners VO list.
	 *
	 * @param lotteryWinnnersList
	 *            the lottery winnners list
	 * @param rwdType
	 *            the rwd type
	 * @param rwdVal
	 *            the rwd val
	 * @return the list
	 */
	private static List<ContestGrandWinnerDVO> populateLotterywinnersVOList(List<ContestWinnerDVO> lotteryWinnnersList,
			String rwdType, Double rwdVal) {
		List<ContestGrandWinnerDVO> lotteryWinnerList = new ArrayList<ContestGrandWinnerDVO>();
		for (ContestWinnerDVO lotObj : lotteryWinnnersList) {
			ContestGrandWinnerDVO vo = new ContestGrandWinnerDVO(lotObj.getClId(), lotObj.getCoId(), lotObj.getCuId(),
					rwdType, rwdVal, null, null, null, null, null);
			lotteryWinnerList.add(vo);
		}
		return lotteryWinnerList;

	}

	/**
	 * Gets the random winners.
	 *
	 * @param winnerList
	 *            the winner list
	 * @param totalWinners
	 *            the total winners
	 * @return the random winners
	 */
	private static List<ContestWinnerDVO> getRandomWinners(List<ContestWinnerDVO> winnerList, int totalWinners) {
		Random rand = new Random();
		List<ContestWinnerDVO> rList = new ArrayList<ContestWinnerDVO>();
		for (int i = 0; i < totalWinners; i++) {
			int randomIndex = rand.nextInt(winnerList.size());
			rList.add(winnerList.get(randomIndex));
			winnerList.remove(randomIndex);
		}
		return rList;
	}

	/**
	 * Updated winner DVO list wtih rewards.
	 *
	 * @param winnerMap
	 *            the winner map
	 * @param rewardsPerWinner
	 *            the rewards per winner
	 * @param rwdType
	 *            the rwd type
	 * @return the list
	 */
	private static List<ContestWinnerDVO> updatedWinnerDVOListWtihRewards(Map<String, ContestWinnerDVO> winnerMap,
			Double rewardsPerWinner, String rwdType) {

		List<ContestWinnerDVO> winnerList = winnerMap.values().stream().collect(Collectors.toList());
		for (ContestWinnerDVO dvo : winnerList) {
			dvo.setRwdType(rwdType);
			dvo.setRwdVal(rewardsPerWinner);
		}
		return winnerList;

	}

	/**
	 * Updated winner DVO list wtih user dets.
	 *
	 * @param winnerMap
	 *            the winner map
	 * @param clintId
	 *            the clint id
	 * @param contestId
	 *            the contest id
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	private static List<ContestWinnerDVO> updatedWinnerDVOListWtihUserDets(Map<String, ContestWinnerDVO> winnerMap,
			String clintId, String contestId) throws Exception {

		Map<String, ContestWinnerDVO> dispMap = new HashMap<String, ContestWinnerDVO>();
		if (winnerMap != null && winnerMap.size() > 100) {
			int count = 0;
			for (Map.Entry<String, ContestWinnerDVO> entry : winnerMap.entrySet()) {
				if (count >= 100)
					break;
				dispMap.put(entry.getKey(), entry.getValue());
				count++;
			}
		} else {
			dispMap = winnerMap;
		}
		List<String> winnerIdList = dispMap.keySet().stream().collect(Collectors.toList());

		Map<String, ClientCustomerDVO> cliCustMap = ClientCustomerCassDAO.getClientCustomerByCustomerId(clintId,
				winnerIdList);
		ClientConfigDVO clientConfigDVO = ClientConfigService.getContestConnfigByKey(clintId, Constant.LIVETRIVIA,
				Constant.LIVE_TRIVIA_USERID_AUTO_GENERATED);
		boolean isAuto = Boolean.FALSE;
		if(clientConfigDVO != null && clientConfigDVO.getValue()!=null && clientConfigDVO.getValue().equalsIgnoreCase("YES")){
			isAuto = Boolean.TRUE;
		}
		for (String cuId : dispMap.keySet()) {
			ContestWinnerDVO dvo = dispMap.get(cuId);
			ClientCustomerDVO dvoMstr = cliCustMap.get(cuId);
			if (dvoMstr != null) {
				dvo.setEmail(dvoMstr.getEmail());
				dvo.setImgU(dvoMstr.getPurl());
				dvo.setuId(isAuto==true?dvoMstr.getUserId():dvoMstr.getCuId());
			}
		}
		List<ContestWinnerDVO> lst = dispMap.values().stream().collect(Collectors.toList());
		LivtContestSessionUtil.contestWinnerMap.remove(contestId);
		LivtContestSessionUtil.contestWinnerMap.put(contestId, lst);
		Integer totWinnerCnt = 0;
		if (winnerMap != null)
			totWinnerCnt = winnerMap.size();
		LivtContestSessionUtil.contestWinnerCountMap.put(contestId, totWinnerCnt);
		return lst;

	}

	/**
	 * Updated jackpot winner DVO list wtih user dets.
	 *
	 * @param winnerMap
	 *            the winner map
	 * @param clintId
	 *            the clint id
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	private static List<ContestJackpotWinnerDVO> updatedJackpotWinnerDVOListWtihUserDets(
			Map<String, ContestJackpotWinnerDVO> winnerMap, String clintId) throws Exception {

		List<String> winnerIdList = winnerMap.keySet().stream().collect(Collectors.toList());
		Map<String, ClientCustomerDVO> cliCustMap = ClientCustomerCassDAO.getClientCustomerByCustomerId(clintId,
				winnerIdList);
		ClientConfigDVO clientConfigDVO = ClientConfigService.getContestConnfigByKey(clintId, Constant.LIVETRIVIA,
				Constant.LIVE_TRIVIA_USERID_AUTO_GENERATED);
		boolean isAuto = Boolean.FALSE;
		if(clientConfigDVO != null && clientConfigDVO.getValue()!=null && clientConfigDVO.getValue().equalsIgnoreCase("YES")){
			isAuto = Boolean.TRUE;
		}
		for (String cuId : winnerMap.keySet()) {
			ContestJackpotWinnerDVO dvo = winnerMap.get(cuId);
			ClientCustomerDVO dvoMstr = cliCustMap.get(cuId);
			if (dvoMstr != null) {
				dvo.setEmail(dvoMstr.getEmail());
				dvo.setImgU(dvoMstr.getPurl());
				dvo.setuId(isAuto==true?dvoMstr.getUserId():dvoMstr.getCuId());
			}
		}
		return winnerMap.values().stream().collect(Collectors.toList());

	}

	/**
	 * Gets the contest jackpot winners list.
	 *
	 * @param contest
	 *            the contest
	 * @param que
	 *            the que
	 * @return the contest jackpot winners list
	 */
	public static List<ContestJackpotWinnerDVO> getContestJackpotWinnersList(ContestDVO contest,
			LivtContestQuestionDVO que) {
		List<ContestJackpotWinnerDVO> finalWinnerList = new ArrayList<ContestJackpotWinnerDVO>();
		;
		try {
			Map<String, ContestJackpotWinnerDVO> winMap = LiveContestJackpotWinnerCassDAO
					.getJackpotWinnerByQNo(contest.getClId(), que.getConid(), que.getConqsnid());
			if (winMap != null && !winMap.isEmpty()) {
				finalWinnerList = updatedJackpotWinnerDVOListWtihUserDets(winMap, contest.getClId());
				return finalWinnerList;
			}
			List<LiveContestAnswerDVO> correctAnsList = new ArrayList<LiveContestAnswerDVO>();

			List<LiveContestAnswerDVO> custansList = LiveContestAnswerDAO.getAllAnswerByQNo(contest.getClId(),
					que.getConid(), que.getQsnseqno());

			for (LiveContestAnswerDVO ans : custansList) {
				if (ans.getIsCrt()) {
					correctAnsList.add(ans);
				}
			}

			if (que.getMjWinnersCount() >= correctAnsList.size()) {
				for (LiveContestAnswerDVO ans : correctAnsList) {
					ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
					win.setClId(contest.getClId());
					win.setCoId(ans.getCoId());
					win.setCuId(ans.getCuId());
					win.setqNo(ans.getqNo());
					win.setjType(contest.getCoType());
					win.setqId(que.getConqsnid());
					win.setRwdType(que.getMjRwdType());
					win.setRwdVal(que.getMjRwdVal());
					win.setStatus("ACTIVE");
					finalWinnerList.add(win);
				}
				LiveContestJackpotWinnerCassDAO.saveAll(finalWinnerList);
			} else {
				Map<String, ContestJackpotWinnerDVO> extWinMap = LiveContestJackpotWinnerCassDAO
						.getJackpotWinnerByContest(contest.getClId(), que.getConid());
				List<LiveContestAnswerDVO> finalCorrectAnsList = new ArrayList<LiveContestAnswerDVO>();
				for (LiveContestAnswerDVO ans : correctAnsList) {
					if (extWinMap.get(ans.getCuId()) == null) {
						finalCorrectAnsList.add(ans);
					}
				}
				if (que.getMjWinnersCount() >= finalCorrectAnsList.size()) {
					for (LiveContestAnswerDVO ans : finalCorrectAnsList) {
						ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
						win.setClId(contest.getClId());
						win.setCoId(ans.getCoId());
						win.setCuId(ans.getCuId());
						win.setqNo(ans.getqNo());
						win.setjType(contest.getCoType());
						win.setqId(que.getConqsnid());
						win.setRwdType(que.getMjRwdType());
						win.setRwdVal(que.getMjRwdVal());
						win.setStatus("ACTIVE");
						finalWinnerList.add(win);
					}

					correctAnsList.removeAll(finalCorrectAnsList);
					int remaining = que.getMjWinnersCount() - finalWinnerList.size();
					for (LiveContestAnswerDVO ans : correctAnsList) {
						if (remaining > 0) {
							remaining--;
							ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
							win.setClId(contest.getClId());
							win.setCoId(ans.getCoId());
							win.setCuId(ans.getCuId());
							win.setqNo(ans.getqNo());
							win.setjType(contest.getCoType());
							win.setqId(que.getConqsnid());
							win.setRwdType(que.getMjRwdType());
							win.setRwdVal(que.getMjRwdVal());
							win.setStatus("ACTIVE");
							finalWinnerList.add(win);
						}
					}
				} else {
					Collections.shuffle(finalCorrectAnsList);
					Random rand = new Random();
					int randomIndex = rand.nextInt(finalCorrectAnsList.size() - que.getMjWinnersCount());
					finalCorrectAnsList = finalCorrectAnsList.subList(randomIndex,
							randomIndex + que.getMjWinnersCount());

					for (LiveContestAnswerDVO ans : finalCorrectAnsList) {
						ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
						win.setClId(contest.getClId());
						win.setCoId(ans.getCoId());
						win.setCuId(ans.getCuId());
						win.setqNo(ans.getqNo());
						win.setjType(contest.getCoType());
						win.setqId(que.getConqsnid());
						win.setRwdType(que.getMjRwdType());
						win.setRwdVal(que.getMjRwdVal());
						win.setStatus("ACTIVE");
						finalWinnerList.add(win);
					}
				}
				LiveContestJackpotWinnerCassDAO.saveAll(finalWinnerList);
			}
			Map<String, ContestJackpotWinnerDVO> resultMap = LiveContestJackpotWinnerCassDAO
					.getJackpotWinnerByQNo(contest.getClId(), que.getConid(), que.getConqsnid());
			if (resultMap != null && !resultMap.isEmpty()) {
				finalWinnerList = updatedJackpotWinnerDVOListWtihUserDets(resultMap, contest.getClId());
				return finalWinnerList;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return finalWinnerList;
	}

	/**
	 * Gets the contest mega jackpot winners list.
	 *
	 * @param contest
	 *            the contest
	 * @return the contest mega jackpot winners list
	 */
	public static List<ContestJackpotWinnerDVO> getContestMegaJackpotWinnersList(ContestDVO contest) {
		List<ContestJackpotWinnerDVO> finalWinnerList = null;
		try {
			Map<String, ContestJackpotWinnerDVO> winMap = LiveContestJackpotWinnerCassDAO
					.getJackpotWinnerByContest(contest.getClId(), contest.getCoId());
			if (winMap != null && !winMap.isEmpty()) {
				finalWinnerList = updatedJackpotWinnerDVOListWtihUserDets(winMap, contest.getClId());
				return finalWinnerList;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
