/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.List;

import com.rtf.livt.cass.dao.LiveContestCassDAO;
import com.rtf.livt.dvo.ContestAuditDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LiveContestHost;
import com.rtf.livt.sql.dao.LiveContestSQLDAO;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class LiveContestService.
 */
public class LiveContestService {

	/**
	 * Gets the SQL contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the SQL contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getSQLContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = LiveContestSQLDAO.getContestByContestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the contest by status.
	 *
	 * @param clientId
	 *            the client id
	 * @param status
	 *            the status
	 * @return the contest by status
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByStatus(String clientId, Integer status) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = LiveContestSQLDAO.getContestByStatus(clientId, status);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the all active contest by date and time.
	 *
	 * @param sDate
	 *            the s date
	 * @param eDate
	 *            the e date
	 * @return the all active contest by date and time
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllActiveContestByDateAndTime(java.sql.Timestamp sDate, java.sql.Timestamp eDate)
			throws Exception {
		List<ContestDVO> allContests = null;
		try {
			allContests = LiveContestSQLDAO.getAllActiveContestByDateAndTime(sDate, eDate);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return allContests;
	}
	
	
	
	/**
	 * Gets the all live contest hosts list.
	 *
	 * @param clId
	 *            the client ID
	 * @return List<LiveContestHost> 
	 * @throws Exception
	 *             the exception
	 */
	public static List<LiveContestHost> getAllLiveContestHosts(String clId)throws Exception {
		List<LiveContestHost> allHosts = null;
		try {
			allHosts = LiveContestSQLDAO.getContestHosts(clId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return allHosts;
	}
	
	
	
	/**
	 * Gets contest host by ID.
	 *
	 * @param clId
	 *            the client ID
	 * @param hostId
	 *            the host ID
	 * @return LiveContestHost
	 * @throws Exception
	 *             the exception
	 */
	public static LiveContestHost getLiveContestHostById(String clId,Integer hostId)throws Exception {
		LiveContestHost host = null;
		try {
			host = LiveContestSQLDAO.getLiveContestHostById(clId,hostId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return host;
	}
	

	/**
	 * Gets the todays contest.
	 *
	 * @param clientId
	 *            the client id
	 * @return the todays contest
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getTodaysContest(String clientId) throws Exception {
		ContestDVO sqlContest = null;
		try {
			sqlContest = LiveContestSQLDAO.getTodaysContest(clientId);
			if (sqlContest == null) {
				return null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sqlContest;
	}

	/**
	 * Gets the all contest by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestByStatusandFilter(String clId, String status, String filter,
			String pgNo) throws Exception {
		List<ContestDVO> list = null;
		try {
			list = LiveContestSQLDAO.getAllContestByStatusandFilter(clId, status, filter, pgNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the all contest data to export.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestDataToExport(String clId, String status, String filter)
			throws Exception {
		List<ContestDVO> list = null;
		try {
			list = LiveContestSQLDAO.getAllContestDataToExport(clId, status, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the all contest count by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest count by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllContestCountByStatusandFilter(String clId, String status, String filter)
			throws Exception {
		Integer count = 0;
		try {
			count = LiveContestSQLDAO.getAllContestCountByStatusandFilter(clId, status, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Save contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContest(ContestDVO contest) throws Exception {
		boolean isInserted = false;
		try {
			isInserted = LiveContestSQLDAO.saveContestSQL(contest);
			if(isInserted){
				LiveContestCassDAO.saveDisplayContest(contest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isInserted;
	}
	
	
	
	/**
	 * Reset data in display contest table.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean resetDisplayContests(ContestDVO contest) throws Exception {
		boolean isDel = false;
		try {
			isDel = LiveContestCassDAO.deleteAllDisplayContest(contest.getClId());
			if(isDel){
				List<ContestDVO> contests = LiveContestSQLDAO.getAllContestByStatusandFilter(contest.getClId(), "ACTIVE", null, "1");
				for(ContestDVO co : contests){
					LiveContestCassDAO.saveDisplayContest(co);
				}
			}
		} catch (Exception e) {
			isDel = false;
			e.printStackTrace();
		}
		return isDel;
	}
	

	/**
	 * Update contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContest(ContestDVO contest) throws Exception {
		boolean isUpdated = false;
		try {
			isUpdated = LiveContestSQLDAO.updateContestSQL(contest);
			if(isUpdated){
				LiveContestCassDAO.updateDisplayContest(contest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}
	
	
	
	/**
	 * Update contest script file.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestScriptFile(ContestDVO contest) throws Exception {
		boolean isUpdated = false;
		try {
			isUpdated = LiveContestSQLDAO.updateContestScriptFile(contest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}
	

	/**
	 * Update contest status.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 */
	public static boolean updateContestStatus(ContestDVO contest) {
		boolean isUpdated = false;
		try {
			isUpdated = LiveContestSQLDAO.updateContestStatus(contest);
			if (isUpdated && (contest.getIsAct() == 2 || contest.getIsAct().equals(2))) {
				isUpdated = LiveContestCassDAO.saveContestCass(contest);
			} else if (isUpdated && (contest.getIsAct() == 3 || contest.getIsAct().equals(3))) {
				isUpdated = LiveContestCassDAO.deleteContestCass(contest.getClId(), contest.getCoId());
				LiveContestCassDAO.deleteDisplayContest(contest.getClId(), contest.getCoId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}

	/**
	 * Update contest state.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestState(ContestDVO contest) throws Exception {
		boolean isReset = false;
		try {
			isReset = LiveContestSQLDAO.updateContestState(contest);
			if (isReset) {
				isReset = LiveContestCassDAO.updateContestState(contest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReset;
	}

	/**
	 * Reset contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean resetContest(ContestDVO contest) throws Exception {
		boolean isReset = false;
		try {
			isReset = LiveContestSQLDAO.updateContestState(contest);
			if (isReset) {
				isReset = LiveContestCassDAO.deleteContestCass(contest.getClId(), contest.getCoId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReset;
	}

	/**
	 * Start contest.
	 *
	 * @param contest
	 *            the contest
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean startContest(ContestDVO contest) throws Exception {
		boolean isInsert = false;
		try {
			isInsert = LiveContestSQLDAO.updateContestRunningCount(contest);
			isInsert = LiveContestCassDAO.saveContestCass(contest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isInsert;
	}

	/**
	 * Delete contest.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @param upBy
	 *            the up by
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContest(String clId, String coId, String upBy) throws Exception {
		boolean isDeleted = false;
		try {
			isDeleted = LiveContestSQLDAO.deleteContestSQL(clId, coId, upBy);
			if (isDeleted) {
				isDeleted = LiveContestCassDAO.deleteContestCass(clId, coId);
				LiveContestCassDAO.deleteDisplayContest(clId, coId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isDeleted;
	}

	/**
	 * Save contest audit.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestAudit(ContestDVO co) throws Exception {
		boolean isSaved = false;
		try {
			ContestAuditDVO coAu = new ContestAuditDVO(co);
			isSaved = LiveContestSQLDAO.saveContestAudit(coAu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSaved;
	}

	/**
	 * Gets the contest productset count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest productset count
	 */
	public static Integer getContestProductsetCount(String clientId, String contestId) {
		Integer count = 0;
		try {
			count = LiveContestSQLDAO.getContestProductsetCount(clientId, contestId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	
	
	/**
	 * Fetch Live trivia question bank listfor contest id.
	 *
	 * @param respDTO
	 *            the resp DTO
	 * @param filter
	 *            the filter
	 * @param catType
	 *            the cat type
	 * @param subCatType
	 *            the sub cat type
	 * @param pgNo
	 *            the pg no
	 * @return the Live trivia question DTO
	 */
	public static OTTQuestionDTO fetchLivtQuestionBankListforContestId(OTTQuestionDTO respDTO, String filter,
			String catType, String subCatType, String pgNo) {
		List<QuestionBankDVO> qbList = null;
		try {

			Integer count = 0;
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = QuestionBankSQLDAO.getAllLivtQuestionsFromQBankByContestIdWithFilter(respDTO.getClId(),
					respDTO.getCoId(), catType, subCatType, filterQuery, pgNo,filter);
			if (qbList == null || qbList.size() == 0) {
				respDTO.setSts(0);
				respDTO.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = QuestionBankSQLDAO.getAllLivtQuestionsCountFromQBankByContestIdWithFilter(respDTO.getClId(),
						respDTO.getCoId(), catType, subCatType, filterQuery);
			}
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setQbList(qbList);
			respDTO.setSts(1);
		} catch (Exception ex) {
			respDTO.setSts(0);
			ex.printStackTrace();
		}
		return respDTO;
	}
}
