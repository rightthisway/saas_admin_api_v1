/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.List;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.ClusterNodeConfigDVO;
import com.rtf.livt.dvo.LiveContestUserIdConfDVO;
import com.rtf.livt.sql.dao.ClusterNodeConfigSQLDAO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClusterNodeConfigService.
 */
public class ClusterNodeConfigService {

	/**
	 * Gets the all active cluster node config.
	 *
	 * @param clientId
	 *            the client id
	 * @return the all active cluster node config
	 */
	public static List<ClusterNodeConfigDVO> getAllActiveClusterNodeConfig(String clientId) {
		List<ClusterNodeConfigDVO> configs = null;
		try {
			configs = ClusterNodeConfigSQLDAO.getAllActiveClusterNodeConfig(clientId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return configs;
	}
	
	
	/**
	 * Gets the live contest user Id config.
	 *
	 * @param clientId the client id
	 * @param nodeId the node id
	 * @return the all active cluster node config
	 */
	public static LiveContestUserIdConfDVO getLiveContestUserIdConf(String clientId,String nodeId) {
		LiveContestUserIdConfDVO config = null;
		try {
			config = ClusterNodeConfigSQLDAO.getLiveContestUserIdConf(clientId,nodeId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}
	
	
	/**
	 * Save Live contest user ID config.
	 *
	 * @param LiveContestUserIdConfDVO
	 *            dvo
	 * @return the boolean
	 */
	public static Boolean saveLiveContestUserConf(LiveContestUserIdConfDVO dvo) {
		Boolean isSaved = false;
		try {
			isSaved = ClusterNodeConfigSQLDAO.saveLiveContestUserConf(dvo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSaved;
	}
	
	
	
	/**
	 * Update Live contest user ID config.
	 *
	 * @param LiveContestUserIdConfDVO
	 *            dvo
	 * @return the boolean
	 */
	public static Boolean updateLiveContestUserConf(LiveContestUserIdConfDVO dvo) {
		Boolean isUpd = false;
		try {
			isUpd = ClusterNodeConfigSQLDAO.updateLiveContestUserConf(dvo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpd;
	}
}
