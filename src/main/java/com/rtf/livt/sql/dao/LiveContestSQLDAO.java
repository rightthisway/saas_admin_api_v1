/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.livt.dvo.ContestAuditDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestListDVO;
import com.rtf.livt.dvo.LiveContestHost;
import com.rtf.livt.util.LiveContestGridHeaderFilterUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class LiveContestSQLDAO.
 */
public class LiveContestSQLDAO {

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"SELECT convert(varchar(2), datepart(hour,consrtdate)) as stHour, convert(varchar(2), datepart(minute,consrtdate)) as stMinutes,* ")
				.append("from  pa_livvx_contest_mstr  WHERE clintid = ? AND conid = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setStHour(rs.getString("stHour"));
				contestDVO.setStMinutes(rs.getString("stMinutes"));
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setRunCount(rs.getInt("conrunningno"));
				contestDVO.setIsPwd(rs.getBoolean("ispwd"));
				contestDVO.setConPwd(rs.getString("conpwd"));
				contestDVO.setScrptFile(rs.getString("conscriptfile"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
				contestDVO.setHostedBy(rs.getString("hostname"));
				contestDVO.setHstImgUrl(rs.getString("hostimg"));

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the contest by status.
	 *
	 * @param clientId
	 *            the client id
	 * @param status
	 *            the status
	 * @return the contest by status
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByStatus(String clientId, Integer status) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? AND isconactive = ? ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setInt(2, status);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setIsPwd(rs.getBoolean("ispwd"));
				contestDVO.setConPwd(rs.getString("conpwd"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the todays contest.
	 *
	 * @param clientId
	 *            the client id
	 * @return the todays contest
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getTodaysContest(String clientId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date today = new Date();
		String dateStr = DateFormatUtil.formatDateToMonthDateYear(today);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? AND isconactive in (1,2) ")
				.append(" AND DATEPART(day, consrtdate) = " + DateFormatUtil.extractDateElement(dateStr, "DAY"))
				.append(" AND DATEPART(month,consrtdate) = " + DateFormatUtil.extractDateElement(dateStr, "MONTH"))
				.append(" AND DATEPART(year,consrtdate) = " + DateFormatUtil.extractDateElement(dateStr, "YEAR"))
				.append(" order by consrtdate");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setIsPwd(rs.getBoolean("ispwd"));
				contestDVO.setConPwd(rs.getString("conpwd"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
				return contestDVO;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the all contest by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestByStatusandFilter(String clId, String status, String filter,
			String pgNo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT c.clintid AS clintid, c.conid AS conid ")
				.append(", c.cattype AS cattype, c.subcattype AS subcattype ")
				.append(", c.conname AS conname, c.consrtdate AS consrtdate ")
				.append(", c.contype AS contype, c.anstype AS anstype ")
				.append(", c.iselimtype AS iselimtype, c.noofqns AS noofqns ")
				.append(", c.issumrysplitable AS issumrysplitable, c.sumryrwdtype AS sumryrwdtype ")
				.append(", c.sumryrwdval AS sumryrwdval, c.islotryenabled AS islotryenabled ")
				.append(", c.lotryrwdtype AS lotryrwdtype, c.lotryrwdval AS lotryrwdval ")
				.append(", c.isconactive AS isconactive, c.brndimgurl AS brndimgurl ")
				.append(", c.cardimgurl AS cardimgurl, c.cardseqno AS cardseqno ")
				.append(", c.bgthemcolor AS bgthemcolor, c.bgthemimgurl AS bgthemimgurl ")
				.append(", c.bgthemimgurlmob AS bgthemimgurlmob, c.bgthembankid AS bgthembankid ")
				.append(", c.playimgurl AS playimgurl, c.creby AS creby,c.ispwd as ispwd, c.conpwd as conpwd ")
				.append(", c.credate AS credate, c.updby AS updby, pc.pcdisplay as expPartCntStr ")
				.append(", c.upddate AS upddate, c.qsnrwdtype AS qsnrwdtype, c.particnt as particnt ")
				.append(", c.participantrwdtype AS participantrwdtype, c.participantrwdval AS participantrwdval ")
				.append(", c.migrstatus AS migrstatus, c.lastqsn AS lastqsn, c.extconname as extconname,c.hostname as hostedBy,c.hostimg  as hostImg ")
				.append(", c.lastaction AS lastaction, s.isconactivetext AS status, c.ngrndwnrs as ngrndwnrs, c.conscriptfile as scrptFile ")
				.append("FROM pa_livvx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ")
				.append("LEFT JOIN sd_participants_count pc on c.particnt=pc.pcvalue " + "WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = LiveContestGridHeaderFilterUtil.getContestFilterQuery(filter);
			String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			
			String sortingQuery = GridSortingUtil.getContestSortingQuery(filter);
			if(sortingQuery!=null && !sortingQuery.trim().isEmpty()){
				sql.append(sortingQuery);
			}else{
				if(status.equalsIgnoreCase("EXPIRED")) {
					sql.append(" ORDER BY c.consrtdate desc");
				}else{
					sql.append(" ORDER BY c.consrtdate");
				}
			}
			
			sql.append(paginationQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setStatus(rs.getString("status"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setScrptFile(rs.getString("scrptFile"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setIsPwd(rs.getBoolean("ispwd"));
				contestDVO.setConPwd(rs.getString("conpwd"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
				contestDVO.setExpPartCntStr(rs.getString("expPartCntStr"));
				contestDVO.setHostedBy(rs.getString("hostedBy"));
				contestDVO.setHstImgUrl(rs.getString("hostImg"));
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest data to export.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllContestDataToExport(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT c.clintid AS clintid, c.conid AS conid ")
				.append(", c.cattype AS cattype, c.subcattype AS subcattype ")
				.append(", c.conname AS conname, c.consrtdate AS consrtdate ")
				.append(", c.contype AS contype, c.anstype AS anstype ")
				.append(", c.iselimtype AS iselimtype, c.noofqns AS noofqns ")
				.append(", c.issumrysplitable AS issumrysplitable, c.sumryrwdtype AS sumryrwdtype ")
				.append(", c.sumryrwdval AS sumryrwdval, c.islotryenabled AS islotryenabled ")
				.append(", c.lotryrwdtype AS lotryrwdtype, c.lotryrwdval AS lotryrwdval ")
				.append(", c.isconactive AS isconactive, c.brndimgurl AS brndimgurl ")
				.append(", c.cardimgurl AS cardimgurl, c.cardseqno AS cardseqno ")
				.append(", c.bgthemcolor AS bgthemcolor, c.bgthemimgurl AS bgthemimgurl ")
				.append(", c.bgthemimgurlmob AS bgthemimgurlmob, c.bgthembankid AS bgthembankid ")
				.append(", c.playimgurl AS playimgurl, c.creby AS creby,c.ispwd as ispwd, c.conpwd as conpwd ")
				.append(", c.credate AS credate, c.updby AS updby, pc.pcdisplay as expPartCntStr ")
				.append(", c.upddate AS upddate, c.qsnrwdtype AS qsnrwdtype, c.particnt as particnt ")
				.append(", c.participantrwdtype AS participantrwdtype, c.participantrwdval AS participantrwdval ")
				.append(", c.migrstatus AS migrstatus, c.lastqsn AS lastqsn, c.extconname as extconname  ")
				.append(", c.lastaction AS lastaction, s.isconactivetext AS status, c.ngrndwnrs as ngrndwnrs ")
				.append("FROM pa_livvx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ")
				.append("LEFT JOIN sd_participants_count pc on c.particnt=pc.pcvalue " + "WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = LiveContestGridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY c.consrtdate");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setStatus(rs.getString("status"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setIsPwd(rs.getBoolean("ispwd"));
				contestDVO.setConPwd(rs.getString("conpwd"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
				contestDVO.setExpPartCntStr(rs.getString("expPartCntStr"));
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest count by statusand filter.
	 *
	 * @param clId
	 *            the cl id
	 * @param status
	 *            the status
	 * @param filter
	 *            the filter
	 * @return the all contest count by statusand filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllContestCountByStatusandFilter(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"SELECT count(*) from  pa_livvx_contest_mstr  c join sd_contest_isactive s on c.isconactive=s.isconactive WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = LiveContestGridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Save contest SQL.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO pa_livvx_contest_mstr (clintid,conid,cattype,subcattype,conname,consrtdate,contype,anstype")
		.append(",iselimtype,noofqns,issumrysplitable,sumryrwdtype,sumryrwdval,islotryenabled")
		.append(",lotryrwdtype,lotryrwdval,isconactive,brndimgurl,cardimgurl,cardseqno")
		.append(",bgthemcolor,bgthemimgurl,bgthemimgurlmob,bgthembankid,playimgurl,creby,credate,qsnrwdtype,participantrwdtype")
		.append(",participantrwdval,migrstatus,lastqsn,lastaction,extconname,ngrndwnrs,ispwd,conpwd,upddate,updby,particnt,hostname,hostimg) ")
		.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?)");
		Boolean isInserted = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getClId());
			ps.setString(2, co.getCoId());
			ps.setString(3, co.getCat());
			ps.setString(4, co.getSubCat());
			ps.setString(5, co.getName());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getStDate()));
			ps.setString(7, co.getCoType());
			ps.setString(8, co.getAnsType());
			ps.setBoolean(9, co.getIsElimination());
			ps.setInt(10, co.getqSize());
			ps.setBoolean(11, co.getIsSplitSummary());
			ps.setString(12, co.getSumRwdType());
			ps.setDouble(13, co.getSumRwdVal());
			ps.setBoolean(14, co.getIsLotEnbl());
			ps.setString(15, co.getWinRwdType());
			ps.setDouble(16, co.getWinRwdVal());
			ps.setInt(17, co.getIsAct());
			ps.setString(18, co.getClImgU());
			ps.setString(19, co.getImgU());
			ps.setInt(20, co.getSeqNo() != null ? co.getSeqNo() : 0);
			ps.setString(21, co.getThmColor());
			ps.setString(22, co.getThmImgDesk());
			ps.setString(23, co.getThmImgMob());
			ps.setInt(24, co.getThmId() != null ? co.getThmId() : 0);
			ps.setString(25, co.getPlayGameImg());
			ps.setString(26, co.getCrBy());
			ps.setString(27, co.getQueRwdType());
			ps.setString(28, co.getPartiRwdType());
			ps.setDouble(29, co.getPartiRwdVal());
			ps.setString(30, co.getMigStatus());
			ps.setInt(31, co.getLastQue());
			ps.setString(32, co.getLastAction());
			ps.setString(33, co.getExtName());
			ps.setInt(34, co.getWinnerCount() != null ? co.getWinnerCount() : 0);
			ps.setBoolean(35, co.getIsPwd());
			ps.setString(36, co.getConPwd());
			ps.setString(37, co.getCrBy());
			ps.setInt(38, co.getExpPartCount());
			ps.setString(39, co.getHostedBy());
			ps.setString(40, co.getHstImgUrl());

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
			return isInserted;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Update contest SQL.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_livvx_contest_mstr SET ")
		.append("cattype=?,subcattype=? ,conname=? ,consrtdate=?")
		.append(",contype=? ,anstype=? ,iselimtype=?")
		.append(",noofqns=? ,issumrysplitable=? ,sumryrwdtype=?")
		.append(",sumryrwdval=? ,islotryenabled=? ,lotryrwdtype=?")
		.append(",lotryrwdval=?  ,brndimgurl=?")
		.append(",cardimgurl=? ,cardseqno=? ,bgthemcolor=?")
		.append(",bgthemimgurl=? ,bgthemimgurlmob=? ,bgthembankid=?")
		.append(",playimgurl=? ,updby=? ,upddate=getDate() ,qsnrwdtype=?,participantrwdtype=?")
		.append(" ,participantrwdval=?, extconname=?, isconactive=?, ngrndwnrs=?,ispwd=?,conpwd=?, particnt=?,hostname=?,hostimg=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getCat());
			ps.setString(2, co.getSubCat());
			ps.setString(3, co.getName());
			ps.setTimestamp(4, new java.sql.Timestamp(co.getStDate()));
			ps.setString(5, co.getCoType());
			ps.setString(6, co.getAnsType());
			ps.setBoolean(7, co.getIsElimination());
			ps.setInt(8, co.getqSize());
			ps.setBoolean(9, co.getIsSplitSummary());
			ps.setString(10, co.getSumRwdType());
			ps.setDouble(11, co.getSumRwdVal());
			ps.setBoolean(12, co.getIsLotEnbl());
			ps.setString(13, co.getWinRwdType());
			ps.setDouble(14, co.getWinRwdVal());
			ps.setString(15, co.getClImgU());
			ps.setString(16, co.getImgU());
			ps.setInt(17, co.getSeqNo() != null ? co.getSeqNo() : 0);
			ps.setString(18, co.getThmColor());
			ps.setString(19, co.getThmImgDesk());
			ps.setString(20, co.getThmImgMob());
			ps.setInt(21, co.getThmId() != null ? co.getThmId() : 0);
			ps.setString(22, co.getPlayGameImg());
			ps.setString(23, co.getUpBy());
			ps.setString(24, co.getQueRwdType());
			ps.setString(25, co.getPartiRwdType());
			ps.setDouble(26, co.getPartiRwdVal());
			ps.setString(27, co.getExtName());
			ps.setInt(28, co.getIsAct());
			ps.setInt(29, co.getWinnerCount() != null ? co.getWinnerCount() : 0);
			ps.setBoolean(30, co.getIsPwd());
			ps.setString(31, co.getConPwd());
			ps.setInt(32, co.getExpPartCount());
			ps.setString(33, co.getHostedBy());
			ps.setString(34, co.getHstImgUrl());
			
			
			ps.setString(35, co.getClId());
			ps.setString(36, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
	
	
	/**
	 * Update contest script file.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestScriptFile(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_livvx_contest_mstr SET ")
		.append("updby=? ,upddate=getDate(), conscriptfile=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getUpBy());
			ps.setString(2, co.getScrptFile());

			ps.setString(3, co.getClId());
			ps.setString(4, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}
	
	
	
	

	/**
	 * Update contest status.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestStatus(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;

		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ")
		.append("isconactive=?,upddate=getDate(),updby=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, co.getIsAct());
			ps.setString(2, co.getUpBy());
			ps.setString(3, co.getClId());
			ps.setString(4, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update contest state.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestState(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;

		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ")
		.append("isconactive=?,upddate=getDate(),updby=?, lastqsn=?, lastaction=? ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, co.getIsAct());
			ps.setString(2, co.getUpBy());
			ps.setInt(3, co.getLastQue());
			ps.setString(4, co.getLastAction());
			ps.setString(5, co.getClId());
			ps.setString(6, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update contest running count.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestRunningCount(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;

		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ")
		.append("conrunningno=?, upddate=getDate(),updby=?  ")
		.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, co.getRunCount());
			ps.setString(2, co.getUpBy());
			ps.setString(3, co.getClId());
			ps.setString(4, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Delete contest SQL.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @param upBy
	 *            the up by
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContestSQL(String clientId, String coId, String upBy) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "update  pa_livvx_contest_mstr set isconactive=?,upddate=getDate(),updby=? WHERE clintid=? AND conid=?";
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, 4);
			ps.setString(2, upBy);
			ps.setString(3, clientId);
			ps.setString(4, coId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Save contest audit.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestAudit(ContestAuditDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO pa_livvx_contest_mstr_trns (clintid,conid,cattype,subcattype,conname,consrtdate,contype,anstype")
		.append(",iselimtype,noofqns,issumrysplitable,sumryrwdtype,sumryrwdval,islotryenabled")
		.append(",lotryrwdtype,lotryrwdval,isconactive,brndimgurl,cardimgurl,cardseqno")
		.append(",bgthemcolor,bgthemimgurl,bgthemimgurlmob,bgthembankid,playimgurl,creby")
		.append(",credate,qsnrwdtype,participantrwdtype,participantrwdval,extconname,ngrndwnrs,ispwd,conpwd,particnt) ")
		.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?)");
		Boolean isInserted = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getClId());
			ps.setString(2, co.getCoId());
			ps.setString(3, co.getCat());
			ps.setString(4, co.getSubCat());
			ps.setString(5, co.getName());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getStDate()));
			ps.setString(7, co.getCoType());
			ps.setString(8, co.getAnsType());
			ps.setBoolean(9, co.getIsElimination());
			ps.setInt(10, co.getqSize());
			ps.setBoolean(11, co.getIsSplitSummary());
			ps.setString(12, co.getSumRwdType());
			ps.setDouble(13, co.getSumRwdVal());
			ps.setBoolean(14, co.getIsLotEnbl());
			ps.setString(15, co.getWinRwdType());
			ps.setDouble(16, co.getWinRwdVal());
			ps.setInt(17, co.getIsAct());
			ps.setString(18, co.getClImgU());
			ps.setString(19, co.getImgU());
			ps.setInt(20, co.getSeqNo());
			ps.setString(21, co.getThmColor());
			ps.setString(22, co.getThmImgDesk());
			ps.setString(23, co.getThmImgMob());
			ps.setInt(24, co.getThmId());
			ps.setString(25, co.getPlayGameImg());
			ps.setString(26, co.getCrBy());
			ps.setString(27, co.getQueRwdType());
			ps.setString(28, co.getPartiRwdType());
			ps.setDouble(29, co.getPartiRwdVal());
			ps.setString(30, co.getExtName());
			ps.setInt(31, co.getWinnerCount());
			ps.setBoolean(32, co.getIsPwd());
			ps.setString(33, co.getConPwd());
			ps.setInt(34, co.getExpPartCnt());

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
			return isInserted;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Gets the contest productset count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest productset count
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getContestProductsetCount(String clientId, String contestId) throws Exception {
		Integer count = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT count(1) as pCount from pa_shopm_contest_product_sets WHERE clintid = ? AND conid = ? ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				count = rs.getInt("pCount");

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the all live contest count.
	 *
	 * @return the all live contest count
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllLiveContestCount() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		String sql = "SELECT count(*) from  pa_livvx_contest_mstr  c join sd_contest_isactive s on c.isconactive=s.isconactive WHERE AND c.isconactive = 2  ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the all active contest by date and time.
	 *
	 * @param sDate
	 *            the s date
	 * @param eDate
	 *            the e date
	 * @return the all active contest by date and time
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestDVO> getAllActiveContestByDateAndTime(java.sql.Timestamp sDate, java.sql.Timestamp eDate)
			throws Exception {
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from  pa_livvx_contest_mstr  WHERE consrtdate >= ? AND consrtdate <= ?  AND isconactive in(1,2)";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setTimestamp(1, sDate);
			ps.setTimestamp(2, eDate);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setExtName(rs.getString("extconname"));
				BigDecimal partiCnt = rs.getBigDecimal("particnt");
				contestDVO.setExpPartCount(partiCnt != null ? partiCnt.intValue() : 0);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the expired contest list data.
	 *
	 * @param clId
	 *            the cl id
	 * @return the expired contest list data
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestListDVO> getExpiredContestListData(String clId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestListDVO> list = new ArrayList<ContestListDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT top 50 c.clintid AS clintid, c.conid AS conid ")
		.append(", c.conname AS conname, c.consrtdate AS consrtdate " + "FROM pa_livvx_contest_mstr c ")
		.append("WHERE c.clintid = ? AND c.isconactive = 3");
		try {

			sql.append(" ORDER BY c.consrtdate desc");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestListDVO contestDVO = new ContestListDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	
	
	
	
	/**
	 * Gets the contest host list by client id.
	 *
	 * @param clientId
	 *            the client id
	 * @return the list of contest hosts.
	 * @throws Exception
	 *             the exception
	 */
	public static List<LiveContestHost> getContestHosts(String clientId) throws Exception {
		List<LiveContestHost> allHost = new ArrayList<LiveContestHost>();
		LiveContestHost host = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from sd_host_list WHERE isactive=1 AND clintid=? order by hfname";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			while (rs.next()) {
				host = new LiveContestHost();
				host.setClId(rs.getString("clintid"));
				host.setHostId(rs.getString("hostid"));
				host.setHostImageUrl(rs.getString("himg"));
				host.setfName(rs.getString("hfname"));
				host.setlName(rs.getString("hlname"));
				allHost.add(host);

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return allHost;
	}
	
	
	
	/**
	 * Gets the contest host by host ID.
	 *
	 * @param clientId
	 *            the client id
	 * @param hostId
	 *            the host id
	 * @return the contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static LiveContestHost getLiveContestHostById(String clientId,Integer hostId) throws Exception {
		LiveContestHost host = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from sd_host_list WHERE isactive=1 AND clintid=? AND hostid="+hostId;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			while (rs.next()) {
				host = new LiveContestHost();
				host.setClId(rs.getString("clintid"));
				host.setHostId(rs.getString("hostid"));
				host.setHostImageUrl(rs.getString("himg"));
				host.setfName(rs.getString("hfname"));
				host.setlName(rs.getString("hlname"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return host;
	}
public static void main(String[] args) throws Exception {
	
	getTodaysContest("AMIT202002CID");
}
}
