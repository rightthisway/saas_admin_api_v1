/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.livt.dvo.LiveExpectedParticipantsDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class LiveExpectedParticipantDAO.
 */
public class LiveExpectedParticipantDAO {

	/**
	 * Gets the all expcted participant records.
	 *
	 * @return the all expcted participant records
	 * @throws Exception
	 *             the exception
	 */
	public static List<LiveExpectedParticipantsDVO> getAllExpctedParticipantRecords() throws Exception {
		List<LiveExpectedParticipantsDVO> list = new ArrayList<LiveExpectedParticipantsDVO>();
		LiveExpectedParticipantsDVO dvo = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from  sd_participants_count  WHERE isactive=? ORDER BY pcvalue";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, Boolean.TRUE);
			rs = ps.executeQuery();

			while (rs.next()) {
				dvo = new LiveExpectedParticipantsDVO();
				dvo.setId(rs.getInt("pcid"));
				dvo.setExpCnt(rs.getInt("pcvalue"));
				dvo.setExpCntStr(rs.getString("pcdisplay"));
				dvo.setIsAct(rs.getBoolean("isactive"));
				list.add(dvo);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the expcted participant by id.
	 *
	 * @param id
	 *            the id
	 * @return the expcted participant by id
	 * @throws Exception
	 *             the exception
	 */
	public static LiveExpectedParticipantsDVO getExpctedParticipantById(Integer id) throws Exception {
		LiveExpectedParticipantsDVO dvo = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from  sd_participants_count  WHERE isactive=? AND pcid=?";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, Boolean.TRUE);
			ps.setInt(2, id);
			rs = ps.executeQuery();

			while (rs.next()) {
				dvo = new LiveExpectedParticipantsDVO();
				dvo.setId(rs.getInt("pcid"));
				dvo.setExpCnt(rs.getInt("pcvalue"));
				dvo.setExpCntStr(rs.getString("pcdisplay"));
				dvo.setIsAct(rs.getBoolean("isactive"));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dvo;
	}
}
