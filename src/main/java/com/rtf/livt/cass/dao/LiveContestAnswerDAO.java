/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.rtf.livt.dvo.LiveContestAnswerDVO;
import com.rtf.saas.cass.db.CassandraConnector;

/**
 * The Class LiveContestAnswerDAO.
 */
public class LiveContestAnswerDAO {

	/**
	 * Gets the all answer by Q no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionNo
	 *            the question no
	 * @return the all answer by Q no
	 */
	public static List<LiveContestAnswerDVO> getAllAnswerByQNo(String clientId, String contestId, Integer questionNo) {
		List<LiveContestAnswerDVO> custContAnsList = new ArrayList<LiveContestAnswerDVO>();
		try {
			final ResultSet results = CassandraConnector.getSession().execute(
					"SELECT clintid,custid,conid,custans,iscrtans,islife,conqsnid from pt_livvx_customer_answer WHERE clintid=? and conid=? and qsnseqno=?",
					clientId, contestId, questionNo);

			if (results != null) {
				for (Row row : results) {
					custContAnsList.add(new LiveContestAnswerDVO(row.getString("clintid"), row.getString("custid"),
							contestId, row.getInt("conqsnid"), questionNo, row.getString("custans"),
							row.getBool("iscrtans"), row.getBool("islife")));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return custContAnsList;
	}

	/**
	 * Gets the all answer option cnt by Q no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionNo
	 *            the question no
	 * @return the all answer option cnt by Q no
	 */
	public static Map<String, Integer> getAllAnswerOptionCntByQNo(String clientId, String contestId,
			Integer questionNo) {
		Map<String, Integer> ansCntMap = new HashMap<String, Integer>();
		try {

			final ResultSet results = CassandraConnector.getSession().execute(
					"SELECT custans,cnt from pt_livvx_customer_answer_cnt WHERE clintid=? and conid=? and qsnseqno=?",
					clientId, contestId, questionNo);

			if (results != null) {
				for (Row row : results) {
					Long value = row.getLong("cnt");
					if (value == null) {
						value = 0l;
					}
					ansCntMap.put(row.getString("custans"), value.intValue());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ansCntMap;
	}

	/**
	 * Gets the live contest total cust count.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return the live contest total cust count
	 */
	public static Integer getLiveContestTotalCustCount(String clId, String coId) {
		Integer count = 0;
		try {
			final ResultSet results = CassandraConnector.getSession().execute(
					"SELECT cnt as cCount from pt_livvx_contest_participants_cnt WHERE clintid=? and conid=? ", clId,
					coId);
			Row row = results.one();
			if (row != null) {
				Long value = row.getLong("cCount");
				count = value.intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

}
