/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.ClientCustomerDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;

/**
 * The Class ClientCustomerCassDAO.
 */
public class ClientCustomerCassDAO {

	/**
	 * Gets the client customer by customer id.
	 *
	 * @param clintId
	 *            the clint id
	 * @param custIdList
	 *            the cust id list
	 * @return the client customer by customer id
	 * @throws Exception
	 *             the exception
	 */
	public static Map<String, ClientCustomerDVO> getClientCustomerByCustomerId(String clintId, List<String> custIdList)
			throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		Map<String, ClientCustomerDVO> custdvomap = new HashMap<String, ClientCustomerDVO>();
		try {
			Session session = CassandraConnector.getSession();
			PreparedStatement prepared = session
					.prepare("SELECT * from  pu_client_customer where clintid='" + clintId + "'  and custid IN ?;");
			resultSet = session.execute(prepared.bind(custIdList));
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					String cuId = row.getString("custid");
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(cuId);
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUserId(row.getString("userid"));
					custdvomap.put(cuId, dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return custdvomap;
	}

}