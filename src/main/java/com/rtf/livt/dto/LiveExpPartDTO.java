/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.LiveExpectedParticipantsDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LiveExpPartDTO.
 */
public class LiveExpPartDTO extends RtfSaasBaseDTO {

	/** The part list. */
	private List<LiveExpectedParticipantsDVO> partList;

	/**
	 * Gets the part list.
	 *
	 * @return the part list
	 */
	public List<LiveExpectedParticipantsDVO> getPartList() {
		return partList;
	}

	/**
	 * Sets the part list.
	 *
	 * @param partList
	 *            the new part list
	 */
	public void setPartList(List<LiveExpectedParticipantsDVO> partList) {
		this.partList = partList;
	}

}
