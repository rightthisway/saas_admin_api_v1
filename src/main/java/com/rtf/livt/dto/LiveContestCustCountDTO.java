/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LiveContestCustCountDTO.
 */
public class LiveContestCustCountDTO extends RtfSaasBaseDTO {

	/** The count. */
	private Integer count;

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * Sets the count.
	 *
	 * @param count the new count
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}
