/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LiveContestHost;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class LiveContestDTO.
 */
public class LiveContestDTO extends RtfSaasBaseDTO {

	/** The contests. */
	private List<ContestDVO> contests;

	/** The contest. */
	private ContestDVO contest;

	/** The contest host list. */
	private List<LiveContestHost> hosts;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the contests.
	 *
	 * @return the contests
	 */
	public List<ContestDVO> getContests() {
		return contests;
	}

	/**
	 * Sets the contests.
	 *
	 * @param contests
	 *            the new contests
	 */
	public void setContests(List<ContestDVO> contests) {
		this.contests = contests;
	}

	/**
	 * Gets the contest.
	 *
	 * @return the contest
	 */
	public ContestDVO getContest() {
		return contest;
	}

	/**
	 * Sets the contest.
	 *
	 * @param contest
	 *            the new contest
	 */
	public void setContest(ContestDVO contest) {
		this.contest = contest;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	/**
	 * Gets host list.
	 *
	 * @return hosts
	 */
	public List<LiveContestHost> getHosts() {
		return hosts;
	}

	/**
	 * Sets host list.
	 *
	 * @param hosts
	 *            the host list
	 */
	public void setHosts(List<LiveContestHost> hosts) {
		this.hosts = hosts;
	}

}
