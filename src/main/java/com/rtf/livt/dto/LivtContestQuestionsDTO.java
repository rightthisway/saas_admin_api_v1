/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class LivtContestQuestionsDTO.
 */
public class LivtContestQuestionsDTO extends RtfSaasBaseDTO {

	/** The cont questions. */
	private LivtContestQuestionDVO contQuestions;

	/** The list. */
	private List<LivtContestQuestionDVO> list;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the cont questions.
	 *
	 * @return the cont questions
	 */
	public LivtContestQuestionDVO getContQuestions() {
		return contQuestions;
	}

	/**
	 * Sets the cont questions.
	 *
	 * @param contQuestions
	 *            the new cont questions
	 */
	public void setContQuestions(LivtContestQuestionDVO contQuestions) {
		this.contQuestions = contQuestions;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<LivtContestQuestionDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list
	 *            the new list
	 */
	public void setList(List<LivtContestQuestionDVO> list) {
		this.list = list;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
