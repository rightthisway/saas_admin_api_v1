/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

/**
 * The Class GsonCustomDateSerializer.
 */
public class GsonCustomDateSerializer implements JsonDeserializer<Date> {

	/** The date format. */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

	/** The date format 1. */
	private SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");

	/** The date format 2. */
	private SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Deserialize date with specified format.
	 *
	 * @param arg0 the arg 0
	 * @param arg1 the arg 1
	 * @param arg2 the arg 2
	 * @return the date
	 */
	@Override
	public Date deserialize(JsonElement arg0, java.lang.reflect.Type arg1, JsonDeserializationContext arg2) {
		String date = arg0.getAsString();
		try {
			return dateFormat.parse(date);
		} catch (Exception e) {
			try {
				return dateFormat1.parse(date);
			} catch (Exception e1) {
				try {
					return dateFormat2.parse(date);
				} catch (Exception e2) {
					try {
						return new Date(Long.parseLong(date));
					} catch (Exception e3) {
					}

				}
			}
		}
		return null;
	}

}
