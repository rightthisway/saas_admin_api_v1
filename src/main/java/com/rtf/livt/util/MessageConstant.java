package com.rtf.livt.util;

public class MessageConstant {
	
	
	public static final String INVALID_CONTEST_ID = "Invalid Contest Id";
	public static final String INVALID_EMAIL = "Enter Valid Email.";
	public static final String INVALID_CLIENT = "Invalid Client Id";
	public static final String INVALID_USER = "User Id does Not Exist";
	public static final String INVALID_LOGGEDIN_USER="Invalid loggedin user.";
	public static final String INVALID_PAGENO= "Invalid Page Number";
	public static final String INVALID_START_DATE = "Invalid start date.";
	public static final String INVALID_CATEGORY =  "Invalid category.";
	public static final String INVALID_QUENATURE= "Invalid nature of question.";
	public static final String INVALID_ELITYPE=  "Invalid elimination type.";
	public static final String INVALID_NO_OF_QUES="Invalid No. of question.";
	public static final String INVALID_RUN_LOTTO_OPTION= "Invalid run lottery option." ;
	public static final String INVALID_LOT_WIN_PRZVAL= "Invalid lottery winners prize value" ;
	public static final String INVALID_NO_OF_LOTWINER ="Invalid no. of lottery winners." ;
	public static final String INVALID_PARTICP_PRZ= "Invalid participation prize value." ;
	public static final String INVALID_CONTEST_HOST= "Invalid contest host ID." ;
	public static final String INVALID_CONTEST_DTTM = "Invalid Contest start date and time.";
	public static final String INVALID_CON_WIN_SPLITVALUE = "Invalid contest winners prize split option value";
	public static final String INVALID_CON_WIN_PRZVAL="Invalid contest winners prize value.";
	public static final String INVALID_EXPECT_PART_COUNT = "Invalid expected participants count.";
	public static final String INVALID_QUESTION_COUNT = "Invalid no of question value.";
	public static final String INVALID_ISPASSWORD = "Invalid value for Is password protected game.";
	
	public static final String MANDATORY_START_TIME =  "Contest start time is mandatory.";
	public static final String MANDATORY_CONTEST_TYPE = "Contest type is mandatory.";
	public static final String MANDATORY_EXP_PRTCTP_CNT = "Expected participants count is mandatory";
	public static final String MANDATORY_WINNER_SPLIT="Contest winners prize split option is mandatory.";
	public static final String MANDATORY_COND1 = "Contest winners prize value is mandatory, when Contest winners prize type is selected.";;
	public static final String MANDATORY_COND2 =  "Contest winners prize type is mandatory, when Contest winners prize value is provided.";
	public static final String MANDATORY_COND3 =  "No. of lottery winners is mandatory when lottery option is yes." ;
	public static final String MANDATORY_RUN_LOTTO="Run lottery option is mandatory." ; 
	public static final String MANDATORY_PASSWORD_PROTECTGAME="password is mendatory for pasword protected game.";
	public static final String CONTEST_MSG1 = "Contest start date time should be future date, Minimum ";
	public static final String CONTEST_MSG2= " hours after current time.";
	public static final String CONTEST_SCRIPT_MSG1 = "Contest script file can be uploaded only before ";
	public static final String CONTEST_SCRIPT_MSG2= " of contest start date and time.";
	public static final String CONTEST__DTTM_SLOT_MSG = "This time-slot has been booked completely, Please choose another timeslot.";

	//ERR 
	public static final String CONTEST_NOT_FOUNDFOR_IDENTIFIER = "Contest not found with given identifier.";
	public static final String ERR_NO_FIRESTORE = "Firestore configuration settings not found in db.";
	public static final String GEN_ERR = "Something went wrong, Please try again.";
	public static final String  CON_REM_ERR = "Something went wrong while deleting contest";
	public static final String  GRANDWINNER_ERR = "Error occured while Displaying Grand Winner.";
	
	public static final String PROCESS_CONTEST_CRE_SUCC = "Contest created successfully.";
	public static final String PROCESS_CONTEST_REM_SUCC = "Contest Deleted successfully.";
	public static final String INVALID_CONTEST_SCRIPT_FILE = "Contest script file is mendatory";
	public static final String CONTEST_SCRIPT_FILE_UPLOADED = "Contest script file uploaded successfully.";
	public static final String CONTEST_SCRIPT_FILE_DOES_NOT_EXIST = "Contest script file is not uploaded.";
	public static final String INVALID_QESTION_BANK_ID = "Invalid question bank id.";
	public static final String QUESTION_BANK_ID_ALREADY_EXISTS = "Question Bank question already exist for this contest";
	public static final String QUESTION_TYPE_MISMATCH_WITH_CONT_QUEST_TYPE = "Question Type is mismatch with contest question type.";
	
	public static final String ERROR_WHILE_ADDING_QUESTION = "Error occured while adding question from question bank.";
	public static final String QUESTION_ADDED_TO_CONTEST = "Selected question is added to contest.";
	public static final String CONTEST_QUESTION_SIZE_REACHED = "Cannot add new question, Number of question mentioned at contest level are already added to contest.";



	
	

}
