/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.enums;

/**
 * The Enum LivePlayContest.
 */
public enum LivePlayContest {

	/** The start. */
	START,
	/** The question. */
	QUESTION,
	/** The count. */
	COUNT,
	/** The answer. */
	ANSWER,
	/** The dclminijackpot. */
	DCLMINIJACKPOT,
	/** The dspminijackpot. */
	DSPMINIJACKPOT,
	/** The dclmegajackpot. */
	DCLMEGAJACKPOT,
	/** The dspmegajackpot. */
	DSPMEGAJACKPOT,
	/** The summary. */
	SUMMARY,
	/** The dspsummary. */
	DSPSUMMARY,
	/** The lottery. */
	LOTTERY,
	/** The dclwinner. */
	DCLWINNER,
	/** The dspwinner. */
	DSPWINNER,
	/** The end. */
	END,
	/** The dspproducts. */
	DSPPRODUCTS;
}
