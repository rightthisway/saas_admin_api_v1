package com.rtf.shop.util;

public class ShopMessageConstant {
	
	
	public static final String EMAIL_IS_MANDATORY = "Email  is mandatory";
	
	public static final String PHONE_IS_MANDATORY = "Phone  is mandatory";
	
	public static final String INVALID_CONTEST_ID = "Invalid Contest id.";
	
	public static final String INVALID_PRODUCT_ID = "Invalid product id.";
	
	public static final String INVALID_MERCHANT_ID = "Invalid marchant id.";
	
	public static final String INVALID_MERCHANT_OR_PRODUCT_ID = "Invalid marchant or Product id.";
	
	public static final String PRODUCT_ID_NOT_EXIST = "Product id does not exist";
	
	public static final String PRODUCT_ADD_TO_CONTEST_SUCCESS_MSG = "Selected products added to contest.";
	
	public static final String PRODUCT_STATUS_ACTIVE = "ACTIVE";
	
	public static final String PRODUCT_NAME_MANDATORY = "Product name is mandatory";
	
	public static final String PRODUCT_UNIT_TYPE_MANDATORY = "Product Unit Type is mandatory";
	
	public static final String PRODUCT_IDENTIFER_ON_SELLER_SITE_MANDATORY = "Product Identifier on Seller Site is mandatory";
	
	public static final String PRODUCT_UNIT_MANDATORY = "Product Unit value is mandatory";
	
	public static final String INVALID_PRODUCT_UNIT_FORMAT = "Product Unit Value Format is Invalid";
	
	public static final String PRODUCT_PRICE_PER_UNIT_MANDATORY = "Product Price Per Unit is mandatory";
	
	public static final String INVALID_PRODUCT_PRICE_FORMAT = "Product Price Format is Invalid";
	
	public static final String PRODUCT_ADD_SUCEESS_MSG = "Product has been Added Successfully.";
	
	public static final String MARKET_PLACE_IS_MANDATORY = "Market Place  is mandatory";
	
	public static final String PRODUCT_ID_IS_MANDATORY = "Prod Id  is mandatory";
	
	public static final String CONTEST_ID_IS_MANDATORY = "Contest Id  is mandatory";
	
	public static final String CUST_ID_IS_MANDATORY = "Cust Id is mandatory";
	
	public static final String CONTEST_PROD_DELETE_SUCCESS_MSG = "Selected products removed from contest.";
	
	public static final String PRODUCT_DELETED_SUCCESS_MSG = "Product has been deleted Successfully.";
	
	public static final String PRODUCT_UPDATE_SUCCESS_MSG = "Product has been Updated Successfully.";
	
	public static final String MERCHANT_DELETED_SUCCESS_MSG = "Merchant has been deleted Successfully.";
	
	public static final String PRODUCT_DISPLAYED_SUCCESS_MSG = "Selected product disaplyed.";
	
	public static final String FIRESTORE_CONFIG_NOT_EXISTS = "Firestore configuration settings not found in db.";
	
	public static final String STATUS_IS_MANDATORY = "Status is mandatory.";
	
	public static final String NO_PRODUCT_FOUNF_FOR_CONTEST = "No products found for selected contest.";
	
	public static final String PRODUCTS_HIDED = "Product cleared.";
	
	
	

}
