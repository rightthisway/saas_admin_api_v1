/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.util;

import com.rtf.common.util.DateFormatUtil;

/**
 * The Class ShopGridHeaderFilterUtil.
 */
public class ShopGridHeaderFilterUtil {

	/**
	 * Gets the merchant filter query.
	 *
	 * @param filter the filter
	 * @return the merchant filter query
	 */
	public static String getMerchantFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("mercselrsite")) {
							sql.append(" AND mercselrsite like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mercfname")) {
							sql.append(" AND mercfname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("merclname")) {
							sql.append(" AND merclname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mercemail")) {
							sql.append(" AND mercemail like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mercphone")) {
							sql.append(" AND mercphone like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mrktplce")) {
							sql.append(" AND mrktplce like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mercurl")) {
							sql.append(" AND mercurl like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("creby")) {
							sql.append(" AND creby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("updby")) {
							sql.append(" AND updby like '%" + value + "%' ");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the contest product sets filter query.
	 *
	 * @param filter the filter
	 * @return the contest product sets filter query
	 */
	public static String getContestProductSetsFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("clintId")) {
							sql.append(" AND clintid like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("mercId")) {
							sql.append(" AND mercid=" + value);
						} else if (name.equalsIgnoreCase("conId")) {
							sql.append(" AND conid =" + value + " ");
						} else if (name.equalsIgnoreCase("mercProdid")) {
							sql.append(" AND mercprodid=" + value);
						} else if (name.equalsIgnoreCase("dispSeq")) {
							sql.append(" AND dispseq=" + value);
						} else if (name.equalsIgnoreCase("prodName")) {
							sql.append(" AND prodname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("prodDesc")) {
							sql.append(" AND proddesc like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("unitValue")) {
							sql.append(" AND unitvalue=" + value);
						} else if (name.equalsIgnoreCase("pricePerUnit")) {
							sql.append(" AND priceperunit=" + value);
						} else if (name.equalsIgnoreCase("unitType")) {
							sql.append(" AND unittype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("dispStatus")) {
							sql.append(" AND dispstatus like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("dispTime")) {
							getDatePartQuery(sql, "disptime", value);
						} else if (name.equalsIgnoreCase("creby")) {
							sql.append(" AND creby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("credate")) {
							getDatePartQuery(sql, "credate", value);
						} else if (name.equalsIgnoreCase("updby")) {
							sql.append(" AND updby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("upddate")) {
							getDatePartQuery(sql, "upddate", value);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the merchant product filter query.
	 *
	 * @param filter the filter
	 * @return the merchant product filter query
	 */
	public static String getMerchantProductFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("prodidselrsite")) {
							sql.append(" AND prodidselrsite like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("prodcat")) {
							sql.append(" AND prodcat like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("prodsubcat")) {
							sql.append(" AND prodsubcat like '%" + value + "%'");
						} else if (name.equalsIgnoreCase("prodname")) {
							sql.append(" AND prodname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("proddesc")) {
							sql.append(" AND proddesc like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("unitvalue")) {
							sql.append(" AND unitvalue = " + value);
						} else if (name.equalsIgnoreCase("priceperunit")) {
							sql.append(" AND priceperunit  = " + value);
						} else if (name.equalsIgnoreCase("unittype")) {
							sql.append(" AND unittype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("creby")) {
							sql.append(" AND creby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("updby")) {
							sql.append(" AND updby like '%" + value + "%' ");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the date part query.
	 *
	 * @param sql        the sql
	 * @param columnName the column name
	 * @param value      the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql, String columnName, String value) {
		try {
			if (DateFormatUtil.extractDateElement(value, "DAY") > 0) {
				sql.append(" AND DATEPART(day, " + columnName + ") = " + DateFormatUtil.extractDateElement(value, "DAY")
						+ " ");
			}
			if (DateFormatUtil.extractDateElement(value, "MONTH") > 0) {
				sql.append(" AND DATEPART(month, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "MONTH") + " ");
			}
			if (DateFormatUtil.extractDateElement(value, "YEAR") > 0) {
				sql.append(" AND DATEPART(year, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "YEAR") + " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validate param values.
	 *
	 * @param name  the name
	 * @param value the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name, String value) {
		if (name == null || name.isEmpty() || name.equalsIgnoreCase("undefined") || value == null || value.isEmpty()
				|| value.equalsIgnoreCase("undefined")) {
			return false;
		}
		return true;
	}
}
