/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.StatusEnums;
import com.rtf.shop.dvo.MerchantDVO;

/**
 * The Class MerchantDAO.
 */
public class MerchantDAO {

	/**
	 * Gets the all merchantswith filter.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @param pgNo        the pg no
	 * @return the all merchantswith filter
	 * @throws Exception the exception
	 */
	public static List<MerchantDVO> getAllMerchantswithFilter(String clientId, String filterQuery, String pgNo)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<MerchantDVO> voList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pr_shopm_client_merchant  with(nolock) where clintid = ? and mercstatus = ? ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by mercid ");
		sql.append(paginationQuery);

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			voList = new ArrayList<MerchantDVO>();
			while (rs.next()) {
				MerchantDVO vo = new MerchantDVO();
				Timestamp timestamp = null;
				vo.setClintid(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercSelrSite(rs.getString("mercselrsite"));
				vo.setMercFname(rs.getString("mercfname"));
				vo.setMercLname(rs.getString("merclname"));
				vo.setMercEmail(rs.getString("mercemail"));
				vo.setMercPhone(rs.getString("mercphone"));
				vo.setMrktPlce(rs.getString("mrktplce"));
				vo.setMercUrl(rs.getString("mercurl"));
				vo.setMercStatus(rs.getString("mercstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));

				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));

				voList.add(vo);
			}
			return voList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all merchants.
	 *
	 * @param clientId the client id
	 * @return the all merchants
	 * @throws Exception the exception
	 */
	public static List<MerchantDVO> getAllMerchants(String clientId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<MerchantDVO> voList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pr_shopm_client_merchant  with(nolock) where clintid = ? and mercstatus = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			voList = new ArrayList<MerchantDVO>();
			while (rs.next()) {
				MerchantDVO vo = new MerchantDVO();
				Timestamp timestamp = null;
				vo.setClintid(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercSelrSite(rs.getString("mercselrsite"));
				vo.setMercFname(rs.getString("mercfname"));
				vo.setMercLname(rs.getString("merclname"));
				vo.setMercEmail(rs.getString("mercemail"));
				vo.setMercPhone(rs.getString("mercphone"));
				vo.setMrktPlce(rs.getString("mrktplce"));
				vo.setMercUrl(rs.getString("mercurl"));
				vo.setMercStatus(rs.getString("mercstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));

				voList.add(vo);
			}
			return voList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all merchant data to export.
	 *
	 * @param clientId the client id
	 * @param filter   the filter
	 * @return the all merchant data to export
	 * @throws Exception the exception
	 */
	public static List<MerchantDVO> getAllMerchantDataToExport(String clientId, String filter) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<MerchantDVO> voList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pr_shopm_client_merchant  with(nolock) where clintid = ? and mercstatus = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			voList = new ArrayList<MerchantDVO>();
			while (rs.next()) {
				MerchantDVO vo = new MerchantDVO();
				Timestamp timestamp = null;
				vo.setClintid(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercSelrSite(rs.getString("mercselrsite"));
				vo.setMercFname(rs.getString("mercfname"));
				vo.setMercLname(rs.getString("merclname"));
				vo.setMercEmail(rs.getString("mercemail"));
				vo.setMercPhone(rs.getString("mercphone"));
				vo.setMrktPlce(rs.getString("mrktplce"));
				vo.setMercUrl(rs.getString("mercurl"));
				vo.setMercStatus(rs.getString("mercstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));

				voList.add(vo);
			}
			return voList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all merchants countwith filter.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @return the all merchants countwith filter
	 * @throws Exception the exception
	 */
	public static Integer getAllMerchantsCountwithFilter(String clientId, String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer(" SELECT count(*) from pr_shopm_client_merchant  with(nolock) where clintid = ? and mercstatus = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.ACTIVE.name());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Creates the merchant.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createMerchant(MerchantDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer("  insert into pr_shopm_client_merchant ( ");
		sql.append("	clintid , mercselrsite ,mrktplce ,mercurl ,mercstatus  ,mercemail ,mercphone ");
		sql.append(" ,mercfname ,merclname  ,creby,credate,updby,upddate) values ");
		sql.append("  (? , ? , ? , ? , ? , " + "  ? , ? ," + "  ? , ? , " + "  ? , getDate(),?,getDate()) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintid());
			ps.setString(2, dvo.getMercSelrSite());
			ps.setString(3, dvo.getMrktPlce());
			ps.setString(4, dvo.getMercUrl());
			ps.setString(5, StatusEnums.ACTIVE.name());
			ps.setString(6, dvo.getMercEmail());
			ps.setString(7, dvo.getMercPhone());
			ps.setString(8, dvo.getMercFname());
			ps.setString(9, dvo.getMercLname());
			ps.setString(10, dvo.getCreby());
			ps.setString(11, dvo.getCreby());
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update merchant.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMerchant(MerchantDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update   pr_shopm_client_merchant set " + " mercselrsite =?, mrktplce = ? , mercurl=?  ");
		sql.append(" ,mercemail=? , mercphone=? " + " ,mercfname = ? ,merclname=? ,updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getMercSelrSite());
			ps.setString(2, dvo.getMrktPlce());
			ps.setString(3, dvo.getMercUrl());
			ps.setString(4, dvo.getMercEmail());
			ps.setString(5, dvo.getMercPhone());
			ps.setString(6, dvo.getMercFname());
			ps.setString(7, dvo.getMercLname());
			ps.setString(8, dvo.getUpdby());
			ps.setString(9, dvo.getClintid());
			ps.setInt(10, dvo.getMercId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete merchant.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteMerchant(MerchantDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pr_shopm_client_merchant set " + " mercstatus =?, updby = ? , ");
				sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.DELETED.name());
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintid());
			ps.setInt(4, dvo.getMercId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

}
