/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.StatusEnums;
import com.rtf.shop.dvo.ProductsDVO;

/**
 * The Class MerchantProductsDAO.
 */
public class MerchantProductsDAO {

	/**
	 * Gets the all merchant productswith filter.
	 *
	 * @param clientId    the client id
	 * @param mercId      the merc id
	 * @param filterQuery the filter query
	 * @param pgNo        the pg no
	 * @param status      the status
	 * @return the all merchant productswith filter
	 * @throws Exception the exception
	 */
	public static List<ProductsDVO> getAllMerchantProductswithFilter(String clientId, Integer mercId,
			String filterQuery, String pgNo, String status) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<ProductsDVO> voList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pa_shopm_merchant_products  with(nolock) where clintid = ? ");
		sql.append("    and mercid = ? and prodstatus = ? ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by mercprodid ");
		sql.append(paginationQuery);

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, mercId);
			ps.setString(3, status);
			rs = ps.executeQuery();
			voList = new ArrayList<ProductsDVO>();
			while (rs.next()) {
				ProductsDVO vo = new ProductsDVO();
				Timestamp timestamp = null;
				vo.setClintId(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercProdid(rs.getInt("mercProdid"));
				vo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				vo.setProdCat(rs.getString("prodcat"));
				vo.setProdSubcat(rs.getString("prodsubcat"));
				vo.setProdName(rs.getString("prodname"));
				vo.setProdDesc(rs.getString("proddesc"));
				vo.setUnitValue(rs.getDouble("unitvalue"));
				vo.setPricePerUnit(rs.getDouble("priceperunit"));
				vo.setUnitType(rs.getString("unittype"));
				vo.setProdImgUrl(rs.getString("prodimageurl"));
				vo.setStatus(rs.getString("prodstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				voList.add(vo);
			}
			return voList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the merchant product by id.
	 *
	 * @param clientId the client id
	 * @param mercId   the merc id
	 * @param prodId   the prod id
	 * @return the merchant product by id
	 * @throws Exception the exception
	 */
	public static ProductsDVO getMerchantProductById(String clientId, Integer mercId, Integer prodId) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ProductsDVO vo = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pa_shopm_merchant_products  with(nolock) where clintid = ? ");
		sql.append("    and mercid = ? and mercprodid=?  ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, mercId);
			ps.setInt(3, prodId);
			rs = ps.executeQuery();
			while (rs.next()) {
				vo = new ProductsDVO();
				Timestamp timestamp = null;
				vo.setClintId(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercProdid(rs.getInt("mercProdid"));
				vo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				vo.setProdCat(rs.getString("prodcat"));
				vo.setProdSubcat(rs.getString("prodsubcat"));
				vo.setProdName(rs.getString("prodname"));
				vo.setProdDesc(rs.getString("proddesc"));
				vo.setUnitValue(rs.getDouble("unitvalue"));
				vo.setPricePerUnit(rs.getDouble("priceperunit"));
				vo.setUnitType(rs.getString("unittype"));
				vo.setProdImgUrl(rs.getString("prodimageurl"));
				vo.setStatus(rs.getString("prodstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return vo;
	}

	/**
	 * Gets the all merchant products.
	 *
	 * @param clientId the client id
	 * @param mercId   the merc id
	 * @param status   the status
	 * @param filter   the filter
	 * @return the all merchant products
	 * @throws Exception the exception
	 */
	public static List<ProductsDVO> getAllMerchantProducts(String clientId, Integer mercId, String status,
			String filter) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<ProductsDVO> voList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pa_shopm_merchant_products  with(nolock) where clintid = ? ");
		sql.append("    and mercid = ? and prodstatus = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, mercId);
			ps.setString(3, status);
			rs = ps.executeQuery();
			voList = new ArrayList<ProductsDVO>();
			while (rs.next()) {
				ProductsDVO vo = new ProductsDVO();
				Timestamp timestamp = null;
				vo.setClintId(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercProdid(rs.getInt("mercProdid"));
				vo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				vo.setProdCat(rs.getString("prodcat"));
				vo.setProdSubcat(rs.getString("prodsubcat"));
				vo.setProdName(rs.getString("prodname"));
				vo.setProdDesc(rs.getString("proddesc"));
				vo.setUnitValue(rs.getDouble("unitvalue"));
				vo.setPricePerUnit(rs.getDouble("priceperunit"));
				vo.setUnitType(rs.getString("unittype"));
				vo.setProdImgUrl(rs.getString("prodimageurl"));
				vo.setStatus(rs.getString("prodstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				voList.add(vo);
			}
			return voList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all merchant product countwith filter.
	 *
	 * @param clientId    the client id
	 * @param mercId      the merc id
	 * @param filterQuery the filter query
	 * @param status      the status
	 * @return the all merchant product countwith filter
	 * @throws Exception the exception
	 */
	public static Integer getAllMerchantProductCountwithFilter(String clientId, Integer mercId, String filterQuery,
			String status) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer(" SELECT count(*) from pa_shopm_merchant_products  with(nolock) where clintid = ? and mercid = ? and prodstatus = ?  ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, mercId);
			ps.setString(3, status);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Creates the merchant product.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createMerchantProduct(ProductsDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer("  insert into pa_shopm_merchant_products ( " + "	clintid , mercid ,prodidselrsite  ");
		sql.append(" ,prodcat ,prodsubcat, prodname , proddesc ");
		sql.append(" ,unitvalue ,priceperunit, unittype, prodimageurl,prodstatus ");
		sql.append(" ,creby,credate,updby,upddate) values (?,?,?,?,?,?,?, ?,?,?,?,?, ");
		sql.append("  ?, getDate(),?,getDate() )  ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintId());
			ps.setInt(2, dvo.getMercId());
			ps.setString(3, dvo.getProdIdSelrsite());

			ps.setString(4, dvo.getProdCat());
			ps.setString(5, dvo.getProdSubcat());
			ps.setString(6, dvo.getProdName());
			ps.setString(7, dvo.getProdDesc());

			ps.setDouble(8, dvo.getUnitValue());
			ps.setDouble(9, dvo.getPricePerUnit());
			ps.setString(10, dvo.getUnitType());
			ps.setString(11, dvo.getProdImgUrl());
			ps.setString(12, StatusEnums.ACTIVE.name());

			ps.setString(13, dvo.getCreby());
			ps.setString(14, dvo.getCreby());
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update merchant product.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMerchantProduct(ProductsDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pa_shopm_merchant_products set ");
		sql.append(" prodidselrsite =?, prodcat = ? , prodsubcat=? , prodname=? , proddesc=?  ");
		sql.append(" , unitvalue=? ,priceperunit=?, unittype=?, prodimageurl=?  , updby = ? ,  upddate=getDate() ");
		sql.append("  where clintid =  ? and mercid = ? and mercprodid=? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());

			ps.setString(1, dvo.getProdIdSelrsite());
			ps.setString(2, dvo.getProdCat());
			ps.setString(3, dvo.getProdSubcat());
			ps.setString(4, dvo.getProdName());
			ps.setString(5, dvo.getProdDesc());

			ps.setDouble(6, dvo.getUnitValue());
			ps.setDouble(7, dvo.getPricePerUnit());
			ps.setString(8, dvo.getUnitType());
			ps.setString(9, dvo.getProdImgUrl());

			ps.setString(10, dvo.getUpdby());
			ps.setString(11, dvo.getClintId());
			ps.setInt(12, dvo.getMercId());
			ps.setInt(13, dvo.getMercProdid());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update merchant product with out image.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMerchantProductWithOutImage(ProductsDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pa_shopm_merchant_products set ");
		sql.append(" prodidselrsite =?, prodcat = ? , prodsubcat=? , prodname=? , proddesc=?  ");
		sql.append(" , unitvalue=? ,priceperunit=?, unittype=?  , updby = ? ,  upddate=getDate() ");
		sql.append("  where clintid =  ? and mercid = ? and mercprodid=? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());

			ps.setString(1, dvo.getProdIdSelrsite());
			ps.setString(2, dvo.getProdCat());
			ps.setString(3, dvo.getProdSubcat());
			ps.setString(4, dvo.getProdName());
			ps.setString(5, dvo.getProdDesc());

			ps.setDouble(6, dvo.getUnitValue());
			ps.setDouble(7, dvo.getPricePerUnit());
			ps.setString(8, dvo.getUnitType());

			ps.setString(9, dvo.getUpdby());
			ps.setString(10, dvo.getClintId());
			ps.setInt(11, dvo.getMercId());
			ps.setInt(12, dvo.getMercProdid());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete merchant product.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteMerchantProduct(ProductsDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pa_shopm_merchant_products set " + " prodstatus =?, updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? and mercprodid=? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.DELETED.name());
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Gets the product by id.
	 *
	 * @param clientId the client id
	 * @param prodId   the prod id
	 * @return the product by id
	 * @throws Exception the exception
	 */
	public static ProductsDVO getProductById(String clientId, Integer prodId) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ProductsDVO vo = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  pa_shopm_merchant_products  with(nolock) where clintid = ? ");
		sql.append("   and mercprodid=?  ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, prodId);
			rs = ps.executeQuery();
			while (rs.next()) {
				vo = new ProductsDVO();
				Timestamp timestamp = null;
				vo.setClintId(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercProdid(rs.getInt("mercProdid"));
				vo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				vo.setProdCat(rs.getString("prodcat"));
				vo.setProdSubcat(rs.getString("prodsubcat"));
				vo.setProdName(rs.getString("prodname"));
				vo.setProdDesc(rs.getString("proddesc"));
				vo.setUnitValue(rs.getDouble("unitvalue"));
				vo.setPricePerUnit(rs.getDouble("priceperunit"));
				vo.setUnitType(rs.getString("unittype"));
				vo.setProdImgUrl(rs.getString("prodimageurl"));
				vo.setStatus(rs.getString("prodstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return vo;
	}

}
