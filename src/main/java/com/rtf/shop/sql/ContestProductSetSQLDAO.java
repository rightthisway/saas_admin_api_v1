/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.util.ShopGridHeaderFilterUtil;

/**
 * The Class ContestProductSetSQLDAO.
 */
public class ContestProductSetSQLDAO {

	/**
	 * Gets the contest product sets by contest id.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @param filter
	 *            the filter
	 * @return the contest product sets by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestProductSetsDVO> getContestProductSetsByContestId(String clId, String coId, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestProductSetsDVO> list = new ArrayList<ContestProductSetsDVO>();
		String sql = "select * from pa_shopm_contest_product_sets WHERE clintid=? AND conid=? AND dispstatus=? ";
		try {
			String filterQuery = ShopGridHeaderFilterUtil.getContestProductSetsFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql += filterQuery;
			}
			
			String sortingSql = GridSortingUtil.getContestProductSetsSortingQuery(filter);
			if(sortingSql !=null && !sortingSql.trim().isEmpty()){
				sql += sortingSql;
			}else{
				sql += " ORDER BY dispseq";
			}
			
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, coId);
			ps.setString(3, "ACTIVE");
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestProductSetsDVO product = new ContestProductSetsDVO();
				product.setClintId(rs.getString("clintid"));
				product.setConId(rs.getString("conid"));
				product.setCreby(rs.getString("creby"));
				product.setCredate(rs.getTimestamp("credate"));
				product.setDispSeq(rs.getInt("dispseq"));
				product.setDispStatus(rs.getString("dispstatus"));
				product.setDispTime(rs.getTimestamp("disptime"));
				product.setMercId(rs.getInt("mercid"));
				product.setMercProdid(rs.getString("mercprodid"));
				product.setPricePerUnit(rs.getDouble("priceperunit"));
				product.setRegPrice(rs.getDouble("preg_prc"));
				product.setProdDesc(rs.getString("proddesc"));
				product.setPriceType(rs.getString("price_type"));
				product.setProdImgUrl(rs.getString("prodimageurl"));
				product.setProdName(rs.getString("prodname"));
				product.setUnitType(rs.getString("unittype"));
				product.setUnitValue(rs.getDouble("unitvalue"));
				product.setUpdby(rs.getString("updby"));
				product.setProdPriceDtl(rs.getString("pprc_dtl"));
				product.setUpddate(rs.getTimestamp("upddate"));
				product.setMerexternalRefProdId(rs.getString("slrpitms_id"));
				list.add(product);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest product sets by prod id.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @param mercId
	 *            the merc id
	 * @param prodId
	 *            the prod id
	 * @return the contest product sets by prod id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestProductSetsDVO getContestProductSetsByProdId(String clId, String coId, Integer mercId,
			Integer prodId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ContestProductSetsDVO product = null;
		String sql = "select * from pa_shopm_contest_product_sets WHERE clintid=? AND conid=? AND mercprodid=? AND dispstatus=? ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, coId);
			ps.setInt(3, prodId);
			ps.setString(4, "ACTIVE");
			rs = ps.executeQuery();

			while (rs.next()) {
				product = new ContestProductSetsDVO();
				product.setClintId(rs.getString("clintid"));
				product.setConId(rs.getString("conid"));
				product.setCreby(rs.getString("creby"));
				product.setCredate(rs.getTimestamp("credate"));
				product.setDispSeq(rs.getInt("dispseq"));
				product.setDispStatus(rs.getString("dispstatus"));
				product.setDispTime(rs.getTimestamp("disptime"));
				product.setMercId(rs.getInt("mercid"));
				product.setMercProdid(rs.getString("mercprodid"));
				product.setPricePerUnit(rs.getDouble("priceperunit"));
				product.setPriceType(rs.getString("price_type"));
				product.setRegPrice(rs.getDouble("preg_prc"));
				product.setProdPriceDtl(rs.getString("pprc_dtl"));
				product.setProdDesc(rs.getString("proddesc"));
				product.setProdImgUrl(rs.getString("prodimageurl"));
				product.setProdName(rs.getString("prodname"));
				product.setUnitType(rs.getString("unittype"));
				product.setUnitValue(rs.getDouble("unitvalue"));
				product.setUpdby(rs.getString("updby"));
				product.setUpddate(rs.getTimestamp("upddate"));
				product.setMerexternalRefProdId(rs.getString("slrpitms_id"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return product;
	}

	/**
	 * Save contest product SQL.
	 *
	 * @param pro
	 *            the pro
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestProductSQL(ContestProductSetsDVO pro) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pa_shopm_contest_product_sets (clintid,mercid,conid,mercprodid,dispseq,prodname,proddesc,unitvalue,priceperunit,")
				.append("prodimageurl,dispstatus,creby,credate,unittype,preg_prc,price_type,pprc_dtl,slrpitms_id ) ")
				.append("values (?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?)");
		Boolean isInserted = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, pro.getClintId());
			ps.setInt(2, pro.getMercId());
			ps.setString(3, pro.getConId());
			ps.setString(4, pro.getMercProdid());
			ps.setInt(5, pro.getDispSeq());
			ps.setString(6, pro.getProdName());
			ps.setString(7, pro.getProdDesc());
			ps.setDouble(8, pro.getUnitValue());
			ps.setDouble(9, pro.getPricePerUnit());
			ps.setString(10, pro.getProdImgUrl());
			ps.setString(11, pro.getDispStatus());
			ps.setString(12, pro.getCreby());
			ps.setString(13, pro.getUnitType());
			ps.setDouble(14, pro.getRegPrice());
			ps.setString(15, pro.getPriceType());
			ps.setString(16, pro.getProdPriceDtl());
			ps.setString(17, pro.getMerexternalRefProdId());
		

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
			return isInserted;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Update contest product SQL.
	 *
	 * @param pro
	 *            the pro
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestProductSQL(ContestProductSetsDVO pro) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_shopm_contest_product_sets SET ").append("dispseq=?,prodname=? ,proddesc=? ,unitvalue=?")
				.append(",priceperunit=?, unittype=?").append(",updby=? ,upddate=getDate(), preg_prc=?,price_type=?,pprc_dtl=?,slrpitms_id = ?  ")
				.append("WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, pro.getDispSeq());
			ps.setString(2, pro.getProdName());
			ps.setString(3, pro.getProdDesc());
			ps.setDouble(4, pro.getUnitValue());
			ps.setDouble(5, pro.getPricePerUnit());
			ps.setString(6, pro.getUnitType());
			ps.setString(7, pro.getUpdby());
			ps.setDouble(8, pro.getRegPrice());
			ps.setString(9, pro.getPriceType());
			ps.setString(10, pro.getProdPriceDtl());
			ps.setString(11, pro.getMerexternalRefProdId());
			ps.setString(12, pro.getClintId());
			ps.setString(13, pro.getConId());
			ps.setInt(14, pro.getMercId());
			ps.setString(15, pro.getMercProdid());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update contest product display time.
	 *
	 * @param pro
	 *            the pro
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestProductDisplayTime(ContestProductSetsDVO pro) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_shopm_contest_product_sets SET ").append("disptime=?,updby=? ,upddate=getDate() ")
				.append("WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setTimestamp(1, new java.sql.Timestamp(pro.getDispTime().getTime()));
			ps.setString(2, pro.getUpdby());

			ps.setString(3, pro.getClintId());
			ps.setString(4, pro.getConId());
			ps.setInt(5, pro.getMercId());
			ps.setString(6, pro.getMercProdid());

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Delete contest product SQL.
	 *
	 * @param clintId
	 *            the clint id
	 * @param coId
	 *            the co id
	 * @param mercId
	 *            the merc id
	 * @param prodId
	 *            the prod id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContestProductSQL(String clintId, String coId, Integer mercId, String prodId)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;

		String sql ="delete from pa_shopm_contest_product_sets WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?";
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, clintId);
			ps.setString(2, coId);
			ps.setInt(3, mercId);
			ps.setString(4, prodId);

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
