/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.service;

import java.util.List;

import com.rtf.shop.cass.ContestProductSetCassDAO;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.sql.ContestProductSetSQLDAO;

/**
 * The Class ShopContestProductSetService.
 */
public class ShopContestProductSetService {

	/**
	 * Gets the contest product sets by contest id.
	 *
	 * @param clId   the cl id
	 * @param coId   the co id
	 * @param filter the filter
	 * @return the contest product sets by contest id
	 */
	public static List<ContestProductSetsDVO> getContestProductSetsByContestId(String clId, String coId,
			String filter) {
		List<ContestProductSetsDVO> contestProducts = null;
		try {
			contestProducts = ContestProductSetSQLDAO.getContestProductSetsByContestId(clId, coId, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contestProducts;
	}

	/**
	 * Gets the contest product sets by prod id.
	 *
	 * @param clId   the cl id
	 * @param coId   the co id
	 * @param mercId the merc id
	 * @param prodId the prod id
	 * @return the contest product sets by prod id
	 */
	public static ContestProductSetsDVO getContestProductSetsByProdId(String clId, String coId, Integer mercId,
			Integer prodId) {
		ContestProductSetsDVO contestProduct = null;
		try {
			contestProduct = ContestProductSetSQLDAO.getContestProductSetsByProdId(clId, coId, mercId, prodId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contestProduct;
	}

	/**
	 * Save contest product.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 */
	public static boolean saveContestProduct(ContestProductSetsDVO pro) {
		Boolean isSaved = false;
		try {
			isSaved = ContestProductSetSQLDAO.saveContestProductSQL(pro);
			if (isSaved) {
				isSaved = ContestProductSetCassDAO.saveContestProductCass(pro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSaved;
	}

	/**
	 * Update contest product.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 */
	public static boolean updateContestProduct(ContestProductSetsDVO pro) {
		Boolean isUpdated = false;
		try {
			isUpdated = ContestProductSetSQLDAO.updateContestProductSQL(pro);
			if (isUpdated) {
				isUpdated = ContestProductSetCassDAO.updateContestProductCass(pro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}

	/**
	 * Update contest product display time.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 */
	public static boolean updateContestProductDisplayTime(ContestProductSetsDVO pro) {
		Boolean isUpdated = false;
		try {
			isUpdated = ContestProductSetSQLDAO.updateContestProductDisplayTime(pro);
			if (isUpdated) {
				isUpdated = ContestProductSetCassDAO.updateContestProductDisplayTime(pro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}

	/**
	 * Delete contest product.
	 *
	 * @param clintId the clint id
	 * @param coId    the co id
	 * @param mercId  the merc id
	 * @param prodId  the prod id
	 * @return true, if successful
	 */
	public static boolean deleteContestProduct(String clintId, String coId, Integer mercId, String prodId) {
		Boolean isDeleted = false;
		try {
			isDeleted = ContestProductSetSQLDAO.deleteContestProductSQL(clintId, coId, mercId, prodId);
			if (isDeleted) {
				isDeleted = ContestProductSetCassDAO.deleteContestProductCass(clintId, coId, mercId, prodId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isDeleted;
	}
}
