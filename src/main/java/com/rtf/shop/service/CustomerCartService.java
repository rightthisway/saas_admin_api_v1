/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.saas.util.StatusEnums;
import com.rtf.shop.dto.CustomerCartDTO;
import com.rtf.shop.dvo.CustomerCartDVO;
import com.rtf.shop.dvo.ProductsDVO;
import com.rtf.shop.sql.dao.CustomerCartDAO;
import com.rtf.shop.sql.dao.MerchantProductsDAO;

/**
 * The Class CustomerCartService.
 */
public class CustomerCartService {

	/**
	 * Adds the to customer cart.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO addToCustomerCart(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			CustomerCartDVO dvo = dto.getDvo();
			ProductsDVO pdvo = MerchantProductsDAO.getProductById(dvo.getClintId(), dvo.getMercProdid());
			dvo.setMercId(pdvo.getMercId());
			dvo.setBuyUnits(1);
			dvo.setBuyPrice(pdvo.getPricePerUnit());
			dvo.setUnitType(pdvo.getUnitType());
			Integer updCnt = CustomerCartDAO.updateCurrentContestCartItemStatus(dvo, StatusEnums.ACTIVE.name());
			if (updCnt == 1) {
				dto.setSts(1);
			} else {

				CustomerCartDAO.addToCart(dvo, StatusEnums.ACTIVE.name());
				dto.setSts(1);
			}
			updCnt = CustomerCartDAO.updatePreviousContestActiveCartItemStatus(dvo, StatusEnums.DUPLICATE.name());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;

	}

	/**
	 * Removes the from customer cart.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO removeFromCustomerCart(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			CustomerCartDVO dvo = dto.getDvo();
			ProductsDVO pdvo = MerchantProductsDAO.getProductById(dvo.getClintId(), dvo.getMercProdid());
			dvo.setMercId(pdvo.getMercId());
			CustomerCartDAO.deleteItemInCart(dvo, StatusEnums.DELETED.name());
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;

	}

	/**
	 * Removes the from all from customer cart.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO removeFromAllFromCustomerCart(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			List<CustomerCartDVO> lst = dto.getLst();
			CustomerCartDAO.updateCartMultiItemStatus(lst, StatusEnums.DELETED.name());
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * List customer cart items.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO listCustomerCartItems(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			CustomerCartDVO dvo = dto.getDvo();
			List<CustomerCartDVO> lst = CustomerCartDAO.fetchCustomerCart(dvo);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * List all customer cart items.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO listAllCustomerCartItems(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			CustomerCartDVO dvo = dto.getDvo();
			List<CustomerCartDVO> lst = CustomerCartDAO.fetchCustomerCart(dvo);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Fetchall customers cart.
	 *
	 * @param dto the dto
	 * @return the customer cart DTO
	 */
	public static CustomerCartDTO fetchallCustomersCart(CustomerCartDTO dto) {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			Map<String, List<CustomerCartDVO>> allcartMap = new HashMap<String, List<CustomerCartDVO>>();
			Map<String, String> allCustMap = CustomerCartDAO.fetchAllCustomerInCart();
			CustomerCartDVO dvo = dto.getDvo();
			for (Map.Entry<String, String> entry : allCustMap.entrySet()) {
				dvo.setCustId(entry.getKey());
				dvo.setClintId(entry.getValue());
				List<CustomerCartDVO> lst = CustomerCartDAO.fetchCustomerCart(dvo);
				allcartMap.put(dvo.getCustId(), lst);
			}

			dto.setAllcustcartmap(allcartMap);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;
	}

}
