/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.cass;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.dvo.ContestProductSetsDVO;

/**
 * The Class ContestProductSetCassDAO.
 */
public class ContestProductSetCassDAO {

	/**
	 * Save contest product cass.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean saveContestProductCass(ContestProductSetsDVO pro) throws Exception {
		boolean isInserted = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pa_shopm_contest_product_sets (clintid,mercid,conid,mercprodid,dispseq,prodname,proddesc,unitvalue,priceperunit,");
		sql.append("prodimageurl,dispstatus,creby,credate,unittype,preg_prc,price_type,pprc_dtl,slrpitms_id ) ");
		sql.append("values (?,?,?,?,?,?,?,?,?,?,?,?,toTimestamp(now()),?,?,?,?,?)");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { pro.getClintId(), pro.getMercId(), pro.getConId(), pro.getMercProdid(),
							pro.getDispSeq(), pro.getProdName(), pro.getProdDesc(), pro.getUnitValue(),
							pro.getPricePerUnit(), pro.getProdImgUrl(), pro.getDispStatus(), pro.getCreby(),
							pro.getUnitType(), pro.getRegPrice(), pro.getPriceType(),pro.getProdPriceDtl(),pro.getMerexternalRefProdId() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isInserted;
	}

	/**
	 * Update contest product cass.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestProductCass(ContestProductSetsDVO pro) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_shopm_contest_product_sets SET ");
		sql.append("dispseq=?,prodname=? ,proddesc=? ,unitvalue=?");
		sql.append(",priceperunit=?, unittype=?");
		sql.append(",updby=? ,upddate=toTimestamp(now()), preg_prc=?, price_type=?,slrpitms_id=?  ");
		sql.append("WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { pro.getDispSeq(), pro.getProdName(), pro.getProdDesc(), pro.getUnitValue(),
							pro.getPricePerUnit(), pro.getUnitType(), pro.getUpdby(), pro.getRegPrice(),
							pro.getPriceType(), pro.getMerexternalRefProdId(),pro.getClintId(), pro.getConId(), pro.getMercId(), pro.getMercProdid()

					});
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Update contest product display time.
	 *
	 * @param pro the pro
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestProductDisplayTime(ContestProductSetsDVO pro) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_shopm_contest_product_sets SET ");
		sql.append("disptime=?,updby=? ,upddate=toTimestamp(now()) ");
		sql.append("WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { pro.getDispTime(), pro.getUpdby(),
					pro.getClintId(), pro.getConId(), pro.getMercId(), pro.getMercProdid() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Delete contest product cass.
	 *
	 * @param clintId the clint id
	 * @param coId    the co id
	 * @param mercId  the merc id
	 * @param prodId  the prod id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteContestProductCass(String clintId, String coId, Integer mercId, String prodId)
			throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from pa_shopm_contest_product_sets ");
		sql.append("WHERE clintid=? AND conid=? AND mercid=? AND mercprodid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { clintId, coId, mercId, prodId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

}
