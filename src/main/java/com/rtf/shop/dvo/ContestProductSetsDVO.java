/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dvo;

import com.rtf.common.util.DateFormatUtil;

/**
 * The Class ContestProductSetsDVO.
 */
public class ContestProductSetsDVO {

	/** The clint id. */
	private String clintId;

	/** The merc id. */
	private Integer mercId;

	/** The con id. */
	private String conId;

	/** The merc prodid. */
	private String mercProdid;

	/** The disp seq. */
	private Integer dispSeq;

	/** The prod name. */
	private String prodName;

	/** The prod desc. */
	private String prodDesc;

	/** The unit value. */
	private Double unitValue;

	/** The price per unit. */
	private Double pricePerUnit;

	/** The reg price. */
	private Double regPrice;

	/** The unit type. */
	private String unitType;

	/** The prod img url. */
	private String prodImgUrl;

	/** The disp status. */
	private String dispStatus;

	/** The disp time. */
	private java.util.Date dispTime;

	/** The creby. */
	private String creby;

	/** The credate. */
	private java.util.Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private java.util.Date upddate;

	/** The price type. */
	private String priceType;

	/** The cre date str. */
	private String creDateStr;

	/** The upd date str. */
	private String updDateStr;

	/** The disp time str. */
	private String dispTimeStr;
	
	/** The product price details. */
	private String prodPriceDtl;
	
	private String merexternalRefProdId;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the merc id.
	 *
	 * @return the merc id
	 */
	public Integer getMercId() {
		return mercId;
	}

	/**
	 * Sets the merc id.
	 *
	 * @param mercId the new merc id
	 */
	public void setMercId(Integer mercId) {
		this.mercId = mercId;
	}

	/**
	 * Gets the con id.
	 *
	 * @return the con id
	 */
	public String getConId() {
		return conId;
	}

	/**
	 * Sets the con id.
	 *
	 * @param conId the new con id
	 */
	public void setConId(String conId) {
		this.conId = conId;
	}

	/**
	 * Gets the merc prodid.
	 *
	 * @return the merc prodid
	 */
	public String getMercProdid() {
		return mercProdid;
	}

	/**
	 * Sets the merc prodid.
	 *
	 * @param mercProdid the new merc prodid
	 */
	public void setMercProdid(String mercProdid) {
		this.mercProdid = mercProdid;
	}

	/**
	 * Gets the disp seq.
	 *
	 * @return the disp seq
	 */
	public Integer getDispSeq() {
		return dispSeq;
	}

	/**
	 * Sets the disp seq.
	 *
	 * @param dispSeq the new disp seq
	 */
	public void setDispSeq(Integer dispSeq) {
		this.dispSeq = dispSeq;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the prod desc.
	 *
	 * @return the prod desc
	 */
	public String getProdDesc() {
		return prodDesc;
	}

	/**
	 * Sets the prod desc.
	 *
	 * @param prodDesc the new prod desc
	 */
	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	/**
	 * Gets the price per unit.
	 *
	 * @return the price per unit
	 */
	public Double getPricePerUnit() {
		return pricePerUnit;
	}

	/**
	 * Sets the price per unit.
	 *
	 * @param pricePerUnit the new price per unit
	 */
	public void setPricePerUnit(Double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	/**
	 * Gets the prod img url.
	 *
	 * @return the prod img url
	 */
	public String getProdImgUrl() {
		return prodImgUrl;
	}

	/**
	 * Sets the prod img url.
	 *
	 * @param prodImgUrl the new prod img url
	 */
	public void setProdImgUrl(String prodImgUrl) {
		this.prodImgUrl = prodImgUrl;
	}

	/**
	 * Gets the disp status.
	 *
	 * @return the disp status
	 */
	public String getDispStatus() {
		return dispStatus;
	}

	/**
	 * Sets the disp status.
	 *
	 * @param dispStatus the new disp status
	 */
	public void setDispStatus(String dispStatus) {
		this.dispStatus = dispStatus;
	}

	/**
	 * Gets the disp time.
	 *
	 * @return the disp time
	 */
	public java.util.Date getDispTime() {
		return dispTime;
	}

	/**
	 * Sets the disp time.
	 *
	 * @param dispTime the new disp time
	 */
	public void setDispTime(java.util.Date dispTime) {
		this.dispTime = dispTime;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public java.util.Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(java.util.Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public java.util.Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(java.util.Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the unit value.
	 *
	 * @return the unit value
	 */
	public Double getUnitValue() {
		return unitValue;
	}

	/**
	 * Sets the unit value.
	 *
	 * @param unitValue the new unit value
	 */
	public void setUnitValue(Double unitValue) {
		this.unitValue = unitValue;
	}

	/**
	 * Gets the unit type.
	 *
	 * @return the unit type
	 */
	public String getUnitType() {
		return unitType;
	}

	/**
	 * Sets the unit type.
	 *
	 * @param unitType the new unit type
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	/**
	 * Gets the price type.
	 *
	 * @return the price type
	 */
	public String getPriceType() {
		return priceType;
	}

	/**
	 * Sets the price type.
	 *
	 * @param priceType the new price type
	 */
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	/**
	 * Gets the cre date str.
	 *
	 * @return the cre date str
	 */
	public String getCreDateStr() {
		creDateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(credate);
		return creDateStr;
	}

	/**
	 * Sets the cre date str.
	 *
	 * @param creDateStr the new cre date str
	 */
	public void setCreDateStr(String creDateStr) {
		this.creDateStr = creDateStr;
	}

	/**
	 * Gets the upd date str.
	 *
	 * @return the upd date str
	 */
	public String getUpdDateStr() {
		if (upddate != null) {
			updDateStr = DateFormatUtil.getMMDDYYYYHHMMSSString(upddate);
		}
		return updDateStr;
	}

	/**
	 * Sets the upd date str.
	 *
	 * @param updDateStr the new upd date str
	 */
	public void setUpdDateStr(String updDateStr) {
		this.updDateStr = updDateStr;
	}

	/**
	 * Gets the disp time str.
	 *
	 * @return the disp time str
	 */
	public String getDispTimeStr() {
		if (dispTime != null) {
			dispTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(dispTime);
		}
		return dispTimeStr;
	}

	/**
	 * Sets the disp time str.
	 *
	 * @param dispTimeStr the new disp time str
	 */
	public void setDispTimeStr(String dispTimeStr) {
		this.dispTimeStr = dispTimeStr;
	}

	/**
	 * Gets the reg price.
	 *
	 * @return the reg price
	 */
	public Double getRegPrice() {
		return regPrice;
	}

	/**
	 * Sets the reg price.
	 *
	 * @param regPrice the new reg price
	 */
	public void setRegPrice(Double regPrice) {
		this.regPrice = regPrice;
	}

	
	/**
	 * Gets product price details.
	 *
	 * @return prodPriceDtl
	 */
	public String getProdPriceDtl() {
		return prodPriceDtl;
	}

	/**
	 * Sets product price details.
	 *
	 * @param prodPriceDtl
	 */
	public void setProdPriceDtl(String prodPriceDtl) {
		this.prodPriceDtl = prodPriceDtl;
	}

	public String getMerexternalRefProdId() {
		return merexternalRefProdId;
	}

	public void setMerexternalRefProdId(String merexternalRefProdId) {
		this.merexternalRefProdId = merexternalRefProdId;
	}
	
	
	
}