/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.MerchantProductDTO;
import com.rtf.shop.dvo.ProductsDVO;
import com.rtf.shop.service.MerchantProductService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopListMerchantProductServlet.
 */
@WebServlet("/shoplistmerchantproduct.json")
public class ShopListMerchantProductServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 3137369871728163570L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * get all merchant products based on filter
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String mercId = request.getParameter("mercid");
		String clId = request.getParameter("clId");
		String userId = request.getParameter("cau");
		String sts = request.getParameter("sts");
		String pNo = request.getParameter("pgNo");
		String filter = request.getParameter("hfilter");
		Integer mId = null;

		MerchantProductDTO dto = new MerchantProductDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(mercId)) {
				setClientMessage(dto, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				mId = Integer.parseInt(mercId);
			} catch (Exception ex) {
				setClientMessage(dto, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pNo)) {
				setClientMessage(dto, UserMsgConstants.INVALID_PAGE_NO, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sts)) {
				setClientMessage(dto, ShopMessageConstant.STATUS_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			ProductsDVO dvo = new ProductsDVO();
			dvo.setClintId(clId);
			dvo.setMercId(mId);
			dvo.setCreby(userId);
			dto.setDvo(dvo);
			dto = MerchantProductService.fetchMerchantProductList(dto, filter, pNo, mId, sts);

			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}

}
