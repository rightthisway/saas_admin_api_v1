/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.shop.dto.MerchantProductDTO;
import com.rtf.shop.dvo.ProductsDVO;
import com.rtf.shop.service.MerchantProductService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopEditMerchantProductServlet.
 */
@WebServlet("/shopeditmerchantproduct.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class ShopEditMerchantProductServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 4173732805956858465L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Update merchant product details
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String mercId = request.getParameter("mercid");
		String mercProdid = request.getParameter("mercProdid");
		String prodidselrsite = request.getParameter("prodidselrsite");
		String prodcat = request.getParameter("prodcat");
		String prodsubcat = request.getParameter("prodsubcat");
		String prodname = request.getParameter("prodname");
		String proddesc = request.getParameter("proddesc");
		String unitvalue = request.getParameter("unitvalue");
		String priceperunit = request.getParameter("priceperunit");
		String unittype = request.getParameter("unittype");
		String userId = request.getParameter("cau");
		Integer mId = null;
		Double price = null;
		Double units = null;
		Integer mprodId = null;
		String isimgupd = request.getParameter("isimgupd");

		MerchantProductDTO dto = new MerchantProductDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(mercId)) {
				setClientMessage(dto, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				mId = Integer.parseInt(mercId);
			} catch (Exception ex) {
				ex.printStackTrace();
				setClientMessage(dto, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(mercProdid)) {
				setClientMessage(dto, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				mprodId = Integer.parseInt(mercProdid);
			} catch (Exception ex) {
				ex.printStackTrace();
				setClientMessage(dto, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(prodname)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_NAME_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(prodidselrsite)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_IDENTIFER_ON_SELLER_SITE_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(unitvalue)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_UNIT_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				units = Double.parseDouble(unitvalue);
			} catch (Exception ex) {
				ex.printStackTrace();
				setClientMessage(dto, ShopMessageConstant.INVALID_PRODUCT_UNIT_FORMAT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(priceperunit)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_PRICE_PER_UNIT_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				price = Double.parseDouble(priceperunit);
			} catch (Exception ex) {
				ex.printStackTrace();
				setClientMessage(dto, ShopMessageConstant.INVALID_PRODUCT_PRICE_FORMAT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(unittype)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_UNIT_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			ProductsDVO dvo = new ProductsDVO();
			dvo.setClintId(clId);
			dvo.setMercId(mId);
			dvo.setMercProdid(mprodId);
			dvo.setProdIdSelrsite(prodidselrsite);
			dvo.setProdCat(prodcat);
			dvo.setProdSubcat(prodsubcat);
			dvo.setProdName(prodname);
			dvo.setProdDesc(proddesc);
			dvo.setUnitValue(units);
			dvo.setPricePerUnit(price);
			dvo.setUnitType(unittype);

			if ("true".equals(isimgupd)) {
				String TMP_DIR = SaaSAdminAppCache.TMP_FOLDER;
				TMP_DIR = TMP_DIR + File.separator + clId + File.separator;

				File fileSaveDir = new File(TMP_DIR);
				if (!fileSaveDir.exists()) {
					fileSaveDir.mkdirs();
				}
				String origFileName = null;
				try {
					for (Part part : request.getParts()) {
						origFileName = FileUtil.getFileName(part);
					}
				} catch (Exception ex) {
					dto.setSts(0);
					setClientMessage(dto, SAASMessages.RWD_ICON_FILE_UPLOAD_MANDATORY, null);
					generateResponse(request, response, dto);
					return;
				}
				if (GenUtil.isNullOrEmpty(origFileName)) {
					dto.setSts(0);
					setClientMessage(dto, SAASMessages.RWD_ICON_FILE_UPLOAD_MANDATORY, null);
					generateResponse(request, response, dto);
					return;
				}

				String EXTENSION = FileUtil.getFileExtension(origFileName);
				String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
				try {
					for (Part part : request.getParts()) {
						part.write(TMP_DIR + File.separator + fileName);
					}
				} catch (Exception ex) {
					dto.setSts(0);
					setClientMessage(dto, null, null);
					generateResponse(request, response, dto);
					return;
				}
				String s3bucket = SaaSAdminAppCache.s3staticbucket;
				String shopprodfolder = SaaSAdminAppCache.envfoldertype + "/" + SaaSAdminAppCache.s3shopproductsfolder
						+ "/" + clId;
				File tmpfile = new File(TMP_DIR + File.separator + fileName);
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, fileName, shopprodfolder, tmpfile);
				if (null != awsRsponse && awsRsponse.getStatus() != 1) {
					setClientMessage(dto, null, null);
					generateResponse(request, response, dto);
					return;
				}
				String serverLinkPath = SaaSAdminAppCache.cfstaticurl + "/" + shopprodfolder + "/" + fileName;
				dvo.setProdImgUrl(serverLinkPath);
			}
			dvo.setUpdby(userId);
			dto.setDvo(dvo);
			dto = MerchantProductService.updateMerchantProduct(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setMsg(ShopMessageConstant.PRODUCT_UPDATE_SUCCESS_MSG);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}

}
