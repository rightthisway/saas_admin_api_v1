/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.ContestProductDTO;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.service.ShopContestProductSetService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopEditContestProductSetServlet.
 */
@WebServlet("/shopeditcontestproductsets.json")
public class ShopEditContestProductSetServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 4454132524952314887L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * get contest products set to edit
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String merchId = request.getParameter("merchId");
		String prodId = request.getParameter("prodId");

		ContestProductDTO respDTO = new ContestProductDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(prodId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(merchId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Integer proId = 0;
			Integer mercId = 0;
			try {
				proId = Integer.parseInt(prodId);
				mercId = Integer.parseInt(merchId);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_OR_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestProductSetsDVO product = ShopContestProductSetService.getContestProductSetsByProdId(clId, coId,
					mercId, proId);
			if (product == null) {
				setClientMessage(respDTO, ShopMessageConstant.PRODUCT_ID_NOT_EXIST, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			respDTO.setProduct(product);
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
