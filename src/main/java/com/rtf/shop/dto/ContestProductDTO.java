/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.shop.dvo.ContestProductSetsDVO;

/**
 * The Class ContestProductDTO.
 */
public class ContestProductDTO extends RtfSaasBaseDTO {

	/** The products. */
	private List<ContestProductSetsDVO> products;

	/** The product. */
	private ContestProductSetsDVO product;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the products.
	 *
	 * @return the products
	 */
	public List<ContestProductSetsDVO> getProducts() {
		return products;
	}

	/**
	 * Sets the products.
	 *
	 * @param products the new products
	 */
	public void setProducts(List<ContestProductSetsDVO> products) {
		this.products = products;
	}

	/**
	 * Gets the product.
	 *
	 * @return the product
	 */
	public ContestProductSetsDVO getProduct() {
		return product;
	}

	/**
	 * Sets the product.
	 *
	 * @param product the new product
	 */
	public void setProduct(ContestProductSetsDVO product) {
		this.product = product;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
