/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.seller.dvo.PvtSellerDTO;
import com.rtf.inventory.seller.service.ShopifyService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class AddSellerServlet.
 */
//customers/data_request
@WebServlet("/gdprdatarequest.json")

public class ShopifyGDPRdataRequestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PvtSellerDTO dto = new PvtSellerDTO();
		String resMsg = "";	
		
		try {

			dto.setSts(0);
			String shopName = request.getParameter("shop");		
			String cau = request.getParameter("cau");
			String sellerType = request.getParameter("sellerType");
			String clientIdStr = request.getParameter("clId");
			dto.setSellerType(sellerType);
			dto.setApibaseurl(shopName);
			dto.setSts(0);
			try {					
				System.out.println("[/gshpfypvtstoredets]  [ CODE ] + " + dto.getApiky() );
				dto.setSts(1);
			} catch (Exception ex) {
				ex.printStackTrace();
				resMsg = "Issue processing GDPR Data Request   :" + ex.getLocalizedMessage();
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, "  GDPR Data Request processed successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {

		}

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, String> map = new HashMap<String, String>();		
		map.put("resp", rtfSaasBaseDTO.getMsg());
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	

}
