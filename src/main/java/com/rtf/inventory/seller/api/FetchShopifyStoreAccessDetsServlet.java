/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.inventory.seller.dto.SellerDTO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.livt.util.HTTPUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class AddSellerServlet.
 */

@WebServlet("/gstoreaccs.json")

public class FetchShopifyStoreAccessDetsServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SellerDTO dto = new SellerDTO();
		String resMsg = "";
		
		String api_key = "ae539f0b9a7acaab65ad6e1e598eec1e";
		String secretkey = "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";
		try {

			dto.setSts(0);
			String shopName = request.getParameter("shop");
			String clientIdStr = request.getParameter("clId");
			String cau = request.getParameter("cau");
			String code = request.getParameter("code");
			

			// SELLER TYPE ..
			String apikeyTemp = request.getParameter("api_key");
			String secretkeyTemp = request.getParameter("secretkey");
			if(!GenUtil.isNullOrEmpty(apikeyTemp)) {
				api_key = apikeyTemp;
			}
			if(!GenUtil.isNullOrEmpty(secretkeyTemp)) {
				secretkey = secretkeyTemp;
			}	
			Map<String, String> dataparamMap = new HashMap<String, String>();

			dataparamMap.put("client_id", api_key);
			dataparamMap.put("client_secret", secretkey);
			dataparamMap.put("code", code);
			dataparamMap.put("shop", shopName);

			String accessToken = null;

			SellerDVO dvo = new SellerDVO();

			dvo.setClintId(clientIdStr);
			dto.setDvo(dvo);
			dto.setSts(0);
			try {
				accessToken = fetchAccessToken(dataparamMap);
				dvo.setApiKey(accessToken);
				if (GenUtil.isEmptyString(accessToken)) {
					dto.setSts(0);
					dto.setMsg(" Issue While Fetching Access Token ");
					generateResponse(request, response, dto);
					return;
				} else {
					dto.setSts(1);
				}
				
				System.out.println("[/gstoreaccs]  [ CODE ] + " + code   + "shopName "  + shopName  + "accessToken " + accessToken);
			} catch (Exception ex) {
				ex.printStackTrace();
				resMsg = "Issue While Fetching Access Token :" + ex.getLocalizedMessage();
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			setClientMessage(dto, null, " Access Token fetched successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {

		}

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	private static String fetchAccessToken(Map<String, String> params) throws Exception {
		String data = null;
		String accesstok = null;
		System.out.println("[  **************SHOPIFY ][getauthtokens[1]]  " + params);
		String shopname = params.get("shop");
		String authUrl = "https://" + shopname + "/admin/oauth/access_token";
		System.out.println("[  **************SHOPIFY ][getauthtokens[authUrl]  " + authUrl);

		try {
			data = HTTPUtil.execute(params, authUrl);
			System.out.println("[  **************SHOPIFY ][getauthtokens[1][data]]  " + data);
			System.out.println(data);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			accesstok = jsonObject.get("access_token").getAsString();
			return accesstok;
		} catch (Exception e) {
			e.printStackTrace();
			// throw e;
		}
		return accesstok;
	}

}
