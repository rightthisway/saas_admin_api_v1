/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.seller.dto.SellerDTO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.inventory.seller.service.SellerService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class EditSellerServlet.
 */

@WebServlet("/edslr.json")

public class EditSellerServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SellerDTO dto = new SellerDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerIdstr = request.getParameter("slerId");
		String compName = request.getParameter("compName");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String addrLineone = request.getParameter("addrLineone");
		String addrLinetwo = request.getParameter("addrLinetwo");

		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		String pincode = request.getParameter("pincode");
		String cau = request.getParameter("cau");
		
		// SELLER TYPE ..
				String sellerType = request.getParameter("slrtype");
				String sellerbaseurl = request.getParameter("slrweburl");
				String apiKey = request.getParameter("aky");
				String apipassword = request.getParameter("apiss");


		Integer sellerId = null;
		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = "Invalid Seller Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = "Invalid Seller Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(compName)) {
				resMsg = "Invalid Company Name :" + compName;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(email)) {
				resMsg = "Invalid Email :" + email;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(phone)) {
				resMsg = "Invalid Phone :" + phone;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
				if(sellerType.equalsIgnoreCase("SHOPIFY")) {				
					if (GenUtil.isNullOrEmpty(sellerType)) {
						resMsg = "Invalid SellerType :" + sellerType;
						setClientMessage(dto, resMsg, null);
						generateResponse(request, response, dto);
						return;
					}
					if (GenUtil.isNullOrEmpty(apiKey)) {
						resMsg = "Invalid api  key :" + apiKey;
						setClientMessage(dto, resMsg, null);
						generateResponse(request, response, dto);
						return;
					}
					if (GenUtil.isNullOrEmpty(apipassword)) {
						resMsg = "Invalid api password :" + apipassword;
						setClientMessage(dto, resMsg, null);
						generateResponse(request, response, dto);
						return;
					}
				}
			SellerDVO dvo = new SellerDVO();

			dvo.setClintId(clientIdStr);
			dvo.setSlerId(sellerId);
			dvo.setCompName(compName);
			dvo.setEmail(email);
			dvo.setPhone(phone);
			dvo.setAddrLineone(addrLineone);
			dvo.setAddrLinetwo(addrLinetwo);
			dvo.setCity(city);
			dvo.setState(state);
			dvo.setCountry(country);
			dvo.setPincode(pincode);
			dvo.setSlerStatus("ACTIVE");
			dvo.setLupdBy(cau);
			dvo.setApibaseurl(sellerbaseurl);
			dvo.setApiKey(apiKey);
			dvo.setApiPass(apipassword);
			dvo.setSellerType(sellerType);
			dto.setDvo(dvo);
			dto.setSts(0);
			dto = SellerService.updateSellerForPK(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, "Error Occured while Updating Seller", null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Seller Updated Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while Updating Seller :" + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} finally {

		}

	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
