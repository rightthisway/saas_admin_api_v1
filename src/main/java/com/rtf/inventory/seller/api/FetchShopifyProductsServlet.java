/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.inventory.seller.dto.ShopifyProdDTO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.inventory.seller.service.SellerService;
import com.rtf.livt.util.HTTPUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * The Class AddSellerServlet.
 */

@WebServlet("/gshpfyprds.json")

public class FetchShopifyProductsServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ShopifyProdDTO dto = new ShopifyProdDTO();
		dto.setSts(0);
		String resMsg = "";		
		
		try {		

			String shopName = request.getParameter("shop");
			String clientIdStr = request.getParameter("clId");
			String cau = request.getParameter("cau");			
			String slerId = request.getParameter("slerId");
			
			String title = request.getParameter("prodtitle");
			
			String pageInfo = request.getParameter("pageinfo");
			
			String limit = "25";
			
			Integer islerId = null;	
			String accessToken = null;

			SellerDVO dvo = new SellerDVO();
			
		
			try {
				islerId = Integer.parseInt(slerId);
			}catch(Exception ex) {
				ex.printStackTrace();
				dto.setSts(0);
				dto.setMsg(" Seller Id is Invalid");
				generateResponse(request, response, dto);
				return;				
			}
			
			dvo.setClintId(clientIdStr);
			dvo.setSlerId(islerId);
			dvo.setApibaseurl(shopName);
			Map<String , String> inputParams = new HashMap<String,String>();
			
			if(!GenUtil.isNullOrEmpty(pageInfo)) {
				inputParams.put("page_info" , pageInfo);
			}
			
			if(!GenUtil.isNullOrEmpty(title)) {
				inputParams.put("title",title);
			}
			
			inputParams.put("limit",limit);	
				try {
			dvo = 	SellerService.fetchSellerDetsBySlerIdShopName(dvo);	
			Map<String,String> respMap =  fetchShopifyProdList(dvo,inputParams);			
			String prodListStr = respMap.get("HTTPRESPONSE_JSONDATA");			
			dto.setProddets(prodListStr);			
			Map<String,String> finalhdrMap = new HashMap<String,String>();
			finalhdrMap.put("Link", respMap.get("Link"));
			dto.setHdrMap(finalhdrMap);
			dto.setSts(1);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(prodListStr, JsonObject.class);
			
			if(jsonObject.has("errors")) {
			  throw new Exception(prodListStr);
			}				
	
			} catch (Exception ex) {
				dto.setSts(0);
				ex.printStackTrace();
				resMsg = "Issue While Fetching Product Listing :" + ex.getLocalizedMessage();
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Product Listing fetched successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			dto.setSts(0);
			e.printStackTrace();
			resMsg = "Issue While Fetching Product Listing :" + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;
		}

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	private static Map<String,String> fetchShopifyProdList(SellerDVO dvo,Map<String , String> params) throws Exception {
		Map<String,String> respMap= null;
		String accesstok = dvo.getApiKey();
		Map<String,String> hdrMap = new HashMap<String,String>();		
		String headerTok = "X-Shopify-Access-Token";
		hdrMap.put(headerTok, accesstok);
		System.out.println("[  **************SHOPIFY ][Header Map]  " + hdrMap   + "[base url ]  " + dvo.getApibaseurl() );
		
		String url = "https://" + dvo.getApibaseurl() + "/admin/api/2021-07/products.json";
		
		try {
			respMap = HTTPUtil.executeWithHeaderTokens(params, url,hdrMap);
			//System.out.println("[  **************SHOPIFY[prodlist] ]respMap]  " + respMap);
			return respMap;
		} catch (Exception e) {
			e.printStackTrace();		
		}
		return respMap;
	}
	
	public static void main(String[] args)  throws Exception {
		SellerDVO dvo = new SellerDVO();
		dvo.setApiKey("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");
		dvo.setApibaseurl("newtestap.myshopify.com");
		
		Map<String , String> inputParams = new HashMap<String,String>();
		//inputParams.put("page_info" , "");
		inputParams.put("limit","1");
		//inputParams.put("titile",title);
		
		Map<String,String> respMap =  fetchShopifyProdList(dvo,inputParams);			
		String prodListStr = respMap.get("HTTPRESPONSE_JSONDATA");	
		ShopifyProdDTO dto = new ShopifyProdDTO();
		dto.setProddets(prodListStr);			
		Map<String,String> finalhdrMap = new HashMap<String,String>();
		finalhdrMap.put("Link", respMap.get("Link"));
		dto.setHdrMap(finalhdrMap);
		dto.setSts(1);
		Gson gson = GsonCustomConfig.getGsonBuilder();
		JsonObject jsonObject = gson.fromJson(prodListStr, JsonObject.class);
		
		if(jsonObject.has("errors")) {
		  throw new Exception(prodListStr);
		}			
		if(jsonObject.has("errors1")) {
			String errorStr1 = jsonObject.get("errors1").getAsString();
			System.out.println("[  8888888888888888888SHOPIFY ][errorStr1]  " + errorStr1);
		}
	
		System.out.println(" FINAL DTO " + dto);
	}


}
