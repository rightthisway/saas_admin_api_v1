/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.service;

import java.sql.Connection;
import java.util.List;

import com.rtf.inventory.common.util.Messages;
import com.rtf.inventory.product.dao.ProductItemMasterDAO;
import com.rtf.inventory.product.dto.ProductItemMasterDTO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.seller.dao.SellerDAO;
import com.rtf.inventory.seller.dto.SellerDTO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;

/**
 * The Class SellerService.
 */
public class SellerService {

	/**
	 * Creates the seller.
	 *
	 * @param dto the dto
	 * @return the seller DTO
	 * @throws Exception the exception
	 */
	public static SellerDTO createSeller(SellerDTO dto) throws Exception {
		Connection conn = null;
		SellerDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			SellerDVO tdvo = SellerDAO.fetchClientSellerForExistingName(dvo, conn);
			if (tdvo != null && !GenUtil.isEmptyString(tdvo.getFirstName())) {
				dto.setSts(0);
				dto.setMsg(tdvo.getFirstName() + " :: " + Messages.SELLER_NAME_ALREADY_EXISTS);
				return dto;
			}
			Integer cnt = SellerDAO.createSeller(dvo, conn);
			dto.setSts(cnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Fetch all sellers for client.
	 *
	 * @param dto the dto
	 * @return the seller DTO
	 * @throws Exception the exception
	 */
	public static SellerDTO fetchAllSellersForClient(SellerDTO dto,String filter) throws Exception {
		Connection conn = null;
		SellerDVO dvo = dto.getDvo();
		List<SellerDVO> lst = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			lst = SellerDAO.fetchAllClientSellerDetails(dvo, conn,filter);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Update seller for PK.
	 *
	 * @param dto the dto
	 * @return the seller DTO
	 * @throws Exception the exception
	 */
	public static SellerDTO updateSellerForPK(SellerDTO dto) throws Exception {
		Connection conn = null;
		SellerDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = SellerDAO.updateSellerDetails(dvo, conn);
			dto.setDvo(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Update seller status for PK.
	 *
	 * @param dto the dto
	 * @return the seller DTO
	 * @throws Exception the exception
	 */
	public static SellerDTO updateSellerStatusForPK(SellerDTO dto) throws Exception {
		Connection conn = null;
		SellerDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = SellerDAO.updateSellerStatus(dvo, conn);
			dto.setDvo(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Fetch product item master for PK.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO fetchProductItemMasterForPK(ProductItemMasterDTO dto) throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			dvo = ProductItemMasterDAO.fetchProductItemForPK(dvo, conn);
			dto.setDvo(dvo);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}
	
	public static SellerDVO fetchSellerDetsBySlerIdShopName(SellerDVO dvo) throws Exception {
		Connection conn = null;		
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			dvo = SellerDAO.fetchClientSellerBySlerIdShopName(dvo, conn);		
		} catch (Exception ex) {		
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dvo;
	}
	
	
	public static SellerDVO fetchSellerDetsBySlerId(SellerDVO dvo) throws Exception {
		Connection conn = null;		
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			dvo = SellerDAO.fetchClientSellerBySlerId(dvo, conn);		
		} catch (Exception ex) {		
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dvo;
	}
}
