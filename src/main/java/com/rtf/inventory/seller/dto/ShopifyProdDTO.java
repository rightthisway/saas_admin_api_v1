/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.dto;

import java.util.Map;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ShopifyProdDTO.
 */
public class ShopifyProdDTO extends RtfSaasBaseDTO{
	
private String proddets;
private  Map<String,String> hdrMap;

public String getProddets() {
	return proddets;
}

public void setProddets(String proddets) {
	this.proddets = proddets;
}

public Map<String, String> getHdrMap() {
	return hdrMap;
}

public void setHdrMap(Map<String, String> hdrMap) {
	this.hdrMap = hdrMap;
}

@Override
public String toString() {
	return "ShopifyProdDTO [proddets=" + proddets + ", hdrMap=" + hdrMap + "]";
}

}
