package com.rtf.inventory.seller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.rtf.inventory.seller.dvo.PvtSellerDTO;


public class PrivateAppDetailsDAO {
	public static PvtSellerDTO fetchPrivateAppDetails(PvtSellerDTO dto, Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select *  ")
		.append("  from pa_shopify_prvapp_dtls where  str_url = ? and pa_status=?  ");
	
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dto.getApibaseurl());
			ps.setString(2, "AVAILABLE");		
			rs = ps.executeQuery();		
			while (rs.next()) {			
				dto.setId(rs.getInt("paID"));
				dto.setSlerId(rs.getInt("sler_id"));
				dto.setApibaseurl(rs.getString("str_url"));
				dto.setApiky(rs.getString("apikey"));
				dto.setApisky(rs.getString("apiskey"));
				dto.setoAuthUrl(rs.getString("oAuthurl"));
				
			}
			return dto;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
