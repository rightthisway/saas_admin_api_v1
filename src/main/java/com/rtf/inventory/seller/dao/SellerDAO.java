/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.inventory.seller.dvo.SellerDVO;

/**
 * The Class SellerDAO.
 */
public class SellerDAO {

	/**
	 * Creates the seller.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createSeller(SellerDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  rtf_seller_dtls ")
		.append(" ( clintid ,first_name,last_name,comp_name, email ")
		.append(" ,phone,addr_line_one,addr_line_two,city,state ")
		.append(" ,country,pincode,sler_status,lupd_by, lupd_date   ")
		.append(" ,sellerType,apiKey,apiPass,apibaseurl )  ")
		.append(" values(?, ?, ?, ?, ?, " + "        ?, ?, ?, ?, ?, " + "        ?, ?, ?,?,getDate(),   ?,?,?,? ) ");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getFirstName());
			ps.setString(3, dvo.getLastName());
			ps.setString(4, dvo.getCompName());
			ps.setString(5, dvo.getEmail());

			ps.setString(6, dvo.getPhone());
			ps.setString(7, dvo.getAddrLineone());
			ps.setString(8, dvo.getAddrLinetwo());
			ps.setString(9, dvo.getCity());
			ps.setString(10, dvo.getState());

			ps.setString(11, dvo.getCountry());
			ps.setString(12, dvo.getPincode());
			ps.setString(13, dvo.getSlerStatus());
			ps.setString(14, dvo.getLupdBy());			
		
			ps.setString(15, dvo.getSellerType());
			ps.setString(16, dvo.getApiKey());
			ps.setString(17, dvo.getApiPass());
			ps.setString(18, dvo.getApibaseurl());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Seller  Master Details record failed for Client Id " + dvo.getClintId());
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch all client seller details.
	 *
	 * @param rdvo the rdvo
	 * @param conn the conn
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<SellerDVO> fetchAllClientSellerDetails(SellerDVO rdvo, Connection conn,String filter) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select clintid , sler_id, first_name, last_name, comp_name,email ")
		.append("      ,phone,addr_line_one,addr_line_two,city,state ")
		.append("      ,country,pincode,sler_status,lupd_by, lupd_date,sellerType,apibaseurl,apiKey,apiPass ")
		.append("      from rtf_seller_dtls where  clintid = ? and sler_status=? ");
		
		
		String sortingSql = GridSortingUtil.getMerchantSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append(" ORDER BY first_name ");
		}
		
		List<SellerDVO> dvolist = new ArrayList<SellerDVO>();
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, rdvo.getClintId());
			ps.setString(2, rdvo.getSlerStatus());
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = null;
				SellerDVO dvo = new SellerDVO();
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setFirstName(rs.getString("first_name"));
				dvo.setLastName(rs.getString("last_name"));
				dvo.setCompName(rs.getString("comp_name"));
				dvo.setEmail(rs.getString("email"));
				dvo.setPhone(rs.getString("phone"));
				dvo.setAddrLineone(rs.getString("addr_line_one"));
				dvo.setAddrLinetwo(rs.getString("addr_line_two"));
				dvo.setCity(rs.getString("city"));
				dvo.setState(rs.getString("state"));
				dvo.setCountry(rs.getString("country"));
				dvo.setPincode(rs.getString("pincode"));
				dvo.setSlerStatus(rs.getString("sler_status"));
				dvo.setLupdBy(rs.getString("lupd_by"));
				date = rs.getTimestamp("lupd_date");
				dvo.setLupdDate(date != null ? date.getTime() : null);
				dvo.setApibaseurl(rs.getString("apibaseurl"));
				dvo.setSellerType(rs.getString("sellerType"));
				dvo.setApiKey(rs.getString("apiKey"));
				dvo.setApiPass(rs.getString("apiPass"));
				dvolist.add(dvo);
			}
			return dvolist;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch client seller for existing name.
	 *
	 * @param rdvo the rdvo
	 * @param conn the conn
	 * @return the seller DVO
	 * @throws Exception the exception
	 */
	public static SellerDVO fetchClientSellerForExistingName(SellerDVO rdvo, Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select clintid , sler_id, first_name, last_name, comp_name,email ")
		.append("      ,phone,addr_line_one,addr_line_two,city,state ")
		.append( "      ,country,pincode,sler_status,lupd_by, lupd_date ")
		.append("      from rtf_seller_dtls where  clintid = ? and sler_status=? and first_name=? ");
		SellerDVO dvo = null;
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, rdvo.getClintId());
			ps.setString(2, rdvo.getSlerStatus());
			ps.setString(3, rdvo.getFirstName());
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = null;
				dvo = new SellerDVO();
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setFirstName(rs.getString("first_name"));
				dvo.setLastName(rs.getString("last_name"));
				dvo.setCompName(rs.getString("comp_name"));
				dvo.setEmail(rs.getString("email"));
				dvo.setPhone(rs.getString("phone"));
				dvo.setAddrLineone(rs.getString("addr_line_one"));
				dvo.setAddrLinetwo(rs.getString("addr_line_two"));
				dvo.setCity(rs.getString("city"));
				dvo.setState(rs.getString("state"));
				dvo.setCountry(rs.getString("country"));
				dvo.setPincode(rs.getString("pincode"));
				dvo.setSlerStatus(rs.getString("sler_status"));
				dvo.setLupdBy(rs.getString("lupd_by"));
				date = rs.getTimestamp("lupd_date");
				dvo.setLupdDate(date != null ? date.getTime() : null);
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Update seller details.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateSellerDetails(SellerDVO dvo, Connection conn) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append(" update rtf_seller_dtls set ")
		.append("    comp_name=? ,email=? ,phone=? ,addr_line_one=? ,addr_line_two=? ")
		.append(	"  , city=? ,state=? ,country=? ,pincode = ? ")
		.append("  , lupd_by = ?, lupd_date = getDate() ")
		.append("  , sellerType = ?, apiKey =?,apiPass=?,apibaseurl=? ")
		.append("   where  clintid = ? and sler_id=?  ");

	
		
		Integer updCnt = 0;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql.toString());

			ps.setString(1, dvo.getCompName());
			ps.setString(2, dvo.getEmail());
			ps.setString(3, dvo.getPhone());
			ps.setString(4, dvo.getAddrLineone());
			ps.setString(5, dvo.getAddrLinetwo());

			ps.setString(6, dvo.getCity());
			ps.setString(7, dvo.getState());
			ps.setString(8, dvo.getCountry());
			ps.setString(9, dvo.getPincode());

			ps.setString(10, dvo.getLupdBy());
			
			ps.setString(11, dvo.getSellerType());
			ps.setString(12, dvo.getApiKey());
			ps.setString(13, dvo.getApiPass());
			ps.setString(14, dvo.getApibaseurl());

			ps.setString(15, dvo.getClintId());
			ps.setInt(16, dvo.getSlerId());

			updCnt = ps.executeUpdate();
			return updCnt;

		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Update seller status.
	 *
	 * @param rdvo the rdvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateSellerStatus(SellerDVO rdvo, Connection conn) throws Exception {
		String sql = " update rtf_seller_dtls set  sler_status=? ,lupd_by = ? ,lupd_date = getDate() where  clintid = ? and sler_id=?  ";
		Integer updCnt = 0;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, rdvo.getSlerStatus());
			ps.setString(2, rdvo.getLupdBy());
			ps.setString(3, rdvo.getClintId());
			ps.setInt(4, rdvo.getSlerId());
			updCnt = ps.executeUpdate();
			return updCnt;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}
	
	public static SellerDVO fetchClientSellerBySlerIdShopName(SellerDVO rdvo, Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select clintid , sler_id,  ")		
		.append( " apiKey,apibaseurl ")
		.append(" from rtf_seller_dtls where  clintid = ? and sler_status=? and sler_id=? and apibaseurl=?  ");
		SellerDVO dvo = null;
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, rdvo.getClintId());
			ps.setString(2, "ACTIVE");
			ps.setInt(3, rdvo.getSlerId());
			ps.setString(4, rdvo.getApibaseurl());
			
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = null;
				dvo = new SellerDVO();
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setApiKey(rs.getString("apiKey"));
				dvo.setApibaseurl(rs.getString("apibaseurl"));

			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static SellerDVO fetchClientSellerBySlerId(SellerDVO rdvo, Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select clintid , sler_id,  ")		
		.append( " apiKey,apibaseurl ,sellerType")
		.append(" from rtf_seller_dtls where  clintid = ? and sler_status=? and sler_id=? ");
		SellerDVO dvo = null;
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, rdvo.getClintId());
			ps.setString(2, "ACTIVE");
			ps.setInt(3, rdvo.getSlerId());		
			
			rs = ps.executeQuery();
			while (rs.next()) {				
				dvo = new SellerDVO();
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setApiKey(rs.getString("apiKey"));
				dvo.setApibaseurl(rs.getString("apibaseurl"));
				dvo.setSellerType(rs.getString("sellerType"));

			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
