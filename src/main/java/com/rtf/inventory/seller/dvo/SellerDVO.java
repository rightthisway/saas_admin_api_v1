/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.dvo;

/**
 * The Class SellerDVO.
 */
public class SellerDVO {

	/** The clint id. */
	private String clintId;

	/** The sler id. */
	private Integer slerId;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The comp name. */
	private String compName;

	/** The email. */
	private String email;

	/** The phone. */
	private String phone;

	/** The addr lineone. */
	private String addrLineone;

	/** The addr linetwo. */
	private String addrLinetwo;

	/** The city. */
	private String city;

	/** The state. */
	private String state;

	/** The country. */
	private String country;

	/** The pincode. */
	private String pincode;

	/** The sler status. */
	private String slerStatus;

	/** The lupd date. */
	private Long lupdDate;

	/** The lupd by. */
	private String lupdBy;
	
	
	private String sellerType;
	private String apiKey;
	private String apiPass;
	private String apibaseurl;
	
	
	
	
	

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public Integer getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the comp name.
	 *
	 * @return the comp name
	 */
	public String getCompName() {
		return compName;
	}

	/**
	 * Sets the comp name.
	 *
	 * @param compName the new comp name
	 */
	public void setCompName(String compName) {
		this.compName = compName;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the addr lineone.
	 *
	 * @return the addr lineone
	 */
	public String getAddrLineone() {
		return addrLineone;
	}

	/**
	 * Sets the addr lineone.
	 *
	 * @param addrLineone the new addr lineone
	 */
	public void setAddrLineone(String addrLineone) {
		this.addrLineone = addrLineone;
	}

	/**
	 * Gets the addr linetwo.
	 *
	 * @return the addr linetwo
	 */
	public String getAddrLinetwo() {
		return addrLinetwo;
	}

	/**
	 * Sets the addr linetwo.
	 *
	 * @param addrLinetwo the new addr linetwo
	 */
	public void setAddrLinetwo(String addrLinetwo) {
		this.addrLinetwo = addrLinetwo;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the pincode.
	 *
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * Sets the pincode.
	 *
	 * @param pincode the new pincode
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	/**
	 * Gets the sler status.
	 *
	 * @return the sler status
	 */
	public String getSlerStatus() {
		return slerStatus;
	}

	/**
	 * Sets the sler status.
	 *
	 * @param slerStatus the new sler status
	 */
	public void setSlerStatus(String slerStatus) {
		this.slerStatus = slerStatus;
	}

	/**
	 * Gets the lupd date.
	 *
	 * @return the lupd date
	 */
	public Long getLupdDate() {
		return lupdDate;
	}

	/**
	 * Sets the lupd date.
	 *
	 * @param lupdDate the new lupd date
	 */
	public void setLupdDate(Long lupdDate) {
		this.lupdDate = lupdDate;
	}

	/**
	 * Gets the lupd by.
	 *
	 * @return the lupd by
	 */
	public String getLupdBy() {
		return lupdBy;
	}

	/**
	 * Sets the lupd by.
	 *
	 * @param lupdBy the new lupd by
	 */
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SellerDVO [clintid=" + clintId + ", slerId=" + slerId + ", firstName=" + firstName + ", lastName="
				+ lastName + ", compName=" + compName + ", email=" + email + ", phone=" + phone + ", addrLineone="
				+ addrLineone + ", addrLinetwo=" + addrLinetwo + ", city=" + city + ", state=" + state + ", country="
				+ country + ", pincode=" + pincode + ", slerStatus=" + slerStatus + ", lupdDate=" + lupdDate
				+ ", lupdBy=" + lupdBy + "]";
	}

	public String getSellerType() {
		return sellerType;
	}

	public void setSellerType(String sellerType) {
		this.sellerType = sellerType;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getApiPass() {
		return apiPass;
	}

	public void setApiPass(String apiPass) {
		this.apiPass = apiPass;
	}

	public String getApibaseurl() {
		return apibaseurl;
	}

	public void setApibaseurl(String apibaseurl) {
		this.apibaseurl = apibaseurl;
	}

}
