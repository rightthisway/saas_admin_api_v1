package com.rtf.inventory.seller.dvo;

import com.rtf.saas.arch.RtfSaasBaseDTO;

public class PvtSellerDTO extends RtfSaasBaseDTO {
	
	private String apiky;
	private String apisky;
	private String apibaseurl;
	private String cau;
	private String status;
	private Integer slerId;
	private String sellerType;
	private Integer id;
	private String oAuthUrl;
	
	
	public String getApiky() {
		return apiky;
	}
	public void setApiky(String apiky) {
		this.apiky = apiky;
	}
	public String getApisky() {
		return apisky;
	}
	public void setApisky(String apisky) {
		this.apisky = apisky;
	}
	
	public String getApibaseurl() {
		return apibaseurl;
	}
	public void setApibaseurl(String apibaseurl) {
		this.apibaseurl = apibaseurl;
	}
	public String getCau() {
		return cau;
	}
	public void setCau(String cau) {
		this.cau = cau;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getSlerId() {
		return slerId;
	}
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}
	public String getSellerType() {
		return sellerType;
	}
	public void setSellerType(String sellerType) {
		this.sellerType = sellerType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getoAuthUrl() {
		return oAuthUrl;
	}
	public void setoAuthUrl(String oAuthUrl) {
		this.oAuthUrl = oAuthUrl;
	}
	

}
