/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.InventoryUploadDTO;
import com.rtf.inventory.common.service.InventoryFileParserService;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class UploadInventoryDataServlet.
 */

@WebServlet("/upldinvtry.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class UploadInventoryDataServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Import product inventory from excel file and store it.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		InventoryUploadDTO dto = new InventoryUploadDTO();
		dto.setSts(0);
		try {
			String resMsg = "";
			String clientIdStr = request.getParameter("clId");
			String cau = request.getParameter("cau");
			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = MessageConstant.INVALID_USER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = MessageConstant.INVALID_CLIENT;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			String tmpDir = SaaSAdminAppCache.TMP_FOLDER;
			tmpDir = tmpDir + File.separator + clientIdStr + File.separator;
			File fileSaveDir = new File(tmpDir);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			String origFileName = null;
			try {
				for (Part part : request.getParts()) {
					origFileName = FileUtil.getFileName(part);
				}
			} catch (Exception ex) {
				dto.setSts(0);
				dto.setMsg(MessageConstant.INV_DATA_FILE_MENDATORY);
				return;
			}
			if (GenUtil.isNullOrEmpty(origFileName)) {
				dto.setSts(0);
				dto.setMsg(MessageConstant.INV_DATA_FILE_MENDATORY);
				return;
			}
			String ext = FileUtil.getFileExtension(origFileName);
			String fileName = clientIdStr + "_" + System.currentTimeMillis() + "." + ext;
			try {
				for (Part part : request.getParts()) {
					part.write(tmpDir + File.separator + fileName);
				}
			} catch (Exception ex) {
				dto.setSts(0);
				dto.setMsg(ex.getLocalizedMessage());
			}
			File uploadedFile = new File(tmpDir + File.separator + fileName);
			dto.setUploadedFile(uploadedFile);
			dto.setClId(clientIdStr);
			dto.setCau(cau);
			dto.setSts(0);
			dto = InventoryFileParserService.processInventoryUploadDataFile(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, MessageConstant.INV_UPLOAD_ERROR + dto.getMsg(), null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, MessageConstant.INV_DATA_UPLOADED);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			setClientMessage(dto, MessageConstant.INV_UPLOAD_ERROR + dto.getMsg(), null);
			generateResponse(request, response, dto);
			return;

		} finally {
		}
	}
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
}
