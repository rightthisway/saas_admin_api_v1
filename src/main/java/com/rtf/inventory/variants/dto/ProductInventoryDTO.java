/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dto;

import java.util.List;

import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.dvo.VariantNameValueMapDVO;
import com.rtf.inventory.variants.dvo.VariantOptValueDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ProductInventoryDTO.
 */
public class ProductInventoryDTO extends RtfSaasBaseDTO{
	
	/** The slrinv id. */
	private Integer slrinvId;
	
	/** The slrpivarcom id. */
	private Integer slrpivarcomId;
	
	/** The setuniq id. */
	private Integer setuniqId;
	
	/** The slrpitms id. */
	private String slrpitmsId;
	
	/** The sler id. */
	private Integer slerId;
	
	/** The lupd by. */
	private String lupdBy;
	
	/** The prdstatus. */
	private String prdstatus;
	
	/** The variant opt value DV olst. */
	private List<VariantOptValueDVO> variantOptValueDVOlst;
	
	/** The opt nameval map. */
	List<VariantNameValueMapDVO> optNamevalMap;
	
	/**
	 * Gets the prdstatus.
	 *
	 * @return the prdstatus
	 */
	public String getPrdstatus() {
		return prdstatus;
	}

	/**
	 * Sets the prdstatus.
	 *
	 * @param prdstatus the new prdstatus
	 */
	public void setPrdstatus(String prdstatus) {
		this.prdstatus = prdstatus;
	}

	/** The pimasterdvo. */
	private ProductItemMasterDVO pimasterdvo;
	
	/** The lst. */
	private List<ProductInventoryDVO> lst;

	/**
	 * Gets the lst.
	 *
	 * @return the lst
	 */
	public List<ProductInventoryDVO> getLst() {
		return lst;
	}

	/**
	 * Sets the lst.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<ProductInventoryDVO> lst) {
		this.lst = lst;
	}

	/**
	 * Gets the slrinv id.
	 *
	 * @return the slrinv id
	 */
	public Integer getSlrinvId() {
		return slrinvId;
	}

	/**
	 * Sets the slrinv id.
	 *
	 * @param slrinvId the new slrinv id
	 */
	public void setSlrinvId(Integer slrinvId) {
		this.slrinvId = slrinvId;
	}

	/**
	 * Gets the slrpivarcom id.
	 *
	 * @return the slrpivarcom id
	 */
	public Integer getSlrpivarcomId() {
		return slrpivarcomId;
	}

	/**
	 * Sets the slrpivarcom id.
	 *
	 * @param slrpivarcomId the new slrpivarcom id
	 */
	public void setSlrpivarcomId(Integer slrpivarcomId) {
		this.slrpivarcomId = slrpivarcomId;
	}

	/**
	 * Gets the setuniq id.
	 *
	 * @return the setuniq id
	 */
	public Integer getSetuniqId() {
		return setuniqId;
	}

	/**
	 * Sets the setuniq id.
	 *
	 * @param setuniqId the new setuniq id
	 */
	public void setSetuniqId(Integer setuniqId) {
		this.setuniqId = setuniqId;
	}

	/**
	 * Gets the slrpitms id.
	 *
	 * @return the slrpitms id
	 */
	public String getSlrpitmsId() {
		return slrpitmsId;
	}

	/**
	 * Sets the slrpitms id.
	 *
	 * @param slrpitmsId the new slrpitms id
	 */
	public void setSlrpitmsId(String slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public Integer getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ProductInventoryDTO [slrinvId=" + slrinvId + ", slrpivarcomId=" + slrpivarcomId + ", setuniqId="
				+ setuniqId + ", slrpitmsId=" + slrpitmsId + ", slerId=" + slerId + ", lupdBy=" + lupdBy
				+ ", prdstatus=" + prdstatus + ", variantOptValueDVOlst=" + variantOptValueDVOlst + ", optNamevalMap="
				+ optNamevalMap + ", pimasterdvo=" + pimasterdvo + ", lst=" + lst + "]";
	}

	/**
	 * Gets the pimasterdvo.
	 *
	 * @return the pimasterdvo
	 */
	public ProductItemMasterDVO getPimasterdvo() {
		return pimasterdvo;
	}

	/**
	 * Sets the pimasterdvo.
	 *
	 * @param pimasterdvo the new pimasterdvo
	 */
	public void setPimasterdvo(ProductItemMasterDVO pimasterdvo) {
		this.pimasterdvo = pimasterdvo;
	}

	/**
	 * Gets the lupd by.
	 *
	 * @return the lupd by
	 */
	public String getLupdBy() {
		return lupdBy;
	}

	/**
	 * Sets the lupd by.
	 *
	 * @param lupdBy the new lupd by
	 */
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}

	/**
	 * Gets the variant opt value DV olst.
	 *
	 * @return the variant opt value DV olst
	 */
	public List<VariantOptValueDVO> getVariantOptValueDVOlst() {
		return variantOptValueDVOlst;
	}

	/**
	 * Sets the variant opt value DV olst.
	 *
	 * @param variantOptValueDVOlst the new variant opt value DV olst
	 */
	public void setVariantOptValueDVOlst(List<VariantOptValueDVO> variantOptValueDVOlst) {
		this.variantOptValueDVOlst = variantOptValueDVOlst;
	}

	/**
	 * Gets the opt nameval map.
	 *
	 * @return the opt nameval map
	 */
	public List<VariantNameValueMapDVO> getOptNamevalMap() {
		return optNamevalMap;
	}

	/**
	 * Sets the opt nameval map.
	 *
	 * @param optNamevalMap the new opt nameval map
	 */
	public void setOptNamevalMap(List<VariantNameValueMapDVO> optNamevalMap) {
		this.optNamevalMap = optNamevalMap;
	}

}
