/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

/**
 * The Class VariantOptValueDVO.
 */
public class VariantOptValueDVO {

	/** The clint id. */
	private String clintId;

	/** The varoval id. */
	private Integer varovalId;

	/** The slrpivar id. */
	private Integer slrpivarId;

	/** The voptvalue. */
	private String voptvalue;

	/** The imgurl. */
	private String imgurl;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the varoval id.
	 *
	 * @return the varoval id
	 */
	public Integer getVarovalId() {
		return varovalId;
	}

	/**
	 * Sets the varoval id.
	 *
	 * @param varovalId the new varoval id
	 */
	public void setVarovalId(Integer varovalId) {
		this.varovalId = varovalId;
	}

	/**
	 * Gets the slrpivar id.
	 *
	 * @return the slrpivar id
	 */
	public Integer getSlrpivarId() {
		return slrpivarId;
	}

	/**
	 * Sets the slrpivar id.
	 *
	 * @param slrpivarId the new slrpivar id
	 */
	public void setSlrpivarId(Integer slrpivarId) {
		this.slrpivarId = slrpivarId;
	}

	/**
	 * Gets the voptvalue.
	 *
	 * @return the voptvalue
	 */
	public String getVoptvalue() {
		return voptvalue;
	}

	/**
	 * Sets the voptvalue.
	 *
	 * @param voptvalue the new voptvalue
	 */
	public void setVoptvalue(String voptvalue) {
		this.voptvalue = voptvalue;
	}

	/**
	 * Gets the imgurl.
	 *
	 * @return the imgurl
	 */
	public String getImgurl() {
		return imgurl;
	}

	/**
	 * Sets the imgurl.
	 *
	 * @param imgurl the new imgurl
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "VariantOptValueDVO [clintId=" + clintId + ", varovalId=" + varovalId + ", slrpivarId=" + slrpivarId
				+ ", voptvalue=" + voptvalue + ", imgurl=" + imgurl + "]";
	}

}
