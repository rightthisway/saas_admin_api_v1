/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

import java.util.Map;

/**
 * The Class ProductItemVariantNameDVO.
 */
public class ProductItemVariantNameDVO {

	/** The clint id. */
	private String clintId;

	/** The slrpivar id. */
	private Integer slrpivarId;

	/** The slrpitms id. */
	private String slrpitmsId;

	/** The sler id. */
	private String slerId;

	/** The voptname. */
	private String voptname;

	/** The varoval id. */
	private Integer varovalId;

	/** The voptvalue. */
	private String voptvalue;

	/** The voptvalue iimgurl. */
	private String voptvalueIimgurl;

	/** The lupd date. */
	private Long lupdDate;

	/** The lupd by. */
	private String lupdBy;

	/** The variant name opt val file name map. */
	Map<String, Map<String, String>> variantNameOptValFileNameMap;

	/**
	 * Gets the variant name opt val file name map.
	 *
	 * @return the variant name opt val file name map
	 */
	public Map<String, Map<String, String>> getVariantNameOptValFileNameMap() {
		return variantNameOptValFileNameMap;
	}

	/**
	 * Sets the variant name opt val file name map.
	 *
	 * @param variantNameOptValFileNameMap the variant name opt val file name map
	 */
	public void setVariantNameOptValFileNameMap(Map<String, Map<String, String>> variantNameOptValFileNameMap) {
		this.variantNameOptValFileNameMap = variantNameOptValFileNameMap;
	}

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the slrpitms id.
	 *
	 * @return the slrpitms id
	 */
	public String getSlrpitmsId() {
		return slrpitmsId;
	}

	/**
	 * Sets the slrpitms id.
	 *
	 * @param slrpitmsId the new slrpitms id
	 */
	public void setSlrpitmsId(String slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public String getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(String slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the lupd date.
	 *
	 * @return the lupd date
	 */
	public Long getLupdDate() {
		return lupdDate;
	}

	/**
	 * Sets the lupd date.
	 *
	 * @param lupdDate the new lupd date
	 */
	public void setLupdDate(Long lupdDate) {
		this.lupdDate = lupdDate;
	}

	/**
	 * Gets the lupd by.
	 *
	 * @return the lupd by
	 */
	public String getLupdBy() {
		return lupdBy;
	}

	/**
	 * Sets the lupd by.
	 *
	 * @param lupdBy the new lupd by
	 */
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}

	/**
	 * Gets the slrpivar id.
	 *
	 * @return the slrpivar id
	 */
	public Integer getSlrpivarId() {
		return slrpivarId;
	}

	/**
	 * Sets the slrpivar id.
	 *
	 * @param slrpivarId the new slrpivar id
	 */
	public void setSlrpivarId(Integer slrpivarId) {
		this.slrpivarId = slrpivarId;
	}

	/**
	 * Gets the voptname.
	 *
	 * @return the voptname
	 */
	public String getVoptname() {
		return voptname;
	}

	/**
	 * Sets the voptname.
	 *
	 * @param voptname the new voptname
	 */
	public void setVoptname(String voptname) {
		this.voptname = voptname;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ProductItemVariantNameDVO [clintId=" + clintId + ", slrpivarId=" + slrpivarId + ", slrpitmsId="
				+ slrpitmsId + ", slerId=" + slerId + ", voptname=" + voptname + ", lupdDate=" + lupdDate + ", lupdBy="
				+ lupdBy + "]";
	}

	/**
	 * Gets the voptvalue.
	 *
	 * @return the voptvalue
	 */
	public String getVoptvalue() {
		return voptvalue;
	}

	/**
	 * Sets the voptvalue.
	 *
	 * @param voptvalue the new voptvalue
	 */
	public void setVoptvalue(String voptvalue) {
		this.voptvalue = voptvalue;
	}

	/**
	 * Gets the voptvalue iimgurl.
	 *
	 * @return the voptvalue iimgurl
	 */
	public String getVoptvalueIimgurl() {
		return voptvalueIimgurl;
	}

	/**
	 * Sets the voptvalue iimgurl.
	 *
	 * @param voptvalueIimgurl the new voptvalue iimgurl
	 */
	public void setVoptvalueIimgurl(String voptvalueIimgurl) {
		this.voptvalueIimgurl = voptvalueIimgurl;
	}

	/**
	 * Gets the varoval id.
	 *
	 * @return the varoval id
	 */
	public Integer getVarovalId() {
		return varovalId;
	}

	/**
	 * Sets the varoval id.
	 *
	 * @param varovalId the new varoval id
	 */
	public void setVarovalId(Integer varovalId) {
		this.varovalId = varovalId;
	}

}
