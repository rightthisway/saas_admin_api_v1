/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

/**
 * The Class VariantMatrixDVO.
 */
public class VariantMatrixDVO {

	/** The clint id. */
	private String clintId;

	/** The slrpivarcom id. */
	private Integer slrpivarcomId;

	/** The setuniq id. */
	private Integer setuniqId;

	/** The slrpitm id. */
	private String slrpitmId;

	/** The sler id. */
	private Integer slerId;

	/** The voptnameval idcom. */
	private String voptnamevalIdcom;

	/** The voptnamevalcom. */
	private String voptnamevalcom;

	/** The vonameone. */
	private String vonameone;

	/** The vovalueone. */
	private String vovalueone;

	/** The vonametwo. */
	private String vonametwo;

	/** The vovaluetwo. */
	private String vovaluetwo;

	/** The vonamethree. */
	private String vonamethree;

	/** The vovaluethree. */
	private String vovaluethree;

	/** The vonamefour. */
	private String vonamefour;

	/** The vovaluefour. */
	private String vovaluefour;

	/** The slrpivar idone. */
	private Integer slrpivarIdone;

	/** The varoval idone. */
	private Integer varovalIdone;

	/** The slrpivar idtwo. */
	private Integer slrpivarIdtwo;

	/** The varova idtwo. */
	private Integer varovaIdtwo;

	/** The slrpivar idthree. */
	private Integer slrpivarIdthree;

	/** The varoval idthree. */
	private Integer varovalIdthree;

	/** The slrpivar idfour. */
	private Integer slrpivarIdfour;

	/** The varoval idfour. */
	private Integer varovalIdfour;

	/** The lupddate. */
	private Long lupddate;

	/** The lupd date. */
	private Long lupdDate;

	/** The lupd by. */
	private String lupdBy;

}
