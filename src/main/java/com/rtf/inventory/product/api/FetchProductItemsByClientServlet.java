/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.product.dto.ProductItemMasterDTO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.product.service.ProductItemMasterService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class FetchProductItemsByClientServlet.
 */

@WebServlet("/livtftchproditemlst.json")
public class FetchProductItemsByClientServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Get all product of selected seller.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductItemMasterDTO dto = new ProductItemMasterDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerIdstr = request.getParameter("slerId");
		String pstatus = request.getParameter("pstatus");
		String filter = request.getParameter("hfilter");
		String cau = request.getParameter("cau");

		Integer sellerId = null;

		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = MessageConstant.INVALID_USER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = MessageConstant.INVALID_CLIENT;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = MessageConstant.INVALID_SELLER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_SELLER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pstatus)) {
				resMsg = MessageConstant.INVALID_PRODUCT_STATUS;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			ProductItemMasterDVO dvo = new ProductItemMasterDVO();
			dvo.setClintId(clientIdStr);
			dvo.setSlrpitmsId(sellerprodid);
			dvo.setSlerId(sellerId);
			dvo.setPstatus(pstatus);
			dvo.setLupdBy(cau);
			dto.setDvo(dvo);
			dto.setSts(0);
			dto = ProductItemMasterService.fetchAllProductItemsForClient(dto,filter);
			if (dto.getSts() == 0) {
				setClientMessage(dto, MessageConstant.PRODUCT_GET_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, MessageConstant.PRODUCT_FETCHED_MSG);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = MessageConstant.PRODUCT_GET_ERROR+ e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} finally {
		}

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
