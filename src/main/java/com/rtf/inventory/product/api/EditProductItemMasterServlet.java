/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.FileUploadGenericDTO;
import com.rtf.inventory.common.util.ASWS3ImageUploaderUtil;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.product.dto.ProductItemMasterDTO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.product.service.ProductItemMasterService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class EditProductItemMasterServlet.
 */

@WebServlet("/livteditpditemaster.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class EditProductItemMasterServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3153019520791999936L;

	/**
	 * Update the selected product.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductItemMasterDTO dto = new ProductItemMasterDTO();
		dto.setSts(0);	
		String resMsg = "";
		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerIdstr = request.getParameter("slerId");
		String pbrand = request.getParameter("pbrand");
		String pname = request.getParameter("pname");
		String pdesc = request.getParameter("pdesc");
		String plongdesc = request.getParameter("plongdesc");
		String psku = request.getParameter("psku");
		String cau = request.getParameter("cau");
		String isvariantstr = request.getParameter("isvariant");
		String priceType = request.getParameter("priceType");
		String pselMinPrcstr = request.getParameter("pselMinPrc");
		String pregMinPrcstr = request.getParameter("pregMinPrc");

		String maxcustqtystr = request.getParameter("maxcustqty");
		String dispstartdatestr = request.getParameter("dispstartdate");
		String pexpdatestr = request.getParameter("pexpdate");
		String prodPriceDtl = request.getParameter("prodPriceDtl");

		String isimgupd = request.getParameter("isimgupd");
		String itemsId = request.getParameter("itemsId");

		Integer maxcustqty = null;
		Double pselMinPrc, pregMinPrc = null;
		Date dispstartdate, pexpdate = null;
		Boolean isVariant = Boolean.FALSE;
		Integer sellerId = null;
		Integer itmsId = null;

		try {			
			
			if (GenUtil.isNullOrEmpty(itemsId)) {
				resMsg = MessageConstant.INVALID_PROD_ID;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = MessageConstant.INVALID_USER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = MessageConstant.INVALID_CLIENT;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerprodid)) {
				resMsg =MessageConstant.INVALID_PRODUCT;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = MessageConstant.INVALID_SELLER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			try {
				itmsId = Integer.parseInt(itemsId);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_PROD_ID;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_SELLER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pname)) {
				resMsg = MessageConstant.INVALID_PRODUCT_NAME;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pdesc)) {
				resMsg =MessageConstant.INVALID_PRODUCT_DESC;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pselMinPrcstr)) {
				resMsg = MessageConstant.INVALID_SELL_PRICE;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pregMinPrcstr)) {
				pregMinPrcstr="0";
			}
			/*
			 * if (GenUtil.isNullOrEmpty(psku)) { resMsg = MessageConstant.INVALID_SKU;
			 * setClientMessage(dto, resMsg, null); generateResponse(request, response,
			 * dto); return; }
			 */
			/*
			 * if (GenUtil.isNullOrEmpty(pexpdatestr)) { resMsg
			 * =MessageConstant.INVALID_EXP_DATE; setClientMessage(dto, resMsg, null);
			 * generateResponse(request, response, dto); return; }
			 */
			if (GenUtil.isNullOrEmpty(isvariantstr)) {
				resMsg = MessageConstant.INVALID_VARIANT_FLAG;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(maxcustqtystr)) {
				resMsg = MessageConstant.INVALID_MAX_QTY;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			/*
			 * if (GenUtil.isNullOrEmpty(dispstartdatestr)) { resMsg =
			 * MessageConstant.INVALID_DISP_DATE; setClientMessage(dto, resMsg, null);
			 * generateResponse(request, response, dto); return; }
			 */

			if (GenUtil.isNullOrEmpty(priceType) || priceType.equalsIgnoreCase("NONE")) {
				priceType = null;
			}

			try {
				maxcustqty = new Integer(maxcustqtystr);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_MAX_QTY;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			try {
				pselMinPrc = new Double(pselMinPrcstr);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_SELL_PRICE;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				pregMinPrc = new Double(pregMinPrcstr);
			} catch (Exception ex) {
				resMsg = MessageConstant.INVALID_REG_PRICE;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");

			/*
			 * try { dispstartdate = formatDate.parse(dispstartdatestr); } catch (Exception
			 * ex) { resMsg = MessageConstant.INVALID_DISP_DATE; setClientMessage(dto,
			 * resMsg, null); generateResponse(request, response, dto); return; } try {
			 * pexpdate = formatDate.parse(pexpdatestr); } catch (Exception ex) { resMsg =
			 * MessageConstant.INVALID_EXP_DATE; setClientMessage(dto, resMsg, null);
			 * generateResponse(request, response, dto); return; }
			 * 
			 * try { isVariant = Boolean.valueOf(isvariantstr); } catch (Exception ex) {
			 * resMsg = MessageConstant.INVALID_VARIANT_FLAG; setClientMessage(dto, resMsg,
			 * null); generateResponse(request, response, dto); return; }
			 */
			
			Boolean itemExists = ProductItemMasterService.checkIfItemIdExistsForOtherClientSellerProdId(clientIdStr,sellerprodid,sellerId,itmsId);
			if(itemExists) {			
				setClientMessage(dto, MessageConstant.PRODUCT_EXIST + "[ " + sellerprodid +"]" , null);
				generateResponse(request, response, dto);
				return;			
			}
			
			FileUploadGenericDTO upldto = new FileUploadGenericDTO();
			if (Constant.TRUE.equals(isimgupd)) {
				String s3bucket = SaaSAdminAppCache.s3staticbucket;
				String s3moduleFolder = SaaSAdminAppCache.s3inventoryoductsfolder;
				upldto = ASWS3ImageUploaderUtil.uploadFileToS3(clientIdStr, request, upldto, s3bucket, s3moduleFolder);
				if (upldto.getSts() != 1) {
					resMsg = "Error Uploading Image  :" + upldto.getMsg();
					setClientMessage(dto, "Error Uploading Image  :" + upldto.getMsg(), null);
					generateResponse(request, response, dto);
					return;
				}
				if (GenUtil.isNullOrEmpty(upldto.getAwsfinalUrl())) {
					resMsg = "Error Uploading Image  :" + upldto.getMsg();
					setClientMessage(dto, "Error Uploading Image  :" + upldto.getMsg(), null);
					generateResponse(request, response, dto);
					return;
				}
			}
			ProductItemMasterDVO dvo = new ProductItemMasterDVO();
			dvo.setItemsId(itmsId);			
			dvo.setClintId(clientIdStr);
			dvo.setSlrpitmsId(sellerprodid);
			dvo.setSlerId(sellerId);
			dvo.setPbrand(pbrand);
			dvo.setPname(pname);
			dvo.setPdesc(pdesc);
			dvo.setPlongDesc(plongdesc);
			dvo.setPimg(upldto.getAwsfinalUrl());
			//dvo.setPsku(psku);
			dvo.setPstatus("ACTIVE");
			dvo.setLupdBy(cau);
			//dvo.setIsVariant(isVariant);
			dvo.setPriceType(priceType);
			dvo.setPselMinPrc(pselMinPrc);
			dvo.setPregMinPrc(pregMinPrc);
			dvo.setProdPriceDtl(prodPriceDtl);
			dvo.setMaxCustQty(maxcustqty);
			//dvo.setDispStartdate(dispstartdate.getTime());
			//dvo.setPexpDate(pexpdate.getTime());
			dto.setDvo(dvo);
			dto.setSts(0);
			dto = ProductItemMasterService.updateProductItemMasterForPK(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, MessageConstant.PRODUCT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, MessageConstant.PRODUCT_ITEM_UPDATED);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = MessageConstant.PRODUCT_ERROR + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} 

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
