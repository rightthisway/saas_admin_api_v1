/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.dao;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ProductItemMasterDAO.
 */
public class ProductItemMasterCassDAO {

	/**
	 * Creates the new product item.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductItem(ProductItemMasterDVO dvo) throws Exception {
		   

			Integer status = 0;
			StringBuffer sql = new StringBuffer();
			sql.append(
				"INSERT INTO rtf_seller_product_items_mstr (clintid ,itmsid,slrpitms_id,sler_id,");			
			sql.append("pbrand ,pname ,pdesc , plong_desc, pimg,");
			sql.append(" pprc_dtl, psel_min_prc ,price_type, preg_min_prc,");
			sql.append( "pstatus,lupd_by,lupd_date) ");
			
			sql.append("values (?,?,?,?,") ;  
			sql.append( " ?,?,?,?,?," ) ;
			sql.append(" ?,?,?,?," ) ;
			sql.append("?, ? , toTimestamp(now()))");
			try {
				CassandraConnector.getSession().execute(sql.toString(),
						new Object[] { dvo.getClintId(), dvo.getItemsId(),dvo.getSlrpitmsId(),dvo.getSlerId(),
								dvo.getPbrand(),dvo.getPname(),dvo.getPdesc(),dvo.getPlongDesc(),dvo.getPimg(),
								dvo.getProdPriceDtl(),dvo.getPselMinPrc(),dvo.getPriceType(),dvo.getPregMinPrc(),dvo.getPstatus(),
								dvo.getLupdBy() });
				status = 1;
			} catch (final DriverException de) {
				de.printStackTrace();
				throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RTFDataAccessException(ex);
			}
			return status;
		}
	
	
	public static Integer updatePrductItem(ProductItemMasterDVO dvo) throws Exception {
		   

		Integer status = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(
			"INSERT INTO rtf_seller_product_items_mstr (clintid ,itmsid,slrpitms_id,sler_id,");			
		sql.append("pbrand ,pname ,pdesc , plong_desc, ");
		sql.append(" pprc_dtl, psel_min_prc ,price_type, preg_min_prc,");
		if (!GenUtil.isEmptyString(dvo.getPimg())) {
			sql.append( "pstatus,lupd_by,lupd_date , pimg) ");
		}else {
			sql.append( "pstatus,lupd_by,lupd_date ) ");
		}
		
		sql.append("values (?,?,?,?,") ;  
		sql.append( " ?,?,?,?," ) ;
		sql.append(" ?,?,?,?," ) ;
		if (!GenUtil.isEmptyString(dvo.getPimg())) {
			sql.append("?, ? , toTimestamp(now()) , ? )");
		}else {
			sql.append("?, ? , toTimestamp(now())  )");
		}
		
		System.out.println(" Update ITEM MASTER  CASS SQL " + sql);
		try {
			
			if (!GenUtil.isEmptyString(dvo.getPimg())) {
				CassandraConnector.getSession().execute(sql.toString(),
						new Object[] { dvo.getClintId(), dvo.getItemsId(),dvo.getSlrpitmsId(),dvo.getSlerId(),
								dvo.getPbrand(),dvo.getPname(),dvo.getPdesc(),dvo.getPlongDesc(),
								dvo.getProdPriceDtl(),dvo.getPselMinPrc(),dvo.getPriceType(),dvo.getPregMinPrc(),dvo.getPstatus(),
								dvo.getLupdBy(),dvo.getPimg() });
			}else {
				CassandraConnector.getSession().execute(sql.toString(),
						new Object[] { dvo.getClintId(), dvo.getItemsId(),dvo.getSlrpitmsId(),dvo.getSlerId(),
								dvo.getPbrand(),dvo.getPname(),dvo.getPdesc(),dvo.getPlongDesc(),
								dvo.getProdPriceDtl(),dvo.getPselMinPrc(),dvo.getPriceType(),dvo.getPregMinPrc(),dvo.getPstatus(),
								dvo.getLupdBy() });
			}	
			status = 1;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return status;
	}

}
