/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.dto;

import java.util.List;

import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ProductItemMasterDTO.
 */
public class ProductItemMasterDTO extends RtfSaasBaseDTO {

	/** The ProductItemMasterDVO. */
	private ProductItemMasterDVO dvo;

	/** The lst. */
	private List<ProductItemMasterDVO> lst;

	/**
	 * Gets the ProductItemMasterDVO.
	 *
	 * @return the dvo
	 */
	public ProductItemMasterDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the ProductItemMasterDVO.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ProductItemMasterDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the ProductItemMasterDVO list.
	 *
	 * @return the lst
	 */
	public List<ProductItemMasterDVO> getLst() {
		return lst;
	}

	/**
	 * Sets the ProductItemMasterDVO list.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<ProductItemMasterDVO> lst) {
		this.lst = lst;
	}

}
