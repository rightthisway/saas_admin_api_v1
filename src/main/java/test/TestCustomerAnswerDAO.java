package test;




import com.rtf.ott.cass.dao.CustomerAnswerDAO;
import com.rtf.ott.cass.dvo.CustomerAnswerDVO;

public class TestCustomerAnswerDAO {

	public static void main(String[] args) throws Exception {
		CustomerAnswerDVO obj = new CustomerAnswerDVO();
		obj = getAnswerObjForInsert(obj);
		//testAnswerInsert(obj);
		
		System.out.println("Update Cust Answer - 0 ");
		obj = getAnswerObjForUpdate(obj);
		testAnswerUpdate(obj);
		

	}


	public static CustomerAnswerDVO testAnswerInsert(CustomerAnswerDVO obj) throws Exception {

		/**
		 * 
		 * conid text, custid text, qsnbnkid int, conname text, consrtdate timestamp,
		 * credate timestamp, custans text, custplayid text, iscrtans boolean, qsnbnkans
		 * text, qsnseqno int, rwdtype text, rwdvalue decimal, upddate timestamp,
		 * PRIMARY KEY (conid, custid, custplayid, qsnbnkid, qsnseqno)
		 * 
		 */
		try {		
			CustomerAnswerDAO.insertCustomerQuenAnswerDetails(obj);
			System.out.println("Inserted cust answer");
		} catch (Exception ex) {

			ex.printStackTrace();
		}

		return null;
	}
	
	public static CustomerAnswerDVO testAnswerUpdate(CustomerAnswerDVO obj) throws Exception {
		System.out.println("Update  cust answer - 1");
		/**
		 * 
		 * conid text, custid text, qsnbnkid int, conname text, consrtdate timestamp,
		 * credate timestamp, custans text, custplayid text, iscrtans boolean, qsnbnkans
		 * text, qsnseqno int, rwdtype text, rwdvalue decimal, upddate timestamp,
		 * PRIMARY KEY (conid, custid, custplayid, qsnbnkid, qsnseqno)
		 * 
		 */
		try {		
			CustomerAnswerDAO.updateCustomerQuenAnswerDetails(obj);
			System.out.println("Updated  cust answer - 2");
		} catch (Exception ex) {

			ex.printStackTrace();
		}

		return null;
	}

	private static CustomerAnswerDVO getAnswerObjForUpdate(CustomerAnswerDVO obj) {
		obj.setIscrtans(true);
		obj.setCustans("D");
		obj.setRwdvalue(20.0);
		return obj;
	}

	private static CustomerAnswerDVO getAnswerObjForInsert(CustomerAnswerDVO obj) {
		obj.setConid("conid-1");
		obj.setCustid("abc-123-pers-id-1234");
		obj.setCustplayid("custplayid-123-ert-play123");		
		obj.setConname("conname1-football-trivia");
		//obj.setCustans("C");
		obj.setQsnbnkans("B");
		obj.setRwdtype("POINTS");
		obj.setQsnbnkid(123456);
		obj.setQsnseqno(1);
		// obj.setConsrtdate();
		// obj.setCredate();
		// obj.setUpddate();
		//obj.setIscrtans(true);
		obj.setRwdvalue(10.0);
		return obj;
	}

}
